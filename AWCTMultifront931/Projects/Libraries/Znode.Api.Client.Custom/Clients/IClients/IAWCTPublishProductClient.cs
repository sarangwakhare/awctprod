﻿
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomProductModel;


namespace Znode.Api.Client.Custom.Clients.IClients
{
    public interface IAWCTPublishProductClient : IPublishProductClient
    {
        ConfigurableProductViewModel GetConfigurableProductViewModel(int publishProductId, FilterCollection filters, ExpandCollection expands);
        ConfigurableProductViewModel GetPriceSizeList(int configurableProductId, FilterCollection filters, ExpandCollection expands);

        new AWCTPublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, ExpandCollection expands);

        AWCTTruColorModel GetGlobalAttributeData(string globalAttributeCodes, FilterCollection filters, ExpandCollection expands);

        new AWCTPublishProductModel GetExtendedPublishProductDetails(int publishProductId, FilterCollection filters, ExpandCollection expands);
    }
}
