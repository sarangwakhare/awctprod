﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.Sample.Api.Model.Responses;


namespace Znode.Api.Client.Custom.Clients.Clients
{
   public class AWCTPublishProductClient : PublishProductClient,IAWCTPublishProductClient
    {
        public  ConfigurableProductViewModel GetConfigurableProductViewModel(int publishProductId, FilterCollection filters, ExpandCollection expands)
        {
            string endpoint = AWCTPublishProductEndpoint.GetConfigurableProductViewModel(publishProductId);
            endpoint += BuildEndpointQueryString(expands, filters,null,null, null);
            ApiStatus status = new ApiStatus();

            AWCTPublishProductResponse response = GetResourceFromEndpoint<AWCTPublishProductResponse>(endpoint,status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (ConfigurableProductViewModel)response?.ConfigurableProductViewModel;
        }

        public ConfigurableProductViewModel GetPriceSizeList(int configurableProductId, FilterCollection filters, ExpandCollection expands)
        {
            string endpoint = AWCTPublishProductEndpoint.GetPriceSizeList(configurableProductId);
            endpoint += BuildEndpointQueryString(expands, filters, null, null, null);
            ApiStatus status = new ApiStatus();

            AWCTPublishProductResponse response = GetResourceFromEndpoint<AWCTPublishProductResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (ConfigurableProductViewModel)response?.ConfigurableProductViewModel;
        }
        public AWCTTruColorModel GetGlobalAttributeData(string globalAttributeCodes, FilterCollection filters, ExpandCollection expands)
        {
            string endpoint = AWCTPublishProductEndpoint.GetGlobalAttributeData(globalAttributeCodes);
            endpoint += BuildEndpointQueryString(expands, filters, null, null, null);
            ApiStatus status = new ApiStatus();

            AWCTPublishProductResponse response = GetResourceFromEndpoint<AWCTPublishProductResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (AWCTTruColorModel)response?.AWCTTruColorModel;
        }
     

        public new AWCTPublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, ExpandCollection expands)
        {
            string endpoint = AWCTPublishProductEndpoint.GetAWCTPublishProduct(publishProductId);
            endpoint += BuildEndpointQueryString(expands, filters, null, null, null);

            ApiStatus status = new ApiStatus();
            AWCTPublishProductResponse response = GetResourceFromEndpoint<AWCTPublishProductResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.AWCTPublishProduct;
        }
        public new AWCTPublishProductModel GetExtendedPublishProductDetails(int publishProductId, FilterCollection filters, ExpandCollection expands)
        {
            string endpoint = AWCTPublishProductEndpoint.GetExtendedProductDetails(publishProductId);
            endpoint += BuildEndpointQueryString(expands, filters, null, null, null);

            ApiStatus status = new ApiStatus();
            AWCTPublishProductResponse response = GetResourceFromEndpoint<AWCTPublishProductResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.AWCTPublishProduct;
        }
    }
}
