﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.ABSConnector;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.ERPConnector
{
    public class ZnodeABSConnector : BaseERP
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeImportTemplateMapping> _importTemplateMappingRepository;
        private readonly IZnodeRepository<ZnodeImportHead> _importHeadRepository;
        private readonly IZnodeRepository<ZnodeImportTemplate> _importTemplateRepository;
        private readonly IZnodeRepository<ZnodeOmsOrder> _orderRepository;
        private ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();
        private ImportHelper importHelper;
        private IZnodeRepository<ZnodeImportProcessLog> _importProcessLogs;
        private IZnodeRepository<ZnodeImportLog> _importLogs;
        private IZnodeRepository<ZnodeERPTaskScheduler> _erpTaskScheduler;
        private IOrderClient _orderClient;
        private readonly IZnodeRepository<ZnodeOmsOrderState> _orderStateRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailRepository;
        private readonly IZnodeRepository<ZnodeERPConfigurator> _eRPConfiguratorRepository;
        #endregion

        #region Constructor
        public ZnodeABSConnector()
        {
            _importTemplateMappingRepository = new ZnodeRepository<ZnodeImportTemplateMapping>();
            _importHeadRepository = new ZnodeRepository<ZnodeImportHead>();
            importHelper = new ImportHelper();
            _importTemplateRepository = new ZnodeRepository<ZnodeImportTemplate>();
            _importProcessLogs = new ZnodeRepository<ZnodeImportProcessLog>();
            _erpTaskScheduler = new ZnodeRepository<ZnodeERPTaskScheduler>();
            _importLogs = new ZnodeRepository<ZnodeImportLog>();
            _orderRepository = new ZnodeRepository<ZnodeOmsOrder>();
            _orderClient = GetClient<OrderClient>();
            _orderStateRepository = new ZnodeRepository<ZnodeOmsOrderState>();
            _orderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _eRPConfiguratorRepository = new ZnodeRepository<ZnodeERPConfigurator>();
        }
        #endregion

        #region Public Methods

        #region Batch Process
        //Product Refresh - Batch Process.
        public override bool ProductRefresh()
        {
            //int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "Product")?.FirstOrDefault().ImportHeadId;
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "AWCTPRD")?.FirstOrDefault().ImportHeadId;

            bool status = false;
            Guid tableGuid = Guid.NewGuid();
            //Import PRDH.
            //ImportABSData(ABSCSVNames.ProductParentLevelDetail, tableGuid, importHeadId);
            ////Import PRDHA.
            //ImportABSData(ABSCSVNames.ProductParenLevelAttributes, tableGuid, importHeadId);
            ////Import PRDD.
            //ImportABSData(ABSCSVNames.ProductSKULevelDetail, tableGuid, importHeadId);
            ////Import PRDDA.
            //ImportABSData(ABSCSVNames.ProductSKULevelAttributes, tableGuid, importHeadId);

            //Import AWCTPRD.
            /*Original Code*/
            //ImportABSData(ABSCSVNames.ProductAwct, tableGuid, importHeadId);
            //MoveFileToArchiveOnSuccess(ABSCSVNames.ProductAwct, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));

            /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
            string Filepath = GetProcessingFileName(ABSCSVNames.ProductAwct);
            ImportABSData(ABSCSVNames.ProductAwct, tableGuid, importHeadId, Filepath);
            MoveFileToArchiveOnSuccess(Filepath, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));
            /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/

            ZnodeLogging.LogMessage("Process for product refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandTimeout = 30000;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();
                status = true;
                ZnodeLogging.LogMessage("Process for product refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                GC.Collect();
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Product Attribute Refresh - Batch Process.
        public bool AttributeRefresh()
        {
            bool status = false;
            Guid tableGuid = Guid.NewGuid();
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "ProductAttribute")?.FirstOrDefault().ImportHeadId;

            //Import MAGATTR attributes.
            /*Original Code*/
            //ImportABSData(ABSCSVNames.Attributes, tableGuid, importHeadId);

            /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
            ImportABSData(ABSCSVNames.Attributes, tableGuid, importHeadId, "");
            /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/

            ZnodeLogging.LogMessage("Process for attribute refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

                status = true;
                ZnodeLogging.LogMessage("Process for attribute refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Inventory Refresh - Batch Process.
        public override bool InventoryRefresh()
        {
            bool status = false;
            Guid tableGuid = Guid.NewGuid();
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "Inventory")?.FirstOrDefault().ImportHeadId;

            //Import MAGINV.
            /*Original Code*/
            //ImportABSData(ABSCSVNames.InventoryBySKU, tableGuid, importHeadId);
            //MoveFileToArchiveOnSuccess(ABSCSVNames.InventoryBySKU, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));

            /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
            string Filepath = GetProcessingFileName(ABSCSVNames.InventoryBySKU);
            ImportABSData(ABSCSVNames.InventoryBySKU, tableGuid, importHeadId, Filepath);
            MoveFileToArchiveOnSuccess(Filepath, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));
            /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/


            ZnodeLogging.LogMessage("Process for inventory refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

                status = true;

                ZnodeLogging.LogMessage("Process for inventory refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Inventory Refresh - Batch Process.
        public override bool PricingStandardPriceListRefresh()
        {
            bool status = false;
            Guid tableGuid = Guid.NewGuid();
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "Pricing")?.FirstOrDefault().ImportHeadId;

            //Import TPRICE.
            /*Original Code*/
            //ImportABSData(ABSCSVNames.Pricing, tableGuid, importHeadId);

            /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
            ImportABSData(ABSCSVNames.Pricing, tableGuid, importHeadId, "");
            /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/

            ZnodeLogging.LogMessage("Process for price refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

                status = true;
                ZnodeLogging.LogMessage("Process for price refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Category Refresh - Batch Process.
        public override bool CategoryRefresh()
        {
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "Category")?.FirstOrDefault().ImportHeadId;

            bool status = false;
            Guid tableGuid = Guid.NewGuid();

            //Import PRDHA/Category.
            /*Original Code*/
            //ImportABSData(ABSCSVNames.ProductParenLevelAttributes, tableGuid, importHeadId);
            //ImportABSData(ABSCSVNames.ProductSKULevelDetail, tableGuid, importHeadId);

            /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
            //Import PRDHA/Category.
            ImportABSData(ABSCSVNames.ProductParenLevelAttributes, tableGuid, importHeadId, "");
            ImportABSData(ABSCSVNames.ProductSKULevelDetail, tableGuid, importHeadId, "");
            /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/

            ZnodeLogging.LogMessage("Process for category refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

                status = true;
                ZnodeLogging.LogMessage("Process for category refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Customer Refresh - Batch Process.
        public override bool CustomerDetailsRefresh()
        {
            bool status = false;
            Guid tableGuid = Guid.NewGuid();
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "CustomerAddress")?.FirstOrDefault().ImportHeadId;

            /*Original Code*/
            //Import MAGSOLD.
            //ImportABSData(ABSCSVNames.CustomerSoldTo, tableGuid, importHeadId);
            //MoveFileToArchiveOnSuccess(ABSCSVNames.CustomerSoldTo, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));

            //Import MAGSHIP.
            //ImportABSData(ABSCSVNames.CustomerShipTo, tableGuid, importHeadId);
            //MoveFileToArchiveOnSuccess(ABSCSVNames.CustomerShipTo, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));

            /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
            //Import MAGSOLD.
            string Filepath = GetProcessingFileName(ABSCSVNames.CustomerSoldTo);
            ImportABSData(ABSCSVNames.CustomerSoldTo, tableGuid, importHeadId, Filepath);
            MoveFileToArchiveOnSuccess(Filepath, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));

            //Import MAGSHIP.
            string ShipToFilepath = GetProcessingFileName(ABSCSVNames.CustomerShipTo);
            ImportABSData(ABSCSVNames.CustomerShipTo, tableGuid, importHeadId, ShipToFilepath);
            MoveFileToArchiveOnSuccess(ShipToFilepath, Convert.ToString(ConfigurationManager.AppSettings["ArchivePath"]));
            /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/

            ZnodeLogging.LogMessage("Process for customer refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

                status = true;
                ZnodeLogging.LogMessage("Process for customer refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Order status update.
        public bool OrderStatusUpdate()
        {
            bool status = false;
            ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "OrderStatusUpdate")?.FirstOrDefault()?.ERPTaskSchedulerId });
            try
            {
                //Read the csv file path.
                string csvPath = (GetCSVFilePath(ABSCSVNames.LineDetailStatusInformation));
                DataTable dataTable = new DataTable();
                string Fulltext;
                List<ZnodeOmsOrderDetail> models = new List<ZnodeOmsOrderDetail>();
                using (StreamReader sr = new StreamReader(csvPath))
                {
                    while (!sr.EndOfStream)
                    {
                        Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                        string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                        string[] lines = Fulltext.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string item in lines)
                        {
                            string[] values = item.Split(',');
                            string orderState = Enum.GetName(typeof(ABSOrderStateEnum), Convert.ToChar(values[0].Replace("\"", "")));
                            ZnodeOmsOrderDetail model = new ZnodeOmsOrderDetail
                            {
                                OmsOrderId = Convert.ToInt32(values[1]),
                                OmsOrderStateId = _orderStateRepository.Table.Where(x => x.OrderStateName == orderState).Select(y => y.OmsOrderStateId).FirstOrDefault(),
                                TrackingNumber = Convert.ToString(values[15]),
                            };

                            models.Add(model);
                        }
                    }
                }

                foreach (ZnodeOmsOrderDetail item in models)
                {
                    string orderId = Convert.ToString(item.OmsOrderId);
                    ZnodeOmsOrderDetail details = (from order in _orderRepository.Table
                                                   join orderDetails in _orderDetailRepository.Table on order.OmsOrderId equals orderDetails.OmsOrderId
                                                   where order.OmsOrderId == item.OmsOrderId
                                                   select new
                                                   {
                                                       OmsOrderId = order.OmsOrderId,
                                                       OmsOrderStateId = orderDetails.OmsOrderStateId,
                                                       OmsOrderDetailsId = orderDetails.OmsOrderDetailsId,
                                                   }).ToList().Select(x => new ZnodeOmsOrderDetail()
                                                   {
                                                       OmsOrderId = x.OmsOrderId,
                                                       OmsOrderStateId = x.OmsOrderStateId,
                                                       OmsOrderDetailsId = x.OmsOrderDetailsId,
                                                   }).FirstOrDefault();

                    _orderClient.UpdateOrderStatus(new OrderStateParameterModel { OmsOrderId = details.OmsOrderId, OmsOrderDetailsId = details.OmsOrderDetailsId, TrackingNumber = item.TrackingNumber, OmsOrderStateId = item.OmsOrderStateId });
                }

                status = true;
                _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
                ZnodeLogging.LogMessage("Process for updating the order status completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return status;
            }
            catch (Exception ex)
            {
                bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
                if (importProcessLogUpdated)
                {
                    _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "53", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });
                }

                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Create order files.
        public override bool OrderCreate()
        {
            bool status = false;
            ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "OrderCreate")?.FirstOrDefault()?.ERPTaskSchedulerId });
            try
            {
                string lastOrderFilePath = HttpContext.Current.Server.MapPath($"{ConfigurationManager.AppSettings["LastOrderFilePath"]}/Temp.csv");
                string lastOrderId = string.Empty;
                using (StreamReader sr = new StreamReader(lastOrderFilePath))
                {
                    while (!sr.EndOfStream)
                    {
                        lastOrderId = sr.ReadToEnd().ToString(); //read full file text  
                    }
                }
                if (!string.IsNullOrEmpty(lastOrderId))
                {
                    // orderDetails = orderDetails.Where(x => x.OmsOrderId > Convert.ToInt32(lastOrderId)).ToList();
                }
                ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
                objStoredProc.GetParameter("@OrderId", lastOrderId.Trim(), ParameterDirection.Input, SqlDbType.VarChar);
                DataSet dataSet = objStoredProc.GetSPResultInDataSet("Znode_GetOrderDetails");

                List<OrderModel> orderDetails = dataSet.Tables[0].AsEnumerable().Select(m => new OrderModel()
                {
                    OmsOrderId = m.Field<int>("OmsOrderID"),
                    OrderNumber = m.Field<string>("OrderNumber"),
                    PortalId = m.Field<int>("PortalId"),
                    UserId = m.Field<int>("UserId"),
                    OmsOrderStateId = m.Field<int>("OmsOrderStateId"),
                    ShippingId = m.Field<int>("ShippingID"),
                    PaymentTypeId = m.Field<int>("PaymentTypeId"),
                    TaxCost = Math.Round(m.Field<decimal>("TaxCost"), 2),
                    ShippingCost = Math.Round(m.Field<decimal>("ShippingCost"), 2),
                    SubTotal = Math.Round(m.Field<decimal>("SubTotal"), 2),
                    DiscountAmount = Math.Round(m.Field<decimal>("DiscountAmount"), 2),
                    Total = Math.Round(m.Field<decimal>("Total"), 2),
                    OrderDate = m.Field<DateTime>("OrderDate"),
                    ShippingNumber = m.Field<string>("ShippingNumber"),
                    TrackingNumber = m.Field<string>("TrackingNumber"),
                    CouponCode = m.Field<string>("CouponCode"),
                    PurchaseOrderNumber = m.Field<string>("PurchaseOrderNumber"),
                    ShipDate = m.Field<DateTime>("ShipDate"),
                    ReturnDate = m.Field<DateTime>("ReturnDate"),
                    OmsOrderDetailsId = m.Field<int>("OmsOrderDetailsId"),
                    PaymentDisplayName = m.Field<string>("PaymentName"),
                    ShippingTypeName = m.Field<string>("ShippingCode"),
                    BillingAddressHtml = m.Field<string>("Notes"),
                    CreditCardNumber = m.Field<string>("CreditCardNumber"),
                    CardType = m.Field<string>("CardType"),//.Field<string>("CardType").Length==1? "000"+ m.Field<string>("CardType"): 
                    PaymentTransactionToken = m.Field<string>("PaymentTransactionToken"),
                    Custom1 = m.Field<string>("CCExpiration"),
                    Custom2 = m.Field<string>("CustomerProfileId"),
                    Custom3 = m.Field<string>("CustomerPaymentId"),
                    Custom4 = m.Field<string>("NBLCode"),
                    WebServiceDownloadDate = m.Field<DateTime?>("WebServiceDownloadDate"),
                    CSRDiscountAmount = Math.Round(Convert.ToDecimal(m.Field<string>("TotalAdditionalCost")), 2),
                    KeyReceiptHtml = m.Field<string>("Custom2"),
                    Custom5 = m.Field<string>("Custom3"),
                    CreatedByName = m.Field<string>("ResponseCode"),
                    BillingAddress = new AddressModel()
                    {
                        FirstName = m.Field<string>("BillingFirstName") + " " + m.Field<string>("BillingLastName"),
                        Address1 = m.Field<string>("BillingStreet1"),
                        Address2 = m.Field<string>("BillingStreet2"),
                        CityName = m.Field<string>("BillingCity"),
                        StateCode = m.Field<string>("BillingStateCode"),
                        PostalCode = m.Field<string>("BillingPostalCode"),
                        EmailAddress = m.Field<string>("BillingEmailId"),
                        PhoneNumber = m.Field<string>("BillingPhoneNumber"),
                        ExternalId = m.Field<string>("UserExternalId"),
                        CompanyName = m.Field<string>("CompanyName"),

                    },
                    ShippingAddress = new AddressModel()
                    {
                        Address1 = m.Field<string>("ShipToStreet1"),
                        Address2 = m.Field<string>("ShipToStreet2"),
                        FirstName = m.Field<string>("ShipToFirstName"),
                        LastName = m.Field<string>("ShipToLastName"),
                        CityName = m.Field<string>("ShipToCity"),
                        StateCode = m.Field<string>("ShipToStateCode"),
                        PostalCode = m.Field<string>("ShipToPostalCode"),
                        PhoneNumber = m.Field<string>("ShipToPhoneNumber"),
                        EmailAddress = m.Field<string>("ShipToEmailId"),
                        ExternalId = m.Field<string>("AccountExternalId"),
                        CompanyName = m.Field<string>("CompanyName"),
                    }
                }).ToList();


                List<OrderLineItemModel> orderLines = dataSet.Tables[1].AsEnumerable().Select(m => new OrderLineItemModel()
                {
                    OmsOrderDetailsId = m.Field<int>("OmsOrderDetailsId"),
                    Quantity = Math.Round(m.Field<decimal>("Quantity"), 2),
                    Price = Math.Round(m.Field<decimal>("Price"), 2),
                    TrackingNumber = m.Field<string>("TrackingNumber"),
                    Sku = m.Field<string>("Sku"),
                    ProductName = m.Field<string>("ProductName"),
                    ShipDate = m.Field<DateTime>("ShipDate"),
                    ShippingCost = Math.Round(m.Field<decimal>("ShippingCost"), 2),
                    Custom3 = m.Field<string>("Custom3"),
                    Custom2 = m.Field<string>("PersonaliseValuesDetail")
                }).ToList();

                orderDetails.ForEach(d =>
                {
                    IEnumerable<OrderLineItemModel> si = orderLines
                                .Where(s => s.OmsOrderDetailsId == d.OmsOrderDetailsId);

                    d.OrderLineItems = si != null ? si.ToList() : null;
                });

                if (orderDetails.Count > 0)
                {
                    int lastOmsOrderId = orderDetails.Last().OmsOrderId;
                    WriteTextStorage(lastOmsOrderId.ToString(), HttpContext.Current.Server.MapPath($"{ConfigurationManager.AppSettings["LastOrderFilePath"]}/Temp.csv"), Mode.Write);

                    CreateMAGOD(orderDetails);
                    CreateMAGOH(orderDetails);
                    CreateMAGOS(orderDetails);
                    UpdateOrderDetails(orderDetails);
                }
                status = true;
                _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
                ZnodeLogging.LogMessage("Process for creating order files completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return status;
            }
            catch (Exception ex)
            {
                bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
                if (importProcessLogUpdated)
                {
                    _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "52", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });
                }

                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }
        #endregion

        #region Real Time Process
        //AR List - Real time process.
        public bool ARList()
        {
            //ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();

            //ABSARListRequestModel param = new ABSARListRequestModel();
            //bool status = false;
            //ABSMapper.MapABSARListRequestModel(param);

            ////Log the started message.
            //ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "ARList")?.FirstOrDefault()?.ERPTaskSchedulerId });
            //ZnodeLogging.LogMessage("Real time process for AR List started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //try
            //{
            //    List<ABSARListResponseModel> _model = _abs.GetResponse<List<ABSARListResponseModel>, ABSARListRequestModel>(GetDestinationUrl("ARListPort"), param, ABSRequestTypes.ARList);

            //    //Log the success message on completion of real time process.
            //    if (_model.Count > 0)
            //    {
            //        _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
            //        ZnodeLogging.LogMessage("Real time process for AR List completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //    }
            //    status = true;
            //}
            //catch (Exception ex)
            //{
            //    //Log the failure message on failing the real time process.
            //    bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

            //    if (importProcessLogUpdated)
            //        _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });

            //    ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
            //    status = false;
            //}

            //return status;
            return false;
        }

        //AR Payment - Real time process.
        [RealTime("RealTime")]
        public bool ARPayment(OrderModel model)
        {
            //ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();

            //ABSARPaymentRequestModel param = new ABSARPaymentRequestModel();
            //bool status = false;

            ////Log the started message.
            //ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "ARPayment")?.FirstOrDefault()?.ERPTaskSchedulerId });

            //try
            //{
            //    List<ABSARPaymentResponseModel> _model = _abs.GetResponse<List<ABSARPaymentResponseModel>, ABSARPaymentRequestModel>(GetDestinationUrl("ARPaymentPort"), ABSMapper.MapABSARPaymentRequestModel(model), ABSRequestTypes.ARPayment);
            //    //Log the success message on completion of real time process.
            //    if (_model.Count > 0)
            //    {
            //        _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
            //        ZnodeLogging.LogMessage("Real time process for AR Payment completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //    }
            //    status = true;
            //}
            //catch (Exception ex)
            //{
            //    //Log the failure message on failing the real time process.
            //    bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

            //    if (importProcessLogUpdated)
            //        _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });

            //    ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
            //    status = false;
            //}

            //return status;
            return false;
        }

        //Inventory - Real time process.
        [RealTime("RealTime")]
        public List<InventorySKUModel> Inventory(InventorySKUModel model)
        {
            //ZnodeLogging.LogMessage("Real time process for inventory Check 1.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();

            ////Log the started message.
            //ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "Inventory")?.FirstOrDefault()?.ERPTaskSchedulerId });
            //try
            //{
            //    List<ABSInventoryResponseModel> _model = _abs.GetResponse<List<ABSInventoryResponseModel>, ABSInventoryRequestModel>(GetDestinationUrl("InventoryRequestPort"), ABSMapper.MapABSInventoryRequestModel(model), ABSRequestTypes.Inventory);

            //    List<InventorySKUModel> inventoryModel = new List<InventorySKUModel>();

            //    inventoryModel = (from item in _model
            //                      select new InventorySKUModel
            //                      {
            //                          Quantity = item.InventoryQty,
            //                      }).ToList();

            //    //Log the success message on completion of real time process.
            //    if (_model.Count > 0)
            //    {
            //        ZnodeLogging.LogMessage("Real time process for inventory Check 2.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //        _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
            //        ZnodeLogging.LogMessage("Real time process for inventory completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //    }
            //    return inventoryModel;
            //}
            //catch (Exception ex)
            //{

            //    //Log the failure message on failing the real time process.
            //    bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

            //    if (importProcessLogUpdated)
            //        _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });

            //    ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
            //}

            return null;
        }

        //PDF Print - Real time process.
        [RealTime("RealTime")]
        public bool PDFPrint()
        {
            //ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();

            //ABSPrintInfoRequestModel param = new ABSPrintInfoRequestModel();
            bool status = false;
            //ABSMapper.MapABSPrintInfoRequestModel(param);

            ////Log the started message.
            //ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "PDFPrint")?.FirstOrDefault()?.ERPTaskSchedulerId });

            //try
            //{
            //    List<ABSPrintInfoResponseModel> _model = _abs.GetResponse<List<ABSPrintInfoResponseModel>, ABSPrintInfoRequestModel>(GetDestinationUrl("PDFPrintPort"), param, ABSRequestTypes.PDFPrint);
            //    //Log the success message on completion of real time process.
            //    if (_model.Count > 0)
            //    {
            //        _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
            //        ZnodeLogging.LogMessage("Real time process for PDF print completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //    }
            //    status = true;
            //}
            //catch (Exception ex)
            //{
            //    //Log the failure message on failing the real time process.
            //    bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

            //    if (importProcessLogUpdated)
            //        _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });

            //    ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
            //    status = false;
            //}

            return status;
        }

        //Email Update - Real time process.
        [RealTime("RealTime")]
        public ABSEmptyResponseModel EmailUpdate(ZnodeUser model)
        {
            //ABSConnector.ABSConnector absConnector = new ABSConnector.ABSConnector();

            ////Log the started message.
            //ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "EmailUpdate")?.FirstOrDefault()?.ERPTaskSchedulerId });

            //try
            //{
            //    ABSEmptyResponseModel _model = absConnector.GetResponse<ABSEmptyResponseModel, ABSEmailRequestModel>(GetDestinationUrl("EmailAddressPort"), ABSMapper.MapABSEmailRequestModel(model), ABSRequestTypes.Email);
            //    //Log the success message on completion of real time process.
            //    if (_model.Success)
            //    {
            //        _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
            //        ZnodeLogging.LogMessage("Real time process for email update completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //        return _model;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //Log the failure message on failing the real time process.
            //    bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

            //    if (importProcessLogUpdated)
            //        _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });

            //    ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

            //}
            return null;
        }

        //Bill To Address Update - Real time process.
        [RealTime("RealTime")]
        public ABSEmptyResponseModel BillToAddressUpdate(ZnodeAddress model)
        {
            //ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();

            ////Log the started message.
            //ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "BillToAddressUpdate")?.FirstOrDefault()?.ERPTaskSchedulerId });

            //try
            //{
            //    ABSEmptyResponseModel _model = _abs.GetResponse<ABSEmptyResponseModel, ABSBillToChangeRequestModel>(GetDestinationUrl("BillToAddressPort"), ABSMapper.MapABSBillToChangeRequestModel(model), ABSRequestTypes.BillToAddress);
            //    //Log the success message on completion of real time process.
            //    if (_model.Success)
            //    {
            //        _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
            //        ZnodeLogging.LogMessage("Real time process for bill to change update completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //        return _model;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //Log the failure message on failing the real time process.
            //    bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

            //    if (importProcessLogUpdated)
            //        _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });

            //    ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

            //}
            return null;
        }

        //Get Order Number - Real time process.
        [RealTime("RealTime")]
        public string GetOrderNumber(SubmitOrderModel model)
        {
            ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();
            ABSOrderNumberRequestModel param = new ABSOrderNumberRequestModel();

            //Log the started message.
            ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "GetOrderNumber")?.FirstOrDefault()?.ERPTaskSchedulerId });
            try
            {

                List<ABSOrderNumberResponseModel> _model = _abs.GetResponse<List<ABSOrderNumberResponseModel>, ABSOrderNumberRequestModel>(GetDestinationUrl("OrderNumberPort"), ABSMapper.MapABSOrderNumberRequestModel(model), ABSRequestTypes.OrderNumber);
                //Log the success message on completion of real time process.
                if (_model.Count > 0)
                {
                    _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
                    ZnodeLogging.LogMessage("Real time process for get order number completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    return _model?.FirstOrDefault()?.NextOrderNumber;
                }
            }
            catch (Exception ex)
            {
                //Log the failure message on failing the real time process.
                bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

                if (importProcessLogUpdated)
                {
                    _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });
                }
                /*Nivi Code start to send the email to the admin and the customer service if the ABS service is down.*/
                string ABSmail = ConfigurationManager.AppSettings["ABSmail"].ToString();
                string Subject = ConfigurationManager.AppSettings["Subject"].ToString();
                string Body = ConfigurationManager.AppSettings["Body"].ToString();
                string Adminmail = ConfigurationManager.AppSettings["Adminmail"].ToString();
                ZnodeEmail.SendEmail(ABSmail, Adminmail, "", Subject, Body, true, "", ABSmail);
                /*Nivi Code end to send the email to the admin and the customer service if the ABS service is down.*/
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

            }
            return null;
        }

        //Tier Price change - Real time process.
        public override bool GetPricing()
        {
            //ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();

            //ABSTierPriceRequestModel param = new ABSTierPriceRequestModel();
            //bool status = false;
            //ABSMapper.MapABSPriceRequestModel(param);

            ////Log the started message.
            //ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "PricingRealTime")?.FirstOrDefault()?.ERPTaskSchedulerId });
            //try
            //{
            //    ABSEmptyResponseModel _model = _abs.GetResponse<ABSEmptyResponseModel, ABSTierPriceRequestModel>(GetDestinationUrl("PriceTierPort"), param, ABSRequestTypes.PriceTier);
            //    //Log the success message on completion of real time process.
            //    if (_model.Success)
            //    {
            //        _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
            //        ZnodeLogging.LogMessage("Real time process for pricing completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //    }
            //    status = true;
            //}
            //catch (Exception ex)
            //{
            //    //Log the failure message on failing the real time process.
            //    bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

            //    if (importProcessLogUpdated)
            //        _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });

            //    ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
            //    status = false;
            //}

            //return status;
            return false;
        }

        //Get Order Number - Real time process.
        [RealTime("RealTime")]
        public OrderModel OrderInformation(OrderModel orderModel)
        {

            ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();
            ABSOrderInfoRequestModel param = new ABSOrderInfoRequestModel();
            ABSMapper.MapABSOrderInfoRequestModel(param);
            param.OrderNumberRequested = orderModel.OmsOrderId.ToString();

            param.SummaryRequested = "O";
            OrderModel Model = null;
            //Log the started message.
            ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "OrderInformation")?.FirstOrDefault()?.ERPTaskSchedulerId });

            try
            {

                List<ABSOrderResponseDetailsModel> _model = _abs.GetResponse<List<ABSOrderResponseDetailsModel>, ABSOrderInfoRequestModel>(GetDestinationUrl("OrderHistoryPort"), param, ABSRequestTypes.OrderHistory);
                if (_model != null)
                {
                    ZnodeLogging.LogMessage("Model is not Null-order history", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    IEnumerable<OrderModel> Order = (from r in _model
                                                     select new OrderModel
                                                     {
                                                         ShippingAddress = new AddressModel
                                                         {
                                                             Address1 = r.ShipToInformation.ShipToAddress1,
                                                             Address2 = r.ShipToInformation.ShipToAddress2,
                                                             DisplayName = r.ShipToInformation.ShipToName,
                                                             CityName = r.ShipToInformation.ShipToCity,
                                                             StateName = r.ShipToInformation.ShipToState,
                                                             PostalCode = r.ShipToInformation.ShipToZip,
                                                         },

                                                         BillingAddress = new AddressModel
                                                         {
                                                             Address1 = r.BillToInformation.BillToAddress1,
                                                             Address2 = r.BillToInformation.BillToAddress2,
                                                             DisplayName = r.BillToInformation.BillToName,
                                                             CityName = r.BillToInformation.BillToCity,
                                                             StateName = r.BillToInformation.BillToState,
                                                             PostalCode = r.BillToInformation.BillToZip,
                                                         },
                                                         OrderLineItems = r.OrderDetailLine.Select(item => new OrderLineItemModel()
                                                         {

                                                             ProductName = item.LineStyleDescription,
                                                             Description = item.LineColorCode + "-" + item.LineColorDescription,
                                                             Quantity = Convert.ToDecimal(item.LineQuantity),
                                                             Price = Convert.ToDecimal(item.LinePrice),
                                                             Sku = item.LineUpcCode,
                                                             Custom1 = item.LineStyleNumber,
                                                             Custom2 = item.LineSize,
                                                             OrderLineItemState = SetLineItemStatus(item.LineItemStatus),
                                                             /*Nivi Code start for Line Ship Date*/
                                                             Custom3 = item.LineShipDate
                                                             /*Nivi Code end for Line Ship Date*/

                                                         }).ToList(),
                                                         OrderNumber = r.OrderHeader.OrderNumber,
                                                         OmsOrderId = Convert.ToInt32(r.OrderHeader.OrderNumber),
                                                         OrderDate = DateTime.ParseExact(r.OrderHeader.OrderDate, "yyyyMMdd", CultureInfo.InvariantCulture),
                                                         Custom1 = SetHeaderStatus(r.OrderHeader.OrderHeaderStatus),
                                                         Custom2 = r.SoldToInformation.SoldToName + "</br>" + r.SoldToInformation.SoldToAddress1 + "</br>" +
                                                                  r.SoldToInformation.SoldToAddress2 + "</br>" + r.SoldToInformation.SoldToCity + "</br>" + r.SoldToInformation.SoldToState + "</br>" + r.SoldToInformation.SoldToZip,

                                                         TrackingNumber = r.TrackingInformation.TrackingNumber1 + "," + r.TrackingInformation.TrackingNumber2
                                                                          + "," + r.TrackingInformation.TrackingNumber3 + "," + r.TrackingInformation.TrackingNumber4,
                                                         PaymentDisplayName = r.PaymentInformation.OpenTermsDescription,
                                                         //SubTotal = Convert.ToDecimal(r.OrderTotal.TotalOrderAmount),
                                                         Total = Convert.ToDecimal(r.OrderTotal.TotalOrderAmount),
                                                         //DiscountAmount = Convert.ToDecimal(r.OrderTotal.TotalPromoDiscount),
                                                         //CSRDiscountAmount = Convert.ToDecimal(r.OrderTotal.TotalNbiDiscount),
                                                         //TaxCost = Convert.ToDecimal(r.OrderTotal.TotalOrderFreight),
                                                     });
                    string tPath = "";
                    try
                    {
                        tPath = ConfigurationManager.AppSettings["UPTrackingReceiptUrl"].ToString() + "&tracknum=";
                    }
                    catch (Exception ex)
                    {
                        tPath = "";
                        ZnodeLogging.LogMessage("Rorder info Error tracking path." + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    }
                    try
                    {
                        Model = (OrderModel)Order.FirstOrDefault();
                        string Tracking = "";
                        if (Model.TrackingNumber != null)
                        {
                            foreach (string item in Model.TrackingNumber.Split(','))
                            {
                                if (item != string.Empty)
                                {
                                    Tracking += "<a href=" + tPath + item + " target='_blank'>" + item + "</a>,";
                                }
                            }

                            Tracking = Tracking.Substring(0, Tracking.Length - 1);
                        }
                        Model.TrackingNumber = Tracking;
                    }
                    catch (Exception ex)
                    {
                        Model.TrackingNumber = "";
                        ZnodeLogging.LogMessage("Rorder info Error Tracing." + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    }
                    // return (OrderModel)Order.FirstOrDefault();
                    return Model;

                }

                //Log the success message on completion of real time process.
                if (_model.Count > 0)
                {
                    _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
                    ZnodeLogging.LogMessage("Real time process for order info completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Log the failure message on failing the real time process.
                bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

                if (importProcessLogUpdated)
                {
                    _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });
                }

                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

            }
            return null;
        }
        //Get Order Number - Real time process.
        [RealTime("RealTime")]
        public OrdersListModel OrderList(OrdersListModel ordersListModel)
        {
            ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();
            ABSOrderInfoRequestModel param = new ABSOrderInfoRequestModel();
            ABSMapper.MapABSOrderInfoRequestModel(param);
            param.SoldTo = ordersListModel.Custom5.ToString();
            // param.CurrentRecord = Convert.ToString(ordersListModel.PageIndex);
            param.NumberOfRecords = Convert.ToInt32(ordersListModel.PageSize);
            //Log the started message.
            ZnodeImportProcessLog importProcessLogEntity = _importProcessLogs.Insert(new ZnodeImportProcessLog { Status = "Started", ImportTemplateId = null, ProcessStartedDate = DateTime.Now, ProcessCompletedDate = null, ERPTaskSchedulerId = _erpTaskScheduler.Table.Where(x => x.TouchPointName == "OrderInformation")?.FirstOrDefault()?.ERPTaskSchedulerId });

            try
            {
                List<ABSOrderResponseTop5Model> _model = _abs.GetResponse<List<ABSOrderResponseTop5Model>, ABSOrderInfoRequestModel>(GetDestinationUrl("OrderHistoryPort"), param, ABSRequestTypes.OrderHistoryList);

                List<OrderModel> order = (from r in _model
                                          select new OrderModel
                                          {
                                              OrderNumber = r.OrderNumber,
                                              OmsOrderId = Convert.ToInt32(r.OrderNumber),
                                              Total = Convert.ToDecimal(r.SummaryAmount),
                                              OrderDate = DateTime.ParseExact(r.OrderDate, "yyyyMMdd", CultureInfo.InvariantCulture),
                                              ItemCount = Convert.ToInt32(r.SummaryUnits),
                                              PurchaseOrderNumber = Convert.ToString(r.CustomerOrderNumber)
                                          }
               ).ToList();
                OrdersListModel ordersList = new OrdersListModel { Orders = order.ToList() };
                //Log the success message on completion of real time process.
                if (_model.Count > 0)
                {
                    _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Completed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });
                    ZnodeLogging.LogMessage("Real time process for order info completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    return ordersList;
                }
                return ordersList;
            }
            catch (Exception ex)
            {
                //Log the failure message on failing the real time process.
                bool importProcessLogUpdated = _importProcessLogs.Update(new ZnodeImportProcessLog { Status = "Failed", ProcessStartedDate = importProcessLogEntity.ProcessStartedDate, ImportTemplateId = importProcessLogEntity.ImportTemplateId, ProcessCompletedDate = DateTime.Now, ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ERPTaskSchedulerId = importProcessLogEntity.ERPTaskSchedulerId });

                if (importProcessLogUpdated)
                {
                    _importLogs.Insert(new ZnodeImportLog { ImportProcessLogId = importProcessLogEntity.ImportProcessLogId, ErrorDescription = "48", DefaultErrorValue = ex.Message, ColumnName = null, RowNumber = null, Data = null, Guid = null });
                }

                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

            }
            return null;
        }
        #endregion

        #endregion

        /// <summary>
        ///  Inventory Sync from ABS API
        /// </summary>
        /// <param name="inventoryReuqestSKUModel"></param>
        /// <returns></returns>
        [RealTime("RealTime")]
        public InventorySKUModel InventorySyncOnCheckout(InventorySKUModel inventoryReuqestSKUModel)
        {
            List<InventorySKUModel> inventoryModel = new List<InventorySKUModel>();
            ABSConnector.ABSConnector _abs = new ABSConnector.ABSConnector();
            try
            {
                List<ABSInventoryResponseModel> _model = _abs.GetResponse<List<ABSInventoryResponseModel>, ABSInventoryRequestModel>(GetDestinationUrl("InventoryRequestPort"), ABSMapper.MapABSInventoryRequestModel(inventoryReuqestSKUModel), ABSRequestTypes.Inventory);
                inventoryModel = (from item in _model
                                  select new InventorySKUModel
                                  {
                                      SKU = item.UpcNumber,
                                      Quantity = item.InventoryQty,
                                  }).ToList();

                if (_model.Count > 0)
                {
                    ZnodeLogging.LogMessage("Real time process for Inventory sync completed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    ZnodeLogging.LogMessage("inventoryModel quntity", Convert.ToString(inventoryModel?.FirstOrDefault()?.Quantity), TraceLevel.Error);
                    return inventoryModel?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }
            return inventoryModel.FirstOrDefault(); ;
        }


        #region Private Methods
        private void UpdateOrderDetails(List<OrderModel> orderDetails)
        {
            List<int> OmsOrderId = orderDetails?.Select(x => x.OmsOrderId)?.ToList();
            string OmsOrderIds = string.Join(",", OmsOrderId);
            try
            {
                SqlConnection conn = GetSqlConnection();
                SqlCommand cmd = new SqlCommand("AWCT_UpdateOrderDetails", conn);
                cmd.CommandTimeout = 30000;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OmsOrderIds", OmsOrderIds);
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                throw;
            }

        }
        //Get default family id.
        private int? GetDefaultFamilyId(bool isCategory)
        {
            ZnodeRepository<ZnodePimAttributeFamily> _family = new ZnodeRepository<ZnodePimAttributeFamily>();
            return _family.Table.Where(x => x.IsDefaultFamily == true && x.IsCategory == isCategory)?.Select(x => x.PimAttributeFamilyId)?.FirstOrDefault();
        }

        //Get default locale id.
        private int GetDefaultLocaleId()
        {
            ZnodeRepository<ZnodeLocale> _localeRepository = new ZnodeRepository<ZnodeLocale>();
            return _localeRepository.Table.Where(x => x.IsDefault == true).Select(x => x.LocaleId).FirstOrDefault();
        }


        private void ReadRowsWithoutHeader(DataTable dataTable, string[] rows, string headers, string fileLocation)
        {
            string[] headerArray = headers?.Split(',');

            for (int j = 0; j < headerArray?.Count(); j++)
            {
                dataTable.Columns.Add(headerArray[j]); //add headers  
            }

            for (int i = 0; i < rows.Count(); i++)
            {
                /* Commneted By Nilesh on 11/30/2018
                //string[] rowValues = (fileLocation == "TPRICE") || (fileLocation == "MAGINV") ? rows[i].Split(',') : (fileLocation == "MAGSOLD") || (fileLocation == "MAGSHIP") ? rows[i].Split('|') : rows[i].Split('~'); //split each row with comma or tilde to get individual values  
                //{
                //    DataRow dr = dataTable.NewRow();
                //    for (int k = 0; k < rowValues.Count(); k++)
                //        dr[k] = Convert.ToString(rowValues[k].Replace('"', ' ').Trim());

                //    dataTable.Rows.Add(dr); //add other rows  
                //}*/
                string[] rowValues = (fileLocation == "TPRICE") || (fileLocation == "MAGINV") ? rows[i].Split(',') : (fileLocation == "MAGSOLD") ||
                (fileLocation == "MAGSHIP") ? rows[i].Split('|') : rows[i].Split(','); //split each row with comma or tilde to get individual values  
                {
                    if (fileLocation == "MAGSHIP")
                    {
                        #region Customer
                        if (rowValues.Count() >= 15)
                        {
                            int rowsColomnsDifference = rowValues.Count() - headerArray.Count();
                            List<string> list = new List<string>(rowValues);
                            if (rowsColomnsDifference == 1)
                            {
                                rowValues[5] = rowValues[5] + rowValues[6];
                                list.Remove(rowValues[6]);
                            }
                            else if (rowsColomnsDifference == 2)
                            {
                                rowValues[5] = rowValues[5] + rowValues[6] + rowValues[7];
                                list.Remove(rowValues[6]);
                                list.Remove(rowValues[7]);
                            }
                            else if (rowsColomnsDifference == 3)
                            {
                                rowValues[5] = rowValues[5] + rowValues[6] + rowValues[7] + rowValues[8];
                                list.Remove(rowValues[6]);
                                list.Remove(rowValues[7]);
                                list.Remove(rowValues[8]);
                            }
                            else if (rowsColomnsDifference == 4)
                            {
                                rowValues[5] = rowValues[5] + rowValues[6] + rowValues[7] + rowValues[8] + rowValues[9];
                                list.Remove(rowValues[6]);
                                list.Remove(rowValues[7]);
                                list.Remove(rowValues[8]);
                                list.Remove(rowValues[9]);
                            }
                            rowValues = list.ToArray();


                        }
                        #endregion
                    }

                    #region Actual Code
                    try
                    {
                        DataRow dr = dataTable.NewRow();
                        for (int k = 0; k < rowValues.Count(); k++)
                        {
                            dr[k] = Convert.ToString(rowValues[k].Replace('"', ' ').Trim());
                        }

                        dataTable.Rows.Add(dr); //add other rows 
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage("i" + i.ToString(), ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    }
                    #endregion
                }
            }
        }
        private void ReadRowsProductRefresh(DataTable dataTable, string[] rows, string headers, string fileLocation)
        {
            try
            {
                string[] headerArray = headers?.Split(',');

                for (int j = 0; j < headerArray?.Count(); j++)
                {
                    dataTable.Columns.Add(headerArray[j]); //add headers  
                }

                for (int i = 0; i < rows.Count(); i++)
                {
                    string[] rowValues = rows[i].Split('|'); //split each row with comma or tilde to get individual values  
                    {
                        #region Actual Code
                        try
                        {
                            DataRow dr = dataTable.NewRow();
                            for (int k = 0; k < rowValues.Count(); k++)
                            {
                                //if (k == 12)
                                //{
                                //    dr[k] = Convert.ToString(rowValues[k].Replace("-", "").Replace('"', ' ').Trim());
                                //}
                                //else if(k==10)
                                //{
                                //    if (rowValues[k].Length == 1)
                                //        dr[k] = Convert.ToString(rowValues[k] + "00");
                                //}
                                //else
                                //{
                                dr[k] = Convert.ToString(rowValues[k].Replace('"', ' ').Trim());
                                //}
                            }

                            dataTable.Rows.Add(dr); //add other rows 
                        }
                        catch
                        {
                            ZnodeLogging.LogMessage("i" + i.ToString(), ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                            continue;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {

                ZnodeLogging.LogMessage("Product Refesh ReadRowsProductRefresh Data err--" + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

            }
        }
        //Get the port values.
        private string GetDestinationUrl(string portName)
        {
            string Json = _eRPConfiguratorRepository.Table.FirstOrDefault(x => x.IsActive == true)?.JsonSetting;
            NameValueCollection nameValueColl = new NameValueCollection();
            JObject obj = JObject.Parse(Json);
            string erpLinkUrlValue = string.Empty;
            foreach (JToken item in obj[ZnodeConstant.ERPConnectorControlList])
            {
                nameValueColl.Add(item[ZnodeConstant.Name].ToString(), item[ZnodeConstant.Value].ToString());
            }
            // nameValueColl.Add(item[ZnodeConstant.Name].ToString(), DecryptData(item[ZnodeConstant.Value].ToString()));
            if (portName.Equals("ABSFolderPath"))
            {
                return $"{ nameValueColl["ABSFolderPath"]}";
            }

            if (portName.Equals("FTPPath") || portName.Equals("FTPUsername") || portName.Equals("FTPPassword"))
            {
                return $"{ nameValueColl[portName]}";
            }

            return $"{nameValueColl["IPAddress"]}:{nameValueColl[portName]}";
        }

        private string GetCSVFilePath(string fileName)
        => $"{GetDestinationUrl("ABSFolderPath")}/{fileName}.csv";

        //Decrypt the data.
        private static string DecryptData(string data) => !string.IsNullOrEmpty(data) ? new ZnodeEncryption().DecryptData(data) : null;

        /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
        private string GetABSFolderPath(string ImportName)
        {
            string FolderName = "";
            switch (ImportName)
            {
                case ABSCSVNames.ProductAwct:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSProductPath"]);
                    break;
                case ABSCSVNames.InventoryBySKU:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSInventoryPath"]);
                    break;
                case ABSCSVNames.CustomerShipTo:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSCustomerPath"]);
                    break;
                case ABSCSVNames.CustomerSoldTo:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSCustomerPath"]);
                    break;
            }
            return $"{GetDestinationUrl("ABSFolderPath")}/{FolderName}/";
        }
        /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/

        /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/

        //Import ABS related files.
        private void ImportABSData(string fileLocation, Guid tableGuid, int? importHeadId, string fileName)
        {
            try
            {

                ZnodeLogging.LogMessage("Import Data--" + fileLocation, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                ZnodeLogging.LogMessage("Import Data--" + importHeadId, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/

                if (string.IsNullOrEmpty(fileName))
                {
                    //Read the csv file path.// (GetCSVFilePath(fileLocation));
                    string csvPath = (GetCSVFilePath(fileLocation));// @"E:\WorkSpace\AWCT changes\ABS_07232019\" + fileLocation + ".CSV";
                                                                    /*Nivi Code start for if file not exists do not process.*/
                    if (!File.Exists(csvPath))
                    {
                        return;
                    }
                    fileName = csvPath;
                }
                /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/

                if (!File.Exists(fileName))
                {
                    return;
                }
                /*Nivi Code end for if file not exists do not process.*/
                DataTable dataTable = new DataTable();
                string Fulltext;


                using (StreamReader sr = new StreamReader(fileName))
                {
                    IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();
                    //Get the headers of current csv.
                    IList<string> headers = objStoredProc.ExecuteStoredProcedureList($"Znode_ImportGetTableDetails {fileLocation},{importHeadId}");
                    while (!sr.EndOfStream)
                    {
                        Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                                                              //string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                        var lineSplitter = new LineSplitter();
                        string[] rows = lineSplitter.SplitYield(Fulltext)
                            .Select(stringLine => stringLine.Value).ToArray();

                        if (ABSCSVNames.ProductAwct == fileLocation)
                        {
                            ReadRowsProductRefresh(dataTable, rows, headers.FirstOrDefault(), fileLocation);
                        }
                        else
                        {
                            ReadRowsWithoutHeader(dataTable, rows, headers.FirstOrDefault(), fileLocation);
                        }
                    }

                    string uniqueIdentifier = string.Empty;
                    //Create hash table for the respective csv's.
                    ABSReadAndCreateTable(out uniqueIdentifier, headers.FirstOrDefault(), dataTable, tableGuid, fileLocation);

                }

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Import Data err--" + ex.Message + "  strackTrace:=" + ex.StackTrace, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
            }
        }
        /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/

        /*Original Code*/
        //Import ABS related files.
        //private void ImportABSData(string fileLocation, Guid tableGuid, int? importHeadId)
        //{
        //    try
        //    {

        //        ZnodeLogging.LogMessage("Import Data--" + fileLocation, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
        //        ZnodeLogging.LogMessage("Import Data--" + importHeadId, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

        //        //Read the csv file path.// (GetCSVFilePath(fileLocation));
        //        string csvPath = (GetCSVFilePath(fileLocation));// @"E:\WorkSpace\AWCT changes\ABS_07232019\" + fileLocation + ".CSV";
        //        /*Nivi Code start for if file not exists do not process.*/
        //        if (!File.Exists(csvPath))
        //        {
        //            return;
        //        }
        //        /*Nivi Code end for if file not exists do not process.*/
        //        DataTable dataTable = new DataTable();
        //        string Fulltext;


        //        using (StreamReader sr = new StreamReader(csvPath))
        //        {
        //            IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();
        //            //Get the headers of current csv.
        //            IList<string> headers = objStoredProc.ExecuteStoredProcedureList($"Znode_ImportGetTableDetails {fileLocation},{importHeadId}");
        //            while (!sr.EndOfStream)
        //            {
        //                Fulltext = sr.ReadToEnd().ToString(); //read full file text  
        //               //string[] rows = Fulltext.Split('\n'); //split full file text into rows  
        //                var lineSplitter = new LineSplitter();
        //                string[] rows = lineSplitter.SplitYield(Fulltext)
        //                    .Select(stringLine => stringLine.Value).ToArray();

        //                if (ABSCSVNames.ProductAwct == fileLocation)
        //                {
        //                    ReadRowsProductRefresh(dataTable, rows, headers.FirstOrDefault(), fileLocation);
        //                }
        //                else
        //                {
        //                    ReadRowsWithoutHeader(dataTable, rows, headers.FirstOrDefault(), fileLocation);
        //                }
        //            }

        //            string uniqueIdentifier = string.Empty;
        //            //Create hash table for the respective csv's.
        //            ABSReadAndCreateTable(out uniqueIdentifier, headers.FirstOrDefault(), dataTable, tableGuid, fileLocation);

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("Import Data err--" + ex.Message + "  strackTrace:=" + ex.StackTrace, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
        //    }
        //}

        //Create hash tables in database for ABS files import.
        private string ABSReadAndCreateTable(out string uniqueIdentifier, string headers, DataTable dt, Guid tableGuid, string csvName)
        {
            string tableName = string.Empty;
            try
            {
                uniqueIdentifier = string.Empty;
                string[] columnNames = headers?.Split(',')?.ToArray();
                //generate the doble hash temp table name
                if (columnNames?.Count() > 0)
                {
                    if (csvName == ABSCSVNames.CustomerSoldTo)
                    {
                        tableName = $"[dbo].[{csvName}_{tableGuid}]";
                    }
                    else if (csvName == ABSCSVNames.CustomerShipTo)
                    {
                        tableName = $"[dbo].[{csvName}_{tableGuid}]";
                    }
                    else if (csvName == ABSCSVNames.ProductAwct)
                    {
                        tableName = $"[dbo].[{csvName}_{tableGuid}]";
                    }
                    else
                    {
                        tableName = $"tempdb..[##{csvName}_{tableGuid}]";
                    }

                    //add the same guid in datatable
                    DataColumn guidCol = new DataColumn("guid", typeof(String));
                    guidCol.DefaultValue = tableGuid;
                    dt.Columns.Add(guidCol);

                    //create the doble hash temp table
                    MakeTable(tableName, GetTableColumnsFromFirstLine(columnNames));

                    //assign the guid to out parameter
                    uniqueIdentifier = Convert.ToString(tableGuid);
                }

                //If table created and it has headers then dump the CSV data in doble hash temp table
                if (!string.IsNullOrEmpty(tableName) && columnNames.Count() > 0)
                {
                    SaveDataInChunk(dt, tableName);
                }
                return tableName;
            }
            catch (Exception ex)
            {
                uniqueIdentifier = string.Empty;
                ZnodeLogging.LogMessage("Product Refesh ABSReadAndCreateTable Data err--" + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                return uniqueIdentifier;
            }

        }

        //This method will create ##temp table in SQL
        private void MakeTable(string tableName, string columnList)
        {
            SqlConnection conn = GetSqlConnection();
            SqlCommand cmd = new SqlCommand("CREATE TABLE " + tableName + "  " + columnList + " ", conn);

            if (conn.State.Equals(ConnectionState.Closed))
            {
                conn.Open();
            }

            cmd.ExecuteNonQuery();
        }

        //This method will create the table columns and append the datatype to the column headers
        private string GetTableColumnsFromFirstLine(string[] firstLine)
        {
            StringBuilder sbColumnList = new StringBuilder();
            sbColumnList.Append("(");
            sbColumnList.Append(string.Join(" nvarchar(max) , ", firstLine));
            sbColumnList.Append(" nvarchar(max), guid nvarchar(max) )");
            return sbColumnList.ToString();
        }

        //This method will divide the data in chunk.  The chunk size is 5000.
        private void SaveDataInChunk(DataTable dt, string tableName)
        {
            if (HelperUtility.IsNotNull(dt) && dt.Rows?.Count > 0)
            {
                int chunkSize = int.Parse(ConfigurationManager.AppSettings["ZnodeImportChunkLimit"].ToString());
                int startIndex = 0;
                int totalRows = dt.Rows.Count;
                int totalRowsCount = totalRows / chunkSize;

                if (totalRows % chunkSize > 0)
                {
                    totalRowsCount++;
                }

                for (int iCount = 0; iCount < totalRowsCount; iCount++)
                {
                    DataTable fileData = dt.Rows.Cast<DataRow>().Skip(startIndex).Take(chunkSize).CopyToDataTable();
                    startIndex = startIndex + chunkSize;
                    InsertData(tableName, fileData);
                }
            }
        }

        //This method will save the chunk data in ##temp table using Bulk upload
        private void InsertData(string tableName, DataTable fileData)
        {
            SqlConnection conn = GetSqlConnection();
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
            {
                bulkCopy.DestinationTableName = tableName;
                bulkCopy.BulkCopyTimeout = 20000;
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                bulkCopy.WriteToServer(fileData);
                conn.Close();
            }
        }

        //This method will provide the SQL connection
        private SqlConnection GetSqlConnection() => new SqlConnection(HelperMethods.ConnectionString);

        /// <summary>
        /// Writes text file to persistant storage.
        /// </summary>
        /// <param name="fileData">Specify the string that has the file content.</param>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <param name="fileMode">Specify the file write mode operatation. </param>
        private static void WriteTextStorage(string fileData, string filePath, Mode fileMode)
        {
            try
            {


                // Create directory if not exists.
                string logFilePath = filePath;
                FileInfo fileInfo = new FileInfo(logFilePath);

                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                // Check file write mode and write content.
                if (Equals(fileMode, Mode.Append))
                {
                    File.AppendAllText(logFilePath, fileData);
                }
                else
                {
                    File.WriteAllText(logFilePath, fileData);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                throw;
            }
        }

        private enum Mode
        {
            Read,  // Indicates text file read mode operation.
            Write, // Indicates text file write mode operation. It deletes the previous content.
            Append  // Indicates text file append mode operation. it preserves the previous content.
        }
        private string SetLineItemStatus(string Status)
        {
            /*Nivi Code start for Line Ship Date*/
            switch (Status)
            {
                case "O": return "Work in Progress";
                case "A": return "Work in Progress";

                case "W": return "Work in Progress";
                case "P": return "Work in Progress";
                case "S": return "Shipped";
                case "C": return "Cancelled";
                case "D": return "Deleted";
                case "M": return "Credit Memo";
                default: return "";
            }
            /*Nivi Code end for Line Ship Date*/
        }
        private string SetHeaderStatus(string Status)
        {
            switch (Status)
            {
                case "O": return "Open";
                case "S": return "Shipped";
                case "C": return "Cancelled";
                case "D": return "Deleted";

                default: return "";
            }
        }
        //Create order ship to file to  be submitted to ABS with the name as ORDSHP.
        //private void CreateMAGOS(List<OrderModel> model)
        //{
        //    StringBuilder MAGOS = new StringBuilder();
        //    foreach (var item in model)
        //        MAGOS.AppendLine($"{item.OrderNumber},0,0,{item.ShippingAddress?.FirstName},{item.ShippingAddress?.LastName},{item.ShippingAddress?.Address1},{item.ShippingAddress?.Address2},{item.ShippingAddress?.CityName},{item.ShippingAddress?.StateCode},{item.ShippingAddress?.PostalCode},{item.ShippingTypeName},0,{item.TaxCost},0,,,{item.ShippingAddress?.Address3}");
        //    string dateString = $"{DateTime.Now:yyyyMMdd-HH.mm.ss}";
        //    WriteTextStorage(MAGOS.ToString(), $"{ConfigurationManager.AppSettings["ABSOrderFilePath"]}/ToABS/ORDSHP-" + dateString + ".csv", Mode.Write);

        //}
        private void CreateMAGOS(List<OrderModel> model)
        {
            try
            {
                StringBuilder MAGOS = new StringBuilder();
                foreach (OrderModel item in model)
                {
                    //MAGOS.AppendLine($"{item.OrderNumber},0,0,{item.ShippingAddress?.FirstName},{item.ShippingAddress?.LastName},{item.ShippingAddress?.Address1},{item.ShippingAddress?.Address2},{item.ShippingAddress?.CityName},{item.ShippingAddress?.StateCode},{item.ShippingAddress?.PostalCode},{item.ShippingTypeName},0,{item.ShippingCost},0,,,{item.ShippingAddress?.Address3}");
                    //MAGOS.AppendLine($"{item.OrderNumber}|0|0|{item.ShippingAddress?.FirstName}|{item.ShippingAddress?.LastName}|{item.ShippingAddress?.Address1}|{item.ShippingAddress?.Address2}|{item.ShippingAddress?.CityName}|{item.ShippingAddress?.StateCode}|{item.ShippingAddress?.PostalCode}|{item.ShippingTypeName}|0|{item.ShippingCost}|0|||{item.ShippingAddress?.Address3}");
                    //MAGOS.AppendLine($"{item.OrderNumber}|0|0|{item.ShippingAddress?.FirstName}|{item.ShippingAddress?.LastName}|{item.ShippingAddress?.Address1}|{item.ShippingAddress?.CompanyName}|{item.ShippingAddress?.CityName}|{item.ShippingAddress?.StateCode}|{item.ShippingAddress?.PostalCode}|{item.ShippingTypeName}|0|{item.ShippingCost}|0|||{item.ShippingAddress?.Address3}");
                    MAGOS.AppendLine($"{item.OrderNumber}|0|0|{item.ShippingAddress?.CompanyName}|{item.ShippingAddress?.LastName}|{item.ShippingAddress?.Address1}|{item.ShippingAddress?.FirstName}|{item.ShippingAddress?.CityName}|{item.ShippingAddress?.StateCode}|{item.ShippingAddress?.PostalCode}|{item.ShippingTypeName}|0|{item.ShippingCost}|0|||{item.ShippingAddress?.Address3}");
                    ZnodeLogging.LogMessage("COMPANY NAME =" + item.ShippingAddress?.CompanyName, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                }

                string dateString = $"{DateTime.Now:yyyyMMdd-HH.mm.ss}";
                WriteTextStorage(MAGOS.ToString(), $"{ConfigurationManager.AppSettings["ABSOrderFilePath"]}/ToABS/ORDSHP-" + dateString + ".csv", Mode.Write);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("CreateMAGOS " + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }

        }
        //Create order header file to  be submitted to ABS with the name as ORDHDR.
        //private void CreateMAGOH(List<OrderModel> model)
        //{
        //    StringBuilder MAGOH = new StringBuilder();
        //    foreach (var item in model)
        //        MAGOH.AppendLine($"{item.OrderNumber},0,{item.OrderNumber},,,,,,,,,,,,{item.BillingAddress.EmailAddress},{item.BillingAddress.FirstName},{item.BillingAddress.AddressId},{item.BillingAddress.Address2},{item.BillingAddress.CityName},{item.BillingAddress.StateCode},{item.BillingAddress.PostalCode},{item.BillingAddress.CountryName},{item?.OrderDate.Date.ToString("yyyyMMdd")},{item?.OrderDate.TimeOfDay.ToString("hhmmss")},0,0,0,0,0,0,{item.Total},{item.TaxCost},,{item.PaymentType},{item.CreditCardNumber},,,,0,,,,,,,,,{item.BillingAddress.Address3},,,,,{item.PurchaseOrderNumber},{item.CouponCode}");
        //    string dateString = $"{DateTime.Now:yyyyMMdd-HH.mm.ss}";
        //    WriteTextStorage(MAGOH.ToString(), $"{ConfigurationManager.AppSettings["ABSOrderFilePath"]}/ToABS/ORDHDR-" + dateString + ".csv", Mode.Write);

        //}
        //Create order header file to  be submitted to ABS with the name as ORDHDR.___
        private void CreateMAGOH(List<OrderModel> model)
        {
            try
            {

                StringBuilder MAGOH = new StringBuilder();
                foreach (OrderModel item in model)
                {
                    string NeedDate = "";
                    if (item.WebServiceDownloadDate != null)
                    {
                        NeedDate = item?.WebServiceDownloadDate.Value.Date.ToString("yyyyMMdd");
                    }
                    //MAGOH.AppendLine($"{item.OrderNumber},0,{item.OrderNumber},{item.ShippingAddress.ExternalId},{item.BillingAddress.ExternalId},,,,,,,,,,{item.BillingAddress.EmailAddress},{item.BillingAddress.FirstName},{item.BillingAddress.Address1},{item.BillingAddress.Address2},{item.BillingAddress.CityName},{item.BillingAddress.StateCode},{item.BillingAddress.PostalCode},{item.BillingAddress.CountryName},{item?.OrderDate.Date.ToString("yyyyMMdd")},{item?.OrderDate.TimeOfDay.ToString("hhmmss")},0,0,0,0,0,0,{item.Total},{item.TaxCost},{item.BillingAddress.FirstName},{item.PaymentDisplayName},{item.CreditCardNumber},{item.Custom1},{item.BillingAddress.PhoneNumber},{item.PaymentTransactionToken},0,{item.PaymentTransactionToken},,,{item.Custom2},{item.Custom3},,,{item.Custom5},{item.BillingAddress.Address3},,{item.KeyReceiptHtml},,,{item.PurchaseOrderNumber},{item.CouponCode},{item.Custom4},{item.BillingAddressHtml},{item.DiscountAmount},{item.CSRDiscountAmount},{item.WebServiceDownloadDate},");
                    MAGOH.AppendLine($"{item.OrderNumber}|0|{item.OrderNumber}|{item.ShippingAddress.ExternalId}|{item.BillingAddress.ExternalId}||||||||||{item.BillingAddress.EmailAddress}|{item.BillingAddress.FirstName}|{item.BillingAddress.Address1}|{item.BillingAddress.Address2}|{item.BillingAddress.CityName}|{item.BillingAddress.StateCode}|{item.BillingAddress.PostalCode}|{item.BillingAddress.CountryName}|{item?.OrderDate.Date.ToString("yyyyMMdd")}|{item?.OrderDate.TimeOfDay.ToString("hhmmss")}|0|0|0|0|0|0|{item.Total}|{item.TaxCost}|{item.BillingAddress.FirstName}|{item.PaymentDisplayName}|{item.CreditCardNumber}|{item.Custom1}|{item.BillingAddress.PhoneNumber}|{item.CreatedByName}|0|{item.PaymentTransactionToken}|{item.CardType}||{item.Custom2}|{item.Custom3}|||{item.Custom5}|{item.BillingAddress.Address3}||{item.KeyReceiptHtml}|||{item.PurchaseOrderNumber}|{item.CouponCode}|{item.Custom4}|{item.BillingAddressHtml}|{item.DiscountAmount}|{item.CSRDiscountAmount}|{NeedDate}|");
                }

                string dateString = $"{DateTime.Now:yyyyMMdd-HH.mm.ss}";

                WriteTextStorage(MAGOH.ToString(), $"{ConfigurationManager.AppSettings["ABSOrderFilePath"]}/ToABS/ORDHDR-" + dateString + ".csv", Mode.Write);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("CreateMAGOH " + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }

        }
        //Create order per item sku file to  be submitted to ABS with the name as ORDDTL.
        private void CreateMAGOD(List<OrderModel> model)
        {
            try
            {
                StringBuilder MAGOD = new StringBuilder();
                //Custom2-Personalized true color product
                foreach (OrderModel item in model)
                {
                    foreach (OrderLineItemModel lineItems in item.OrderLineItems)
                    {

                        MAGOD.AppendLine($"{item?.OrderNumber}|0|{lineItems?.Sku}|{lineItems?.Sku}|{lineItems?.Quantity}|{lineItems?.Price}|||||0|0|0|0|0|{lineItems?.ShipDate.Value.Date.ToString("yyyyMMdd")}|{lineItems.TrackingNumber}|0||{lineItems.Custom2}||{lineItems.ProductName}|{lineItems.Custom3}|");
                    }
                    //MAGOD.AppendLine($"{item?.OrderNumber},0,{lineItems?.Sku},{lineItems?.Sku},{lineItems?.Quantity},{lineItems?.Price},,,,0,0,0,0,0,0,{lineItems?.ShipDate.Value.Date.ToString("yyyyMMdd")},{lineItems.TrackingNumber},0,,,,{lineItems.ProductName},");

                }
                string dateString = $"{DateTime.Now:yyyyMMdd-HH.mm.ss}";
                //ZnodeLogging.LogMessage(MAGOD.ToString(), ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                WriteTextStorage(MAGOD.ToString(), $"{ConfigurationManager.AppSettings["ABSOrderFilePath"]}/ToABS/ORDDTL-" + dateString + ".csv", Mode.Write);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Details " + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }

        }

        private string GetArchiveFolderPath(string foldername)
        {
            return $"{GetDestinationUrl("ABSFolderPath")}\\{foldername}\\";
        }
        /*Original Code*/
        //private bool MoveFileToArchiveOnSuccess(string filename, string foldername)
        //{
        //    try
        //    {
        //        string fileToMove = "";

        //        fileToMove = GetCSVFilePath(filename);
        //        string destinationPath = GetArchiveFolderPath(foldername);

        //        string today = String.Join("_", DateTime.Now.ToString().Split('/'));
        //        string todaydatetime = String.Join("_", today.Split(':'));
        //        //string moveTo = destinationPath + @"/" + todaydatetime + "_" + filename + ".csv";
        //        string moveTo = destinationPath + @"/" + filename + "_" + todaydatetime + ".csv";
        //        //moving file
        //        if(File.Exists(fileToMove))
        //        { 
        //            File.Move(fileToMove, moveTo);
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("ZnodeABSConnector MoveFileToArchiveOnSuccess " + ex.Message, "ZnodeABSConnector", TraceLevel.Error);
        //        return false;
        //    }
        //}

        /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
        private bool MoveFileToArchiveOnSuccess(string filename, string foldername)
        {
            try
            {

                string destinationPath = GetArchiveFolderPath(foldername);

                if (File.Exists(filename))
                {
                    File.Move(filename, destinationPath + Path.GetFileName(filename));
                }
                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("ZnodeABSConnector MoveFileToArchiveOnSuccess " + ex.Message, "ZnodeABSConnector", TraceLevel.Error);
                return false;
            }
        }
        /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/


        //Order status enum
        public enum ABSOrderStateEnum
        {
            OPEN = 'O',
            DELETED = 'D',
            PICKED = 'P',
            WIP = 'W',
            INVENTORY = 'A',
            CANCELLED = 'C',
            SHIPPED = 'S',
        }


        /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
        public string GetProcessingFileName(string ImportName)
        {
            /*Nivi: This code is written to take the files from folders i.e product,customer,Inventory	
            In case if there are multiple files this code is taking the name of first come file i.e.older file. 	
             */
            string FolderName = "";
            switch (ImportName)
            {
                case ABSCSVNames.ProductAwct:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSProductPath"]);
                    break;
                case ABSCSVNames.InventoryBySKU:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSInventoryPath"]);
                    break;
                case ABSCSVNames.CustomerShipTo:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSCustomerPath"]);
                    break;
                case ABSCSVNames.CustomerSoldTo:
                    FolderName = Convert.ToString(ConfigurationManager.AppSettings["ABSCustomerPath"]);
                    break;
            }
            string[] files = Directory.GetFiles($"{GetDestinationUrl("ABSFolderPath")}/{FolderName}/");
            List<AWCTABSFilenames> fileNamesList = new List<AWCTABSFilenames>();
            foreach (string file in files)
            {
                if (file.Contains(ImportName))
                {
                    fileNamesList.Add(new AWCTABSFilenames
                    {
                        fileName = file,
                        modifiedDate = Directory.GetLastWriteTime(file),
                        createdDate = Directory.GetCreationTime(file)
                    });
                }
            }
            return fileNamesList?.OrderBy(x => x.modifiedDate)?.FirstOrDefault()?.fileName;
        }
        /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/
        #endregion
    }

    public class LineSplitter
    {
        protected readonly string[] RowDelimiter = new[] { "\r\n", "\n" };

        public IEnumerable<StringLine> SplitYield(string input)
        {
            using (var stringReader = new StringReader(input))
            {
                int rowIndex = 0;
                string rowString;
                while (!string.IsNullOrEmpty(rowString = stringReader.ReadLine()))
                {
                    rowIndex++;
                    yield return new StringLine(rowString, rowIndex);
                }
                stringReader.Close();
            }
        }
    }

    public class StringLine
    {
        internal StringLine(string line, int rowIndex)
        {
            Value = line;
            RowIndex = rowIndex;
        }

        public string Value { get; private set; }

        public int RowIndex { get; private set; }
    }

    /*Nivi Code start related to the to take the folder wise files ex : Product,Inventory,Customer*/
    public class AWCTABSFilenames
    {
        public string fileName { get; set; }
        public DateTime modifiedDate { get; set; }
        public DateTime createdDate { get; set; }
    }
    /*Nivi Code end related to the to take the folder wise files ex : Product,Inventory,Customer*/
}
