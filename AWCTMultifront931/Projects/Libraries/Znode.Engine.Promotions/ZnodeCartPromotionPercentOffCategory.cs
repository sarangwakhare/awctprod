using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionPercentOffCategory : ZnodeCartPromotionType
    {
        #region Private Variables
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        private List<PromotionCartItemQuantity> promocategorySkus;
        decimal quantity;
        #endregion

        #region Constructor
        public ZnodeCartPromotionPercentOffCategory()
        {
            Name = "Percent Off Category";
            Description = "Applies a percent off products in a particular category; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountPercent);
            Controls.Add(ZnodePromotionControl.RequiredCategory);
            Controls.Add(ZnodePromotionControl.RequiredCategoryMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the percent off products in a particular category in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));
            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            //to get all category of promotion by PromotionId
            List<CategoryModel> promotionCategories = promotionHelper.GetPromotionCategory(PromotionBag.PromotionId);
            
            //to set all promotions brand wise sku to calculate each sku quantity
            promocategorySkus = promotionHelper.SetPromotionCategorySKUQuantity(promotionCategories, ShoppingCart,out quantity);

            // Loop through each cart Item
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                bool isPromotionApplied = false;
                int productId = 0;
                // Get the category by product
                if (cartItem.Product.ZNodeConfigurableProductCollection.Count > 0 || cartItem.Product.ZNodeGroupProductCollection.Count > 0)
                    productId = cartItem.ParentProductId;
                else
                    productId = cartItem.Product.ProductID;

                List<CategoryModel> productCategories = promotionHelper.GetCategoryByProduct(productId);

                foreach (CategoryModel promotion in promotionCategories)
                {
                    foreach (CategoryModel product in productCategories)
                    {
                        if (promotion.PimCategoryId == product.PimCategoryId)
                        {
                            ApplyDiscount(out isPromotionApplied, isCouponValid, couponIndex, cartItem);
                            // Break out of the catalogs loop
                            break;
                        }
                    }
                    if (isPromotionApplied)
                        // Break out of the category loop
                        break;
                }
            }

            AddPromotionMessage(couponIndex);
        }
        #endregion

        #region Private Method
        private void ApplyDiscount(out bool isPromotionApplied, bool isCouponValid, int? couponIndex, ZnodeShoppingCartItem cartItem)
        {
            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);
            decimal qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);
            bool discountApplied = false;

            List<PromotionModel> PromolistModel = ApplicablePromolist.Where(x => x.OrderMinimum <= subTotal).ToList();
            decimal maxValue = PromolistModel.Max(y => y.OrderMinimum).GetValueOrDefault();
            //PromolistModel = new List<PromotionModel> { ApplicablePromolist.Where(x => x.OrderMinimum == maxValue).FirstOrDefault() };

            if (!Equals(PromotionBag.Coupons, null))
                PromolistModel = new List<PromotionModel> { ApplicablePromolist.Where(x => x.OrderMinimum == maxValue && x.PromoCode == PromotionBag.Coupons.Where(y => y.Code == x.PromoCode).Select(p => p.Code).FirstOrDefault()).FirstOrDefault() };
            else
                PromolistModel = new List<PromotionModel> { ApplicablePromolist.Where(x => x.OrderMinimum == maxValue && x.IsCouponRequired == isCouponValid).FirstOrDefault() };

            if (!ZnodePromotionHelper.IsApplicablePromotion(PromolistModel, PromotionBag.PromoCode, ShoppingCart.SubTotal, quantity, PromoApplicabilityCriteria.Both, isCouponValid))
            {
                isPromotionApplied = false;
                return;
            }   

            ZnodePricePromotionManager pricePromoManager = new ZnodePricePromotionManager();
            decimal basePrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

            if (Equals(PromotionBag.Coupons, null))
            {
                discountApplied = ApplyProductDiscount(isCouponValid, couponIndex, subTotal, qtyOrdered, basePrice, cartItem);
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        discountApplied = ApplyProductDiscount(isCouponValid, couponIndex, subTotal, qtyOrdered, basePrice, cartItem, coupon.Code);
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
            }

            isPromotionApplied = discountApplied;
        }
        private bool ApplyProductDiscount(bool isCouponValid, int? couponIndex, decimal subTotal, decimal qtyOrdered, decimal basePrice, ZnodeShoppingCartItem cartItem, string couponCode = "")
        {
            bool discountApplied = false;
            if (cartItem.Product.ZNodeGroupProductCollection.Count > 0)
            {
                foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                {
                    if (IsRequiredMinimumQuantity(cartItem.Product.SKU) && PromotionBag.MinimumOrderAmount <= subTotal && IsDiscountApplicable(cartItem.Product.DiscountAmount, cartItem.Product.FinalPrice))
                    {
                        var quantity = GetGroupProductCartQuantity(cartItem.Product.ProductID, group.ProductID);
                        cartItem.Product.DiscountAmount += cartItem.PromotionalPrice * (PromotionBag.Discount / 100);
                        cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), PromotionBag.Discount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                        cartItem.Quantity = quantity;
                        SetPromotionalPriceAndDiscount(cartItem, PromotionBag.Discount);
                    }
                    discountApplied = true;
                }
            }
            else
            {
                if (IsRequiredMinimumQuantity(cartItem.Product.SKU) && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    // Product discount percent
                    decimal discount = cartItem.PromotionalPrice * (PromotionBag.Discount / 100);
                    cartItem.Product.DiscountAmount += discount;
                    cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                    discountApplied = true;
                    SetPromotionalPriceAndDiscount(cartItem, discount);
                }
                else if (cartItem.Product.DiscountAmount > basePrice * (PromotionBag.Discount / 100))
                {
                    cartItem.Product.DiscountAmount -= basePrice * (PromotionBag.Discount / 100);
                    cartItem.Product.DiscountAmount = cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;
                    cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), cartItem.Product.DiscountAmount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                    discountApplied = true;
                }
            }

            if (!string.IsNullOrEmpty(couponCode) && discountApplied)
            {
                SetCouponApplied(couponCode);
                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
            }
            else
            {
                ShoppingCart.IsAnyPromotionApplied = true;
            }

            return discountApplied;
        }
        // to apply configurable product discout
        private void ApplyConfigurableProductDiscout(ZnodeShoppingCartItem cartItem, decimal qtyOrdered, decimal subTotal, out bool discountApplied, string couponCode = "")
        {
            discountApplied = false;
            foreach (ZnodeProductBaseEntity config in cartItem.Product.ZNodeConfigurableProductCollection)
            {
                decimal basePrice = config.FinalPrice;
                if (IsRequiredMinimumQuantity(cartItem.SKU) && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    decimal discount = basePrice * (PromotionBag.Discount / 100);
                    config.DiscountAmount += discount;
                    config.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), config.OrdersDiscount);
                    discountApplied = true;
                }
                else if (config.DiscountAmount > basePrice * (PromotionBag.Discount / 100))
                {
                    config.DiscountAmount -= basePrice * (PromotionBag.Discount / 100);
                    config.DiscountAmount = cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;
                    config.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), cartItem.Product.DiscountAmount, GetDiscountType(couponCode), config.OrdersDiscount);
                    discountApplied = true;
                }
            }
        }

        // to apply configurable product discout
        private void ApplyGroupProductDiscout(ZnodeShoppingCartItem cartItem, decimal subTotal, out bool discountApplied, string couponCode = "")
        {
            discountApplied = false;
            foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
            {
                decimal basePrice = group.FinalPrice;
                if (IsRequiredMinimumQuantity(cartItem.Product.SKU) && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    decimal discount = basePrice * (PromotionBag.Discount / 100);
                    group.DiscountAmount += discount;
                    group.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), group.OrdersDiscount);
                    discountApplied = true;
                }
                else if (group.DiscountAmount > basePrice * (PromotionBag.Discount / 100))
                {
                    group.DiscountAmount -= basePrice * (PromotionBag.Discount / 100);
                    group.DiscountAmount = cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;
                    group.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), cartItem.Product.DiscountAmount, GetDiscountType(couponCode), group.OrdersDiscount);
                    discountApplied = true;
                }
            }
        }

        //to check minimum quantity of promotion in the shopping cart item
        private bool IsRequiredMinimumQuantity(string sku)
        {
            bool result = false;
            if (promocategorySkus?.Count > 0)
            {
                decimal cartQty = promocategorySkus.Sum(x => x.Quantity);
                if (cartQty >= PromotionBag.RequiredCategoryMinimumQuantity)
                {
                    result = promocategorySkus.Any(x => x.SKU.Contains(sku));
                }
            }
            return result;
        }
        #endregion
    }
}
