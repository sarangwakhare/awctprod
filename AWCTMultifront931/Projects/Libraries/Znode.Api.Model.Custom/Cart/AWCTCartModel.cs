﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Cart
{
    public class AWCTCartModel : BaseModel
    {

        public List<AWCTOutOfStockOptions> OutOfStockOptionList { get; set; }
    }
    public class AWCTOutOfStockOptions
    {

        public string NeedDays { get; set; }
        public string OverrideNeedDate { get; set; }
        public string OutOfStockOption { get; set; }
        public string SKU { get; set; }
        public decimal Quantity { get; set; }
        public string Custom3 { get; set; }

    }
}
