﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
  public class ConfigurableProductViewModel: BaseModel
    {
        public int ConfigurableProductId { get; set; }
        public string ConfigurableProductName { get; set; }

        public string ConfigurableProductDefaultImage { get; set; }

        public string ParentPage { get; set; }
        public string SEOUrl { get; set; }

        public bool EnableProductCompare { get; set; }

        public bool IsNewProduct { get; set; }
        public int CategoryId { get; set; }
        public List<AssociatedProductDetails> AssociatedProducts { get; set; }
        public List<PriceSizeGroup> PriceSizeList { get; set; }
    }

    public class AssociatedProductDetails
    {
        public int AssociatedProductId { get; set; }
        public string SwatchText { get; set; }
        public string SwatchColor { get; set; }
        public string ProductColor { get; set; }
        public string Image { get; set; }
        public string ImagePath { get; set; }
        public string SwatchImagePath { get; set; }

    }

    public class PriceSizeGroup
    {
        
        public string Size { get; set; }

        public decimal Price { get; set; }

        
    }   
}
