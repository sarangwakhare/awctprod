﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTCartAgent : CartAgent, IAWCTCartAgent
    {
        #region Private member
        private readonly IShoppingCartClient _shoppingCartsClient;
        private readonly IPublishProductClient _publishProductClient;
        private readonly IAccountQuoteClient _accountQuoteClient;
        private static List<UpdatedProductQuantityModel> updatedProducts = new List<UpdatedProductQuantityModel>();
        #endregion
        public AWCTCartAgent(IShoppingCartClient shoppingCartsClient, IPublishProductClient publishProductClient, IAccountQuoteClient accountQuoteClient, IUserClient userClient) : base(shoppingCartsClient, publishProductClient, accountQuoteClient, userClient)
        {
            _shoppingCartsClient = GetClient<IShoppingCartClient>(shoppingCartsClient);
            _publishProductClient = GetClient<IPublishProductClient>(publishProductClient);
            _accountQuoteClient = GetClient<IAccountQuoteClient>(accountQuoteClient);
        }
        public virtual CartViewModel RemoveDiscount(string Promostatus)
        {

            //Get shopping cart form session or through cookie.
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                           GetCartFromCookie();


            if (shoppingCartModel.ProfileId != 2)
            {
                Promostatus = "";
            }
            shoppingCartModel.Custom5 = Promostatus;
            ShoppingCartModel calculatedCart = _shoppingCartsClient.Calculate(shoppingCartModel);
            shoppingCartModel.Coupons = calculatedCart?.Coupons;
            shoppingCartModel.Custom3 = calculatedCart.Custom3;
            shoppingCartModel.Total = calculatedCart?.Total;
            shoppingCartModel.Discount = Convert.ToDecimal(calculatedCart?.Discount);
            shoppingCartModel.ShippingCost = Convert.ToDecimal(calculatedCart?.ShippingCost);
            shoppingCartModel.TotalAdditionalCost = Convert.ToDecimal(calculatedCart?.TotalAdditionalCost);
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            CartViewModel cartViewModel = calculatedCart?.ToViewModel<CartViewModel>();
            return cartViewModel;

        }
        public override ShoppingCartItemModel BindGroupProducts(string simpleProduct, string simpleProductQty, CartItemViewModel cartItem, bool isNewExtIdRequired)
        {
            Dictionary<string, object> obj = new Dictionary<string, object>();
            if (cartItem.PersonalisedCodes != null && cartItem.PersonalisedCodes.Contains(simpleProduct))
            {
                obj = cartItem.PersonaliseValuesList;
            }
            else
            {
                obj = null;
            }

            return new ShoppingCartItemModel
            {
                ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId,
                SKU = cartItem.SKU,
                AddOnProductSKUs = cartItem.AddOnProductSKUs,
                AutoAddonSKUs = cartItem.AutoAddonSKUs,
                GroupProducts = new List<AssociatedProductModel> { new AssociatedProductModel { Sku = simpleProduct, Quantity = decimal.Parse(simpleProductQty) } },
                CustomText = cartItem.CustomText,
                //Column1 = cartItem.Custom1,
                //Column2 = cartItem.Custom2,
                Custom3 = cartItem.Custom3,
                Custom4 = cartItem.Custom4,
                PersonaliseValuesList = obj
                // pe= cartItem.PersonalisedValues,
                //PersonaliseValuesList = cartItem.PersonaliseValuesList,
            };
        }

        public override AddToCartViewModel AddToCartProduct(AddToCartViewModel cartItem)
        {
            if (IsNotNull(cartItem))
            {

                cartItem.CookieMappingId = GetFromCookie(WebStoreConstants.CartCookieKey);

                cartItem?.ShoppingCartItems.Where(x => x.SKU == cartItem.SKU).Select(x => { x.ProductType = cartItem.ProductType; return x; })?.ToList();

                //Create new cart.
                cartItem = _shoppingCartsClient.AddToCartProduct(GetShoppingCartValues(cartItem)?.ToModel<AddToCartModel>()).ToViewModel<AddToCartViewModel>();

                SaveInCookie(WebStoreConstants.CartCookieKey, cartItem.CookieMappingId.ToString());

                SaveInSession(WebStoreConstants.CartModelSessionKey, cartItem);

                return cartItem;
            }
            return null;
        }
        public override void PersonalisedItemsForAddToCart(AddToCartViewModel cartItem)
        {
            //Attribute Code And Value 
            string[] Codes = cartItem.PersonalisedCodes?.Split(',');
            string[] Values = cartItem.PersonalisedValues?.Split(',');
            Dictionary<string, object> SelectedAttributes = new Dictionary<string, object>();
            if (IsNotNull(Values))
            {
                //Add code and value pair
                for (int i = 0; i < Codes.Length; i++)
                {
                    SelectedAttributes.Add(Codes[i], Values[i]);
                }
            }
            cartItem.PersonaliseValuesList = SelectedAttributes;
        }
        public override void GetSelectedGroupedProductsForAddToCart(AddToCartViewModel cartItem)
        {
            //Get sku's and quantity of associated group products.
            string[] groupProducts = string.IsNullOrEmpty(cartItem.GroupProductSKUs) ? cartItem.GroupProducts?.Select(x => x.Sku)?.ToArray() : cartItem.GroupProductSKUs?.Split(',');
            string[] groupProductsQuantity = string.IsNullOrEmpty(cartItem.GroupProductsQuantity) ? cartItem.GroupProducts?.Select(x => Convert.ToString(x.Quantity))?.ToArray() : cartItem.GroupProductsQuantity?.Split('_');

            cartItem.SKU = cartItem.SKU;
            cartItem.AddOnProductSKUs = cartItem.AddOnProductSKUs;
            cartItem.AutoAddonSKUs = cartItem.AutoAddonSKUs;

            for (int index = 0; index < groupProducts?.Length; index++)
            {
                bool isNewExtIdRequired = !Equals(index, 0);
                //cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                //cartItem.GroupProducts = new List<AssociatedProductModel> { new AssociatedProductModel { Sku = groupProducts[index], Quantity = decimal.Parse(groupProductsQuantity[index]) } };

                ShoppingCartItemModel cartItemModel = BindConfigurableProducts(groupProducts[index], groupProductsQuantity[index], cartItem, isNewExtIdRequired);

                if (IsNotNull(cartItemModel))
                {
                    cartItem.ShoppingCartItems.Add(cartItemModel);
                }
            }
        }
        public override CartViewModel GetCart(bool isCalCulateTaxAndShipping = true, bool isCalculateCart = true)
        {

            AddToCartViewModel addToCartViewModel = GetFromSession<AddToCartViewModel>(WebStoreConstants.CartModelSessionKey);
            AddToCartModel addToCartModel = GetFromSession<AddToCartModel>(WebStoreConstants.CartModelSessionKey);
            ZnodeLogging.LogMessage("GetCart-Enter 2" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                                      GetCartFromCookie();
            //ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
            //                         GetFromSession<ShoppingCartModel>("RetainCart");
            ZnodeLogging.LogMessage("GetCart-Enter 3" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            //return base.GetCart(isCalCulateTaxAndShipping);
            if (IsNull(shoppingCartModel))
            {
                return new CartViewModel()
                {
                    HasError = true,
                    ErrorMessage = WebStore_Resources.OutofStockMessage
                };
            }
            else if (IsNotNull(addToCartViewModel))
            {
                shoppingCartModel.Coupons = addToCartViewModel.Coupons?.Count > 0 ? addToCartViewModel.Coupons : new List<CouponModel>();
                shoppingCartModel.ShippingId = addToCartViewModel.ShippingId;             
                addToCartViewModel.ShoppingCartItems.Join(shoppingCartModel.ShoppingCartItems, (s) => s.ConfigurableProductSKUs, (p) => p.ProductCode, (s, p) =>
                {                  
                    p.Sequence = s.Sequence;
                    return p;
                }).ToList();
            }
            else if (IsNotNull(addToCartModel))
            {
                shoppingCartModel.Coupons = addToCartModel.Coupons;
                shoppingCartModel.ShippingAddress = new AddressModel { PostalCode = addToCartModel.ZipCode };
             
                foreach (UpdatedProductQuantityModel updatedProduct in updatedProducts)
                {
                    int OmsSavedCartLineItemId = updatedProduct.PersonaliseValuesDetail.Select(x => x.OmsSavedCartLineItemId).FirstOrDefault();
                    ShoppingCartItemModel shoppingCartItem = shoppingCartModel?.ShoppingCartItems?.FirstOrDefault(x =>
                        x.SKU == updatedProduct.SKU &&
                        x.AddOnProductSKUs == updatedProduct.AddOnProductSKUs &&
                        x.AutoAddonSKUs == updatedProduct.AutoAddonSKUs &&
                        x.ConfigurableProductSKUs == updatedProduct.ConfigurableProductSKUs &&
                        x.OmsSavedcartLineItemId == OmsSavedCartLineItemId &&
                        x.BundleProductSKUs == updatedProduct.BundleProductSKUs ||
                        x.PersonaliseValuesDetail == updatedProduct.PersonaliseValuesDetail 
                    );
                    if (IsNotNull(shoppingCartItem))
                    {
                        shoppingCartItem.Quantity = updatedProduct.Quantity;
                    }
                }
                shoppingCartModel.ShippingId = addToCartModel.ShippingId;
            }
            else if (IsNull(addToCartModel))
            {
                shoppingCartModel.ShippingAddress = new AddressModel { PostalCode = null };
            }

            if (shoppingCartModel.ShoppingCartItems?.Count == 0)
            {
                return new CartViewModel();
            }

            //Remove Shipping and Tax calculation From Cart.
            RemoveShippingTaxFromCart(shoppingCartModel);

            //To remove the tax getting calculated when we come back from checkout page to cart page
            shoppingCartModel.ShippingAddress = new AddressModel();
            shoppingCartModel.ShippingAddress.CountryName = "us";
            shoppingCartModel.ShippingAddress.PostalCode = GetFromSession<string>(ZnodeConstant.ShippingEstimatedZipCode);
            ZnodeLogging.LogMessage("GetCart-CalculateCart-" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Calculate cart
            shoppingCartModel = CalculateCart(shoppingCartModel, isCalCulateTaxAndShipping);
            ZnodeLogging.LogMessage("GetCart-CalculateCart-End-" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            //Set currency details.
            return SetCartCurrency(shoppingCartModel);
        }

        private ShoppingCartItemModel BindConfigurableProducts(string configurableSKU, string quantity, AddToCartViewModel cartItem, bool isNewExtIdRequired)
        {
            return new ShoppingCartItemModel
            {
                ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId,
                SKU = cartItem.SKU,
                ConfigurableProductSKUs = configurableSKU,
                AddOnProductSKUs = cartItem.AddOnProductSKUs,
                AutoAddonSKUs = cartItem.AutoAddonSKUs,
                Quantity = Convert.ToDecimal(quantity),
                PersonaliseValuesList = cartItem.PersonaliseValuesList,
                GroupProducts = new List<AssociatedProductModel>(),
                //IsProductEdit = cartItem.IsProductEdit
            };
        }

        public virtual CartViewModel CalculateShippingCartReview(int shippingOptionId, int shippingAddressId, string shippingCode, string additionalInstruction = "")
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                         GetCartFromCookie();

            if (IsNull(cartModel?.Shipping))
            {
                cartModel.Shipping = new OrderShippingModel();
            }

            //bind shipping related data to ShoppingCartModel.
            cartModel.AdditionalInstructions = additionalInstruction;
            cartModel.Shipping.ShippingId = shippingOptionId > 0 ? shippingOptionId : cartModel.Shipping.ShippingId;
            cartModel.ShippingId = shippingOptionId > 0 ? shippingOptionId : cartModel.ShippingId;
            cartModel.Shipping.ResponseCode = (IsNotNull(shippingCode) && shippingCode != string.Empty) ? shippingCode : cartModel.Shipping.ResponseCode;
            cartModel.ShippingAddress = IsNotNull(cartModel?.ShippingAddress) ? cartModel.ShippingAddress : DependencyResolver.Current.GetService<IUserAgent>().GetAddressList()?.AddressList?.FirstOrDefault(x => x.AddressId == shippingAddressId)?.ToModel<AddressModel>();
            cartModel.Shipping.ShippingCountryCode = IsNull(cartModel?.ShippingAddress?.CountryName) ? "us" : cartModel.ShippingAddress.CountryName;
            //Calculate cart.
            CartViewModel cartViewModel = CalculateCart(cartModel, true, true)?.ToViewModel<CartViewModel>();
            cartViewModel.AdditionalInstruction = cartModel.AdditionalInstructions;
            cartModel.Shipping.ShippingDiscount = Convert.ToDecimal(cartViewModel?.Shipping?.ShippingDiscount);
            cartModel.ShippingCost = cartViewModel.ShippingCost;
            cartModel.TaxCost = cartViewModel.TaxCost;
            cartModel.Total = cartViewModel.Total;
            /*Nivi start code for chnages of group items changing estimated ship date*/
            if ((cartModel.Shipping.ResponseCode == "D" || cartModel.Shipping.ResponseCode == "S") && cartModel.Shipping.ShippingCountryCode == "CA")
            {
                cartModel.TotalAdditionalCost = cartViewModel.TotalAdditionalCost;
            }
            /*Nivi end code for chnages of group items changing estimated ship date*/
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);

            if (IsNotNull(cartViewModel))
            {
                cartViewModel.IsSinglePageCheckout = PortalAgent.CurrentPortal.IsEnableSinglePageCheckout;

                if (!string.IsNullOrEmpty(cartViewModel.ShippingResponseErrorMessage))
                {
                    cartViewModel.HasError = true;
                    cartViewModel.IsValidShippingSetting = false;
                    cartViewModel.ErrorMessage = cartViewModel.ShippingResponseErrorMessage;
                }
                else if (cartViewModel.ShoppingCartItems.Where(w => w.IsAllowedTerritories == false).ToList().Count > 0)
                {
                    cartViewModel.HasError = true;
                }

                //Get User details from session.
                UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

                //Bind User details to ShoppingCartModel.
                cartViewModel.CustomerPaymentGUID = userViewModel?.CustomerPaymentGUID;
                cartViewModel.UserId = IsNotNull(userViewModel) ? userViewModel.UserId : 0;
                cartViewModel.OrderStatus = cartModel.OrderStatus;
                cartViewModel.BudgetAmount = (userViewModel?.BudgetAmount).GetValueOrDefault();
                cartViewModel.PermissionCode = userViewModel?.PermissionCode;
                cartViewModel.RoleName = userViewModel?.RoleName;

                if (IsNotNull(cartViewModel?.ShoppingCartItems) && cartViewModel.ShoppingCartItems.Count > 0)
                {
                    cartViewModel.ShoppingCartItems = cartViewModel.ShoppingCartItems.OrderBy(c => c.GroupSequence).ToList();
                }

                if (cartModel.UserId > 0)
                {
                    cartViewModel.IsRequireApprovalRouting = IsRequireApprovalRouting('0', cartViewModel.Total.Value);
                }
            }
            return cartViewModel;
        }
        protected override ShoppingCartModel CalculateCart(ShoppingCartModel shoppingCartModel, bool isCalCulateTaxAndShipping = true, bool isCalculateCart = true)
        {
            //It is for Free shipping 
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            string isFreeShipping = GetFromSession<string>("IsFreeShipping");
            if (IsNotNull(shoppingCartModel))
            {
                shoppingCartModel.LocaleId = PortalAgent.LocaleId;
                shoppingCartModel.IsCalCulateTaxAndShipping = isCalCulateTaxAndShipping;
                string billingEmail = shoppingCartModel.BillingEmail;
                int shippingId = shoppingCartModel.ShippingId;
                int billingAddressId = shoppingCartModel.BillingAddressId;
                int shippingAddressId = shoppingCartModel.ShippingAddressId;
                int selectedAccountUserId = shoppingCartModel.SelectedAccountUserId;
                if (isCalculateCart)
                {
                    shoppingCartModel = _shoppingCartsClient.Calculate(shoppingCartModel);
                }
                //Bind required data to ShoppingCartModel.
                BindShoppingCartData(shoppingCartModel, billingEmail, shippingId, shippingAddressId, billingAddressId, selectedAccountUserId);
            }
            //It is for Free shipping 
            if (isFreeShipping != null && isFreeShipping != string.Empty)
            {
                shoppingCartModel.Custom1 = isFreeShipping;
            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            return shoppingCartModel;
        }
        private void BindShoppingCartData(ShoppingCartModel shoppingCartModel, string billingEmail, int shippingId, int shippingAddressId, int billingAddressId, int selectedAccountUserId)
        {
            shoppingCartModel.BillingEmail = billingEmail;
            shoppingCartModel.ShippingId = shippingId;
            shoppingCartModel.ShippingAddressId = shippingAddressId;
            shoppingCartModel.BillingAddressId = billingAddressId;
            shoppingCartModel.SelectedAccountUserId = selectedAccountUserId;
        }
        //Check whether approval routing is required for the current user quote.
        private bool IsRequireApprovalRouting(int quoteId, decimal quoteTotal)
        {
            bool IsRequireApprovalRouting = false;

            UserApproverListViewModel model = GetUserApproverList(0, true);
            if (model?.UserApprover?.Count > 0)
            {
                int firstLevelOrder = model.UserApprover.Min(x => x.ApproverOrder);
                decimal? firstLevelBudgetStart = model.UserApprover.Where(x => x.ApproverOrder == firstLevelOrder)?.Select(x => x.FromBudgetAmount)?.FirstOrDefault();
                if (IsNotNull(firstLevelBudgetStart) && quoteTotal > firstLevelBudgetStart)
                {
                    IsRequireApprovalRouting = true;
                }
            }
            return IsRequireApprovalRouting;
        }

        //Remove cart item from shopping cart.
        public override bool RemoveCartItem(string guiId)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            if (cartModel?.ShoppingCartItems?.Count < 0) { return false; }

            if (cartModel.ShoppingCartItems.Count.Equals(1))
            {
                //Remove all cart items if shopping cart having only one item.              
                return RemoveAllCartItems();
            }

            ShoppingCartItemModel cartItem = cartModel.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guiId);
            if (Equals(cartItem, null)) { return false; }

            //to remove child item if deleting the parent product having autoaddon associated with it
            if (!string.IsNullOrEmpty(cartItem.AutoAddonSKUs))
            {
                cartModel.RemoveAutoAddonSKU = cartItem.AutoAddonSKUs;
                cartModel.IsParentAutoAddonRemoved = string.Equals(cartItem.SKU.Trim(), cartItem.AutoAddonSKUs.Trim(), StringComparison.OrdinalIgnoreCase) ? false : true;
            }

            // Remove item and update the cart in Session and API.
            cartModel.ShoppingCartItems.Remove(cartItem);
            //To remove Add on Product --Nivi code---start
            string Sku = cartItem.ConfigurableProductSKUs;
            string newCustomText = string.Empty;
            List<ShoppingCartItemModel> addItems = cartModel.ShoppingCartItems.Where(x => x.CustomText != null).ToList();
            if (addItems.Count > 0)
            {
                foreach (ShoppingCartItemModel item in addItems)
                {
                    string customText = item.CustomText;
                    if (customText != string.Empty || customText != null)
                    {
                        if (!customText.Contains(Sku))
                        {
                            continue;
                        }
                        string[] AddOn = customText.Split(',');
                        int iCount = AddOn.Select(x => x == Sku).Count();
                        if (iCount > 1)
                        {
                            int imatch = 0;
                            foreach (string text in AddOn)
                            {
                                if (text == Sku && imatch == 0)
                                {
                                    imatch = 1;
                                }
                                else
                                {
                                    newCustomText = newCustomText + text + ",";
                                }
                            }
                            newCustomText = newCustomText.Substring(0, newCustomText.Length - 1);
                        }
                        else
                        {
                            AddOn = AddOn.Where(w => w != Sku).ToArray();
                            newCustomText = string.Join(",", AddOn);
                        }
                        item.CustomText = newCustomText;
                        item.Quantity = item.Quantity - 1;
                        if (newCustomText == string.Empty)
                        {
                            cartModel.ShoppingCartItems.Remove(item);
                        }
                    }
                }
            }
            //To remove Add on Product --Nivi code---end
            _shoppingCartsClient.RemoveCartLineItem(Convert.ToInt32(cartItem.OmsSavedcartLineItemId));

            //Save shopping cart in session.
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            return true;

        }

        #region To Check Cart Update 
        ////Update quantity of cart item. if commented and call base gives object reference

        public override AddToCartViewModel UpdateQuantityOfCartItem(string guid, string quantity, int productId)
        {
            #region Original Code from 9.2.0.0
            // Get shopping cart from session.
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            decimal newQuantity = ModifyQuantityValue(quantity);

            if (IsNotNull(shoppingCart))
            {
                //Update quantity and update the cart.
                if (productId > 0)
                {
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(x => x.GroupProducts?.Where(y => y.ProductId == productId)?.Select(z => { z.Quantity = newQuantity; return z; })?.FirstOrDefault()).ToList();
                }
                else
                {
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.Quantity = Convert.ToDecimal(newQuantity.ToInventoryRoundOff()); return y; }).ToList();
                }

                ShoppingCartItemModel shoppingCartItemModel = shoppingCart.ShoppingCartItems?.FirstOrDefault(w => w.ExternalId == guid);
                shoppingCartItemModel?.AssociatedAddOnProducts?.ForEach(x => x.Quantity = Convert.ToDecimal(newQuantity.ToInventoryRoundOff()));
                if (IsNotNull(shoppingCartItemModel))
                {
                    AddToCartViewModel addToCartViewModel = new AddToCartViewModel();
                    addToCartViewModel.CookieMappingId = GetFromCookie(WebStoreConstants.CartCookieKey);
                    addToCartViewModel.GroupId = shoppingCartItemModel.GroupId;
                    addToCartViewModel.AddOnProductSKUs = shoppingCartItemModel.AddOnProductSKUs;
                    addToCartViewModel.AutoAddonSKUs = shoppingCartItemModel.AutoAddonSKUs;
                    addToCartViewModel.SKU = shoppingCartItemModel.SKU;
                    addToCartViewModel.ParentOmsSavedcartLineItemId = shoppingCartItemModel.ParentOmsSavedcartLineItemId;
                    addToCartViewModel.AssociatedAddOnProducts = shoppingCartItemModel.AssociatedAddOnProducts;
                    if (!string.IsNullOrEmpty(shoppingCartItemModel.BundleProductSKUs))
                    {
                        addToCartViewModel.OmsSavedCartLineItemId = shoppingCartItemModel.OmsSavedcartLineItemId;
                        addToCartViewModel.Quantity = shoppingCartItemModel.Quantity;
                        GetSelectedBundleProductsForAddToCart(addToCartViewModel, shoppingCartItemModel.BundleProductSKUs);
                    }
                    else if (shoppingCartItemModel.GroupProducts?.Count > 0)
                    {
                        shoppingCartItemModel.Quantity = shoppingCartItemModel.GroupProducts.FirstOrDefault().Quantity;
                        addToCartViewModel.ShoppingCartItems.Add(shoppingCartItemModel);
                    }

                    else if (!string.IsNullOrEmpty(shoppingCartItemModel.ConfigurableProductSKUs))
                    {
                        addToCartViewModel.OmsSavedCartLineItemId = shoppingCartItemModel.OmsSavedcartLineItemId;
                        addToCartViewModel.Quantity = shoppingCartItemModel.Quantity;
                        GetSelectedConfigurableProductsForAddToCart(addToCartViewModel, shoppingCartItemModel.ConfigurableProductSKUs);
                    }

                    else
                    {
                        addToCartViewModel.ShoppingCartItems.Add(shoppingCartItemModel);
                    }

                    AddToCartModel addToCartModel = addToCartViewModel.ToModel<AddToCartModel>();
                    addToCartModel.PublishedCatalogId = shoppingCart.PublishedCatalogId;
                    addToCartModel.Coupons = shoppingCart.Coupons;
                    addToCartModel.ZipCode = shoppingCart?.ShippingAddress?.PostalCode;
                    addToCartModel.UserId = shoppingCart.UserId;
                    addToCartModel.PortalId = shoppingCart.PortalId;                  
                    addToCartModel = _shoppingCartsClient.AddToCartProduct(addToCartModel);
                    addToCartModel.ShippingId = shoppingCart.ShippingId;
                    //Save items updated quantities with sku in list.
                    UpdateQuantityItem(shoppingCartItemModel);
                    SaveInSession(WebStoreConstants.CartModelSessionKey, addToCartModel);
                    SaveInCookie(WebStoreConstants.CartCookieKey, addToCartModel.CookieMappingId);
                    //SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, shoppingCart);
                }
            }
            return new AddToCartViewModel();
            #endregion

        }
        //Convert string type to decimal for quantity.
        private static decimal ModifyQuantityValue(string quantity)
        {
            decimal newQuantity = 0;
            if (quantity.Contains(","))
            {
                newQuantity = Convert.ToDecimal((quantity).Replace(",", "."));
            }
            else
            {
                newQuantity = decimal.Parse(quantity, CultureInfo.InvariantCulture);
            }

            return newQuantity;
        }
        private void UpdateQuantityItem(ShoppingCartItemModel shoppingCartItemModel)
        {

            UpdatedProductQuantityModel updatedProduct = updatedProducts.FirstOrDefault(x =>
                                    x.SKU == shoppingCartItemModel.SKU &&
                                    x.AddOnProductSKUs == shoppingCartItemModel.AddOnProductSKUs &&
                                    x.AutoAddonSKUs == shoppingCartItemModel.AutoAddonSKUs &&
                                    x.ConfigurableProductSKUs == shoppingCartItemModel.ConfigurableProductSKUs &&
                                    x.BundleProductSKUs == shoppingCartItemModel.BundleProductSKUs &&
                                    x.PersonaliseValuesDetail == shoppingCartItemModel.PersonaliseValuesDetail
                                    );
            if (IsNotNull(updatedProduct))
            {
                updatedProduct.Quantity = shoppingCartItemModel.Quantity;
                
            }
            else
            {
                updatedProducts.Add(new UpdatedProductQuantityModel()
                {
                    SKU = shoppingCartItemModel.SKU,
                    AddOnProductSKUs = shoppingCartItemModel.AddOnProductSKUs,
                    AutoAddonSKUs = shoppingCartItemModel.AutoAddonSKUs,
                    BundleProductSKUs = shoppingCartItemModel.BundleProductSKUs,
                    ConfigurableProductSKUs = shoppingCartItemModel.ConfigurableProductSKUs,
                    Quantity = shoppingCartItemModel.Quantity,
                    PersonaliseValuesDetail = shoppingCartItemModel.PersonaliseValuesDetail
                });
            }
        }

        #endregion
    }
}
