﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTUserAgent : UserAgent
    {
        #region Private Variables
        private readonly ICountryClient _countryClient;
        private readonly IWebStoreUserClient _webStoreAccountClient;
        private readonly IWishListClient _wishListClient;
        private readonly IUserClient _userClient;
        private readonly IPublishProductClient _productClient;
        private readonly ICustomerReviewClient _customerReviewClient;
        private readonly IOrderClient _orderClient;
        private readonly IGiftCardClient _giftCardClient;
        private readonly IAccountClient _accountClient;
        private readonly IAccountQuoteClient _accountQuoteClient;
        private readonly IOrderStateClient _orderStateClient;
        private readonly IPortalCountryClient _portalCountryClient;
        private readonly IProductAgent _productAgent;
        private readonly IShippingClient _shippingClient;
        private readonly ICartAgent _cartAgent;
        private readonly IPaymentClient _paymentClient;
        private readonly ICustomerClient _customerClient;
        private readonly IRoleClient _roleClient;
        private readonly IStateClient _stateClient;
        private readonly IPortalProfileClient _portalProfileClient;
        // private readonly IAWCTUserClient _AWCTUserClient;
        private string _domainName;
        private string _domainKey;


        #endregion
        public AWCTUserAgent
            (ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient, IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :

            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient, orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            _countryClient = GetClient<ICountryClient>(countryClient);
            _webStoreAccountClient = GetClient<IWebStoreUserClient>(webStoreAccountClient);
            _wishListClient = GetClient<IWishListClient>(wishListClient);
            _userClient = GetClient<IUserClient>(userClient);
            _productClient = GetClient<IPublishProductClient>(productClient);
            _customerReviewClient = GetClient<ICustomerReviewClient>(customerReviewClient);
            _orderClient = GetClient<IOrderClient>(orderClient);
            _giftCardClient = GetClient<IGiftCardClient>(giftCardClient);
            _accountClient = GetClient<IAccountClient>(accountClient);
            _accountQuoteClient = GetClient<IAccountQuoteClient>(accountQuoteClient);
            _orderStateClient = GetClient<IOrderStateClient>(orderStateClient);
            _portalCountryClient = GetClient<IPortalCountryClient>(portalCountryClient);
            _productAgent = new ProductAgent(GetClient<CustomerReviewClient>(), GetClient<PublishProductClient>(), GetClient<WebStoreProductClient>(), GetClient<SearchClient>(), GetClient<HighlightClient>(), GetClient<PublishCategoryClient>());
            _shippingClient = GetClient<IShippingClient>(shippingClient);
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
            _customerClient = GetClient<ICustomerClient>(customerClient);
            _roleClient = GetClient<RoleClient>();
            _stateClient = GetClient<IStateClient>(stateClient);
            _portalProfileClient = GetClient<IPortalProfileClient>(portalProfileClient);
        }
        private string SetTrackingUrl(string trackingNo, int shippingId)
        {
            string trackingUrl = GetTrackingUrlByShippingId(shippingId);
            return string.IsNullOrEmpty(trackingUrl) ? trackingNo : "<a target=_blank href=" + GetTrackingUrlByShippingId(shippingId) + trackingNo + ">" + trackingNo + "</a>";
        }
        private string GetTrackingUrlByShippingId(int shippingId)
            => _shippingClient.GetShipping(shippingId)?.TrackingUrl;
        //Get order details to generate the order reciept.
        public override OrdersViewModel GetOrderDetails(int orderId, int portalId = 0)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());
            filters.Add("IsHistoryUserReceipt", FilterOperators.Equals, "1");
            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (HelperUtility.IsNotNull(userViewModel) && orderId > 0)
            {
                try
                {
                    OrderModel orderModel = _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);
                    orderModel.BillingAddressHtml = orderModel.BillingAddress.DisplayName + "</Br>" + orderModel.BillingAddress.Address1 + "</Br>" + orderModel.BillingAddress.Address2 +
                                                   "</Br>" + orderModel.BillingAddress.CityName + "</Br>" + orderModel.BillingAddress.StateName + "</Br>" + orderModel.BillingAddress.PostalCode;
                    orderModel.OrderLineItems[0].ShippingAddressHtml = orderModel.ShippingAddress.DisplayName + "</Br>" + orderModel.ShippingAddress.Address1 + "</Br>" + orderModel.ShippingAddress.Address2 +
                                                   "</Br>" + orderModel.ShippingAddress.CityName + "</Br>" + orderModel.ShippingAddress.StateName + "</Br>" + orderModel.ShippingAddress.PostalCode;
                    OrdersViewModel orderDetails = orderModel?.ToViewModel<OrdersViewModel>();
                    orderDetails.CultureCode = PortalAgent.CurrentPortal?.CultureCode;

                    return orderDetails;
                }
                catch
                {
                    return base.GetOrderDetails(orderId, portalId);
                }
            }
            return new OrdersViewModel();
        }

        private static ExpandCollection SetExpandsForReceipt()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString());
            expands.Add(ExpandKeys.Store);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString());
            expands.Add(ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.IsFromOrderReceipt);
            expands.Add(ExpandKeys.PortalTrackingPixel);
            expands.Add(ExpandKeys.IsWebStoreOrderReciept);
            return expands;
        }

        public override LoginViewModel Login(LoginViewModel model)
        {
            LoginViewModel loginViewModel;
            try
            {
                model.PortalId = PortalAgent.CurrentPortal?.PortalId;
                //Authenticate the user credentials.
                UserModel userModel = _userClient.Login(UserViewModelMap.ToLoginModel(model), GetExpands());
                if (HelperUtility.IsNotNull(userModel))
                {
                    //Check of Reset password Condition.
                    if (HelperUtility.IsNotNull(userModel.User) && !string.IsNullOrEmpty(userModel.User.PasswordToken))
                    {
                        loginViewModel = UserViewModelMap.ToLoginViewModel(userModel);
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword, true, loginViewModel);
                    }
                    // Check user associated profiles.                
                    if (!userModel.Profiles.Any() && !userModel.IsAdminUser)
                    {
                        return ReturnErrorModel(WebStore_Resources.ProfileLoginFailedErrorMessage);
                    }

                    SetLoginUserProfile(userModel);
                    UserViewModel userViewModel = userModel.ToViewModel<UserViewModel>();
                    //bind userName to userViewModel
                    userViewModel.UserName = userModel.UserName;
                    //Save the User Details in Session.
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                    return UserViewModelMap.ToLoginViewModel(userModel);
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case 2://Error Code to Reset the Password for the first time login.
                        return ReturnErrorModel(ex.ErrorMessage, true);
                    case ErrorCodes.AccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorAccountLocked);
                    case ErrorCodes.LoginFailed:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                    case ErrorCodes.TwoAttemptsToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorTwoAttemptsRemain);
                    case ErrorCodes.OneAttemptToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorOneAttemptRemain);
                    case ErrorCodes.IsUsed:
                        return ReturnErrorModel(WebStore_Resources.ErrorMultipleRecordsLogin);
                    default:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
            }
            return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
        }

        //Set the Error properties values for Login View Model.
        private LoginViewModel ReturnErrorModel(string errorMessage, bool hasResetPassword = false, LoginViewModel model = null)
        {
            if (HelperUtility.IsNull(model))
            {
                model = new LoginViewModel();
            }

            //Set Model Properties.
            model.HasError = true;
            model.IsResetPassword = hasResetPassword;
            model.ErrorMessage = string.IsNullOrEmpty(errorMessage) ? WebStore_Resources.InvalidUserNamePassword : errorMessage;
            return model;
        }
        //Forgot Password, use to send the Reset Password Link to the user.
        public override UserViewModel ForgotPassword(UserViewModel model)
        {
            if (HelperUtility.IsNotNull(model))
            {
                try
                {
                    //trim all white spaces from username
                    model.UserName = Regex.Replace(model.UserName, @"\s", "");
                    //Get the User Details based on username.
                    UserModel userModel = _userClient.GetAccountByUser(model.UserName);
                    if (HelperUtility.IsNotNull(userModel))
                    {
                        if (!model.HasError)
                        {
                            //Check if the Username Validates or not.
                            if (string.Equals(model.UserName, userModel.UserName, StringComparison.InvariantCultureIgnoreCase) || string.Equals(model.UserName, userModel.Email, StringComparison.InvariantCultureIgnoreCase))
                            {
                                //Set the Current Domain Url.
                                model.BaseUrl = GetDomainUrl();

                                //Reset the Password for the user, to send the Reset Email Password Link.
                                model = ResetPassword(model);
                                return model;
                            }
                            else
                            {
                                return SetErrorProperties(model, WebStore_Resources.ValidEmailAddress);
                            }
                        }
                    }
                    else
                    {
                        return SetErrorProperties(model, WebStore_Resources.InvalidAccountInformation);
                    }
                }
                catch (ZnodeException ex)
                {
                    ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                    switch (ex.ErrorCode)
                    {
                        case ErrorCodes.IsUsed:
                            return SetErrorProperties(model, WebStore_Resources.ErrorMultipleRecordsResetPassword);
                        default:
                            return SetErrorProperties(model, WebStore_Resources.ErrorInvalidUsernamePassword);

                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                    return SetErrorProperties(model, WebStore_Resources.InvalidAccountInformation);
                }
            }

            return model;
        }

        //to set login user profile Id
        public override void SetLoginUserProfile(UserModel userModel)
        {
            base.SetLoginUserProfile(userModel);
            //For both Login and Impersonation Login
            SaveInSession("LoginFirstName", userModel.FirstName);
            SaveInSession("IsFreeShipping", userModel.Custom2);
        }

        //Update the Model with error properties.Return the updated model in UserViewModelFormat.
        private UserViewModel SetErrorProperties(UserViewModel model, string errorMessage)
        {
            model.HasError = true;
            model.ErrorMessage = errorMessage;
            return model;
        }
        //Method gets the Domain Base Url.
        private string GetDomainUrl()
            => (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)))
            ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;

        public override UserViewModel GetDashboard()
        {
            //Get user model data from session.
            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            try
            {
                if (IsNull(userViewModel))
                {
                    Logout();
                    return null;
                }
                BindDashBoardData(userViewModel);

                SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);

                return userViewModel;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                return userViewModel;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return userViewModel;
            }
        }

        #region Dashboard Private Method
        //Bind address data for dashboard.
        private void BindAddressData(UserViewModel userViewModel)
        {
            AddressListViewModel addresses = GetAddressList();
            userViewModel.AddressList = GetAddressesInSelectList(addresses?.AddressList);
            //If address not available in address book then there will not be any default address for user.
            userViewModel.ShippingAddress = addresses?.AddressList?.FirstOrDefault(x => x.IsDefaultShipping && x.DontAddUpdateAddress == false);
            userViewModel.BillingAddress = addresses?.AddressList?.FirstOrDefault(x => x.IsDefaultBilling && x.DontAddUpdateAddress == false);
        }

        //Bind product review data for dashboard.
        private void BindProductReviewData(UserViewModel userViewModel) => userViewModel.ReviewCount = GetProductReviewList().Count;

        //Bind gift card history data for dashboard.
        private void BindGiftCardData(UserViewModel userViewModel) => userViewModel.GiftCardCount = GetGiftCardHistoryList().GiftCardHistory.Count;

        //Bind wishlist data for dashboard.
        private void BindWishListData(UserViewModel userViewModel)
        {
            if (userViewModel?.WishList?.Count > 0)
                userViewModel.WishList = GetWishListProductData(userViewModel, string.Join(",", userViewModel.WishList.Select(x => x.SKU).ToList())).WishList;
        }
        //Get product expands.
        private ExpandCollection GetProductExpands()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ExpandKeys.Promotions);
            expands.Add(ExpandKeys.Pricing);
            expands.Add(ExpandKeys.Inventory);
            expands.Add(ExpandKeys.SEO);
            return expands;
        }
        //Get wishlisted product data.
        private WishListListViewModel GetWishListProductData(UserViewModel userModel, string skus)
        {
            _productClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
            ExpandCollection expands = GetProductExpands();
            expands.Add(ExpandKeys.WishlistAddOns);
            PublishProductListModel productList = _productClient.GetPublishProductList(expands, GetRequiredFilters(), null, null, null, new ParameterKeyModel { Ids = skus, ParameterKey = "SKU" });
            WishListListViewModel wishListViewModel = new WishListListViewModel { WishList = new List<WishListViewModel>() };
            wishListViewModel.WishList = userModel?.WishList;
            wishListViewModel.WishList.ForEach(item =>
            {
                var _product = productList?.PublishProducts?.FirstOrDefault(y => y.SKU == item.SKU);
                item.Product = _product?.ToViewModel<ProductViewModel>();

                if (HelperUtility.IsNotNull(item.Product))
                {
                    Helper.SetProductCartParameter(item.Product, item.AddOnProductSKUs);

                    string minQuantity = item.Product.Attributes?.Value(ZnodeConstant.MinimumQuantity);

                    decimal quantity = Convert.ToDecimal(string.IsNullOrEmpty(minQuantity) ? "0" : minQuantity);
                    _productAgent.CheckInventory(item.Product, quantity);

                    if (!Equals(item.Product.ProductType, ZnodeConstant.GroupedProduct) || !Equals(item.Product.ProductType, ZnodeConstant.ConfigurableProduct))
                    {
                        string addonSKu = item.AddOnProductSKUs ?? string.Empty;
                        GetProductFinalPrice(item.Product, item.Product.AddOns, 1, addonSKu);
                    }
                }
            });

            return wishListViewModel;
        }
        //Bind dashboard data.
        private void BindDashBoardData(UserViewModel userViewModel)
        {
            BindWishListData(userViewModel);
            BindProductReviewData(userViewModel);
            BindAddressData(userViewModel);
            BindUserProfileDropdownData(userViewModel);
            BindGiftCardData(userViewModel);
            //Orders. - To show top 3 orders only on dashboard.
            //userViewModel.OrderList = GetOrderList(Filters, null, 1, 3).List;
            BindCreditCardData(userViewModel);
            BindReferralCommissionData(userViewModel);
            BindUserApproverData(userViewModel);
        }

        //Bind user approvers data.
        private void BindUserApproverData(UserViewModel userViewModel)
        {
            ApproverDetailsModel approverDetailsModel = SessionProxyHelper.GetApproverDetails(userViewModel.UserId);
            if (IsNotNull(approverDetailsModel))
            {
                userViewModel.IsApprover = approverDetailsModel.IsApprover;
                userViewModel.HasApprovers = approverDetailsModel.HasApprovers;
            }
        }

        //Bind credit card data.
        private void BindCreditCardData(UserViewModel userViewModel)
        {
            //Added For Getting Active payment card ID
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "1"));
            filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, Convert.ToString(PortalAgent.CurrentPortal.PortalId)));
            filters.Add(new FilterTuple(FilterKeys.UserId, FilterOperators.Equals, Convert.ToString(userViewModel.UserId)));
            //Get Payment option list.
            PaymentSettingListModel paymentOptionListModel = _paymentClient.GetPaymentSettings(null, filters, null, null, null);
            userViewModel.PaymentSettingId = paymentOptionListModel?.PaymentSettings?.Where(x => x.PaymentTypeName == ZnodeConstant.CreditCard)?.Select(x => x.PaymentSettingId)?.FirstOrDefault();
        }

        //Bind referral commission data of a user.
        private void BindReferralCommissionData(UserViewModel userViewModel)
        {
            //Get referral commission data of a user.
            ReferralCommissionModel referralCommissionModel = _customerClient.GetCustomerAffiliate(userViewModel.UserId, null);
            userViewModel.ReferralCommissionData = referralCommissionModel?.ToViewModel<ReferralCommissionViewModel>();
        }

        //Bind user profile dropdown for dashboard.
        private void BindUserProfileDropdownData(UserViewModel userViewModel)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, Convert.ToString(PortalAgent.CurrentPortal.PortalId)));

            ProfileListModel list = new ProfileListModel();
            if (userViewModel.IsAdminUser)
            {
                PortalProfileListModel portalProfilelist = _portalProfileClient.GetPortalProfiles(null, filters, null, null, null);
                list.Profiles = MapPortalProfileToProfileModel(portalProfilelist?.PortalProfiles);
            }
            else
            {
                filters.Add(new FilterTuple(FilterKeys.UserId, FilterOperators.Equals, Convert.ToString(userViewModel.UserId)));
                SortCollection sort = new SortCollection();
                sort.Add(ZnodeProfileEnum.ProfileId.ToString(), DynamicGridConstants.ASCKey);
                //Get profile list.
                list = _customerClient.GetCustomerPortalProfilelist(null, filters, sort, null, null);
            }
            userViewModel.ProfileList = GetProfiles(list.Profiles);
        }
        private List<ProfileModel> MapPortalProfileToProfileModel(List<PortalProfileModel> portalProfileList)
        {
            List<ProfileModel> list = new List<ProfileModel>();
            if (portalProfileList?.Count > 0)
                foreach (var item in portalProfileList)
                    list.Add(
                        new ProfileModel
                        {
                            ProfileId = item.ProfileId,
                            Name = item.ProfileName,
                            ProfileName = item.ProfileName,
                        });

            return list;
        }
        #endregion

        //Reset the Password for the user.
        private UserViewModel ResetPassword(UserViewModel model)
        {
            try
            {
                _userClient.ForgotPassword(UserViewModelMap.ToUserModel(model));
                model.SuccessMessage = WebStore_Resources.SuccessResetPassword;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                SetErrorProperties(model, ex.ErrorMessage);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return model;
        }
        // Returns the Expands needed for the user agent.       
        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.GiftCardHistory,
                };
        }

    }
}
