﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTCheckoutAgent : CheckoutAgent
    {

        private readonly ICartAgent _cartAgent;
        private readonly IUserClient _userClient;
        private readonly IPaymentAgent _paymentAgent;
        private IAWCTOrderClient _AWCTOrderClient;
        private readonly IUserAgent _userAgent;
        public AWCTCheckoutAgent(IShippingClient shippingsClient, IPaymentClient paymentClient,
            IPortalProfileClient profileClient, ICustomerClient customerClient, 
            IUserClient userClient, IOrderClient orderClient, IAccountClient accountClient,
            IWebStoreUserClient webStoreAccountClient, IPortalClient portalClient, 
            IShoppingCartClient shoppingCartClient, IAddressClient addressClient, IAWCTOrderClient awctOrderClient) 
            : base(shippingsClient, paymentClient, profileClient, customerClient, userClient, orderClient, accountClient, webStoreAccountClient, portalClient, shoppingCartClient, addressClient)
        {
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _userClient = GetClient<IUserClient>(userClient);
            _paymentAgent = new PaymentAgent(GetClient<PaymentClient>(), GetClient<OrderClient>());
            _AWCTOrderClient = awctOrderClient;
            _userAgent = new UserAgent(GetClient<CountryClient>(), GetClient<WebStoreUserClient>(), GetClient<WishLishClient>(), GetClient<UserClient>(), GetClient<PublishProductClient>(), GetClient<CustomerReviewClient>(), GetClient<OrderClient>(), GetClient<GiftCardClient>(), GetClient<AccountClient>(), GetClient<AccountQuoteClient>(), GetClient<OrderStateClient>(), GetClient<PortalCountryClient>(), GetClient<ShippingClient>(), GetClient<PaymentClient>(), GetClient<CustomerClient>(), GetClient<StateClient>(), GetClient<PortalProfileClient>());
        }
        public override OrdersViewModel SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {

            //Get cart from session or by cookie.
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                      _cartAgent.GetCartFromCookie();

            /*nivi code for estimate date to upper date*/
            if (cartModel.Shipping.ResponseCode == "D")
            {//updated estimate with upper most date
                
                string[] Needdays = null;
                
                cartModel.ShoppingCartItems.ForEach(x => {
                    
                    Needdays = x.Custom3.Split('-');
                    
                    x.CartAddOnDetails = Needdays[0];
                });
                var highestEstimateDay = cartModel.ShoppingCartItems.OrderByDescending(x => Convert.ToInt32(x.CartAddOnDetails)).Select(x => x.CartAddOnDetails).FirstOrDefault();


                cartModel.ShoppingCartItems.ForEach(x => {
                    var firstSpaceIndex = x.Custom3.IndexOf("-");
                    var secondString = x.Custom3.Substring(firstSpaceIndex + 1);
                    x.Custom3 = highestEstimateDay + "-" + secondString;
                   
                });

            }
            else if (cartModel.Shipping.ResponseCode == "S")
            {
                cartModel.ShoppingCartItems.ForEach(x => {
                      if (!string.IsNullOrEmpty(x.CartAddOnDetails))
                    {
                        var firstSpaceIndex = x.Custom3.IndexOf("-");
                        var firstString = x.Custom3.Substring(0, firstSpaceIndex); 
                        var secondString = x.Custom3.Substring(firstSpaceIndex + 1); 
                        x.Custom3 = x.CartAddOnDetails + "-" + secondString;
                       
                    }
                   
                });

            }
            
            /*end*/

            string discount = cartModel.Custom3;
            try
            {
                cartModel.Custom1 = ((string[])submitOrderViewModel.Custom1)[0];
                cartModel.Custom2 = ((string[])submitOrderViewModel.Custom2)[0];
                cartModel.Custom3 = ((string[])submitOrderViewModel.Custom3)[0];
                cartModel.CSRDiscountDescription = discount;
            }
            catch (Exception)
            {
                cartModel.Custom1 = null;
                cartModel.Custom2 = ((string[])submitOrderViewModel.Custom2)[0];
                cartModel.Custom3 = ((string[])submitOrderViewModel.Custom3)[0];
                cartModel.CSRDiscountDescription = discount;
                ZnodeLogging.LogMessage("SubmitOrder .Web service Date Null", ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            }

            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            // Run time Address chnages issue
            //Get the list of all addresses associated to current logged in user.
            AddressModel shippingAddress = cartModel.ShippingAddress;
            AddressModel billingAdress = cartModel.BillingAddress;
            List<AddressModel> userAddresses = GetUserAddressList();
            cartModel.ShippingAddress = shippingAddress;
            cartModel.BillingAddress = billingAdress;

            ZnodeLogging.LogMessage("AWCTCheckoutAgent SubmitOrder=" + cartModel.Total, "SubmitOrder", TraceLevel.Error);
            ZnodeLogging.LogMessage("AWCTCheckoutAgent SubmitOrder=" + cartModel.TotalAdditionalCost, "SubmitOrder", TraceLevel.Error);


            OrdersViewModel modelorder = base.SubmitOrder(submitOrderViewModel);
            modelorder.Custom1 = cartModel.Custom1;
            modelorder.Custom4 = discount;
            return modelorder;
        }
        private List<AddressModel> GetUserAddressList()
        {
            var a = _userAgent.GetAddressList()?.AddressList?.ToModel<AddressModel>()?.ToList();
            return a;
        }

        public override List<BaseDropDownOptions> PaymentOptions()
        {
            List<BaseDropDownOptions> list = base.PaymentOptions();
            try
            {
                list.ForEach(x => { if (x.Id == "4") { x.Id = "_50"; } });

            }
            catch (Exception ex)
            {

            }

            return list;
        }
        public override GatewayResponseModel GetPaymentResponse(ShoppingCartModel cartModel, SubmitOrderViewModel submitOrderViewModel)
        {
            //ZnodeLogging.LogMessage("AWCTCheckoutAgent GetPaymentResponse Called", "CheckoutAgent", TraceLevel.Error);
            // Map shopping Cart model and submit Payment view model to Submit payment model 

            SubmitPaymentModel model = PaymentViewModelMap.ToModel(cartModel, submitOrderViewModel);

            // Map Customer Payment Guid for Save Credit Card 
            if (!string.IsNullOrEmpty(submitOrderViewModel.CustomerGuid) && string.IsNullOrEmpty(cartModel.UserDetails.CustomerPaymentGUID))
            {
                UserModel userModel = _userClient.GetUserAccountData(submitOrderViewModel.UserId);
                userModel.CustomerPaymentGUID = submitOrderViewModel.CustomerGuid;
                _userClient.UpdateCustomerAccount(userModel);

                UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

                if (string.IsNullOrEmpty(userViewModel.CustomerPaymentGUID))
                {
                    userViewModel.CustomerPaymentGUID = submitOrderViewModel.CustomerGuid;
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                }
            }
            //ZnodeLogging.LogMessage("AWCTCheckoutAgent cartModel.Total before=" + cartModel.Total, "CheckoutAgent", TraceLevel.Error);
            // ZnodeLogging.LogMessage("AWCTCheckoutAgent  cartModel.TotalAdditionalCost before=" + cartModel.TotalAdditionalCost, "CheckoutAgent", TraceLevel.Error);
            //model.Total = _paymentAgent.GetOrderTotal();
            /*Original Code*/
            //decimal? modeltotal = cartModel.Total + Math.Round(Convert.ToDecimal(cartModel.TotalAdditionalCost),2);
            //ZnodeLogging.LogMessage("AWCTCheckoutAgent modeltotal=" + modeltotal, "CheckoutAgent", TraceLevel.Error);

            decimal? modeltotal = 0;
            if (cartModel.ShippingAddress.CountryName == "CA")
            {
                modeltotal = cartModel.Total + Math.Round(Convert.ToDecimal(cartModel.TotalAdditionalCost), 2);
            }
            else
            {
                modeltotal = cartModel.Total;
                cartModel.TotalAdditionalCost = 0;
            }
            ZnodeLogging.LogMessage("AWCTCheckoutAgent modeltotal=" + modeltotal + " for country-" + cartModel.ShippingAddress.CountryName, "CheckoutAgent", TraceLevel.Error);
            model.Total = ConvertTotalToLocale(Convert.ToString(modeltotal));
           

            string[] data = submitOrderViewModel.PODocumentName.Split(',');
            if (data.Length > 0)
            {
                if (data[0] == "credit_card_50")
                {

                    //model.Total = ConvertTotalToLocale((Convert.ToDecimal(cartModel.Total+cartModel.TotalAdditionalCost) / 2).ToString("#.##"));
                    if (cartModel.ShippingAddress.CountryName == "CA")
                    {
                        model.Total = ConvertTotalToLocale((Convert.ToDecimal(cartModel.Total + cartModel.TotalAdditionalCost) / 2).ToString("#.##"));
                    }
                    else
                    {
                        model.Total = ConvertTotalToLocale((Convert.ToDecimal(cartModel.Total) / 2).ToString("#.##"));
                    }
                    ZnodeLogging.LogMessage("AWCTCheckoutAgent credit_card_50 model.Total after =" + model.Total, "CheckoutAgent", TraceLevel.Error);
                }
            }
            ZnodeLogging.LogMessage("AWCTCheckoutAgent GetPaymentResponse model.Total after=" + model.Total, "CheckoutAgent", TraceLevel.Error);
            
            return _paymentAgent.ProcessPayNow(model);
        }
        #region Private Method
        //to convert total amount to locale wise
        private string ConvertTotalToLocale(string total)
            => total.Replace(",", ".");
        #endregion
        //To generate unique order number on basis of current date.
        public override string GenerateOrderNumber(int portalId)
        {
            try
            {
                string orderNumber = string.Empty;
                orderNumber= _AWCTOrderClient.GenerateOrderNumber();
                return orderNumber;
            }
            catch (Exception ex)
            {
                return base.GenerateOrderNumber(portalId);
            }
           
           
        }
    }
}
