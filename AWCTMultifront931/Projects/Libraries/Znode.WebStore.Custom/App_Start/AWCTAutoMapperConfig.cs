﻿using AutoMapper;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.ViewModels;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomizedFormModel;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.WebStore.Custom.ViewModel;
using Znode.Sample.Api.Model.Cart;


namespace Znode.WebStore.Custom
{
    public static class AWCTAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<AssociatedProductDetails, ConfigurableAssociatedProductDetails>().ReverseMap();
            Mapper.CreateMap<PriceSizeGroup, ConfigurablePriceSizeGroup>().ReverseMap();
            Mapper.CreateMap<ConfigurableProductModel, ConfigurableProductViewModel>().ReverseMap();
            Mapper.CreateMap<ProductDetailGridViewModel, ProductDetailGridModel>().ReverseMap();
            Mapper.CreateMap<AWCTProductViewModel, AWCTPublishProductModel>().ReverseMap();
            Mapper.CreateMap<AWCTTruColorViewModel, AWCTTruColorModel>().ReverseMap();
            Mapper.CreateMap<AWCTTruColor, AWCTTruColorList>().ReverseMap();
            Mapper.CreateMap<ShoppingCartModel, AddToCartViewModel>();
            Mapper.CreateMap<AWCTNewCustomerApplicationModel, AWCTNewCustomerApplicationViewModel>().ReverseMap();
            Mapper.CreateMap<AWCTBecomeAContributerModel, AWCTBecomeAContributerViewModel>().ReverseMap();
            Mapper.CreateMap<AWCTModelSearchModel, AWCTModelSearchViewModel>().ReverseMap();
            /*Start Quote Lookup*/
            Mapper.CreateMap<AWCTQuoteLookupModel, AWCTQuoteLookupViewModel>().ReverseMap();
            /*End Quote Lookup*/
            /*Nivi Code start for estimated ship date cart refresh change*/
            Mapper.CreateMap<AWCTCartModel, AWCTOutOfStockOptions>().ReverseMap();
            /*Nivi Code end for estimated ship date cart refresh change*/


            //Mapper.CreateMap<ShoppingCartModel, CartViewModel>()
            //  .ForMember(d => d.SubTotal, opt => opt.MapFrom(src => src.SubTotal.ToPriceRoundOff()))
            //    .ForMember(d => d.Total, opt => opt.MapFrom(src => src.Total))
            //    .ForMember(d => d.Discount, opt => opt.MapFrom(src => src.Discount))
            //    .ForMember(d => d.ShippingResponseErrorMessage, opt => opt.MapFrom(src => src.Shipping.ResponseMessage))
            //    .ForMember(d => d.IsValidShippingSetting, opt => opt.MapFrom(src => src.Shipping.IsValidShippingSetting));
        }
    }
}
