﻿using DevTrends.MvcDonutCaching;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.IO;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Xml.Serialization;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.SAML;
using Znode.Libraries.SAMLWeb;
using Znode.WebStore.Core.Agents;

namespace Znode.Engine.WebStore.Controllers
{
    public class CustomUserController : UserController
    {
        #region Private Read-only members
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly IUserAgent _userAgent;
        private readonly ICartAgent _cartAgent;
        #endregion

        #region Public Constructor     
        public CustomUserController(IUserAgent userAgent, ICartAgent cartAgent, IAuthenticationHelper authenticationHelper, IPaymentAgent paymentAgent, IImportAgent importAgent, IFormBuilderAgent formBuilderAgent)
       : base(userAgent, cartAgent, authenticationHelper, paymentAgent, importAgent, formBuilderAgent)
        {
            _authenticationHelper = authenticationHelper;
            _userAgent = userAgent;
            _cartAgent = cartAgent;
        }
        #endregion

        [AllowAnonymous]
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public override ActionResult Login(string returnUrl)
        {
            return base.Login(returnUrl);
        }
        //Login.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Login(LoginViewModel model, string returnUrl, bool isSinglePageCheckout = false)
        {
            //Clear all donut caching.
            OutputCacheManager cacheManager = new OutputCacheManager();
            cacheManager.RemoveItems();
            // return base.Login(model, returnUrl, isSinglePageCheckout);
            if (ModelState.IsValid)
            {
                model.IsWebStoreUser = true;
                //Authenticate the User Credentials.
                LoginViewModel loginviewModel = _userAgent.Login(model);
                if (!loginviewModel.HasError)
                {

                    string FirstName = SessionHelper.GetDataFromSession<string>("LoginFirstName");
                    TempData["FirstName"] = FirstName;
                    //Set the Authentication Cookie.           
                    _authenticationHelper.SetAuthCookie(model.Username, model.RememberMe);

                    //Remember me.
                    if (model.RememberMe)
                    {
                        SaveLoginRememberMeCookie(model.Username);
                    }

                    ShoppingCartModel cart = SessionHelper.GetDataFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
                    TempData["CartCount"] = cart?.ShoppingCartItems?.Count;

                    //_cartAgent.MergeGuestUserCart();

                    //Added the code to navigate to the relative path provided in the global attribute
                    string globalattributeRedirectUrl = PortalAgent.CurrentPortal?.GlobalAttributes?.Attributes?.FirstOrDefault(x => x.AttributeCode == WebStoreConstants.RedirectPageOnLogin)?.AttributeValue;

                    if (!string.IsNullOrEmpty(globalattributeRedirectUrl))
                    {
                        return Redirect(globalattributeRedirectUrl);
                    }

                    if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Contains("checkout"))
                    {
                        return RedirectToAction<CheckoutController>(x => x.Index(isSinglePageCheckout));
                    }

                    if (PortalAgent.CurrentPortal.PersistentCartEnabled && _cartAgent.GetCartCount() > 0)
                    {
                        return RedirectToAction("Index", "Cart");
                    }

                    if (string.IsNullOrEmpty(returnUrl) || (!string.IsNullOrEmpty(returnUrl) && returnUrl.ToLower().Contains("login")))
                    {
                        return RedirectToAction<HomeController>(x => x.Index());
                    }

                    //Encode the query string parameter for special characters.
                    if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Contains("AddToWishList"))
                    {
                        returnUrl = $"{returnUrl.Remove(returnUrl.IndexOf("="))}={HttpUtility.UrlEncode(returnUrl.Substring(returnUrl.IndexOf("productSKU")).Split('=')[1])}";
                        return Redirect(returnUrl);
                    }

                    //Redirection to the Login Url.
                    _authenticationHelper.RedirectFromLoginPage(model.Username, true);
                    if (Request.IsAjaxRequest())
                    {
                        return Json(new { status = true, error = "" }, JsonRequestBehavior.AllowGet);
                    }
                    return new EmptyResult();
                }

                if (Request.IsAjaxRequest() == false && HelperUtility.IsNotNull(loginviewModel) && loginviewModel.IsResetPassword)
                {
                    TempData[WebStoreConstants.UserName] = model.Username;
                    return RedirectToAction<UserController>(x => x.ResetWebstorePassword());
                }

                if (Request.IsAjaxRequest() && HelperUtility.IsNotNull(loginviewModel) && loginviewModel.IsResetPassword)
                {
                    TempData[WebStoreConstants.UserName] = model.Username;
                    TempData[WebStoreConstants.CheckoutReturnUrl] = WebStoreConstants.CheckoutReturnUrl;
                    return Json(new { status = true, error = "", isresetpassword = true }, JsonRequestBehavior.AllowGet);
                }

                if (Request.IsAjaxRequest())
                {
                    return Json(new { status = false, error = loginviewModel.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                SetNotificationMessage(GetErrorNotificationMessage(loginviewModel.ErrorMessage));
                return View(loginviewModel);
            }
            return View(model);
        }
        //SAML call back 
        public ActionResult SAMLCallback(string returnUrl = "")
        {
            SessionObj sessionObj = SessionHelper.GetDataFromSession<SessionObj>(Request.Params["state"]);

            string samlResponseXML = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(sessionObj.SAMLToken));

            XmlSerializer serializer = new XmlSerializer(typeof(ResponseType));

            ResponseType response = (ResponseType)serializer.Deserialize(new StringReader(samlResponseXML));

            string username = string.Empty;
            //Find assertion
            AssertionType assertion = null;
            foreach (object item in response.Items)
            {
                if (item is AssertionType)
                {
                    assertion = item as AssertionType;
                }
            }

            //Find the username
            if (assertion != null)
            {
                foreach (object item in assertion.Subject.Items)
                {
                    if (item is NameIDType)
                    {

                        if (!string.IsNullOrEmpty(((NameIDType)item).Value))
                        {
                            username = ((NameIDType)item).Value;
                        }
                    }
                }
            }
            string id = Request.Params["id"];

            //Set the Authentication Cookie.  
            SetAuthCookie(username, id);

            return RedirectToAction<HomeController>(x => x.Index());
        }

        private void SetAuthCookie(string userName, string id)
        {
            Session.Add(WebStoreConstants.UserAccountKey, new UserViewModel { UserName = userName, UserId = Convert.ToInt32(id), RoleName = "admin" });

            //Set the Authentication Cookie.           
            _authenticationHelper.SetAuthCookie(userName, true);
        }

        //protected string GenerateRouteUri(string Uri)
        //{            
        //    string uri = new UriBuilder(Uri) { Host = _domainName }.Uri.ToString();
        //    return uri;
        //}

        //Login Status.
        [AllowAnonymous]
        public override PartialViewResult LoginStatus()
        {
            string FirstName = SessionHelper.GetDataFromSession<string>("LoginFirstName");
            TempData["FirstName"] = FirstName;
            return PartialView("_LoginPartial");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Address(AddressViewModel addressViewModel)
        {
            ModelState.Remove("LastName");
            ModelState.Remove("DisplayName");
            return base.Address(addressViewModel);
        }
        /*Nivi Code- Overridden to clear session in else part on logout*/
        public override ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                ExternalLoginInfo loginInfo = SessionHelper.GetDataFromSession<ExternalLoginInfo>(WebStoreConstants.SocialLoginDetails);

                if (HelperUtility.IsNotNull(loginInfo))
                    return Redirect(_userAgent.Logout(loginInfo));
                _userAgent.Logout();
            }
            else
            {
                SessionHelper.Abandon();
                SessionHelper.Clear();
            }
            if (Request.IsAjaxRequest())
                return Json(null, JsonRequestBehavior.AllowGet);
            return RedirectToAction<UserController>(x => x.Login("/"));
        }

        [Authorize]
        public override ActionResult ChangePassword()
        {
            return View();
        }

        #region Change Password
        //Change Password Page.
        //Change Password.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (SessionHelper.GetDataFromSession<ImpersonationModel>(WebStoreConstants.ImpersonationSessionKey) != null)
            {
                if (!ModelState.IsValid)
                {
                    foreach (var key in ModelState.Keys)
                    {
                        if (key == "OldPassword")
                            ModelState[key].Errors.Clear();
                    }
                }
            }
            if (ModelState.IsValid)
            {
                model.UserName = User.Identity.Name;

                //Change the Password for the user.
                model = _userAgent.ChangePassword(model);
                SetNotificationMessage(model.HasError
                    ? GetErrorNotificationMessage(model.ErrorMessage)
                    : GetSuccessNotificationMessage(model.SuccessMessage));

                if (!model.HasError)
                    return RedirectToAction<UserController>(x => x.Dashboard());
            }
            return View(model);
        }
        #endregion
    }
}