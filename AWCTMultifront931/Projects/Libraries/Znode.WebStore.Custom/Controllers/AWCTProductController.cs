﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.WebStore.Custom.Controllers
{
    public class AWCTProductController : ProductController
    {
        #region Private Readonly members
        private readonly IAWCTProductAgent _productAgent;
        //private readonly IUserAgent _accountAgent;
        //private readonly IAttributeAgent _attributeAgent;
        private readonly IAWCTCartAgent _cartAgent;
        //private const string ComparableProductsView = "_ComparableProducts";
        //private const string ProductComparePopup = "_ProductComparePopup";
        //private const string SendMailPopUp = "_SendMailView";
        private readonly IWidgetDataAgent _widgetDataAgent;

        #endregion
        #region Public Constructor
        public AWCTProductController(IAWCTProductAgent productAgent, IUserAgent userAgent, IAWCTCartAgent cartAgent, IWidgetDataAgent widgetDataAgent, IAttributeAgent attributeAgent) :
            base(productAgent, userAgent, cartAgent, widgetDataAgent, attributeAgent)
        {
            _productAgent = productAgent;
            _cartAgent = cartAgent;
            _widgetDataAgent = widgetDataAgent;
        }
        //public RSProductController() :
        //    base(null, null, null, null, null)
        //{ }

        #endregion

        /// <summary>
        /// GetProductSwatchImage Called from _ProductTile.cshtml and _ProductTitleHome.cshtml for PLP listing Color Swatches.
        /// </summary>
        /// <param name="PublishProductId"></param>
        /// <param name="ConfigurablePrdImage"></param>
        /// <param name="ConfigurablePrdName"></param>
        /// <param name="EnableProductCompare"></param>
        /// <param name="CategoryId"></param>
        /// <param name="IsNewProduct"></param>
        /// <param name="parentPage"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetProductSwatchImage(int PublishProductId, string ConfigurablePrdImage, string ConfigurablePrdName, bool EnableProductCompare, int CategoryId, bool IsNewProduct, string parentPage)
        {

            ConfigurableProductModel product = _productAgent.GetConfigurableProductViewModel(PublishProductId);
            // string imageSrc = string.IsNullOrEmpty(model.ImageSmallPath) ? "/no-image.png" : model.ImageSmallPath;
            product.ConfigurableProductDefaultImage = ConfigurablePrdImage;
            product.ConfigurableProductName = ConfigurablePrdName;
            product.EnableProductCompare = EnableProductCompare;
            product.CategoryId = CategoryId;
            product.ParentPage = parentPage;
            product.ConfigurableProductId = PublishProductId;
            product.IsNewProduct = IsNewProduct;


            return PartialView("_ProductSwatchImage", product);
        }
        [HttpPost]
        public override ActionResult AddToCart(CartItemViewModel cartItem)
        {
            _cartAgent.CreateCart(cartItem);
            return RedirectToAction<CartController>(x => x.Index());
        }
        /// <summary>
        /// GetPriceSizeList Called from _ProductOverview.cshtml for PDP Price-Size Chart.
        /// </summary>
        /// <param name="ConfigurableProductId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetPriceSizeList(int ConfigurableProductId)
        {

            ConfigurableProductModel product = _productAgent.GetPriceSizeList(ConfigurableProductId);
            return PartialView("_ProductPriceSizeGroup", product);
        }
        [HttpGet]
        [ChildActionOnly]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;publishState")]
        public override ActionResult BriefContent(int id = 0, string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        {
            ViewBag.ProductId = id;
            ViewBag.Seo = seo;
            ViewBag.IsQuickView = isQuickView;
            ViewBag.IsLite = true;

            ProductViewModel product = _productAgent.GetProductBrief(id);
            if (IsNull(product))
                return Redirect("/404");

            product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;

            string templateName = product.ProductTemplateName + "_Lite";

            return PartialView(templateName, product);
        }

        //Original Method
         
        [ChildActionOnly]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id")]
        public override ActionResult DetailsContent(int id = 0, string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        {
            ProductViewModel product = _productAgent.GetProduct(id);
            if (product == null)
                return Redirect("/404");

            product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;

            return PartialView(product.ProductTemplateName, product);

        }


        [ChildActionOnly]
       // [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;isloginuser;")]//;isloginuser;
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;isvalidrequest;")]
        public ActionResult AWCTDetailsContent(int id = 0, string seo = "", bool isQuickView = false, string publishState = "PRODUCTION", bool isvalidrequest = false)
        {
            ProductViewModel product = _productAgent.GetProduct(id);
            if (product == null)
                return Redirect("/404");

            product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;

            return PartialView(product.ProductTemplateName, product);

        }


        [HttpGet]
        public ActionResult GetGlobalAttributeData(string globalAttributeCodes, string svgPath)
        {

            AWCTTruColorViewModel trucolorModel = _productAgent.GetGlobalAttributeData(globalAttributeCodes);
          
            if (trucolorModel.TruColorList.Count > 0)
            {
                trucolorModel.TruColorList.FirstOrDefault().SvgFilePath = svgPath;
                return PartialView("_ProductTruColor", trucolorModel);
            }
            else
            {
                return new HttpNotFoundResult("True colors are not available.");

            }
            //trucolorModel.TruColorList.FirstOrDefault().SvgFilePath = svgPath;
            // return PartialView("_ProductTruColor", trucolorModel);
        }


        //[HttpPost]
        //public override ActionResult AddToCartProduct(AddToCartViewModel cartItem, bool IsRedirectToCart = false)
        //{
        //    cartItem = _cartAgent.AddToCartProduct(cartItem);
        //    if (cartItem?.ShoppingCartItems?.Count > 0)
        //    {
        //        return Json
        //                       (
        //                       new
        //                       {
        //                           total = cartItem?.ShoppingCartItems?.Sum(x => x.ExtendedPrice),
        //                           cartCount = cartItem?.ShoppingCartItems?.Sum(x => x.Quantity),
        //                       }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //        return RedirectToAction<CartController>(x => x.Index());
        //}

        [HttpGet]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
        public override ActionResult AlternateProductImages(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        {
            string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
            ShortProductViewModel product = null; //_productAgent.GetExtendedProductDetails(id, expandKeys);
            if (IsNull(product.ProductImage))
                return new HttpNotFoundResult("One of the supplied expands is not valid.");

            return PartialView("_AlternateImages", product);
        }
        [HttpGet]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
        public override ActionResult ProductInfo(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        {
            string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
            ShortProductViewModel product =  _productAgent.GetExtendedProductDetails(id, expandKeys);
            if (IsNull(product.ProductImage))
                return new HttpNotFoundResult("One of the supplied expands is not valid.");

            return PartialView("_ProductInfoLite", product);
        }
        //Gets the breadcrumb.
        public override ActionResult GetBreadCrumb(int categoryId, string productAssociatedCategoryIds, bool checkFromSession)
        {
            productAssociatedCategoryIds = string.IsNullOrEmpty(productAssociatedCategoryIds) ? null : productAssociatedCategoryIds;
            string breadCrumb = _productAgent.GetBreadCrumb(categoryId, productAssociatedCategoryIds?.Split(','), checkFromSession);
            string seeMore = _productAgent.GetSeeMoreString(breadCrumb);
            return Json(new
            {
                breadCrumb = breadCrumb ?? string.Empty,
                seeMore = seeMore ?? string.Empty,
            }, JsonRequestBehavior.AllowGet);
        }
        public override ActionResult GetHighlightInfo(int highLightId, int productId, string sku = "")
        {
            HighlightsViewModel highlight = _productAgent.GetHighlightInfo(highLightId, productId, sku);
            highlight = IsNotNull(highlight) ? highlight : new HighlightsViewModel();
            highlight.PublishProductId = productId;
            return View("HighlightInfo", highlight);
        }
        //Get Product highlights.
        [ChildActionOnly]
        public override ActionResult GetProductHighlights(int productId, string highLightsCodes = "", string sku = "")
        {
            List<HighlightsViewModel> viewModel = _productAgent.GetProductHighlights(productId, highLightsCodes);

            //Assign product id to highlight list.
            viewModel.ForEach(x => x.PublishProductId = productId);
            //Assign SKU to highlight list.
            viewModel.ForEach(x => x.SKU = sku);
            return View("_ProductHighLights", viewModel);
        }
        ////Get link products.
        //public override ActionResult GetLinkProducts(int productId)
        //{
        //    List<LinkProductViewModel> model = _widgetDataAgent.GetLinkProductList(new WidgetParameter { CMSMappingId = productId });
        //    foreach (ProductViewModel lnkProduct in model[0].PublishProduct)
        //    {
        //        ZnodeLogging.LogMessage("WebStoreLInk controller Product Image" + lnkProduct.Attributes.FirstOrDefault(x => x.AttributeCode == "ProductImage").AttributeValues, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                
        //    }
        //    return PartialView("_LinkProductList", model);
        //}
    }
}
