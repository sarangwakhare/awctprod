﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomProductModel;



namespace Znode.Api.Custom.Service.IService
{
    public interface IAWCTPublishProductService
    {
        //  ConfigurableProductViewModel GetConfigurableProductViewModel(int publishProductId, FilterCollection filters, NameValueCollection expands);

        ConfigurableProductViewModel GetPriceSizeList(int configurableProductId);

        AWCTPublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands);
        AWCTTruColorModel GetGlobalAttributeData(string globalAttributeCodes);
        AWCTPublishProductModel GetExtendedProductDetails(int publishProductId, FilterCollection filters, NameValueCollection expands);
      }
}
