﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTPaymentSettingService : PaymentSettingService
    {
        public override PaymentSettingListModel GetPaymentSettingList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            //UpdatePaymentSettingIdFilter(filters);

            int userId = GetUserIdFromFilter(filters);
            int portalId = GetPortalIdFromFilter(filters);
            int profileId = GetProfileIdFromFilter(filters);
            bool isAssociated = GetIsAssociatedFromFilter(filters);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IZnodeViewRepository<PaymentSettingModel> objStoredProc = new ZnodeViewRepository<PaymentSettingModel>();
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@ProfileId", profileId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsAssociated", isAssociated ? "1" : "0", ParameterDirection.Input, DbType.Int32);

            IList<PaymentSettingModel> list = objStoredProc.ExecuteStoredProcedureList("Znode_GetPaymentSetting @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT, @PortalId, @ProfileId, @UserId,@IsAssociated", 4, out pageListModel.TotalRowCount);
            PaymentSettingListModel paymentSettingListModel = new PaymentSettingListModel();
            paymentSettingListModel.PaymentSettings = list?.Count > 0 ? list?.ToList() : null;

            paymentSettingListModel.BindPageListModel(pageListModel);

            return paymentSettingListModel;
        }
        //Get UserId From Filter.
        private int GetUserIdFromFilter(FilterCollection filters)
        {
            int userId = 0;
            if (filters?.Count > 0 && filters.Any(x => x.FilterName.ToLower() == ZnodeUserEnum.UserId.ToString().ToLower()))
            {
                //Get filter value
                userId = Convert.ToInt32(filters.FirstOrDefault(x => x.FilterName.ToLower() == ZnodeUserEnum.UserId.ToString().ToLower())?.FilterValue);
                //Remove userId Filter from filters list
                filters.RemoveAll(x => x.FilterName.ToLower() == ZnodeUserEnum.UserId.ToString().ToLower());
            }
            return userId;
        }
        //Get profileId From Filter.
        private bool GetIsAssociatedFromFilter(FilterCollection filters)
        {
            bool profileId = false;
            if (filters?.Count > 0 && filters.Any(x => string.Equals(x.FilterName, FilterKeys.IsAssociated, StringComparison.InvariantCultureIgnoreCase)))
            {
                //Get filter value
                profileId = Convert.ToBoolean(filters.FirstOrDefault(x => string.Equals(x.FilterName, FilterKeys.IsAssociated, StringComparison.InvariantCultureIgnoreCase))?.FilterValue);
                //Remove profileId Filter from filters list
                filters.RemoveAll(x => string.Equals(x.FilterName, FilterKeys.IsAssociated, StringComparison.InvariantCultureIgnoreCase));
            }
            return profileId;
        }

        //Get portalId From Filter.
        private int GetPortalIdFromFilter(FilterCollection filters)
        {
            int portalId = 0;
            if (filters?.Count > 0 && filters.Any(x => string.Equals(x.FilterName, ZnodePortalPaymentSettingEnum.PortalId.ToString(), StringComparison.InvariantCultureIgnoreCase)))
            {
                //Get filter value
                portalId = Convert.ToInt32(filters.FirstOrDefault(x => string.Equals(x.FilterName, ZnodePortalPaymentSettingEnum.PortalId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue);
                //Remove portalId Filter from filters list
                filters.RemoveAll(x => string.Equals(x.FilterName, ZnodePortalPaymentSettingEnum.PortalId.ToString(), StringComparison.InvariantCultureIgnoreCase));
            }
            return portalId;
        }

        //Get profileId From Filter.
        private int GetProfileIdFromFilter(FilterCollection filters)
        {
            int profileId = 0;
            if (filters?.Count > 0 && filters.Any(x => string.Equals(x.FilterName, ZnodeProfileEnum.ProfileId.ToString(), StringComparison.InvariantCultureIgnoreCase)))
            {
                //Get filter value
                profileId = Convert.ToInt32(filters.FirstOrDefault(x => string.Equals(x.FilterName, ZnodeProfileEnum.ProfileId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue);
                //Remove profileId Filter from filters list
                filters.RemoveAll(x => string.Equals(x.FilterName, ZnodeProfileEnum.ProfileId.ToString(), StringComparison.InvariantCultureIgnoreCase));
            }
            return profileId;
        }

    }
}
