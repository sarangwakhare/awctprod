﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;


namespace Znode.Api.Custom.Service.Service
{
   public class AWCTFormBuilderService : FormBuilderService
    {
        private readonly IZnodeRepository<ZnodeEmailTemplateMapper> _emailTemplateMapperRepository;
        private readonly IZnodeRepository<ZnodeFormWidgetEmailConfiguration> _formWidgetEmailConfigurationRepository;
        private readonly IZnodeRepository<ZnodeEmailTemplateArea> _emailTemplateAreaRepository;
        private readonly IZnodeRepository<ZnodeCMSFormWidgetConfiguration> _formWidgetConfigurationRepository;


        public AWCTFormBuilderService()
        {
            _emailTemplateMapperRepository = new ZnodeRepository<ZnodeEmailTemplateMapper>();
            _formWidgetEmailConfigurationRepository = new ZnodeRepository<ZnodeFormWidgetEmailConfiguration>();
            _emailTemplateAreaRepository = new ZnodeRepository<ZnodeEmailTemplateArea>();
            _formWidgetConfigurationRepository = new ZnodeRepository<ZnodeCMSFormWidgetConfiguration>();

        }
        

        public override FormSubmitModel CreateFormTemplate(FormSubmitModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            var xmlData = HelperUtility.ToXML<List<FormSubmitAttributeModel>>(model.Attributes);

            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("GlobalEntityValueXml", xmlData, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("FormBuilderId", model.FormBuilderId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("LocaleId", model.LocaleId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("PortalId", model.PortalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("UserId", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);

            var result = objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateFormBuilderGlobalAttributeValue @GlobalEntityValueXml,@FormBuilderId,@LocaleId,@PortalId,@UserId")?.FirstOrDefault();
            ZnodeLogging.LogMessage("result:", ZnodeLogging.Components.Warehouse.ToString(), TraceLevel.Verbose, result);
            ZnodeLogging.LogMessage(Admin_Resources.SuccessFormAttributeSave, string.Empty, TraceLevel.Info);
            //Send notification email
            ZnodeLogging.LogMessage("SendEmailNotification method call with parameters", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.FormBuilderId, model?.CustomerEmail, model.LocaleId, model?.PortalId });
            SendEmailNotification(model,model.FormBuilderId, model.CustomerEmail, model.LocaleId, model.PortalId);
            model.IsSuccess = result?.Status ?? false;

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            return model;
        }
        private void SendEmailNotification(FormSubmitModel formSubmitModel, int formBuilderId, string customerEmail, int localeId, int portalId)
        {
            if (!string.IsNullOrEmpty(customerEmail))
            {
                //Get email detaisl in which email need to send
                FormWidgetEmailConfigurationModel model = FormNotificationSetting(formBuilderId);
                //Sending mail to respective user
                ZnodeLogging.LogMessage("SendEmail method call with parameters", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model, customerEmail, localeId, portalId });
                SendEmail(formSubmitModel, model, customerEmail, localeId, portalId);
            }
        }
        private FormWidgetEmailConfigurationModel FormNotificationSetting(int formBuilderId)
        {
            ZnodeLogging.LogMessage("formBuilderId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { formBuilderId });

            FormWidgetEmailConfigurationModel model = (from frm in _formWidgetEmailConfigurationRepository.Table
                                                       join map in _formWidgetConfigurationRepository.Table on frm.CMSContentPagesId equals map.CMSMappingId
                                                       where map.FormBuilderId == formBuilderId
                                                       select new FormWidgetEmailConfigurationModel
                                                       {
                                                           NotificationEmailId = frm.NotificationEmailId,
                                                           NotificationEmailTemplateId = frm.NotificationEmailTemplateId.Value,
                                                           AcknowledgementEmailTemplateId = frm.AcknowledgementEmailTemplateId.Value
                                                       }).FirstOrDefault() ?? new FormWidgetEmailConfigurationModel();
            ZnodeLogging.LogMessage("FormWidget Email Configuration Id:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, model?.FormWidgetEmailConfigurationId);
            return model;
        }
        private void SendEmail(FormSubmitModel formSubmitModel, FormWidgetEmailConfigurationModel model, string customerEmail, int localeId, int portalId)
        {
            if (!string.IsNullOrEmpty(model.NotificationEmailId))
            {
                ZnodeLogging.LogMessage("SendEmailNotificationEmail method call with parameters", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.NotificationEmailTemplateId, portalId, localeId, model.NotificationEmailId });
                SendEmailNotificationEmailToAdmin(formSubmitModel, model.NotificationEmailTemplateId, portalId, localeId, model.NotificationEmailId);

            }
            if (model.AcknowledgementEmailTemplateId > 0)
            {
                ZnodeLogging.LogMessage("SendEmailNotificationEmail method call with parameters", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.AcknowledgementEmailTemplateId, portalId, localeId, customerEmail });
                SendEmailNotificationEmail(model.AcknowledgementEmailTemplateId, portalId, localeId, customerEmail);
            }
        }
        private void SendEmailNotificationEmail(int notificationEmailTemplateId, int portalId, int localeId, string customerEmail)
        {
            try
            {
                string emailTemplateCode = GetEmailTemplateCode(notificationEmailTemplateId);
                ZnodeLogging.LogMessage("emailTemplateCode returned from GetEmailTemplateCode ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { emailTemplateCode });

                EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(emailTemplateCode, portalId, localeId);

                if (IsNotNull(emailTemplateMapperModel))
                {
                    try
                    {
                        ZnodeEmail.SendEmail(customerEmail, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, string.Empty), emailTemplateMapperModel.Subject, emailTemplateMapperModel.Descriptions, true);
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Customers.ToString(), TraceLevel.Error);
            }
        }

        private void SendEmailNotificationEmailToAdmin(FormSubmitModel formSubmitModel, int notificationEmailTemplateId, int portalId, int localeId, string customerEmail)
        {
            try
            {
                string emailTemplateCode = GetEmailTemplateCode(notificationEmailTemplateId);
                ZnodeLogging.LogMessage("emailTemplateCode returned from GetEmailTemplateCode ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { emailTemplateCode });

                EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(emailTemplateCode, portalId, localeId);
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);
                string messageText = GetEmailTemplate(formSubmitModel, receiptHelper);
                emailTemplateMapperModel.Subject = emailTemplateMapperModel.Subject + " - " + formSubmitModel.CustomerEmail;
                if (IsNotNull(emailTemplateMapperModel))
                {
                    try
                    {
                        ZnodeEmail.SendEmail(customerEmail, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, string.Empty), emailTemplateMapperModel.Subject, messageText, true);
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Customers.ToString(), TraceLevel.Error);
            }
        }

        private string GetEmailTemplateCode(int emailTemplateId)
        {
            ZnodeLogging.LogMessage("emailTemplateId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { emailTemplateId });

            var emailTemplateCode = (from map in _emailTemplateMapperRepository.Table
                                     join area in _emailTemplateAreaRepository.Table on map.EmailTemplateAreasId equals area.EmailTemplateAreasId
                                     where map.EmailTemplateId == emailTemplateId
                                     select (area.Code)).FirstOrDefault();
            ZnodeLogging.LogMessage("Email Template Code:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, emailTemplateCode);
            return Convert.ToString(emailTemplateCode);
        }

        

        private string GetEmailTemplate(FormSubmitModel formSubmitModel, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;

            string storeLogoPath = GetCustomPortalDetails(PortalId)?.StoreLogo;
            Regex rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            foreach (var item in formSubmitModel.Attributes)
            {
                rx1 = new Regex("#"+item.AttributeCode+"#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, item.AttributeValue != null ? item.AttributeValue : string.Empty);
            }
            
            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }
    }
}
