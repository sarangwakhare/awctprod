﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Observer;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model.Cart;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;


namespace Znode.Api.Custom.Service.Service
{
    public class AWCTShoppingCartService : ShoppingCartService
    {

        private readonly IShoppingCartMap _shoppingCartMap;
        private readonly IZnodeRepository<ZnodeOmsSavedCartLineItem> _savedCartLineItemService;
        private readonly IZnodeRepository<ZnodeOmsSavedCart> _omsSavedRepository;
        public int duty = Convert.ToInt32(ConfigurationManager.AppSettings["DutyCanada"]);
        private readonly IZnodeOrderHelper orderHelper;
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;

        public AWCTShoppingCartService()
        {
            publishProductHelper = GetService<IPublishProductHelper>();
            _shoppingCartMap = GetService<IShoppingCartMap>();
            _savedCartLineItemService = new ZnodeRepository<ZnodeOmsSavedCartLineItem>();
            _omsSavedRepository = new ZnodeRepository<ZnodeOmsSavedCart>();
            orderHelper = GetService<IZnodeOrderHelper>();
            _ProductMongoRepository = new MongoRepository<ProductEntity>();

        }
        public override ShoppingCartModel Calculate(ShoppingCartModel shoppingCartModel)
        {

            ShoppingCartModel calculatedModel = null;

            ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
            ProfileModel profile = promotionHelper.GetProfileCache();
            int profileId = 0;
            if (HelperUtility.IsNotNull(profile))
            {
                profileId = profile.ProfileId;
            }

            if (shoppingCartModel.ShippingAddress != null)
            {
                if (shoppingCartModel.ShippingAddress.StateName != null)
                {
                    decimal cost = Convert.ToDecimal(GetShippingRate(shoppingCartModel.ShippingAddress.StateName,
                                                               shoppingCartModel.ShippingAddress.CountryName,
                                                               Convert.ToDecimal(shoppingCartModel.SubTotal)));
                    shoppingCartModel.CustomShippingCost = cost;
                    shoppingCartModel.ShippingCost = cost;
                }
            }
            //It is for Free shipping 
            if (Convert.ToString(shoppingCartModel.Custom1) == "Y")
            {
                shoppingCartModel.CustomShippingCost = 0;
                shoppingCartModel.ShippingCost = 0;
            }
            else if (shoppingCartModel.Custom5 == "Remove" && shoppingCartModel.ShippingCost == 0)
            {

                decimal cost = Convert.ToDecimal(GetShippingRate("BC", "US", 1200));
                shoppingCartModel.CustomShippingCost = cost;
                shoppingCartModel.ShippingCost = cost;

            }

            shoppingCartModel.ProfileId = profileId;
            shoppingCartModel.IsAllowWithOtherPromotionsAndCoupons = DefaultGlobalConfigSettingHelper.IsAllowWithOtherPromotionsAndCoupons;

            #region
            /*Nivi end code for estimated ship date is not changing for the products are added in the cart*/
            string ProductIdliststr = string.Empty;
            if (shoppingCartModel.ShoppingCartItems?.Count > 0)
            {
                ProductIdliststr = String.Join(",", shoppingCartModel.ShoppingCartItems?.Select(x => x.ProductCode)?.ToArray());
            }

            string OmsSavedCartLineItemId = string.Empty;
            string ParentSavedCartLineItemId = string.Empty;
            if (shoppingCartModel.ShoppingCartItems?.Count > 0)
            {
                OmsSavedCartLineItemId = String.Join(",", shoppingCartModel.ShoppingCartItems?.Select(x => x.OmsSavedcartLineItemId)?.ToArray());
                //ParentSavedCartLineItemId = String.Join(",", shoppingCartModel.ShoppingCartItems?.Select(x => x.ParentOmsSavedcartLineItemId)?.Distinct().ToArray());
                //OmsSavedCartLineItemId = OmsSavedCartLineItemId +","+ ParentSavedCartLineItemId;
            }

            AWCTCartModel aWCTCartModel = new AWCTCartModel();
            List<AWCTOutOfStockOptions> _OutOfStockOptionList = GetEstimatedShipDateDataList(ProductIdliststr, OmsSavedCartLineItemId);

            List<string> skulist = new List<string>();
            string[] SimpleProductSKU = ProductIdliststr.Split(',');
            decimal Stock = new decimal();
            foreach (string NewSimpleProductSKU in SimpleProductSKU)
            {
                ZnodeLogging.LogMessage("EstimatedShipDate.Error" + NewSimpleProductSKU, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

                skulist.Add(NewSimpleProductSKU);
                List<InventorySKUModel> model = publishProductHelper.GetInventoryBySKUs(skulist, shoppingCartModel.PortalId);
                Stock = model == null ? 0 : Convert.ToDecimal(model.FirstOrDefault(x => x.SKU == NewSimpleProductSKU)?.Quantity);
                _OutOfStockOptionList.ForEach(x =>
                {
                    if (x.SKU == NewSimpleProductSKU)
                    {
                        try
                        {
                            //x.Quantity = Convert.ToString(Stock);

                            switch (x.OutOfStockOption)
                            {
                                case "DontTrackInventory":

                                    shoppingCartModel.ShoppingCartItems.ForEach(y =>
                                    {
                                        //Stock = Convert.ToInt32(y.QuantityOnHand);
                                        if (Convert.ToInt32(x.NeedDays) > 0)
                                        {
                                            if (y.ProductCode == NewSimpleProductSKU)
                                            {
                                                //string[] Needdays = null;
                                                //Needdays = y.Custom3.Split('-');
                                                //ZnodeLogging.LogMessage("EstimatedShipDate.Error" + NewSimpleProductSKU, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

                                                //y.CartAddOnDetails = Needdays[0];
                                                //string a = shoppingCartModel.ShoppingCartItems.OrderByDescending(z => Convert.ToInt32(z.CartAddOnDetails)).Select(z => z.CartAddOnDetails).FirstOrDefault();
                                                int firstSpaceIndex = y.Custom3.IndexOf("-");
                                                string secondString = y.Custom3.Substring(firstSpaceIndex + 1);
                                                y.Custom3 = Convert.ToInt32(x.NeedDays) + "-" + secondString;
                                            }

                                        }
                                    });



                                    break;
                                case "DisablePurchasing":
                                    //    //If stock is > 0 (Zero) then show esti. date otherwise do not show product
                                    shoppingCartModel.ShoppingCartItems.ForEach(y =>
                                    {
                                        //Stock = Convert.ToInt32(y.QuantityOnHand);
                                        if (Convert.ToInt32(x.NeedDays) > 0 && Stock > 0)
                                        {
                                            if (y.ProductCode == NewSimpleProductSKU)
                                            {
                                                //string[] Needdays = null;
                                                //Needdays = y.Custom3.Split('-');

                                                //y.CartAddOnDetails = Needdays[0];
                                                //string a = shoppingCartModel.ShoppingCartItems.OrderByDescending(z => Convert.ToInt32(z.CartAddOnDetails)).Select(z => z.CartAddOnDetails).FirstOrDefault();
                                                int firstSpaceIndex = y.Custom3.IndexOf("-");
                                                string secondString = y.Custom3.Substring(firstSpaceIndex + 1);
                                                y.Custom3 = Convert.ToInt32(x.NeedDays) + "-" + secondString;
                                            }
                                        }
                                    });

                                    break;
                                case "AllowBackOrdering":

                                    shoppingCartModel.ShoppingCartItems.ForEach(y =>
                                    {
                                        //Stock = Convert.ToInt32(y.QuantityOnHand);

                                        if (Convert.ToInt32(x.OverrideNeedDate) > 0 && Convert.ToInt32(y.Quantity) > Stock)
                                        {

                                            if (y.ProductCode == NewSimpleProductSKU)
                                            {
                                                //string[] OverrideNeedDate = null;
                                                //OverrideNeedDate = y.Custom3.Split('-');

                                                //y.CartAddOnDetails = OverrideNeedDate[0];
                                                //string a = shoppingCartModel.ShoppingCartItems.OrderByDescending(z => Convert.ToInt32(z.CartAddOnDetails)).Select(z => z.CartAddOnDetails).FirstOrDefault();
                                                int firstSpaceIndex = y.Custom3.IndexOf("-");
                                                string secondString = y.Custom3.Substring(firstSpaceIndex + 1);
                                                y.Custom3 = Convert.ToInt32(x.OverrideNeedDate) + "-" + secondString;
                                                ZnodeLogging.LogMessage("EstimatedShipDate.Error" + y.Custom3, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

                                            }
                                        }

                                        else if (Convert.ToInt32(x.NeedDays) > 0)
                                        {
                                            if (y.ProductCode == NewSimpleProductSKU)
                                            {

                                                //string[] Needdays = null;
                                                //Needdays = y.Custom3.Split('-');

                                                //y.CartAddOnDetails = Needdays[0];
                                                //string a = shoppingCartModel.ShoppingCartItems.OrderByDescending(z => Convert.ToInt32(z.CartAddOnDetails)).Select(z => z.CartAddOnDetails).FirstOrDefault();
                                                int firstSpaceIndex = y.Custom3.IndexOf("-");
                                                string secondString = y.Custom3.Substring(firstSpaceIndex + 1);
                                                y.Custom3 = Convert.ToInt32(x.NeedDays) + "-" + secondString;
                                                ZnodeLogging.LogMessage("EstimatedShipDate.Error" + y.Custom3, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

                                            }
                                        }

                                    });

                                    break;
                            }

                        }
                        catch (Exception ex)
                        {
                            ZnodeLogging.LogMessage("EstimatedShipDate.Error" + ex.Message, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

                        }
                    }
                });
            }
            /*Nivi end code for estimated ship date is not changing for the products are added in the cart*/
            #endregion



            /*Nivi start code for chnages of group items changing estimated ship date*/

            if (shoppingCartModel.Shipping.ResponseCode == "D")
            {
                //updated estimate with upper most date
             
                string[] Needdays = null;
         
                shoppingCartModel.ShoppingCartItems.ForEach(x => {
                
                    Needdays = x.Custom3.Split('-');
                 
                    x.CartAddOnDetails = Needdays[0];
                });
                var highestEstimateDay = shoppingCartModel.ShoppingCartItems.OrderByDescending(x => Convert.ToInt32(x.CartAddOnDetails)).Select(x => x.CartAddOnDetails).FirstOrDefault();


                shoppingCartModel.ShoppingCartItems.ForEach(x => {
                    var firstSpaceIndex = x.Custom3.IndexOf("-");
                    var secondString = x.Custom3.Substring(firstSpaceIndex + 1);
                    x.Custom3 = highestEstimateDay + "-" + secondString;
                 
                });

            }
            else if (shoppingCartModel.Shipping.ResponseCode == "S" || shoppingCartModel.Shipping.ResponseCode == "0")
            {
                shoppingCartModel.ShoppingCartItems.ForEach(x => {
                    
                    if (!string.IsNullOrEmpty(x.CartAddOnDetails))
                    {
                        var firstSpaceIndex = x.Custom3.IndexOf("-");
                        var firstString = x.Custom3.Substring(0, firstSpaceIndex); 
                        var secondString = x.Custom3.Substring(firstSpaceIndex + 1);
                        x.Custom3 = x.CartAddOnDetails + "-" + secondString;
                       
                    }
                    


                });

            }
            /*Nivi end code for chnages of group items changing estimated ship date*/

            calculatedModel = base.Calculate(shoppingCartModel);

            if (calculatedModel.ShippingAddress.CountryName == "CA")
            {
                calculatedModel.TotalAdditionalCost = Math.Round(Convert.ToDecimal(calculatedModel.ShoppingCartItems.Where(a => a.Custom2 == "True").Sum(x => (x.Quantity * x.UnitPrice * duty) / 100)), 2);
            }
            //Name to same 
            shoppingCartModel.ShoppingCartItems.Join(calculatedModel.ShoppingCartItems, (s) => s.ConfigurableProductSKUs, (p) => p.ConfigurableProductSKUs, (s, p) =>
            {
                p.ProductName = s.ProductName;
                return p;
            }).ToList();
            DataTable dtDisplayOrder = SetDisplayOrder(GetSavedCartId(Convert.ToInt32(shoppingCartModel.UserId), Convert.ToInt32(shoppingCartModel.PortalId)));

            calculatedModel.ShoppingCartItems.ForEach(x =>
            {
                DataRow[] dr = dtDisplayOrder.Select("SKU='" + x.ConfigurableProductSKUs + "'");
                if (dr.Length > 0)
                {
                    x.Sequence = Convert.ToInt32(dr[0]["DisplayOrder"]);
                }
                /*Nivi Code for API check from ABS*/
                try
                {
                    if (shoppingCartModel.Shipping.ResponseCode == "S" || shoppingCartModel.Shipping.ResponseCode == "D")
                    {
                        x = InventoryApiCheck(x);
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage("Input Parameter:", "Inventory", TraceLevel.Error, ex.Message);
                }
                /*Nivi Code for API check from ABS*/
            });
            //If Customer profile is Sp. customer then set zero shipping cost

            return calculatedModel;
        }
        public override ShippingListModel GetShippingEstimates(string zipCode, ShoppingCartModel model)
        {
            try
            {
                ShippingListModel slist = base.GetShippingEstimates(zipCode, model);
                decimal cost = 0;
                string StateName = "", CountryName = "";
                decimal SubTotal = 0;
                if (slist.ShippingList.Count > 0)
                {
                    try
                    {
                        StateName = string.IsNullOrEmpty(model.ShippingAddress.StateName) ? string.Empty : model.ShippingAddress.StateName;
                        CountryName = string.IsNullOrEmpty(model.ShippingAddress.CountryName) ? string.Empty : model.ShippingAddress.CountryName;
                        SubTotal = Convert.ToDecimal(model.SubTotal);
                        if (model.ShippingAddress != null)
                        {
                            cost = Convert.ToDecimal(GetShippingRate(StateName, CountryName, SubTotal));
                        }
                        //It is for Free shipping 
                        if (Convert.ToString(model.Custom1) == "Y")
                        {
                            model.CustomShippingCost = 0;
                            model.ShippingCost = 0;
                            cost = 0;
                        }
                        else if (model.Custom5 == "Remove" && (model.ShippingCost == 0 || model.ShippingCost == 35))
                        {
                            cost = Convert.ToDecimal(GetShippingRate("BC", "US", 1200));
                            model.CustomShippingCost = cost;
                            model.ShippingCost = cost;

                        }
                    }
                    catch (Exception)
                    {
                        if (model.ShippingAddress != null)
                        {

                            cost = Convert.ToDecimal(GetShippingRate(StateName, CountryName, SubTotal));
                            //model.ShippingCost = cost;
                            //model.OrderLevelShipping += cost;
                        }
                    }
                    slist.ShippingList[0].ShippingRate = cost;

                    //return new ShippingListModel { ShippingList = list };
                    return slist;
                }
                else
                {
                    return new ShippingListModel { ShippingList = new List<ShippingModel>() };
                }
            }
            catch (Exception)
            {
                return new ShippingListModel { ShippingList = new List<ShippingModel>() };
            }

        }
        private string GetShippingRate(string StateName, string CountryName, decimal SubTotal)
        {
            IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();

            objStoredProc.SetParameter("@State", StateName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Country", CountryName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SubTotal", SubTotal, ParameterDirection.Input, DbType.Decimal);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("Nivi_AWCTShippingRate @State,@Country,@SubTotal").Take(1).First();
        }

        private readonly IPublishProductHelper publishProductHelper;

        //public override AddToCartModel AddToCartProduct(AddToCartModel shoppingCart)
        //{

        //    string shoppingCartCustomText = "";
        //    var personaliseAttr = shoppingCart.ShoppingCartItems[0].PersonaliseValuesList.Where(x => x.Key.Contains("filename")).Select(y => y.Value).First()??null;

        //    // shoppingCartCustomText = Convert.ToString(personaliseAttr.Value);
        //    //if (string.IsNullOrEmpty(personaliseAttr.Value.First().ToString()))
        //    //{
        //    //    string[] personalise = personaliseAttr.Value.First().ToString().Split('~');
        //    //    shoppingCartCustomText = personalise[1] ?? null;


        //    //}
        //    shoppingCartCustomText = Convert.ToString(personaliseAttr);
        //    if (!string.IsNullOrEmpty(shoppingCartCustomText))
        //    {
        //        long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        //        string fileName = "UserId_" + shoppingCart.UserId + "_" + milliseconds.ToString() + ".Svg";
        //        SaveCustomSvgImage(shoppingCartCustomText, fileName);
        //    }

        //    base.AddToCartProduct(shoppingCart);

        //    return shoppingCart;
        //}

        public override AddToCartModel AddToCartProduct(AddToCartModel shoppingCart)
        {
            JavaScriptSerializer js = new JavaScriptSerializer()
            { MaxJsonLength = int.MaxValue };

            ///Nivi:added to remove DataURi String from PersonaliseValuesList item and get it to save the svg file

            if (shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Count() > 0)
            {
                KeyValuePair<string, object>? personaliseValue = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("Value"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonFileUriValue = js.Serialize(personaliseValue);
                PersonaliseData personalizeOptionValue = js.Deserialize<PersonaliseData>(jsonFileUriValue);
                personalizeOptionValue.Value = personalizeOptionValue.Value.ToString().Replace("\"", "").Replace(":", "").Replace("Value", "").Replace("Options  ", "");
                //personalizeOptionValue.Value = personalizeOptionValue.Value.ToString().Replace("\"", "").Replace("Value", "");



                KeyValuePair<string, object>? personaliseSku = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("sku"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonPersonaliseSku = js.Serialize(personaliseSku);
                PersonaliseData personaliseClassSku = js.Deserialize<PersonaliseData>(jsonPersonaliseSku);
                personaliseClassSku.Value = personaliseClassSku.Value.ToString().Replace("\"", "").Replace(":", "").Replace("sku", "").Replace("{", "");
                personaliseClassSku.Value = personaliseClassSku.Value.Replace("{PersonaliseItems[{", "").Replace("PersonaliseItems[", "");

                KeyValuePair<string, object>? personaliseFileUriValue = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("FileUriValue"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonSerializeFileUriValue = js.Serialize(personaliseFileUriValue);
                PersonaliseData personalizeClassFileUriValue = js.Deserialize<PersonaliseData>(jsonSerializeFileUriValue);
                personalizeClassFileUriValue.Value = personalizeClassFileUriValue.Value.ToString().Replace("\"", "").Replace(":", "").Replace("Filename", "");

                KeyValuePair<string, object>? personaliseFilename = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("Filename"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonFilename = js.Serialize(personaliseFilename);
                PersonaliseData personaliseClassFilename = js.Deserialize<PersonaliseData>(jsonFilename);
                personaliseClassFilename.Value = personaliseClassFilename.Value.ToString().Replace("\"", "").Replace(":", "").Replace("Filename", "").Replace("}]", "");

                shoppingCart.ShoppingCartItems.ToList().ForEach(s =>
                {
                    s.PersonaliseValuesList.Remove("Value");
                    s.PersonaliseValuesList.Remove("sku");
                    s.PersonaliseValuesList.Remove("Filename");
                    s.PersonaliseValuesList.Remove("FileUriValue");

                });

                string SkuString1 = personaliseClassSku.Value.ToString().Replace("\"", "").Replace("PersonaliseItems", "").Replace("{:[{", "").Replace("sku:", "");

                HashSet<string> validNames = new HashSet<string>(SkuString1.Split('|'));
                shoppingCart.ShoppingCartItems.Where(x => validNames.Contains(x.ConfigurableProductSKUs)).ToList().ForEach(s =>
                {
                    s.PersonaliseValuesList.Add("OptionValue", personalizeOptionValue.Value);
                    s.PersonaliseValuesList.Add("sku", personaliseClassSku.Value);
                    s.PersonaliseValuesList.Add("Filename", personaliseClassFilename.Value);
                });
                ///Nivi:End


                personalizeClassFileUriValue.Value = personalizeClassFileUriValue.Value.ToString().Replace("\"", "").Replace("FileUriValue", "");
                string shoppingCartCustomText = personalizeClassFileUriValue.Value;
                if (!string.IsNullOrEmpty(shoppingCartCustomText))
                {
                    SaveCustomSvgImage(shoppingCartCustomText, personaliseClassFilename.Value.ToString());
                }
            }
            //999999347725-Dance|999999347732-Dance|
            string cartsku = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom1);
            cartsku = cartsku.Substring(2, cartsku.Length - 4);
            string[] skus = cartsku.Split('|');

            string awctcatalogName = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom2);
            awctcatalogName = awctcatalogName.Substring(2, awctcatalogName.Length - 4);
            string[] awctcatalogNames = awctcatalogName.Split('|');

            string Isdutiable = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom3);
            Isdutiable = Isdutiable.Substring(2, Isdutiable.Length - 4);
            string[] Isdutiables = Isdutiable.Split('|');

            string date = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom4);
            date = date.Substring(2, date.Length - 4);
            string[] dates = date.Split('|');

            string UploadFileName = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom5);
            UploadFileName = UploadFileName.Substring(2, UploadFileName.Length - 4);


            foreach (ShoppingCartItemModel cartItems in shoppingCart?.ShoppingCartItems)
            {
                int icount = 0;
                foreach (string sku in skus)
                {
                    if (cartItems.ConfigurableProductSKUs == sku)
                    {
                        cartItems.Custom1 = awctcatalogNames[icount];
                        cartItems.Custom2 = Isdutiables[icount];
                        cartItems.Custom3 = dates[icount];
                        cartItems.CustomText = cartItems.Custom1;
                        cartItems.Custom5 = UploadFileName;

                    }
                    icount = icount + 1;
                }
            }

            AddToCartModel cartModel = base.AddToCartProduct(shoppingCart);
            DataTable dtDisplayOrder = SetDisplayOrder(GetSavedCartId(Convert.ToInt32(shoppingCart.UserId), Convert.ToInt32(shoppingCart.PortalId)));
            //Set to Sequence 
            //cartModel.ShoppingCartItems.Join(lineItems, (s) => s.ConfigurableProductSKUs, (p) => p.SKU, (s, p) =>
            //{
            //    s.Sequence = p.Sequence;
            //    return p;
            //}).ToList();
            cartModel.ShoppingCartItems.ForEach(x =>
            {
                DataRow[] dr = dtDisplayOrder.Select("SKU='" + x.ConfigurableProductSKUs + "'");
                if (dr.Length > 0)
                {
                    x.Sequence = Convert.ToInt32(dr[0]["DisplayOrder"]);
                }
            });
            return cartModel;
        }
        #region For cart Item sequence changes
        private int GetSavedCartId(int userId, int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info, new { userId = userId, portalId = portalId });

            int cookieMappingId = orderHelper.GetCookieMappingId(userId, portalId);
            int newSavedCartId = orderHelper.GetSavedCartId(cookieMappingId);
            return newSavedCartId;
        }
        private DataTable SetDisplayOrder(int OMSSavedCartId)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@OMSSaveCartId", OMSSavedCartId, ParameterDirection.Input, SqlDbType.VarChar);
            DataSet data = objStoredProc.GetSPResultInDataSet("AWCT_CartCheckoutDisplayOrder");
            DataTable sizeDisplayOrderTable = data.Tables[0];

            return sizeDisplayOrderTable;

        }

        #endregion

        //To get ShoppingCart by cookieId 
        //public override ShoppingCartModel GetShoppingCartDetails(CartParameterModel cartParameterModel, ShoppingCartModel cartModel = null)
        //{
        //    ZnodeShoppingCart shopping = new ZnodeShoppingCart();
        //    ZnodeShoppingCart znodeShoppingCart = shopping.LoadFromDatabase(cartParameterModel);
        //    znodeShoppingCart.IsAllowWithOtherPromotionsAndCoupons = DefaultGlobalConfigSettingHelper.IsAllowWithOtherPromotionsAndCoupons;
        //    //to map cart model to znode shopping cart  
        //    BindCartModel(cartModel, znodeShoppingCart);

        //    //Map Libraries.ECommerce.ShoppingCart to ShoppingCartModel.
        //    ShoppingCartModel shoppingCartModel = Engine.Services.Maps.ShoppingCartMap.ToModel(znodeShoppingCart);

        //    //Get coupons if already applied.
        //    if (cartModel?.Coupons?.Count > 0)
        //        shoppingCartModel.Coupons = cartModel.Coupons;

        //    if (HelperUtility.IsNotNull(cartParameterModel.ShippingId))
        //    {
        //        IZnodeRepository<ZnodeShipping> _shippingRepository = new ZnodeRepository<ZnodeShipping>();

        //        //Check if Shipping is null or not,If null then get the shipping 
        //        //on the basis of ShippingId form ShoppingCartModel.
        //        ZnodeShipping shipping = _shippingRepository.Table.Where(x => x.ShippingId == cartParameterModel.ShippingId)?.FirstOrDefault();
        //        if (HelperUtility.IsNotNull(shipping))
        //        {
        //            shoppingCartModel.Shipping = new OrderShippingModel
        //            {
        //                ShippingId = shipping.ShippingId,
        //                ShippingDiscountDescription = shipping.Description,
        //                ShippingCountryCode = string.IsNullOrEmpty(cartParameterModel.ShippingCountryCode) ? string.Empty : cartParameterModel.ShippingCountryCode
        //            };
        //        }
        //    }

        //    //Check product inventory of the product for all type of product in cartline item.
        //    if (HelperUtility.IsNotNull(shoppingCartModel?.ShoppingCartItems))
        //    {
        //        if (cartModel?.ShoppingCartItems?.Count > 0)
        //        {
        //            shoppingCartModel.ShoppingCartItems.ForEach(cartItem =>
        //            {
        //                var lineItem = cartModel.ShoppingCartItems
        //                            .Where(cartLineItem => cartLineItem.SKU == cartItem.SKU && cartLineItem.AddOnProductSKUs == cartItem.AddOnProductSKUs)?.FirstOrDefault();
        //                cartItem.OmsOrderLineItemsId = (lineItem?.OmsOrderLineItemsId).GetValueOrDefault();
        //                cartItem.CartDescription = string.IsNullOrEmpty(cartItem.CartDescription) ? lineItem?.CartDescription : cartItem.CartDescription;

        //            });
        //        }

        //        //Single call instead multiple inventory check.
        //        CheckBaglineItemInventory(shoppingCartModel, cartParameterModel);
        //    }

        //    //Bind cookieMappingId, PortalId, LocaleId, CatalogId, UserId.
        //    BindCartData(shoppingCartModel, cartParameterModel);

        //    if (cartModel?.OmsOrderId > 0)
        //        shoppingCartModel.BillingAddress = cartModel.BillingAddress;

        //    return shoppingCartModel;
        //}
        private void SaveCustomSvgImage(string svgDataUri, string fileName)
        {
            try
            {
                svgDataUri = svgDataUri.Replace("$$", ";");
                string path;
                string base64Data = Regex.Match(svgDataUri, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                byte[] binData = Convert.FromBase64String(svgDataUri);
                path = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["SVGPath"].ToString();
                fileName = Path.Combine(path, fileName);
                if (!File.Exists(fileName))
                {

                    File.WriteAllBytes(fileName, binData);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("SaveSVGImage.Error" + ex.Message, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
        }

        public Dictionary<string, object> GetPersonalisedValueCartLineItem(int savedCartLineItemId, int localeId)
        {
            IZnodeRepository<ZnodeOmsPersonalizeCartItem> _savedOmsPersonalizeCartItemRepository = new ZnodeRepository<ZnodeOmsPersonalizeCartItem>();

            IZnodeRepository<ZnodeOmsSavedCartLineItem> _omsSavedCartLineItem = new ZnodeRepository<ZnodeOmsSavedCartLineItem>();
            int OmsSavedCartLineItemId = (from p in _omsSavedCartLineItem.Table
                                          where p.ParentOmsSavedCartLineItemId == savedCartLineItemId
                                          select p.OmsSavedCartLineItemId).FirstOrDefault();
            int iSavedCartLineItemId = Convert.ToInt32(OmsSavedCartLineItemId);
            Dictionary<string, object> personaliseItem = new Dictionary<string, object>();
            foreach (KeyValuePair<string, string> personaliseAttr in _savedOmsPersonalizeCartItemRepository.Table.Where(x => x.OmsSavedCartLineItemId == iSavedCartLineItemId)?.ToDictionary(x => x.PersonalizeCode, x => x.PersonalizeValue))
            {
                personaliseItem.Add(personaliseAttr.Key, (object)personaliseAttr.Value);
            }

            return personaliseItem;
        }
        #region To check product inventory 9.2.1.1
        ////To check product inventory 9.2.1.1
        //public override void CheckBaglineItemInventory(ShoppingCartModel shoppingCartModel, CartParameterModel cartParameterModel)
        //{
        //    ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    List<ShoppingCartItemModel> shoppingCartItemList = HelperUtility.IsNotNull(shoppingCartModel) ? shoppingCartModel.ShoppingCartItems : new List<ShoppingCartItemModel>();
        //    ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { shoppingCartItemListCount = shoppingCartItemList });

        //    string allSku = string.Empty;
        //    shoppingCartItemList.ForEach(shoppingCartItem =>
        //    {
        //        List<string> skus = new List<string>();
        //        List<string> productSkus = new List<string>();
        //        //Get all associated skus of items.
        //        GetAllItemsSku(shoppingCartItem, productSkus, out allSku);
        //        skus.AddRange(productSkus);
        //        skus = skus?.Distinct().ToList();
        //        //Get the inventory list of skus.
        //        List<InventorySKUModel> inventoryList = skus.Count > 0 ? publishProductHelper.GetInventoryBySKUs(skus, shoppingCartModel.PortalId) : new List<InventorySKUModel>();
        //        ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { skus = skus });
        //        //Get inventory setting details from publish products.
        //        List<ProductEntity> invertorySetting = new List<ProductEntity>();
        //        foreach (var item in skus)
        //        {
        //            ProductEntity Products = publishProductHelper.GetPublishProductBySKUs(item, cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId, GetCatalogVersionId(cartParameterModel.PublishedCatalogId)).FirstOrDefault();
        //            invertorySetting.Add(Products);
        //        }
        //        decimal selectedQuantity = new decimal();

        //        //Get insufficient Quantity Count of current cart item.
        //        int insufficientQuantityCount = GetCartInsufficientQuantityCount(shoppingCartItem, skus, ref selectedQuantity, inventoryList, invertorySetting);

        //        //if insufficient Quantity Count is greateer then 0 then current cart is out of stock.
        //        if (insufficientQuantityCount > 0)
        //            shoppingCartItem.InsufficientQuantity = true;
        //    });
        //    ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //}
        #endregion

        //Commented as Cart is showing qty error 
        //Get insufficient Quantity Count of current cart item.
        //To check product inventory 9.1.0.9
        //public override void CheckBaglineItemInventory(ShoppingCartModel shoppingCartModel, CartParameterModel cartParameterModel)
        //{

        //    List<ShoppingCartItemModel> shoppingCartItemList = HelperUtility.IsNotNull(shoppingCartModel) ? shoppingCartModel.ShoppingCartItems : new List<ShoppingCartItemModel>();

        //    List<string> skus = new List<string>();
        //    string allSku = string.Empty;

        //    shoppingCartItemList.ForEach(shoppingCartItem =>
        //    {

        //        List<string> productSkus = new List<string>();
        //        //Get all associated skus of items.
        //        GetAllItemsSku(shoppingCartItem, productSkus, out allSku);
        //        skus.AddRange(productSkus);
        //        skus = skus?.Distinct().ToList();
        //    });

        //    //Get the inventory list of skus.
        //    List<InventorySKUModel> inventoryList = skus.Count > 0 ? publishProductHelper.GetInventoryBySKUs(skus, shoppingCartModel.PortalId) : new List<InventorySKUModel>();

        //    //Get inventory setting details from publish products.
        //    List<ProductEntity> invertorySetting = publishProductHelper.GetPublishProductBySKUs(allSku, cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId, GetCatalogVersionId(cartParameterModel.PublishedCatalogId));

        //    shoppingCartItemList.ForEach(shoppingCartItem =>
        //    {

        //        decimal selectedQuantity = shoppingCartItem.Quantity;

        //        //Get insufficient Quantity Count of current cart item.
        //        int insufficientQuantityCount = GetCartInsufficientQuantityCount(shoppingCartItem, skus, ref selectedQuantity, inventoryList, invertorySetting);

        //        //if insufficient Quantity Count is greateer then 0 then current cart is out of stock.
        //        if (insufficientQuantityCount > 0)
        //        {
        //            shoppingCartItem.InsufficientQuantity = true;
        //        }
        //    });
        //}

        private int GetCartInsufficientQuantityCount(ShoppingCartItemModel shoppingCartItem, List<string> skus, ref decimal selectedQuantity, List<InventorySKUModel> inventoryList, List<ProductEntity> invertorySetting)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            int insufficientQuantityCount = 0;
            foreach (ProductEntity products in invertorySetting)
            {

                //Check for group product inventory.
                if (shoppingCartItem?.GroupProducts?.Count > 0)
                {

                    foreach (AssociatedProductModel associatedProduct in shoppingCartItem.GroupProducts)
                    {
                        if (string.Equals(associatedProduct.Sku, products.SKU))
                        {
                            selectedQuantity = associatedProduct.Quantity;

                            //Get insufficient Quantity Count of current item.
                            insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryList, insufficientQuantityCount, products);
                        }
                    }
                }
                //Check for Configurable product inventory.
                else if (!string.IsNullOrEmpty(shoppingCartItem?.ConfigurableProductSKUs))
                {
                    List<InventorySKUModel> inventoryListForConfigProd = inventoryList.Where(w => w.SKU == shoppingCartItem.ConfigurableProductSKUs).ToList();
                    if (string.Equals(products.SKU, shoppingCartItem.ConfigurableProductSKUs))
                    {
                        insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryListForConfigProd, insufficientQuantityCount, products);
                    }
                }
                //Check for Bundle product inventory.
                else if (!string.IsNullOrEmpty(shoppingCartItem?.BundleProductSKUs))
                {
                    foreach (string BundleSku in skus)
                    {
                        if (string.Equals(products.SKU, BundleSku))
                        {
                            List<InventorySKUModel> inventoryListForBundleProd = inventoryList.Where(w => w.SKU == BundleSku).ToList();
                            insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryListForBundleProd, insufficientQuantityCount, products);

                        }
                    }
                }
                //Check for Simple product inventory.
                else
                {
                    //Get insufficient Quantity Count of current item.
                    insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryList, insufficientQuantityCount, products);
                }
            }
            ZnodeLogging.LogMessage("Insufficient quantity count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, insufficientQuantityCount);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return insufficientQuantityCount;
        }
        public override bool RemoveSavedCartLineItem(int omsSavedCartLineItemId)
        {
            string newCustomText = string.Empty; int addOmsSavedCartLineItemId = 0;
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { omsSavedCartLineItemId = omsSavedCartLineItemId });
            bool status = false;
            if (omsSavedCartLineItemId > 0)
            {
                List<ZnodeOmsSavedCartLineItem> savedCartLineItems = _savedCartLineItemService.Table.Where(o => o.OmsSavedCartLineItemId == omsSavedCartLineItemId && o.ParentOmsSavedCartLineItemId != null)
                                                                      ?.ToList();

                int omsSavedCartId = savedCartLineItems.Count > 0 ? savedCartLineItems.FirstOrDefault().OmsSavedCartId : 0;
                //Get parent ids
                List<int?> parentIds = _savedCartLineItemService.Table.Where(o => o.OmsSavedCartLineItemId == omsSavedCartLineItemId && o.ParentOmsSavedCartLineItemId != null)
                                                                      ?.Select(o => o.ParentOmsSavedCartLineItemId)
                                                                      ?.ToList();
                ZnodeLogging.LogMessage("parentIds:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, parentIds);

                //Remove personalised attribute
                RemovePersonlisedAttribute(parentIds, omsSavedCartLineItemId);

                //Get child line item count
                int childCount = parentIds.Join(_savedCartLineItemService.Table,
                                          o => o,
                                          ob => ob.ParentOmsSavedCartLineItemId,
                                          (o, ob) => o)
                                          .Count();
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { childCount = childCount });


                //GEt line items to delete, if there is only one child then remove parent line as well otherwise do not delete the parent.
                IQueryable<ZnodeOmsSavedCartLineItem> lineItems = childCount > 1 ? _savedCartLineItemService.Table.Where(o => o.OmsSavedCartLineItemId == omsSavedCartLineItemId || o.ParentOmsSavedCartLineItemId == omsSavedCartLineItemId)
                                           : _savedCartLineItemService.Table.Where(o => o.OmsSavedCartLineItemId == omsSavedCartLineItemId || o.ParentOmsSavedCartLineItemId == omsSavedCartLineItemId || (parentIds.Contains(o.OmsSavedCartLineItemId)));
                //AddOn Changes
                string sku = savedCartLineItems[0].SKU;
                List<ZnodeOmsSavedCartLineItem> CartLineItemsWithCustomText = _savedCartLineItemService.Table.Where(o => o.OmsSavedCartId == omsSavedCartId && o.CustomText != null).ToList();
                foreach (ZnodeOmsSavedCartLineItem item in CartLineItemsWithCustomText)
                {
                    string customText = item.CustomText;
                    if (!customText.Contains(sku))
                    {
                        continue;
                    }
                    if (customText != string.Empty)
                    {
                        //999998174209,999998174209
                        string[] AddOn = customText.Split(',');
                        int iCount = AddOn.Where(x => x == sku).Count();
                        if (iCount > 1)
                        {
                            int imatch = 0;
                            foreach (string text in AddOn)
                            {
                                if (text == sku && imatch == 0)
                                {
                                    imatch = 1;
                                }
                                else
                                {
                                    newCustomText = newCustomText + text + ",";
                                }
                            }

                            newCustomText = newCustomText.Substring(0, newCustomText.Length - 1);
                        }
                        else
                        {
                            AddOn = AddOn.Where(w => w != sku).ToArray();
                            newCustomText = string.Join(",", AddOn);
                        }

                        item.CustomText = newCustomText;
                        item.Quantity = item.Quantity - 1;
                        if (newCustomText == string.Empty)
                        {
                            addOmsSavedCartLineItemId = item.OmsSavedCartLineItemId;
                        }
                        _savedCartLineItemService.Update(item);
                    }
                }

                ZnodeLogging.LogMessage("Removing line item :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, lineItems);
                status = _savedCartLineItemService.Delete(lineItems);

                if (status && omsSavedCartId > 0)
                {
                    ZnodeOmsSavedCart savedCart = _omsSavedRepository.Table.FirstOrDefault(x => x.OmsSavedCartId == omsSavedCartId);
                    savedCart.ModifiedDate = DateTime.Now;
                    _omsSavedRepository.Update(savedCart);
                }
                if (addOmsSavedCartLineItemId > 0)
                {
                    RemoveSavedCartLineItem(addOmsSavedCartLineItemId);
                }
                ZnodeLogging.LogMessage(status ? "Saved cart line item deleted successfully." : "Failed to delete saved cart line item.", string.Empty, TraceLevel.Info);
            }
            return status;
        }

        public override bool RemoveSavedCartItems(int? userId, int? cookieMappingId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { userId = userId, cookieMappingId = cookieMappingId });
            bool status = false;
            //To delete saved cart line items
            status = DeleteSavedCartItems(userId.GetValueOrDefault(), cookieMappingId.GetValueOrDefault());
            ZnodeLogging.LogMessage(status ? Admin_Resources.SuccessSavedCartLineItemDelete : Admin_Resources.ErrorSavedCartLineItemDelete, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return status;
        }
        //Get PortalId from SiteConfig
        public static int GetPortalId()
        {
            string domainName = HelperUtility.GetPortalDomainName();
            return ZnodeConfigManager.GetSiteConfig(domainName)?.PortalId ?? 0;
        }

        private bool DeleteSavedCartItems(int userId, int cookieMappingId)
        {
            IZnodeViewRepository<SEODetailsModel> objStoredProc = new ZnodeViewRepository<SEODetailsModel>();
            objStoredProc.SetParameter("@OmsCookieMappingId", cookieMappingId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Status", null, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", GetPortalId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.ExecuteStoredProcedureList("Znode_DeleteSavedCartItem @OmsCookieMappingId,@UserId,@Status OUT,@PortalId", 2, out int status);
            return status == 1;
        }
        //if any item from cart is out of stock so it return total no of item which is "out stock item".
        public override int IsItemOutOfStock(ShoppingCartItemModel shoppingCartItem, List<string> skus, decimal selectedQuantity, List<InventorySKUModel> inventoryList, int insufficientQuantity, ProductEntity products)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            selectedQuantity = shoppingCartItem.Quantity;
            //Get inventory setting for product.
            AWCTPublishProductService _service = new AWCTPublishProductService();
            DataTable outOfStockOptionsDetails = _service.GetOutOfStockOptionsDetails(products.ZnodeProductId.ToString());
            DataRow[] dr1 = outOfStockOptionsDetails.Select("ProductId=" + products.ZnodeProductId);
            List<SelectValuesEntity> inventorySettingList = new List<SelectValuesEntity>();
            string inventorySetting = string.Empty;
            if (dr1.Length > 0)
            {
                inventorySetting = Convert.ToString(dr1[0]["Option"]);
            }
            else
            {
                inventorySettingList = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues;
                inventorySetting = inventorySettingList?.Count > 0 ? inventorySettingList.FirstOrDefault().Code : ZnodeConstant.DontTrackInventory;
            }
            string minimumQuantity = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.MinimumQuantity)?.AttributeValues;
            string maximumQuantity = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.MaximumQuantity)?.AttributeValues;
            List<string> sku = skus.Where(m => m == products.SKU).ToList();
            ZnodeLogging.LogMessage("Parameter :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { minimumQuantity = minimumQuantity, maximumQuantity = maximumQuantity, InventorySetting = inventorySetting });

            switch (inventorySetting)
            {
                case ZnodeConstant.DisablePurchasing:
                    shoppingCartItem.InsufficientQuantity = IsInsufficientQuantity(sku, selectedQuantity, inventoryList);
                    if (shoppingCartItem.InsufficientQuantity)
                    {
                        insufficientQuantity++;
                    }

                    break;

                case ZnodeConstant.AllowBackOrdering:
                    shoppingCartItem.InsufficientQuantity = false;
                    break;

                case ZnodeConstant.DontTrackInventory:
                    shoppingCartItem.InsufficientQuantity = false;
                    break;

                default:
                    //Between true if want to include min and max number in comparison.
                    shoppingCartItem.InsufficientQuantity = string.IsNullOrEmpty(minimumQuantity) ? false : !HelperUtility.Between(Convert.ToDecimal(shoppingCartItem.Quantity), Convert.ToDecimal(minimumQuantity), Convert.ToDecimal(maximumQuantity), true);
                    if (shoppingCartItem.InsufficientQuantity)
                    {
                        insufficientQuantity++;
                    }

                    break;
            }
            ZnodeLogging.LogMessage("Insufficient Quantity:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, insufficientQuantity);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            return insufficientQuantity;
        }
        /// <summary>
        /// API check from ABS
        /// </summary>
        /// <param name="shoppingCartItemModel"></param>
        /// <returns></returns>
        /*Nivi Code for API check from ABS*/
        public ShoppingCartItemModel InventoryApiCheck(ShoppingCartItemModel shoppingCartItemModel)
        {
            List<ProductEntity> products = new List<ProductEntity>();

            /*Nivi code start for inventory check from ABS*/
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.SKU.ToString(), FilterOperators.Equals, Convert.ToString(shoppingCartItemModel.ProductCode));
            products = _ProductMongoRepository?.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));

            var productsLatestVersion = products?.OrderByDescending(y => y.VersionId)?.FirstOrDefault();

            if (!string.IsNullOrEmpty(productsLatestVersion?.SKU))
            {
                var OutOfstockOptions = products?.FirstOrDefault()?.Attributes?.Where(y => y.AttributeCode == "OutOfStockOptions").Select(y => y.SelectValues.FirstOrDefault().Code)?.FirstOrDefault();
                if (OutOfstockOptions == "DisablePurchasing")
                {
                    InventorySKUModel inventoryRequest = new InventorySKUModel
                    {
                        SKU = shoppingCartItemModel.ProductCode
                    };
                    InventorySKUModel inventoryResponse = new InventorySKUModel();
                    try
                    {
                        ERPInitializer<InventorySKUModel> _erpInc = new ERPInitializer<InventorySKUModel>(inventoryRequest, "InventorySyncOnCheckout");
                        inventoryResponse = (InventorySKUModel)_erpInc?.Result;
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage("InventoryApiCheck: Error ", "Inventory", TraceLevel.Error, ex.Message);



                    }
                    if (inventoryResponse != null)
                    {

                        if (shoppingCartItemModel.Quantity > inventoryResponse.Quantity)
                        {
                            shoppingCartItemModel.QuantityOnHand = inventoryResponse.Quantity;
                            shoppingCartItemModel.InsufficientQuantity = true;
                        }
                    }
                }
            }
            products = null;
            /*Nivi code end for inventory check from ABS*/
            return shoppingCartItemModel;
        }
        ///*Nivi Code To Get estimated ship date data*/

        private List<AWCTOutOfStockOptions> GetEstimatedShipDateDataList(string ProductIdliststr, string OmsSavedCartLineItemId)
        {
            IZnodeViewRepository<AWCTOutOfStockOptions> objStoredProc = new ZnodeViewRepository<AWCTOutOfStockOptions>();
            objStoredProc.SetParameter("@productIdliststr", ProductIdliststr, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@OmsSavedCartLineItemId", OmsSavedCartLineItemId, ParameterDirection.Input, DbType.String);
            //Data on the basis of product skus and product ids
            var aaaa = objStoredProc.ExecuteStoredProcedureList("AWCT_CartGetOutOfStockOptionDetails @productIdliststr,@OmsSavedCartLineItemId").ToList();
            return aaaa;
        }

        ///*Nivi Code To Get estimated ship date data*/
    }
    public class PersonaliseData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
