﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomProductModel;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTPublishProductService : PublishProductService, IAWCTPublishProductService
    {
        #region Private Variables
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        private PublishProductHelper publishProductHelper = new PublishProductHelper();
        private IMongoRepository<ConfigurableProductEntity> _configurableproductRepository;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly ISEOService _seoService;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository;

        #endregion

        #region Constructor
        public AWCTPublishProductService()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>();
            _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            _seoService = new SEOService();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
        }
        #endregion

        #region Public Method
        public ConfigurableProductViewModel GetPriceSizeList(int configurableProductId)
        {
            ConfigurableProductViewModel configurableProduct = new ConfigurableProductViewModel();
            List<PriceSizeGroup> _priceSizeList = GetPriceSizeGrouping(configurableProductId);
            configurableProduct.PriceSizeList = _priceSizeList;
            return configurableProduct;
        }

        public new AWCTPublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
          
            try
            {
                GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);

                //Remove portal id filter.
                filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

                //Replace filter keys.
                ReplaceFilterKeys(ref filters);

                //get catalog current version id by catalog id.
                int? catalogVersionId = GetCatalogVersionId(catalogId);
              //  ZnodeLogging.LogMessage("publishProductId" + Convert.ToString(publishProductId), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Error);

               // ZnodeLogging.LogMessage("catalogVersionId" + Convert.ToString(catalogVersionId), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Error);

                filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(catalogVersionId));
                filters.Add(WebStoreEnum.ZnodeCategoryId.ToString(), FilterOperators.NotEquals, Convert.ToString(0));
                AWCTPublishProductModel publishProduct = null;
                //Get publish product from mongo
                List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));

                List<int> associatedCategoryIds = new List<int>();
                
                if (HelperUtility.IsNotNull(products))
                {
                   
                    foreach (ProductEntity product in products)
                    {
                       
                        CategoryEntity category = _categoryMongoRepository.GetEntity(MongoQueryHelper.GenerateDynamicWhereClause(new FilterMongoCollection() { new FilterMongoTuple(FilterKeys.ZnodeCategoryId, FilterOperators.Equals, product.ZnodeCategoryIds.ToString()) }));
                        if ((category?.IsActive).GetValueOrDefault())
                        {
                            publishProduct = product.ToModel<AWCTPublishProductModel>();
                            if (publishProduct.PublishProductId == 0)
                            {
                                publishProduct.PublishProductId = product.ZnodeProductId;
                            }

                            associatedCategoryIds.Add(category.ZnodeCategoryId);
                        }
                    }
                }
                
                if (HelperUtility.IsNotNull(publishProduct))
                {
                    List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(publishProductId, catalogVersionId);
                    //Get associated configurable product list.
                    List<ProductEntity> associatedProducts = publishProductHelper.GetAssociatedProducts(publishProductId, localeId, catalogVersionId, configEntiy);

                    if (associatedProducts?.Count > 0)
                    {
                        publishProduct = GetDefaultConfiurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy.FirstOrDefault().ConfigurableAttributeCodes);
                    }
                    else
                    {
                        //Get expands associated to Product
                        publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault());
                    }

                    GetProductImagePath(portalId, publishProduct);
                    // publishProduct = SetAttributeList(associatedProducts, publishProduct);
                    publishProduct.CategoryHierarchy = GetProductCategory(string.Join(",", associatedCategoryIds), localeId, catalogId, _seoService.GetPublishSEOSettingList("Category", portalId, localeId));

                    //set stored based In Stock, Out Of Stock, Back Order Message.
                    SetPortalBasedDetails(portalId, publishProduct);
                    publishProduct.ZnodeProductCategoryIds = associatedCategoryIds;

                   
                    //publishProduct = SetAttributeList(associatedProducts, publishProduct);
                    publishProduct = GetPDPColorSizeGridData(associatedProducts, portalId, publishProduct);
                    if (publishProduct.AddOns != null && publishProduct.AddOns.Count > 0)
                    {
                        publishProduct = GetAddOnGridData(publishProduct.AddOns[0], portalId, publishProduct);
                    }
                    
                    publishProduct.ConfigurableProductName = products[0].Name;
                    //string revisionType = products[0].revisionType;
                    //if (revisionType.ToProperCase() == "Preview")
                    //{
                    //    products.Where(x => (x.Attributes.Where(y => y.AttributeCode == "ProductType").Select(p => p.SelectValues[0]).ToString() == "ConfigurableProduct")).ToList();
                    //}


                    AssignConfigGalImagesToSimpleProduct(products, publishProduct, portalId);
                   
                }
                return publishProduct;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("ConfigurableAttributes" + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
                throw ex;
            }


        }

        public AWCTPublishProductModel GetExtendedProductDetails(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);

            //get catalog current version id by catalog id.
            int? catalogVersionId = GetCatalogVersionId(catalogId);

            PublishProductModel publishProduct = GetPublishedProductFromMongo(publishProductId, filters);
            AWCTPublishProductModel AWCTpublishProduct = new AWCTPublishProductModel();


            if (HelperUtility.IsNotNull(publishProduct))
            {
                List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(publishProductId, catalogVersionId);
                //Get associated configurable product list.
                List<ProductEntity> associatedProducts = publishProductHelper.GetAssociatedProducts(publishProductId, localeId, catalogVersionId, configEntiy);

                if (associatedProducts?.Count > 0)
                {
                    publishProduct = GetDefaultConfiurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy?.SelectMany(x => x.ConfigurableAttributeCodes).Distinct()?.ToList(), catalogVersionId.GetValueOrDefault());
                }
                else
                {
                    //Get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault());
                }

                GetProductImagePath(portalId, publishProduct, false);

                SetPortalBasedDetails(portalId, publishProduct);
                AWCTpublishProduct = AutoMapper.Mapper.Map<AWCTPublishProductModel>(publishProduct);
                publishProduct = GetPDPColorSizeGridData(associatedProducts, portalId, AWCTpublishProduct);
                AssignConfigGalImagesToSimpleProduct(publishProduct, AWCTpublishProduct, portalId);
            }
            return AWCTpublishProduct;
        }
        private AWCTPublishProductModel GetPDPColorSizeGridData(List<ProductEntity> associatedProducts, int portalId, AWCTPublishProductModel publishProduct)
        {
           
            try
            {
                if (publishProduct != null)
                {
                    List<string> skulist = new List<string>();

                    foreach (ProductEntity pe in associatedProducts)
                    {
                        string sku = pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductSKU)?.AttributeValues;
                        skulist.Add(sku);
                    }
                    List<InventorySKUModel> model = publishProductHelper.GetInventoryBySKUs(skulist, portalId);
                    if (model == null)
                    {
                        ZnodeLogging.LogMessage("model is null in GetPDPColorSizeGridData", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    }
                    List<PriceSKUModel> priceModel = publishProductHelper.GetPricingBySKUs(skulist, portalId, 0);
                    if (priceModel == null)
                    {
                        ZnodeLogging.LogMessage("priceModel is null in GetPDPColorSizeGridData", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    }

                    int index = 0;
                    string _swatchImagePath = "";
                    string _zoomImagePath = "";
                    string _imagePath = "";
                    if (publishProduct.ImageThumbNailPath == null)
                    {
                        ImageHelper image = new ImageHelper(portalId);
                        string ImageMediumPath = image.GetImageHttpPathMedium("image");
                        string OriginalImagepath = image.GetOriginalImagepath("image");
                        string ImageLargePath = image.GetImageHttpPathLarge("image");
                        //ImageMediumPath=http://localhost:44762/Data/Media/Catalog/7/700/
                        index = ImageMediumPath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _imagePath = ImageMediumPath.Substring(0, index) + "/";
                        }
                        //OriginalImagepath = http://localhost:44762/Data/Media/

                        index = OriginalImagepath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
                        }
                        //ImageLargePath=http://localhost:44762/Data/Media/Catalog/7/1000/
                        /* search for images 1000 and add ZoomImage Prorty in ProductDetailGridModel and ProductDetailGridViewModel and asign it in below manner*/
                        index = ImageLargePath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _zoomImagePath = ImageLargePath.Substring(0, index) + "/";
                        }
                        /* */
                    }
                    else
                    {
                        //ImageMediumPath=http://localhost:44762/Data/Media/Catalog/7/700/
                        index = publishProduct.ImageMediumPath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _imagePath = publishProduct.ImageMediumPath.Substring(0, index) + "/";
                        }

                        //OriginalImagepath = http://localhost:44762/Data/Media/

                        index = publishProduct.OriginalImagepath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _swatchImagePath = publishProduct.OriginalImagepath.Substring(0, index) + "/";
                        }


                        //ImageLargePath=http://localhost:44762/Data/Media/Catalog/7/1000/
                        /* search for images 1000 and add ZoomImage Prorty in ProductDetailGridModel and ProductDetailGridViewModel and asign it in below manner*/
                        index = publishProduct.ImageLargePath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _zoomImagePath = publishProduct.ImageLargePath.Substring(0, index) + "/";
                        }
                        /* */
                    }


                    if (publishProduct.ColorSizeStockList == null)
                    {
                        publishProduct.ColorSizeStockList = new List<ProductDetailGridModel>();
                    }
                    List<string> associatedProductIdsList = associatedProducts.Select(x => x.ZnodeProductId.ToString()).ToList<string>();
                    string productIdliststr = String.Join(",", associatedProductIdsList);
                    DataTable outOfStockOptionsDetails = GetOutOfStockOptionsDetails(productIdliststr);
                    foreach (ProductEntity pe in associatedProducts)
                    {

                        ProductDetailGridModel _pdModel = new ProductDetailGridModel();
                        DataRow[] dr1 = outOfStockOptionsDetails.Select("ProductId=" + pe.ZnodeProductId);
                        if (dr1.Length > 0)
                        {
                            _pdModel.OutOfstockOptions = Convert.ToString(dr1[0]["Option"]);

                        }

                        _pdModel.ProductType = "Product";
                        _pdModel.AssociatedProductId = pe.ZnodeProductId;
                        _pdModel.AssociatedProductSKU = pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductSKU)?.AttributeValues;

                        _pdModel.Stock = model == null ? 0 : Convert.ToDecimal(model.FirstOrDefault(x => x.SKU == _pdModel.AssociatedProductSKU)?.Quantity);
                        if (_pdModel.Stock < 0)
                            _pdModel.Stock = 0;

                        _pdModel.MinQty = Convert.ToInt32(pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.MinimumQuantity)?.AttributeValues);

                        _pdModel.MaxQty = Convert.ToInt32(pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.MaximumQuantity)?.AttributeValues);

                        //_pdModel.OutOfstockOptions = pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues[0]?.Code;

                        _pdModel.ReOrderLevel = model == null ? 0 : model.FirstOrDefault(x => x.SKU == _pdModel.AssociatedProductSKU)?.ReOrderLevel;

                        //_pdModel.Size = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctsize")?.AttributeValues;

                        _pdModel.Size = "|" + pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctsize")?.SelectValues[0].DisplayOrder + "|" +
                                        pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctsize")?.AttributeValues + "|";

                        _pdModel.ColorNumber = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcolornum")?.AttributeValues;


                        _pdModel.Color = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcolorname")?.AttributeValues;

                        _pdModel.SwatchImage = _swatchImagePath + pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcolorname")?.SelectValues[0]?.Path;

                        _pdModel.ProductImage = _imagePath + pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;

                        _pdModel.ZoomImage = _zoomImagePath + pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;

                        _pdModel.SalesPrice = priceModel == null ? 0 : Convert.ToDecimal(priceModel.FirstOrDefault(x => x.SKU == _pdModel.AssociatedProductSKU)?.SalesPrice);

                        //Written for the Clearance product Estimate Date string awctcatalog = "";
                        // _pdModel.CatalogName = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcatalog")?.AttributeValues;
                        _pdModel.CatalogName = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "Season")?.SelectValues[0]?.Code;

                        _pdModel.IsDutiable = Convert.ToString(pe.Attributes.FirstOrDefault(y => y.AttributeCode == "IsDutiable")?.AttributeValues);

                        _pdModel.NeedDays = Convert.ToInt32(pe.Attributes.FirstOrDefault(y => y.AttributeCode == "NeedDays")?.AttributeValues);

                        _pdModel.OverideNeedDays = Convert.ToInt32(pe.Attributes.FirstOrDefault(y => y.AttributeCode == "Overrideneeddate")?.AttributeValues);

                        if (_pdModel.IsDutiable == string.Empty || _pdModel.IsDutiable == null || _pdModel.IsDutiable == "false")
                        {
                            _pdModel.IsDutiable = "False";
                        }
                        else
                        {
                            _pdModel.IsDutiable = "True";
                        }

                        decimal stock = 0;
                        stock = _pdModel.Stock;
                        string awctcatalog = "";
                        awctcatalog = _pdModel.CatalogName;
                        _pdModel.DisableClearanceQuantity = "";
                        //NeedDays--&#9733;
                        try
                        {
                            string starMarking = "";
                            _pdModel.EstimateQuantitySymbol = "";
                            switch (_pdModel.OutOfstockOptions)
                            {
                                case "DontTrackInventory":
                                    starMarking = "&#9733;";
                                    //We skip stock check as there is no need to check as this items will be made to order 
                                    if (_pdModel.NeedDays > 0)
                                    {
                                        /*Nivi Code for dance estimated ship date excluding sat and sun*/
                                        if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Saturday)))
                                        {

                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 2).ToString("MM/dd/yyyy");
                                        }
                                        else if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Sunday)))
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 1).ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        }
                                        /*Nivi Original Code Working for dance*/
                                        //_pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        _pdModel.EstimateQuantitySymbol = "x";
                                    }
                                    //_pdModel.starMarkingWithQty = Convert.ToInt32(_pdModel.Stock).ToString() + starMarking;
                                    _pdModel.starMarkingWithQty = starMarking;
                                    break;
                                case "DisablePurchasing":
                                    starMarking = "&#9733;&#9733;";
                                    //If stock is > 0 (Zero) then show esti. date otherwise do not show product
                                    if (_pdModel.NeedDays > 0 && stock > 0)
                                    {
                                        /*Nivi Code for dance estimated ship date excluding sat and sun*/
                                        if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Saturday)))
                                        {

                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 2).ToString("MM/dd/yyyy");
                                        }
                                        else if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Sunday)))
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 1).ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        }

                                        /*Nivi Original Code Working for dance*/
                                        //_pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        _pdModel.starMarkingWithQty = Convert.ToInt32(_pdModel.Stock).ToString() + starMarking;
                                    }
                                    else if (stock <= 0)
                                    {
                                        _pdModel.starMarkingWithQty = "-1";
                                        _pdModel.DisableClearanceQuantity = "disabled";
                                    }
                                    break;
                                case "AllowBackOrdering":
                                    starMarking = "&#9733;&#9733;&#9733;";
                                    //We can skip stock check as we allow to order 
                                    if (_pdModel.OverideNeedDays > 0 && stock == 0)
                                    {
                                        /*Nivi Code Change for estimated date change excluding sat and sun*/
                                        //_pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.OverideNeedDays).ToString("MM/dd/yyyy");
                                        if ((Equals(DateTime.Now.AddDays(_pdModel.OverideNeedDays).DayOfWeek, DayOfWeek.Saturday)))
                                        {

                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.OverideNeedDays + 2).ToString("MM/dd/yyyy");
                                        }
                                        else if ((Equals(DateTime.Now.AddDays(_pdModel.OverideNeedDays).DayOfWeek, DayOfWeek.Sunday)))
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.OverideNeedDays + 1).ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.OverideNeedDays).ToString("MM/dd/yyyy");
                                        }
                                    }
                                    else if (_pdModel.NeedDays > 0)
                                    {
                                        /*Nivi Code for dance estimated ship date excluding sat and sun*/
                                        if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Saturday)))
                                        {

                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 2).ToString("MM/dd/yyyy");
                                        }
                                        else if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Sunday)))
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 1).ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        }
                                        /*Nivi Original Code Working for dance*/
                                        // _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        _pdModel.EstimateQuantitySymbol = "x";
                                    }
                                    _pdModel.starMarkingWithQty = Convert.ToInt32(_pdModel.Stock).ToString() + starMarking;
                                    break;
                            }
                        }

                        catch (Exception ex)
                        {
                            _pdModel.EstimateQuantitySymbol = "x";
                            ZnodeLogging.LogMessage("EstimateQuantitySymbol--" + ex.Message, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                        }


                        publishProduct.ColorSizeStockList.Add(_pdModel);
                    }
                }
                else
                {
                    ZnodeLogging.LogMessage("PublishProduct is null in GetPDPColorSizeGridData", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                }
                return publishProduct;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return publishProduct;
            }
        }
        public DataTable GetOutOfStockOptionsDetails(string productIdliststr)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@productIdliststr", productIdliststr, ParameterDirection.Input, SqlDbType.VarChar);
            DataSet data = objStoredProc.GetSPResultInDataSet("AWCT_GetOutOfStockOptionDetails");
            DataTable OutOfOptionsDetailsTable = data.Tables[0];
            return OutOfOptionsDetailsTable;
        }
        private AWCTPublishProductModel GetAddOnGridData(WebStoreAddOnModel addOns, int portalId, AWCTPublishProductModel publishProduct)
        {
            try
            {
                if (publishProduct != null)
                {
                    List<string> skulist = new List<string>();

                    foreach (WebStoreAddOnValueModel pe in addOns.AddOnValues)
                    {
                        string sku = pe.SKU;//.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductSKU)?.AttributeValues;
                        skulist.Add(sku);
                    }
                    List<InventorySKUModel> model = publishProductHelper.GetInventoryBySKUs(skulist, portalId);
                    if (model == null)
                    {
                        ZnodeLogging.LogMessage("model is null in GetPDPColorSizeGridData", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    }
                    List<PriceSKUModel> priceModel = publishProductHelper.GetPricingBySKUs(skulist, portalId, 0);
                    if (priceModel == null)
                    {
                        ZnodeLogging.LogMessage("priceModel is null in GetPDPColorSizeGridData", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    }

                    int index = 0;
                    string _swatchImagePath = "";
                    string _zoomImagePath = "";
                    string _imagePath = "";
                    if (publishProduct.ImageThumbNailPath == null)
                    {
                        ImageHelper image = new ImageHelper(portalId);
                        string ImageMediumPath = image.GetImageHttpPathMedium("image");
                        string OriginalImagepath = image.GetOriginalImagepath("image");
                        string ImageLargePath = image.GetImageHttpPathLarge("image");
                        //ImageMediumPath=http://localhost:44762/Data/Media/Catalog/7/700/
                        index = ImageMediumPath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _imagePath = ImageMediumPath.Substring(0, index) + "/";
                        }
                        //OriginalImagepath = http://localhost:44762/Data/Media/

                        index = OriginalImagepath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
                        }
                        //ImageLargePath=http://localhost:44762/Data/Media/Catalog/7/1000/
                        /* search for images 1000 and add ZoomImage Prorty in ProductDetailGridModel and ProductDetailGridViewModel and asign it in below manner*/
                        index = ImageLargePath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _zoomImagePath = ImageLargePath.Substring(0, index) + "/";
                        }
                        /* */
                    }
                    else
                    {
                        //ImageMediumPath=http://localhost:44762/Data/Media/Catalog/7/700/
                        index = publishProduct.ImageMediumPath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _imagePath = publishProduct.ImageMediumPath.Substring(0, index) + "/";
                        }

                        //OriginalImagepath = http://localhost:44762/Data/Media/

                        index = publishProduct.OriginalImagepath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _swatchImagePath = publishProduct.OriginalImagepath.Substring(0, index) + "/";
                        }


                        //ImageLargePath=http://localhost:44762/Data/Media/Catalog/7/1000/
                        /* search for images 1000 and add ZoomImage Prorty in ProductDetailGridModel and ProductDetailGridViewModel and asign it in below manner*/
                        index = publishProduct.ImageLargePath.LastIndexOf('/');
                        if (index != -1)
                        {
                            _zoomImagePath = publishProduct.ImageLargePath.Substring(0, index) + "/";
                        }
                        /* */
                    }


                    if (publishProduct.ColorSizeStockList == null)
                    {
                        publishProduct.ColorSizeStockList = new List<ProductDetailGridModel>();
                    }

                    foreach (WebStoreAddOnValueModel pe in addOns.AddOnValues)
                    {

                        ProductDetailGridModel _pdModel = new ProductDetailGridModel();
                        _pdModel.ProductType = "AddOn";
                        _pdModel.AssociatedProductId = pe.PublishProductId;
                        _pdModel.AssociatedProductSKU = pe.SKU;

                        _pdModel.Stock = model == null ? 0 : Convert.ToDecimal(model.FirstOrDefault(x => x.SKU == _pdModel.AssociatedProductSKU)?.Quantity);

                        _pdModel.MinQty = Convert.ToInt32(pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.MinimumQuantity)?.AttributeValues);

                        _pdModel.MaxQty = Convert.ToInt32(pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.MaximumQuantity)?.AttributeValues);

                        _pdModel.OutOfstockOptions = pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues[0]?.Code;

                        _pdModel.ReOrderLevel = model == null ? 0 : model.FirstOrDefault(x => x.SKU == _pdModel.AssociatedProductSKU)?.ReOrderLevel;

                        /*Original Code Working for Dance*/
                        //_pdModel.Size = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctsize")?.SelectValues[0].Code;

                        /*nivi code start for AWCT - 568 for addon products are not getting the sorting order proper*/

                        _pdModel.Size = "|" + pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctsize")?.SelectValues[0].DisplayOrder + "|" + pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctsize")?.SelectValues[0].Code + "|";

                        /*nivi code end for AWCT - 568 for addon products are not getting the sorting order proper*/

                        _pdModel.ColorNumber = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcolornum")?.AttributeValues;


                        _pdModel.Color = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcolorname")?.SelectValues[0].Code;

                        _pdModel.SwatchImage = _swatchImagePath + pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcolorname")?.SelectValues[0]?.Path;

                        _pdModel.ProductImage = _imagePath + pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;

                        _pdModel.ZoomImage = _zoomImagePath + pe.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;

                        _pdModel.SalesPrice = priceModel == null ? 0 : Convert.ToDecimal(priceModel.FirstOrDefault(x => x.SKU == _pdModel.AssociatedProductSKU)?.SalesPrice);

                        //Written for the Clearance product Estimate Date string awctcatalog = "";
                        // _pdModel.CatalogName = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "awctcatalog")?.AttributeValues;
                        _pdModel.CatalogName = pe.Attributes.FirstOrDefault(y => y.AttributeCode == "Season")?.SelectValues[0]?.Code;

                        _pdModel.IsDutiable = Convert.ToString(pe.Attributes.FirstOrDefault(y => y.AttributeCode == "IsDutiable")?.AttributeValues);

                        _pdModel.NeedDays = Convert.ToInt32(pe.Attributes.FirstOrDefault(y => y.AttributeCode == "NeedDays")?.AttributeValues);

                        if (_pdModel.IsDutiable == string.Empty || _pdModel.IsDutiable == null || _pdModel.IsDutiable == "false")
                        {
                            _pdModel.IsDutiable = "False";
                        }
                        else
                        {
                            _pdModel.IsDutiable = "True";
                        }

                        decimal stock = 0;
                        stock = _pdModel.Stock;
                        string awctcatalog = "";
                        awctcatalog = _pdModel.CatalogName;
                        _pdModel.DisableClearanceQuantity = "";
                        //NeedDays--&#9733;
                        try
                        {
                            string starMarking = "";
                            _pdModel.EstimateQuantitySymbol = "";
                            switch (_pdModel.OutOfstockOptions)
                            {
                                case "DontTrackInventory":
                                    starMarking = "&#9733;";
                                    //We skip stock check as there is no need to check as this items will be made to order 
                                    if (_pdModel.NeedDays > 0)
                                    {
                                        /*Nivi Code for dance estimated ship date excluding sat and sun*/
                                        if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Saturday)))
                                        {

                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 2).ToString("MM/dd/yyyy");
                                        }
                                        else if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Sunday)))
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 1).ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        }
                                        /*Nivi Original Code Working for dance*/
                                        //_pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        _pdModel.EstimateQuantitySymbol = "x";
                                    }
                                    //_pdModel.starMarkingWithQty = Convert.ToInt32(_pdModel.Stock).ToString() + starMarking;
                                    _pdModel.starMarkingWithQty = starMarking;
                                    break;
                                case "DisablePurchasing":
                                    starMarking = "&#9733;&#9733;";
                                    //If stock is > 0 (Zero) then show esti. date otherwise do not show product
                                    if (_pdModel.NeedDays > 0 && stock > 0)
                                    {
                                        /*Nivi Code for dance estimated ship date excluding sat and sun*/
                                        if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Saturday)))
                                        {

                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 2).ToString("MM/dd/yyyy");
                                        }
                                        else if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Sunday)))
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 1).ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        }
                                        /*Nivi Original Code Working for dance*/
                                        //_pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        _pdModel.starMarkingWithQty = Convert.ToInt32(_pdModel.Stock).ToString() + starMarking;
                                    }
                                    else if (stock <= 0)
                                    {
                                        _pdModel.starMarkingWithQty = "-1";
                                        _pdModel.DisableClearanceQuantity = "disabled";
                                    }
                                    break;
                                case "AllowBackOrdering":
                                    starMarking = "&#9733;&#9733;&#9733;";
                                    //We can skip stock check as we allow to order 
                                    if (_pdModel.NeedDays > 0)
                                    {
                                        /*Nivi Code for dance estimated ship date excluding sat and sun*/
                                        if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Saturday)))
                                        {

                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 2).ToString("MM/dd/yyyy");
                                        }
                                        else if ((Equals(DateTime.Now.AddDays(_pdModel.NeedDays).DayOfWeek, DayOfWeek.Sunday)))
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays + 1).ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            _pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                        }
                                        /*Nivi Original Code Working for dance*/
                                        //_pdModel.EstimateQuantitySymbol = DateTime.Now.AddDays(_pdModel.NeedDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        _pdModel.EstimateQuantitySymbol = "x";
                                    }
                                    _pdModel.starMarkingWithQty = Convert.ToInt32(_pdModel.Stock).ToString() + starMarking;
                                    break;
                            }
                        }

                        catch (Exception ex)
                        {
                            _pdModel.EstimateQuantitySymbol = "x";
                            ZnodeLogging.LogMessage("EstimateQuantitySymbol--" + ex.Message, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                        }


                        publishProduct.ColorSizeStockList.Add(_pdModel);
                    }
                }
                else
                {
                    ZnodeLogging.LogMessage("PublishProduct is null in GetPDPColorSizeGridData", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                }
                return publishProduct;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return publishProduct;
            }
        }
        private void AssignConfigGalImagesToSimpleProduct(PublishProductModel products, AWCTPublishProductModel publishProduct, int portalId)
        {
            if (products != null)
            {
                if (publishProduct.AlternateImages == null)
                {
                    publishProduct.AlternateImages = new List<ProductAlterNateImageModel>();
                }

                ImageHelper image = new ImageHelper(portalId);
                string configProductImage = products.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                string videoUrl = products.Attributes.FirstOrDefault(y => y.AttributeCode == "VideoURL")?.AttributeValues;
                string videoThumb = products.Attributes.FirstOrDefault(y => y.AttributeCode == "videothumb")?.AttributeValues;
                int index = -1;
                string ImageMediumPath = "";
                string OriginalImagePath = "";
                string ImageSmallPath = "";
                string ImageLargePath = "";
                //ImageLargePath=800
                if (publishProduct.ImageThumbNailPath == null)
                {
                    string imageMediumPath = image.GetImageHttpPathMedium("image");
                    string originalImagepath = image.GetOriginalImagepath("image");
                    string imageLargePath = image.GetImageHttpPathLarge("image");
                    string imageSmallPath = image.GetImageHttpPathSmall("image");

                    index = imageLargePath.LastIndexOf('/');
                    if (index != -1)
                    {
                        ImageLargePath = imageLargePath.Substring(0, index) + "/";
                    }
                    //ImageMediumPath=400
                    index = imageMediumPath.LastIndexOf('/');
                    if (index != -1)
                    {
                        ImageMediumPath = imageMediumPath.Substring(0, index) + "/";
                    }
                    //OriginalImagepath=Not detfined in Admin setting
                    index = originalImagepath.LastIndexOf('/');
                    if (index != -1)
                    {
                        OriginalImagePath = originalImagepath.Substring(0, index) + "/";
                    }
                    //ImageSmallPath=250
                    index = imageSmallPath.LastIndexOf('/');
                    if (index != -1)
                    {
                        ImageSmallPath = imageSmallPath.Substring(0, index) + "/";
                    }
                }
                else
                {
                    index = publishProduct.ImageLargePath.LastIndexOf('/');
                    if (index != -1)
                    {
                        ImageLargePath = publishProduct.ImageLargePath.Substring(0, index) + "/";
                    }
                    //ImageMediumPath=400
                    index = publishProduct.ImageMediumPath.LastIndexOf('/');
                    if (index != -1)
                    {
                        ImageMediumPath = publishProduct.ImageMediumPath.Substring(0, index) + "/";
                    }
                    //OriginalImagepath=Not detfined in Admin setting
                    index = publishProduct.OriginalImagepath.LastIndexOf('/');
                    if (index != -1)
                    {
                        OriginalImagePath = publishProduct.OriginalImagepath.Substring(0, index) + "/";
                    }
                    //ImageSmallPath=250
                    index = publishProduct.ImageSmallPath.LastIndexOf('/');
                    if (index != -1)
                    {
                        ImageSmallPath = publishProduct.ImageSmallPath.Substring(0, index) + "/";
                    }
                }
                int ElementIndex = 0;
                ProductAlterNateImageModel simplePrdGalleryImage1 = new ProductAlterNateImageModel();
                if (videoThumb != null)
                {
                    simplePrdGalleryImage1.FileName = "videoThumb";
                    /*Video Url*/
                    simplePrdGalleryImage1.ImageLargePath = videoUrl;
                    /*Video thumb*/
                    simplePrdGalleryImage1.ImageSmallPath = ImageSmallPath + videoThumb;
                    publishProduct.AlternateImages.Add(simplePrdGalleryImage1);
                    ElementIndex = 1;
                }

                List<PublishAttributeModel> galleryImages = products.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages).ToList<PublishAttributeModel>();
                if (galleryImages != null && galleryImages.Count() > 0)
                {

                    string[] galImages = galleryImages[0].AttributeValues.Split(',');
                    foreach (string img in galImages)
                    {

                        ProductAlterNateImageModel simplePrdGalleryImage = new ProductAlterNateImageModel();
                        if (configProductImage == img)
                        {
                            simplePrdGalleryImage.FileName = "IsDefaultImage";
                            // simplePrdGalleryImage.ImageThumbNailPath = "IsDefaultImage";

                        }
                        if (!string.IsNullOrEmpty(img))
                        {
                            // simplePrdGalleryImage.FileName = img;
                            /*PDP Main Image 800*/
                            simplePrdGalleryImage.ImageLargePath = ImageLargePath + img;
                            /*Zoom image 1000-Not any used*/
                            simplePrdGalleryImage.OriginalImagePath = OriginalImagePath + img;
                            /*PDP Main Image 400*/
                            simplePrdGalleryImage.ImageSmallPath = ImageSmallPath + img;
                        }
                        if (configProductImage == img)
                        {
                            /*Nivi:Code to add the Configure Image at first position:case if ElementIndex=1 videothumb is added in this case configure image is added 
                             at 2nd position else it is set to default first position*/
                            if (ElementIndex == 1)
                            {
                                publishProduct.AlternateImages.Insert(1, simplePrdGalleryImage);
                                // simplePrdGalleryImage.ImageThumbNailPath = "IsDefaultImage";
                            }
                            else
                            {
                                publishProduct.AlternateImages.Insert(0, simplePrdGalleryImage);
                            }

                        }
                        else
                        {
                            publishProduct.AlternateImages.Add(simplePrdGalleryImage);
                        }

                    }
                }
            }
            //publishProduct.ProductTemplateName = "_Big_View";
        }
        private void AssignConfigGalImagesToSimpleProduct(List<ProductEntity> products, AWCTPublishProductModel publishProduct, int portalId)
        {
            if (products != null && products.Count > 0)
            {
                if (publishProduct.AlternateImages == null)
                {
                    publishProduct.AlternateImages = new List<ProductAlterNateImageModel>();
                }

                ImageHelper image = new ImageHelper(portalId);
                string configProductImage = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                string videoUrl = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "VideoURL")?.AttributeValues;
                string videoThumb = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "videothumb")?.AttributeValues;


                PublishAttributeModel publishAttributeModel = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "LongDescription").ToModel<PublishAttributeModel>();
                if (publishAttributeModel != null)
                {
                    publishProduct.Attributes.RemoveAll(x => x.AttributeCode == "LongDescription");
                    publishProduct.Attributes.Add(publishAttributeModel);
                }

                //IsFileUpload
                publishAttributeModel = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "IsFileUpload").ToModel<PublishAttributeModel>();
                if (publishAttributeModel != null)
                {
                    publishProduct.Attributes.RemoveAll(x => x.AttributeCode == "IsFileUpload");
                    publishProduct.Attributes.Add(publishAttributeModel);
                }
                publishAttributeModel = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "awctpagenum").ToModel<PublishAttributeModel>();
                if (publishAttributeModel != null)
                {
                    string code = publishProduct.Attributes?.FirstOrDefault(y => y.AttributeCode == "awctpagenum")?.AttributeCode;
                    if (!string.IsNullOrEmpty(code))
                    {
                        publishProduct.Attributes.RemoveAll(x => x.AttributeCode == "awctpagenum");
                    }
                    publishProduct.Attributes.Add(publishAttributeModel);
                }
                //Highlights
                publishAttributeModel = products[0].Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.Highlights)?.ToModel<PublishAttributeModel>();
                if (publishAttributeModel != null)
                {
                    publishProduct.Attributes.RemoveAll(x => x.AttributeCode == ZnodeConstant.Highlights);
                    publishProduct.Attributes.Add(publishAttributeModel);
                }
                //Not use ful
                List<string> codesList = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.Highlights)?.SelectValues?.Select(y => y.Code)?.ToList();
                if (codesList?.Count > 0)
                {
                    string highLightsCodes = string.Join(",", codesList);

                }
                //Additional Notes
                publishAttributeModel = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "additionalnotes").ToModel<PublishAttributeModel>();
                if (publishAttributeModel != null)
                {
                    string code = publishProduct.Attributes?.FirstOrDefault(y => y.AttributeCode == "additionalnotes")?.AttributeCode;
                    if (!string.IsNullOrEmpty(code))
                    {
                        publishProduct.Attributes.RemoveAll(x => x.AttributeCode == "additionalnotes");
                    }
                    publishProduct.Attributes.Add(publishAttributeModel);
                }
                //Additional Notes
                publishAttributeModel = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "addondescription").ToModel<PublishAttributeModel>();
                if (publishAttributeModel != null)
                {
                    string code = publishProduct.Attributes?.FirstOrDefault(y => y.AttributeCode == "addondescription")?.AttributeCode;
                    if (!string.IsNullOrEmpty(code))
                    {
                        publishProduct.Attributes.RemoveAll(x => x.AttributeCode == "addondescription");
                    }
                    publishProduct.Attributes.Add(publishAttributeModel);
                }
                List<PublishAttributeModel> publishAttributeModelList = new List<PublishAttributeModel>();
                string attributelist = "SVGFileName,OptionA,OptionB,OptionC,OptionD,OptionE,OptionF,OptionG,OptionH,OptionR,OptionZ,IsTrueColorFamily";
                foreach (AttributeEntity ae in products[0].Attributes)
                {
                    if (attributelist.Contains(ae.AttributeCode))
                    {
                        publishProduct.Attributes.RemoveAll(x => x.AttributeCode == ae.AttributeCode);
                        publishAttributeModelList.Add(ae.ToModel<PublishAttributeModel>());
                    }

                }
                publishProduct.Attributes.AddRange(publishAttributeModelList);
                int index = -1;
                string ImageMediumPath = "";
                string OriginalImagePath = "";
                string ImageSmallPath = "";
                string ImageLargePath = "";
                //ImageLargePath=800              

                index = publishProduct.ImageLargePath.LastIndexOf('/');
                if (index != -1)
                {
                    ImageLargePath = publishProduct.ImageLargePath.Substring(0, index) + "/";
                }
                //ImageMediumPath=400
                index = publishProduct.ImageMediumPath.LastIndexOf('/');
                if (index != -1)
                {
                    ImageMediumPath = publishProduct.ImageMediumPath.Substring(0, index) + "/";
                }
                //OriginalImagepath=Not detfined in Admin setting
                index = publishProduct.OriginalImagepath.LastIndexOf('/');
                if (index != -1)
                {
                    OriginalImagePath = publishProduct.OriginalImagepath.Substring(0, index) + "/";
                }
                //ImageSmallPath=250
                index = publishProduct.ImageSmallPath.LastIndexOf('/');
                if (index != -1)
                {
                    ImageSmallPath = publishProduct.ImageSmallPath.Substring(0, index) + "/";
                }

                int ElementIndex = 0;
                ProductAlterNateImageModel simplePrdGalleryImage1 = new ProductAlterNateImageModel();
                if (videoThumb != null)
                {
                    simplePrdGalleryImage1.FileName = "videoThumb";
                    /*Video Url*/
                    simplePrdGalleryImage1.ImageLargePath = videoUrl;
                    /*Video thumb*/
                    simplePrdGalleryImage1.ImageSmallPath = ImageSmallPath + videoThumb;
                    publishProduct.AlternateImages.Add(simplePrdGalleryImage1);
                    ElementIndex = 1;
                }

                List<AttributeEntity> galleryImages = products[0].Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages).ToList<AttributeEntity>();
                if (galleryImages != null && galleryImages.Count() > 0)
                {
                    try
                    {


                        string[] galImages = galleryImages[0].AttributeValues.Split(',');
                        List<string> orderedImages = SortedImages(galImages);
                      //  ZnodeLogging.LogMessage("AssignConfigGalImagesToSimpleProduct orderedImages" + orderedImages.Count().ToString(), "Custom", TraceLevel.Error);
                        foreach (string img in orderedImages)
                        //foreach (string img in galImages)
                        {
                         //   ZnodeLogging.LogMessage("AssignConfigGalImagesToSimpleProduct img" + img, "Custom", TraceLevel.Error);
                            ProductAlterNateImageModel simplePrdGalleryImage = new ProductAlterNateImageModel();
                            if (configProductImage == img)
                            {
                                simplePrdGalleryImage.FileName = "IsDefaultImage";
                                // simplePrdGalleryImage.ImageThumbNailPath = "IsDefaultImage";

                            }
                            if (!string.IsNullOrEmpty(img))
                            {
                                // simplePrdGalleryImage.FileName = img;
                                /*PDP Main Image 800*/
                                simplePrdGalleryImage.ImageLargePath = ImageLargePath + img;
                                /*Zoom image 1000-Not any used*/
                                simplePrdGalleryImage.OriginalImagePath = OriginalImagePath + img;
                                /*PDP Main Image 400*/
                                simplePrdGalleryImage.ImageSmallPath = ImageSmallPath + img;
                            }
                            if (configProductImage == img)
                            {
                                /*Nivi:Code to add the Configure Image at first position:case if ElementIndex=1 videothumb is added in this case configure image is added 
                                 at 2nd position else it is set to default first position*/
                                if (ElementIndex == 1)
                                {
                                    publishProduct.AlternateImages.Insert(1, simplePrdGalleryImage);
                                    // simplePrdGalleryImage.ImageThumbNailPath = "IsDefaultImage";
                                }
                                else
                                {
                                    publishProduct.AlternateImages.Insert(0, simplePrdGalleryImage);
                                }

                            }
                            else
                            {
                                publishProduct.AlternateImages.Add(simplePrdGalleryImage);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage("AssignConfigGalImagesToSimpleProduct" + ex.StackTrace, "Custom", TraceLevel.Error);
                    }
                }
            }

            publishProduct.ProductTemplateName = "_Big_View";
        }
        
        //Get Default configurable product.
        protected AWCTPublishProductModel GetDefaultConfiurableProduct(NameValueCollection expands, int portalId, int localeId, AWCTPublishProductModel publishProduct, List<ProductEntity> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {
            ZnodeLogging.LogMessage("Input Parameter portalId,localeId:", string.Empty, TraceLevel.Info, new object[] { portalId, localeId });
            string sku = publishProduct.SKU;
            string parentSEOCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "SKU")?.AttributeValues;
            int categoryIds = publishProduct.ZnodeCategoryIds;
            List<PublishCategoryModel> categoryHierarchyIds = publishProduct.CategoryHierarchy;

            List<PublishAttributeModel> parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
            string parentConfigurableProductName = publishProduct.Name;
            int configurableProductId = publishProduct.PublishProductId;
            ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new { sku = sku, parentSEOCode = parentSEOCode, categoryIds = categoryIds, configurableProductId = configurableProductId });
            List<ProductEntity> newassociatedProducts = new List<ProductEntity>();
            foreach (ProductEntity product in associatedProducts)
            {
                foreach (AttributeEntity attributeEntity in product.Attributes)
                {
                    if (attributeEntity.IsConfigurable)
                    {
                        attributeEntity.AttributeValues = attributeEntity.SelectValues.FirstOrDefault()?.Value;
                        //assigned display order of product to configurable attribute to display it on webstore depend on display order.
                        //if (attributeEntity?.SelectValues?.Count > 0)
                        //{
                        //    attributeEntity.SelectValues.FirstOrDefault().DisplayOrder = product?.DisplayOrder;
                        //}

                        //attributeEntity.DisplayOrder = product.DisplayOrder;
                    }
                }
                newassociatedProducts.Add(product);
            }
            ZnodeLogging.LogMessage("newassociatedProducts list count:", string.Empty, TraceLevel.Verbose, newassociatedProducts?.Count());
            //Get first product from list of associated products 
            publishProduct = newassociatedProducts.FirstOrDefault().ToModel<AWCTPublishProductModel>();

            publishProduct.ConfigurableProductId = configurableProductId;
            publishProduct.ParentSEOCode = parentSEOCode;
            publishProduct.ConfigurableProductSKU = sku;
            publishProduct.CategoryHierarchy = categoryHierarchyIds;

            publishProduct.SKU = sku;
            publishProduct.ZnodeCategoryIds = categoryIds;
            publishProduct.IsConfigurableProduct = true;
            publishProduct.ParentConfiguarableProductName = parentConfigurableProductName;

            catalogVersionId = Convert.ToInt32(catalogVersionId) > 0 ? catalogVersionId : GetCatalogVersionId();
            if (publishProduct.PublishProductId == 0)
            {
                publishProduct.PublishProductId = configurableProductId;
            }
            //Get expands associated to Product.
            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId, WebstoreVersionId, GetProfileId());

            PublishAttributeModel defaultAttribute = publishProduct.Attributes?.FirstOrDefault(x => x.IsConfigurable);
            bool isChildPersonalizableAttribute = Convert.ToBoolean(publishProduct.Attributes?.Contains(publishProduct.Attributes?.FirstOrDefault(x => x.IsPersonalizable)));

            List<PublishAttributeModel> variants = publishProduct?.Attributes?.Where(x => x.IsConfigurable).ToList();
            Dictionary<string, string> selectedAttribute = GetSelectedAttributes(variants);

            List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(newassociatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, newassociatedProducts, ConfigurableAttributeCodes, portalId);
            ZnodeLogging.LogMessage("List count:", string.Empty, TraceLevel.Verbose, new { attributeListCount = attributeList?.Count(), variantsCount = variants?.Count() });
            foreach (PublishAttributeModel item in attributeList)
            {
                publishProduct.Attributes.RemoveAll(x => x.AttributeCode == item.AttributeCode);
            }

            publishProduct.Attributes.AddRange(attributeList);

            return publishProduct;

        }
        private List<PriceSizeGroup> GetPriceSizeGrouping(int configurableProductId)
        {
            IZnodeViewRepository<PriceSizeGroup> objStoredProc = new ZnodeViewRepository<PriceSizeGroup>();
            objStoredProc.SetParameter("@ConfigurableProductId", configurableProductId, ParameterDirection.Input, DbType.Int32);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("AWCT_GetPriceSizeGroup @ConfigurableProductId").ToList();

        }
        public AWCTTruColorModel GetGlobalAttributeData(string globalAttributeCodes)
        {
            AWCTTruColorModel aWCTTruColorViewModel = new AWCTTruColorModel();
            List<AWCTTruColorList> _truColorList = GetGlobalAttributeList(globalAttributeCodes);
            aWCTTruColorViewModel.TruColorList = _truColorList;
            return aWCTTruColorViewModel;
        }
        private List<AWCTTruColorList> GetGlobalAttributeList(string GlobalAttributeCodeCsv)
        {
            IZnodeViewRepository<AWCTTruColorList> objStoredProc = new ZnodeViewRepository<AWCTTruColorList>();
            objStoredProc.SetParameter("@GlobalAttributeCodeCsv", GlobalAttributeCodeCsv, ParameterDirection.Input, DbType.String);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("AWCT_GetGlobalAttributeData @GlobalAttributeCodeCsv").ToList();

        }
        /// <summary>
        /// This method returns product entity for supplied publishProductId from mongo db.
        /// Additionally, it also returns the category hierarchy for the returned product.
        /// Category hierarchy retrieval is merged with product retrieval since they both require traversing through the associated categories at least once. 
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        private PublishProductModel GetPublishedProductFromMongo(int publishProductId, FilterCollection filters)
        {
            GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);

            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            //get catalog current version id by catalog id.
            int? catalogVersionId = GetCatalogVersionId(catalogId);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

            if (catalogVersionId > 0)
            {
                filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");
            }

            PublishProductModel publishProduct = null;
            CategoryEntity associatedCategory = null;
            //Get publish product from mongo
            List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), false);

            if (HelperUtility.IsNotNull(products))
            {
                associatedCategory = GetActiveAssociatedCategory(products);
                if (HelperUtility.IsNotNull(associatedCategory))
                {
                    publishProduct = products?.FirstOrDefault(x => x.ZnodeCategoryIds == associatedCategory.ZnodeCategoryId).ToModel<PublishProductModel>();
                    publishProduct.CategoryHierarchy = GetProductCategory(associatedCategory.ZnodeCategoryId.ToString(), localeId, catalogId, _seoService.GetPublishSEOSettingList(ZnodeConstant.Category, portalId, localeId));
                }
            }

            return publishProduct;
        }
        /// <summary>
        /// This method returns an associated category from mongo db on the basis of following criteria:
        /// 1. A category has been associated to any of the products found in supplied product list.
        /// 2. A category has its flag "IsActive" set to "true".
        /// Single category selection is done on "First to appear" basis.
        /// </summary>
        /// <param name="products">Collection of products which have one category associated with each.</param>
        /// <returns></returns>
        private CategoryEntity GetActiveAssociatedCategory(List<ProductEntity> products)
        {
            List<int> categoryIds = products.Select(x => x.ZnodeCategoryIds).ToList();
            IMongoQuery categoryQuery = Query.And(
                Query<CategoryEntity>.In(x => x.ZnodeCategoryId, categoryIds),
                Query<CategoryEntity>.EQ(x => x.IsActive, true));

            return _categoryMongoRepository.GetEntity(categoryQuery);
        }

        //public List<string> SortedImages(string[] galImages)
        //{
        //    List<string> orderlist = new List<string>();

        //    foreach (string img in galImages)
        //    {
        //        string[] imgorder = img.Split('_');
        //        orderlist.Add(imgorder[1]);
        //    }
        //    orderlist.Sort();
        //    List<string> orderedImages = new List<string>();
        //    foreach (string a in orderlist)
        //    {
        //        string newimg = galImages.Where(x => x.Contains("_" + a)).FirstOrDefault();
        //        orderedImages.Add(newimg);
        //    }

        //    return orderedImages;

        //}
        public List<string> SortedImages(string[] galImages)
        {
            List<string> orderlist = new List<string>();

            foreach (string img in galImages)
            {
                string[] imgorder = img.Split('_');
                byte[] value = Encoding.ASCII.GetBytes(imgorder[1]);
                if (value[0] > 47 && value[0] < 58)
                {
                    orderlist.Add("z" + imgorder[1]);
                }
                else
                    orderlist.Add(imgorder[1]);
            }
            orderlist.Sort();
            List<string> orderedImages = new List<string>();
            foreach (string a in orderlist)
            {
                string a1 = ""; string newimg = "";
                if (a.Contains("z") && a.Length > 1)
                {
                    a1 = a.Remove(0, 1);
                    newimg = galImages.Where(x => x.Contains("_" + a1)).FirstOrDefault();
                }
                else
                    newimg = galImages.Where(x => x.Contains("_" + a)).FirstOrDefault();
                if (newimg != null)
                    orderedImages.Add(newimg);
            }

            return orderedImages;

        }
        #endregion

        #region Frequently Check
        //public override PublishProductListModel GetPublishProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        //{
        //    ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
        //    int catalogId, portalId, localeId, userId;
        //    string productType;
        //    GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

        //    int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.UserId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out userId);
        //    productType = filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.ProductType, StringComparison.InvariantCultureIgnoreCase))?.FilterValue;
        //    ZnodeLogging.LogMessage("productType:", string.Empty, TraceLevel.Verbose, new object[] { productType });
        //    if (!string.IsNullOrEmpty(productType))
        //    {
        //        filters.RemoveAll(x => x.Item1 == FilterKeys.ProductType);
        //        filters.Add(FilterKeys.AttributeValueForProductType, FilterOperators.Contains, productType);
        //    }

        //    if (!string.IsNullOrEmpty(productType))
        //    {
        //        filters.RemoveAll(x => x.Item1 == FilterKeys.ProductType);
        //        filters.Add(FilterKeys.AttributeValueForProductType, FilterOperators.Contains, productType);
        //    }

        //    filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);
        //    filters.RemoveAll(x => x.FilterName == FilterKeys.UserId);

        //    int versionId = GetCatalogVersionId(catalogId, localeId).GetValueOrDefault();
        //    ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new object[] { versionId = versionId, catalogId = catalogId });
        //    //get catalog current version id by catalog id.
        //    if (catalogId > 0)
        //        filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(versionId));
        //    else
        //    {
        //        filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.In, filters.Exists(x => x.Item1 == Libraries.ECommerce.Utilities.FilterKeys.RevisionType) ? GetCatalogAllVersionIds(localeId) : GetCatalogAllVersionIds());

        //        //Get comma separated ids of the catalogs which are associated to the store(s).
        //        string activeCatalogIds = string.Join(",", (from portalCatalog in _portalCatalogRepository.Table
        //                                                    join publishCatalog in _publishCatalogRepository.Table on portalCatalog.PublishCatalogId equals publishCatalog.PublishCatalogId
        //                                                    select portalCatalog.PublishCatalogId).Distinct().ToArray());
        //        ZnodeLogging.LogMessage("activeCatalogIds:", string.Empty, TraceLevel.Verbose, new object[] { activeCatalogIds });
        //        filters.Add(FilterKeys.CatalogId, FilterOperators.In, activeCatalogIds);
        //    }
        //    if (filters.Exists(x => x.Item1 == FilterKeys.RevisionType))
        //        filters.RemoveAll(x => x.Item1 == FilterKeys.RevisionType);
        //    if (userId > 0 && EnableProfileBasedSearch(portalId))
        //    {
        //        int? userProfileId = GetUserProfileId(userId);
        //        ZnodeLogging.LogMessage("Parameter:", string.Empty, TraceLevel.Verbose, new { userProfileId = userProfileId, userId = userId });
        //        if (!Equals(userProfileId, null))
        //            filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(userProfileId));
        //    }
        //    //Get filter value
        //    string filterValue = filters.FirstOrDefault(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In)?.FilterValue;
        //    ZnodeLogging.LogMessage("filterValue:", string.Empty, TraceLevel.Verbose, new object[] { filterValue });
        //    if (!string.IsNullOrEmpty(filterValue))
        //    {
        //        //Remove Payment Setting Filters with IN operator from filters list
        //        filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.AttributeValuesForPromotion.ToString().ToLower() && x.FilterOperator == FilterOperators.In);

        //        //Add Payment Setting Filters
        //        filters.Add(FilterKeys.AttributeValuesForPromotion, FilterOperators.In, filterValue.Replace('_', ','));
        //    }

        //    if (filters.Any(w => w.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower()))
        //    {
        //        //Remove ZnodeCategoryId Filters from filters list
        //        filters.RemoveAll(x => x.FilterName.ToLower() == FilterKeys.fromOrder.ToString().ToLower());
        //        filters.Add(FilterKeys.ZnodeCategoryId, FilterOperators.In, GetActiveCategoryIds(catalogId));
        //    }

        //    //Replace filter keys with mongo filter keys
        //    ReplaceFilterKeys(ref filters);

        //    if (HelperUtility.IsNotNull(sorts))
        //        ReplaceSortKeys(ref sorts);

        //    //Check if products are taken for some specific category.
        //    SetProductIndexFilter(filters);

        //    PageListModel pageListModel = new PageListModel(filters, sorts, page);
        //    ZnodeLogging.LogMessage("pageListModel to publishProducts list :", string.Empty, TraceLevel.Verbose, pageListModel?.ToDebugString());
        //    //get publish products from mongo
        //    List<ProductEntity> publishProducts = publishProductHelper.GetPublishProductList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);
        //    ZnodeLogging.LogMessage("publishProducts list count:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Error, publishProducts?.Count());
        //    PublishProductListModel publishProductListModel = new PublishProductListModel() { PublishProducts = publishProducts.ToModel<PublishProductModel>().ToList() };

        //    GetExpands(portalId, localeId, expands, publishProductListModel, versionId);

        //    //Map pagination parameters
        //    publishProductListModel.BindPageListModel(pageListModel);
        //    ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
        //    return publishProductListModel;
        //}
        #endregion

    }
}
