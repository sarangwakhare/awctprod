﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using WebGrease.Css.Extensions;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTOrderService : OrderService
    {
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;

        public AWCTOrderService()
        {
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
        }
        //To generate unique order number on basis of current date.
        //public override string GenerateOrderNumber(SubmitOrderModel submitOrderModel, ParameterModel portalId = null)
        //{
        //    try
        //    {
        //        var _erpInc = new ERPInitializer<SubmitOrderModel>(submitOrderModel, "GetOrderNumber");
        //        if (string.IsNullOrEmpty(Convert.ToString(_erpInc.Result)))
        //        {
        //            string orderNumber = string.Empty;
        //            if (!string.IsNullOrEmpty(ZnodeConfigManager.SiteConfig.StoreName))
        //                orderNumber = ZnodeConfigManager.SiteConfig.StoreName.Trim().Length > 2 ? ZnodeConfigManager.SiteConfig.StoreName.Substring(0, 2) : ZnodeConfigManager.SiteConfig.StoreName.Substring(0, 1);

        //            DateTime date = DateTime.Now;
        //            String strDate = date.ToString("yyMMdd-HHmmss-fff");
        //            orderNumber += $"-{strDate}";
        //            return orderNumber.ToUpper();
        //        }
        //        else
        //            return Convert.ToString(_erpInc?.Result);
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage($"Error GenerateOrderNumber : {ex}", ZnodeLogging.Components.OMS.ToString());
        //        return base.GenerateOrderNumber(submitOrderModel, portalId = null);
        //    }
        //}

        private int GetExternalId(int? userId)
        {
            if (IsNotNull(userId))
            {
                IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();
                IZnodeRepository<ZnodeAccount> _Account = new ZnodeRepository<ZnodeAccount>();
                int ExternalId = Convert.ToInt32(_userRepository.Table.Where(x => x.UserId == userId).Select(x => x.ExternalId)?.FirstOrDefault());
                //int ExternalId = Convert.ToInt32(_Account.Table.Where(x => x.AccountId == AccountId).Select(x => x.ExternalId)?.FirstOrDefault());
                return ExternalId;
            }
            return 0;
        }
        public override OrdersListModel GetOrderList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            //Bind the Filter conditions for the authorized portal access.
            BindUserPortalFilter(ref filters);

            //Replace sort key name.
            if (HelperUtility.IsNotNull(sorts))
            {
                ReplaceSortKeys(ref sorts);
            }

            int userId = 0;
            GetUserIdFromFilters(filters, ref userId);

            int fromAdmin = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase));
            ReplaceFilterKeys(ref filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IList<OrderModel> list = GetOrderList(pageListModel, userId, fromAdmin);
            OrdersListModel orderListModel = new OrdersListModel();
            if (fromAdmin != 1)
            {
                int ExternalId = GetExternalId(userId);
                orderListModel.Custom5 = ExternalId;
                orderListModel.PageSize = pageListModel.PagingLength;
                orderListModel.PageIndex = pageListModel.PagingStart;
                ERPInitializer<OrdersListModel> _erpInc = new ERPInitializer<OrdersListModel>(orderListModel, "OrderList");
                if (_erpInc.Result != null)
                {
                    orderListModel = (OrdersListModel)_erpInc.Result;
                    /*Nivi Code start for Order not opening in Order history.*/
                    orderListModel.Orders.ForEach(x =>
                    {

                        list.ForEach(y =>
                        {

                            if (x.OmsOrderId == y.OmsOrderId)
                            {

                                y.OrderNumber = x.OrderNumber;

                            }
                        });
                    });
                    /*Nivi Code end for Order not opening in Order history.*/
                }
                else
                {
                    orderListModel = new OrdersListModel() { Orders = list?.ToList() };
                }
            }
            else
            {
                orderListModel = new OrdersListModel() { Orders = list?.ToList() };
            }
            GetCustomerName(userId, orderListModel);

            orderListModel.BindPageListModel(pageListModel);
            return orderListModel;
        }
        private void GetIsHistoryUserReceipt(FilterCollection filters, ref string isHistoryUserReceipt)
        {
            isHistoryUserReceipt = Convert.ToString(filters?.Find(x => string.Equals(x.FilterName, "ishistoryuserreceipt", StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, "IsHistoryUserReceipt", StringComparison.CurrentCultureIgnoreCase));
        }
        public override OrderModel GetOrderById(int orderId, FilterCollection filters, NameValueCollection expands)
        {
            try
            {
                OrderModel orderModel = new OrderModel();
                orderModel.OmsOrderId = orderId;
                string isHistoryUserReceipt = "0";
                GetIsHistoryUserReceipt(filters, ref isHistoryUserReceipt);
                //Variable to check method call from receipt or from other recource.
                bool isFromOrderReceipt = String.IsNullOrEmpty(expands.Get(ExpandKeys.IsFromOrderReceipt));
                bool isFromReOrder = string.IsNullOrEmpty(expands.Get(ExpandKeys.IsFromReOrder));
                bool isOrderHistory = !String.IsNullOrEmpty(expands.Get(ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString()));
                // if (isFromReOrder && !isOrderHistory && !isFromOrderReceipt)
                if (isHistoryUserReceipt == "1")
                {
                    int userId = 0;
                    GetUserIdFromFilters(filters, ref userId);
                    int ExternalId = GetExternalId(userId);
                    orderModel.Custom5 = ExternalId.ToString();
                    ERPInitializer<OrderModel> _erpInc = new ERPInitializer<OrderModel>(orderModel, "OrderInformation");
                    if (_erpInc.Result != null)
                    {
                        orderModel = (OrderModel)_erpInc.Result;
                    }
                }
                else
                {
                    orderModel = GetOrderByOrderDetails(orderId, string.Empty, filters, expands);

                    if (IsNotNull(orderModel?.OrderLineItems) && orderModel.OrderLineItems.Count > 0)
                    {
                        orderModel.OrderLineItems = orderHelper.FormalizeOrderLineItems(orderModel);
                    }
                }
                return orderModel;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("order history" + ex.StackTrace, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return null;
            }
        }
        private static void WriteTextStorage(string fileData, string filePath, Mode fileMode)
        {
            try
            {
                // Create directory if not exists.
                string logFilePath = filePath;
                FileInfo fileInfo = new FileInfo(logFilePath);
                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                // Check file write mode and write content.
                if (Equals(fileMode, Mode.Append))
                {
                    File.AppendAllText(logFilePath, fileData);
                }
                else
                {
                    File.WriteAllText(logFilePath, fileData);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                throw;
            }
        }
        public override OrderModel CreateOrder(ShoppingCartModel model)
        {

            if (IsNull(model))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, "Shopping cart model cannot be null.");
            }

            if (IsAllowedTerritories(model))
            {
                throw new ZnodeException(ErrorCodes.AllowedTerritories, "Some shopping cart itme not allowed to ship in selected country.");
            }

            SubmitOrderModel submitOrderModel = new SubmitOrderModel();

            ParameterModel portalId = new ParameterModel() { Ids = Convert.ToString(model.PortalId) };
            //model.CurrencyCode = "en-US";
            //Get generated unique order number on basis of current date.
            submitOrderModel.OrderNumber = !string.IsNullOrEmpty(model.OrderNumber) ? model.OrderNumber : GenerateOrderNumber(submitOrderModel, portalId);
            //submitOrderModel.OrderNumber = GenerateOrderNumber(submitOrderModel, portalId);
            int OmsOrderId = 0;
            OrderModel ordModel = null;
            try
            {
                model.ShoppingCartItems.ForEach(x =>
                {
                    string[] stringArray = x.Custom3?.Split('-');
                    string EstimateDate = "", ColorNum = "";
                    int stockQty = -1;
                    if (stringArray.Length > 1)
                    {
                        int Needdays = Convert.ToInt32(stringArray[0]);//EstimateDate
                        ZnodeLogging.LogMessage("AWCTORDERSERVICE Product+" + x.ProductName + "NEEDDAYS="+Needdays, "EstimateDate", TraceLevel.Error);
                        //EstimateDate = DateTime.Now.AddDays(Needdays).ToString("MM/dd/yyyy");
                        /*Nivi Code to exclude sat and sun in estimated ship date*/
                        if ((Equals(DateTime.Now.AddDays(Needdays).DayOfWeek, DayOfWeek.Saturday)))
                        {

                            EstimateDate = DateTime.Now.AddDays(Needdays + 2).ToString("MM/dd/yyyy");
                        }
                        else if ((Equals(DateTime.Now.AddDays(Needdays).DayOfWeek, DayOfWeek.Sunday)))
                        {
                            EstimateDate = DateTime.Now.AddDays(Needdays + 1).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            EstimateDate = DateTime.Now.AddDays(Needdays).ToString("MM/dd/yyyy");
                        }
                        ZnodeLogging.LogMessage("AWCTORDERSERVICE Product+" + x.ProductName + "EstimateDate=" + EstimateDate, "EstimateDate", TraceLevel.Error);
                        if (stringArray.Length > 2)
                        {
                            stockQty = Convert.ToInt32(stringArray[3]);
                            if (x.Quantity > stockQty)
                            {
                                //Needdays = Convert.ToInt32(stringArray[2]);
                                int Overridedays = Convert.ToInt32(stringArray[2]);
                                //EstimateDate = DateTime.Now.AddDays(Needdays).ToString("MM/dd/yyyy");
                                if ((Equals(DateTime.Now.AddDays(Overridedays).DayOfWeek, DayOfWeek.Saturday)))
                                {

                                    EstimateDate = DateTime.Now.AddDays(Overridedays + 2).ToString("MM/dd/yyyy");
                                }
                                else if ((Equals(DateTime.Now.AddDays(Overridedays).DayOfWeek, DayOfWeek.Sunday)))
                                {
                                    EstimateDate = DateTime.Now.AddDays(Overridedays + 1).ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    EstimateDate = DateTime.Now.AddDays(Overridedays).ToString("MM/dd/yyyy");
                                }
                                ZnodeLogging.LogMessage("AWCTORDERSERVICE Product+" + x.ProductName + "OverRideDays=" + Overridedays, " Override estimate date="+ EstimateDate + "EstimateDate", TraceLevel.Error);
                            }
                        }
                        ColorNum = Convert.ToString(stringArray[1]);//ColorValue
                        x.Custom3 = EstimateDate + "-" + ColorNum;

                    }
                });
                ordModel = SaveOrder(model, submitOrderModel);
                OmsOrderId = ordModel.OmsOrderId;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("CreateOrder inner.Error" + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return ordModel;


        }


        public override OrderModel SaveOrder(ShoppingCartModel model, SubmitOrderModel updateordermodel = null)
        {
            model.CustomShippingCost = model.ShippingCost;
            return base.SaveOrder(model, updateordermodel);
        }
        public override IZnodeCheckout SetShoppingCartDataToCheckout(IZnodeCheckout checkout, ShoppingCartModel model)
        {

            string Custom5 = model.Custom5;

            IZnodeCheckout zcheckout = base.SetShoppingCartDataToCheckout(checkout, model);
            try
            {
                string discount = zcheckout.ShoppingCart.Custom3;
                if (model.CSRDiscountDescription == null && Custom5 != "Remove")
                {
                    model.CSRDiscountDescription = discount;
                }
                if (model.ShippingAddress.CountryName == "CA")
                {
                    model.TotalAdditionalCost = Convert.ToDecimal(model.ShoppingCartItems.Where(a => a.Custom2 == "True").Sum(x => (x.Quantity * x.UnitPrice * 18) / 100));
                }

                zcheckout.ShoppingCart.Custom2 = Convert.ToString(model.Custom2);
                zcheckout.ShoppingCart.Custom3 = Convert.ToString(model.Custom3);
                zcheckout.ShoppingCart.Custom4 = Convert.ToString(model.CSRDiscountDescription);
                zcheckout.ShoppingCart.Custom5 = model.TotalAdditionalCost.GetValueOrDefault().ToString("#.##");
                // zcheckout.ShoppingCart.SalesTax = model.TotalAdditionalCost.GetValueOrDefault();
                if (model.ShippingAddress.CountryName == "CA")
                {
                    zcheckout.ShoppingCart.SalesTax = model.TotalAdditionalCost.GetValueOrDefault();
                }
                zcheckout.ShoppingCart.Custom1 = Custom5;
                //zcheckout.ShoppingCart.CustomShippingCost = model.ShippingCost;
                // Do the cart calculation
                zcheckout.ShoppingCart.AddressCarts.ForEach(x =>
                {

                    x.Shipping = string.IsNullOrEmpty(x.Shipping.ShippingName) ? new Libraries.ECommerce.Entities.ZnodeShipping
                    {
                        ShippingID = checkout.ShoppingCart.Shipping.ShippingID,
                        ShippingName = checkout.ShoppingCart.Shipping.ShippingName,
                        ShippingCountryCode = checkout?.ShoppingCart?.Shipping.ShippingCountryCode
                    } : x.Shipping;
                    x.IsAnyPromotionApplied = false;
                    x.Payment = checkout.ShoppingCart.Payment;
                    x.PortalId = checkout.PortalID;
                    x.UserId = checkout.ShoppingCart.UserId;
                    x.CurrencyCode = checkout.ShoppingCart.CurrencyCode;
                    x.OrderId = checkout.ShoppingCart.OrderId;
                    x.IsCchCalculate = checkout.ShoppingCart.IsCchCalculate;
                    x.ReturnItemList = checkout.ShoppingCart.ReturnItemList;
                    x.OrderDate = checkout.ShoppingCart.OrderDate;
                    x.CustomShippingCost = model.ShippingCost;
                    x.Custom1 = Custom5;
                    x.Calculate();

                });

            }
            catch (Exception ex)
            {

                ZnodeLogging.LogMessage("SetShoppingCartDataToCheckout .Error" + ex.Message, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                zcheckout.ShoppingCart.Custom5 = model.TotalAdditionalCost.GetValueOrDefault().ToString();
                zcheckout.ShoppingCart.SalesTax = model.TotalAdditionalCost.GetValueOrDefault();
            }

            return zcheckout;
        }
        private string ShowOrderAdditionalDetails(string receiptContent, string customData)
        {
            if (!string.IsNullOrEmpty(customData))
            {
                return receiptContent.Replace("#FeedBack#", GenerateOrderAdditionalInfoTemplate(customData));
            }
            else
            {
                return receiptContent;
            }
        }
        //to generate order additional information template
        private string GenerateOrderAdditionalInfoTemplate(string customData)
        {
            string template = string.Empty;
            try
            {
                Dictionary<string, string> CustomDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(customData);
                if (HelperUtility.IsNotNull(CustomDict))
                {
                    template = ("<b>Additional Information</b>");

                    if (CustomDict.ContainsKey("ProductName"))
                    {
                        template += $" <br />Product will be used by  { CustomDict["ProductName"]}";
                    }

                    if (CustomDict.ContainsKey("RecipientName"))
                    {
                        template += $" <br />Recipient of the product {CustomDict["RecipientName"]}";
                    }

                    if (CustomDict.ContainsKey("ApproverManager"))
                    {
                        template += $" <br />Approving Manager {CustomDict["ApproverManager"]}";
                    }

                    if (CustomDict.ContainsKey("ProjectName"))
                    {
                        template += $" <br />Project Name {CustomDict["ProjectName"]}";
                    }

                    if (CustomDict.ContainsKey("EventDate"))
                    {
                        template += $" <br />Event Date {CustomDict["EventDate"]}";
                    }

                    if (CustomDict.ContainsKey("InHandsDate"))
                    {
                        template += $" <br />In Hands Date {CustomDict["InHandsDate"]}";
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return template;
        }
        public override string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate, out bool isEnableBcc)
        {
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                {
                    item.PersonaliseValueList.Remove("AllocatedLineItems");
                }

                if (item.PersonaliseValuesDetail != null)
                {
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
                }
            }

            AWCTZnodeOrderReceipt receipt = new AWCTZnodeOrderReceipt(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, order.Custom1);
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));

            }
            isEnableBcc = false;
            return string.Empty;
        }
        private string GetFilename(ShoppingCartModel model)
        {
            string filename = string.Empty;
            string Path = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["SVGPath"].ToString();
            try
            {
                List<ShoppingCartItemModel> _truecoloritemlist = model.ShoppingCartItems.Where(p => p.PersonaliseValuesList.Count > 0).ToList();
                if (_truecoloritemlist != null && _truecoloritemlist.Count > 0)
                {
                    foreach (ShoppingCartItemModel item in _truecoloritemlist)
                    {
                        object s = item.PersonaliseValuesList.Where(i => i.Key == "filename").Select(i => i.Value).First();
                        filename += string.Join(",", Path + s);
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return filename;
        }

        public bool SendOrderReceipt(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, string attachmenthPath)
        {
            bool isSuccess = false;
            try
            {
                //This method is used to send an email.
                ZnodeEmail.SendEmail(portalId, userEmailId, senderEmail, bccEmailId, subject, receiptHtml, true, attachmenthPath);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Can not send the Order Receipt. Please verify the SMTP setting.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error, ex);
            }
            return isSuccess;
        }

        private void ReplaceFilterKeys(ref FilterCollection filters)
        {
            foreach (FilterTuple tuple in filters)
            {
                if (tuple.Item1 == Engine.Services.Constants.FilterKeys.OrderDateWithTime) { ReplaceFilterKeyName(ref filters, Engine.Services.Constants.FilterKeys.OrderDateWithTime, Engine.Services.Constants.FilterKeys.OrderDate); }
            }
        }

        //public override OrderModel GetOrderById(int orderId, FilterCollection filters, NameValueCollection expands)
        //{

        //    bool isOrderHistory = String.IsNullOrEmpty(expands.Get(ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString()));
        //    if(!isOrderHistory)
        //     return base.GetOrderById(orderId, filters, expands);
        //    else
        //    {
        //        OrderModel orderModel = null;
        //        return orderModel;
        //    }
        //}
    }
}
