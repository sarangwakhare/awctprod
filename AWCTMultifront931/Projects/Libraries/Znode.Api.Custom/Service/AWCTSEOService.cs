﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Service
{
    public class AWCTSEOService: SEOService
    {
        private readonly IMongoRepository<SeoEntity> _seoMongoRepository;
        private readonly IZnodeRepository<ZnodeCMSSEOType> _seoTypeRepository;
        private readonly IZnodeRepository<ZnodeCMSSEODetail> _seoDetailRepository;
        private readonly IZnodeRepository<ZnodeCMSSEODetailLocale> _seoDetailLocaleRepository;

        #region Public Contructor
        public AWCTSEOService()
        {           
            _seoMongoRepository = new MongoRepository<SeoEntity>(GetCatalogVersionId());
            _seoTypeRepository = new ZnodeRepository<ZnodeCMSSEOType>();
            _seoDetailRepository = new ZnodeRepository<ZnodeCMSSEODetail>();
            _seoDetailLocaleRepository = new ZnodeRepository<ZnodeCMSSEODetailLocale>();
        }
        #endregion
        //public override SEODetailsModel GetPublishSEODetails(int seoDetailId, string seoType, int localeId, int portalId, string seocode)
        //{
        //    ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("Input Parameter seoDetailId, seoType,localeId,portalId,seocode:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { seoDetailId, seoType, localeId, portalId, seocode });

        //    if (seocode == "NaN")
        //        seocode = string.Empty;

        //    if (seoType.Equals(ZnodeConstant.Product, StringComparison.InvariantCultureIgnoreCase))
        //        seoType = ZnodeConstant.Product;

        //    List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
        //    mongoQuery.Add(Query.And(Query<SeoEntity>.EQ(pr => pr.SEOCode, seocode),
        //                          Query<SeoEntity>.EQ(pr => pr.PortalId, portalId),
        //                       Query<SeoEntity>.EQ(pr => pr.SEOTypeName, seoType),
        //                       Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId)));

        //    SeoEntity publishSEOList = _seoMongoRepository.GetEntity(Query.And(mongoQuery));
        //    ZnodeLogging.LogMessage("SEO publishSEOList SEOUrl" + publishSEOList.SEOUrl, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
        //    ZnodeLogging.LogMessage("SEO publishSEOList VersionId" + publishSEOList.VersionId, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
        //    SEODetailsModel model = IsNotNull(publishSEOList) ? publishSEOList.ToModel<SEODetailsModel>() : new SEODetailsModel();

        //    return model;
        //}

        //Get the SEO details.
        public override SEODetailsModel GetSEODetailsBySEOCode(string seoCode, int seoTypeId, int localeId, int portalId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter seoCode, seoTypeId, localeId,portalId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { seoCode, seoTypeId, localeId, portalId });

            //Check for User Portal Access.
            CheckUserPortalAccess(portalId);

            if (string.IsNullOrEmpty(seoCode))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorItemIdLessThan1);
            /*Nivi Code start for Seo details not displaying in admin*/
            //replace the ASCII value with its equivalent character
            //seoCode = seoCode.Replace("038", "&");
            /*Nivi Code end for Seo details not displaying in admin*/

            //If LocaleId is less than 1 get default locale.
            if (localeId < 1)
                localeId = GetDefaultLocaleId();

            //Get the SEO details form SEO detail and SEO detail locale table.
            SEODetailsModel model = GetSEOAndLocaleDetailInformation(seoCode, seoTypeId, localeId, portalId);

            //Get SEO details for default Locale if model returns null for locale Id.
            model = GetSEODetailsIfNullforSEOCode(seoCode, seoTypeId, portalId, model);

            //Maps the field.
            model.SEOTypeName = _seoTypeRepository.Table.Where(x => x.CMSSEOTypeId == seoTypeId).Select(x => x.Name).FirstOrDefault();
            model.CMSSEOTypeId = seoTypeId;
            model.ItemName = GetSeoTypeItemName(seoCode, seoTypeId, localeId, portalId);
            model.LocaleId = (localeId > 0) ? localeId : model.LocaleId;
            model.OldSEOURL = model.SEOUrl;
            model.PimProductId = Convert.ToInt32((new ZnodeRepository<ZnodePublishProduct>()).Table.FirstOrDefault(a => a.PublishProductId == model.SEOId)?.PimProductId);
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return model;
        }

        private SEODetailsModel GetSEOAndLocaleDetailInformation(string seoCode, int seoTypeId, int localeId, int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter seoCode,seoTypeId,localeId,portalId", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { seoCode, seoTypeId, localeId, portalId });
            if (portalId < 1)
            {
                var _seoDetail = _seoDetailRepository.Table.FirstOrDefault(a => a.SEOCode == seoCode && a.CMSSEOTypeId == seoTypeId);
                ZnodeLogging.LogMessage("seoDetail:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { _seoDetail });
                portalId = (IsNotNull(_seoDetail) ? Convert.ToInt32(_seoDetail.PortalId) : portalId);
                ZnodeLogging.LogMessage("portalId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId });

            }

            return (from seoDetail in _seoDetailRepository.Table
                    from seoDetailLocale in _seoDetailLocaleRepository.Table
                    .Where(seoDetailLocale => seoDetail.CMSSEODetailId == seoDetailLocale.CMSSEODetailId && seoDetailLocale.LocaleId == localeId).DefaultIfEmpty()
                    where seoDetail.SEOCode == seoCode && seoDetail.PortalId == portalId && seoDetail.CMSSEOTypeId == seoTypeId
                    select new SEODetailsModel()
                    {
                        CMSSEODetailId = seoDetail.CMSSEODetailId,
                        IsRedirect = (seoDetail.IsRedirect == null) ? false : seoDetail.IsRedirect,
                        CMSSEOTypeId = seoDetail.CMSSEOTypeId,
                        LocaleId = localeId,
                        MetaInformation = seoDetail.MetaInformation,
                        SEOId = seoDetail.SEOId,
                        SEODescription = !Equals(seoDetailLocale, null) ? seoDetailLocale.SEODescription : string.Empty,
                        SEOKeywords = !Equals(seoDetailLocale, null) ? seoDetailLocale.SEOKeywords : string.Empty,
                        SEOTitle = !Equals(seoDetailLocale, null) ? seoDetailLocale.SEOTitle : string.Empty,
                        SEOUrl = !Equals(seoDetailLocale, null) ? seoDetail.SEOUrl : string.Empty,
                        CanonicalURL = !Equals(seoDetailLocale, null) ? seoDetailLocale.CanonicalURL : string.Empty,
                        RobotTag = !Equals(seoDetailLocale, null) ? seoDetailLocale.RobotTag : string.Empty,
                        PortalId = portalId
                    })?.FirstOrDefault();
        }

        //Get SEO details for default Locale if model returns null for locale Id.
        private SEODetailsModel GetSEODetailsIfNullforSEOCode(string seoCode, int seoTypeId, int portalId, SEODetailsModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter seoCode,seoTypeId,portalId", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { seoCode, seoTypeId, portalId });

            if (IsNull(model))
                model = new SEODetailsModel();
            //Get SEO details for default Locale if model returns null for locale Id.
            else if (string.IsNullOrEmpty(model.SEOUrl) && (string.IsNullOrEmpty(model.SEOTitle) && string.IsNullOrEmpty(model.SEOKeywords) && string.IsNullOrEmpty(model.SEODescription)))
            {
                ZnodeLogging.LogMessage("Parameter GetSEOAndLocaleDetailInformation", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { seoCode, seoTypeId, "Method: GetDefaultLocaleId()", portalId });
                model = GetSEOAndLocaleDetailInformation(seoCode, seoTypeId, GetDefaultLocaleId(), portalId);
            }
            return model;
        }
    }
}
