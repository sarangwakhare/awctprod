﻿using Autofac;
using Znode.Api.Client.Custom.Clients;
using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Custom.Controller;
using Znode.Api.Custom.Maps;
using Znode.Api.Custom.Service;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Custom.Service.Service;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.CustomProductModel;

namespace Znode.Api.Custom.Helper
{
    public class DependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<CustomPortalService>().As<ICustomPortalService>().InstancePerRequest();
            //  builder.RegisterType<AWCTPublishProductController>().As<PublishProductController>().InstancePerRequest();
            builder.RegisterType<AWCTPublishProductService>().As<IAWCTPublishProductService>().InstancePerRequest();
            builder.RegisterType<AWCTPublishProductClient>().As<IAWCTPublishProductClient>().InstancePerLifetimeScope();
            //  builder.RegisterType<AWCTPublishProductCache>().As<PublishProductCache>().InstancePerLifetimeScope();
            builder.RegisterType<AWCTSearchService>().As<ISearchService>().InstancePerRequest();
            builder.RegisterType<AWCTPublishProductModel>().As<PublishProductModel>().InstancePerRequest();
            builder.RegisterType<AWCTOrderService>().As<IOrderService>().InstancePerRequest();
            builder.RegisterType<AWCTWebStoreWidgetService>().As<IWebStoreWidgetService>().InstancePerRequest();
            builder.RegisterType<AWCTShoppingCartService>().As<IShoppingCartService>().InstancePerRequest();
            builder.RegisterType<AWCTZnodeOrderReceipt>().As<ZnodeOrderReceipt>().InstancePerRequest();
            builder.RegisterType<AWCTPublishProductHelper>().As<IPublishProductHelper>().InstancePerDependency();
            builder.RegisterType<AWCTCustomizedFormClient>().As<IAWCTCustomizedFormClient>().InstancePerLifetimeScope();
            builder.RegisterType<AWCTCustomizedFormService>().As<IAWCTCustomizedFormService>().InstancePerLifetimeScope();
            builder.RegisterType<AWCTAttributeSwatchHelper>().As<IAttributeSwatchHelper>().InstancePerDependency();
            builder.RegisterType<AWCTUserService>().As<IUserService>().InstancePerDependency();
            builder.RegisterType<AWCTHighlightService>().As<IHighlightService>().InstancePerDependency();
            builder.RegisterType<AWCTZnodeShoppingCart>().As<IZnodeShoppingCart>().InstancePerRequest();
            builder.RegisterType<AWCTCMSWidgetConfigurationService>().As<ICMSWidgetConfigurationService>().InstancePerRequest();
            builder.RegisterType<AWCTProductService>().As<IProductService>().InstancePerRequest();
            builder.RegisterType<AWCTPortalService>().As<IPortalService>().InstancePerRequest();
            builder.RegisterType<AWCTProductService>().As<IProductService>().InstancePerRequest();

            builder.RegisterType<AWCTPIMAttributeService>().As<IPIMAttributeService>().InstancePerRequest();
            builder.RegisterType<AWCTGlobalAttributeService>().As<IGlobalAttributeService>().InstancePerRequest();
            builder.RegisterType<AWCTPromotionService>().As<IPromotionService>().InstancePerRequest();
            builder.RegisterType<AWCTFormBuilderService>().As<IFormBuilderService>().InstancePerRequest();
            builder.RegisterType<AWCTProductFeedService>().As<IProductFeedService>().InstancePerRequest();
            builder.RegisterType<AWCTEmailTemplateService>().As<IEmailTemplateService>().InstancePerRequest();
            builder.RegisterType<AWCTSEOService>().As<ISEOService>().InstancePerRequest();

            builder.RegisterType<AWCTZnodeCheckout>().As<IZnodeCheckout>().InstancePerDependency();



            //builder.RegisterType<AWCTZnodeCheckout>().As<IZnodeCheckout>().InstancePerRequest();
            //Here override znode base code method by injecting dependancy mention as below.
            //"In CustomPortalService.cs we have override 'DeletePortal()' of znode base code".
            //builder.RegisterType<CustomPortalService>().As<IPortalService>().InstancePerRequest();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
