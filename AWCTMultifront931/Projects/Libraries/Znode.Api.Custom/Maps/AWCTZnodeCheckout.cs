﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;


namespace Znode.Api.Custom.Maps
{
    public class AWCTZnodeCheckout : ZnodeCheckout
    {
        private readonly IZnodeOrderHelper orderHelper;
        private readonly int _ShippingID = 0;

        public AWCTZnodeCheckout()
        {
            orderHelper = GetService<IZnodeOrderHelper>();
            //publishProductHelper = GetService<IPublishProductHelper>();
        }

        // Initializes a new instance of the ZNodeCheckout class.
        public AWCTZnodeCheckout(UserAddressModel userAccount, ZnodePortalCart shoppingCart)
        {

            this.UserAccount = userAccount;
            this.ShoppingCart = shoppingCart;
            orderHelper = GetService<IZnodeOrderHelper>();
            //publishProductHelper = GetService<IPublishProductHelper>();
        }

        /*Nivi Code commented for AWCT- 475 for penny issue.*/
        //public override ZnodeOrderFulfillment SubmitOrder(SubmitOrderModel model, ShoppingCartModel shoppingCartModel)
        //{
        //    int portalID = ShoppingCart.PortalID;
        //    int orderId = model?.OrderId.GetValueOrDefault() ?? 0;
        //    int orderDetailId = 0;
        //    ZnodeOrderFulfillment order = this.GetOrderFullfillment(this.UserAccount, this.ShoppingCart, portalID);
        //    /*Nivi Code start for Penny issue.*/
        //    order.Total = Convert.ToDecimal(shoppingCartModel.Total);
        //    /*Nivi Code end for Penny issue.*/
        //    order.Order.OmsOrderDetailsId = model?.OmsOrderDetailsId == null ? 0 : Convert.ToInt32(model?.OmsOrderDetailsId);

        //    if (IsNotNull(shoppingCartModel?.Payment?.PaymentStatusId))
        //    {
        //        order.PaymentStatusID = shoppingCartModel.Payment.PaymentStatusId;
        //    }

        //    if (orderId > 0)
        //    {
        //        SetOrderStateTrackingNumber(order, model);
        //    }

        //    //start transaction
        //    using (SqlConnection connection = new SqlConnection(Libraries.Data.Helpers.HelperMethods.ConnectionString))
        //    {
        //        connection.Open();     // create order object
        //        SqlTransaction transaction = connection.BeginTransaction();// Start a local transaction.

        //        try
        //        {
        //            if (orderId > 0 && !CancelExistingOrder(order, orderId))
        //            {
        //                return order;
        //            }

        //            SetOrderAdditionalDetails(order, model);

        //            bool paymentIsSuccess = SetPaymentDetails(order);
        //            order.Order.OrderNumber = model?.OrderNumber;

        //            // Add the order and line items to database
        //            order.AddOrderToDatabase(order, shoppingCartModel);
        //            if (orderId > 0)
        //            {
        //                //to save return items in data base for selected order
        //                if (IsNotNull(model?.ReturnOrderLineItems))
        //                {
        //                    this.IsSuccess = SaveReturnItems(order.Order.OmsOrderDetailsId, model.ReturnOrderLineItems);
        //                }
        //                ZnodeLogging.LogMessage($"Updated existing order for Order Id:{orderId }", ZnodeLogging.Components.OMS.ToString());
        //            }

        //            //to get current orderdetailid for verifing  order process
        //            orderDetailId = order?.Order?.OmsOrderDetailsId ?? 0;

        //            //Set Order Shipment Details to order Line Item.
        //            SetOrderShipmentDetails(order);

        //            if (paymentIsSuccess)
        //            {
        //                SetOrderDetailsToShoppingCart(order);
        //                //to apply promotion, taxes and shipping calculation 
        //                this.ShoppingCart.PostSubmitOrderProcess(orderId, shoppingCartModel.ShippingAddress.IsGuest);

        //                int? userId = this.ShoppingCart.GetUserId();

        //                //to save referral commission and gift card history
        //                this.SaveReferralCommissionAndGiftCardHistory(order, userId);

        //                //reduce product inventory this code block move out of transaction because all db call of entity type and this is sp call
        //                if (this.ManageOrderInventory(order, this.ShoppingCart))
        //                {
        //                    transaction.Commit();
        //                    this.IsSuccess = true;
        //                }
        //                else
        //                {
        //                    transaction.Rollback();
        //                    this.IsSuccess = false;
        //                }
        //            }
        //            else
        //            {
        //                //// payment submission failed so rollback transaction
        //                transaction.Rollback();
        //                this.IsSuccess = false;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ZnodeLogging.LogMessage(ex.Message, string.Empty, TraceLevel.Error, ex);

        //            if (IsNotNull(ex.InnerException))
        //            {
        //                ZnodeLogging.LogMessage(ex.InnerException.ToString(), string.Empty, TraceLevel.Error); // log exception
        //            }

        //            transaction.Rollback();
        //            this.IsSuccess = false;

        //            VerifySubmitOrderProcess(orderDetailId);

        //            throw;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }

        //    VerifySubmitOrderProcess(orderDetailId);

        //    return order;
        //}
        /*Nivi Code commented for AWCT- 475 for penny issue.*/
        public override ZnodeOrderFulfillment GetOrderFullfillment(UserAddressModel userAccount, ZnodePortalCart shoppingCart, int portalId)
        {
            ZnodeOrderFulfillment order = new ZnodeOrderFulfillment(shoppingCart);

            order.PortalId = portalId;

            SetOrderDetails(order, shoppingCart, userAccount);

            foreach (ZnodeMultipleAddressCart addressCart in shoppingCart.AddressCarts)
            {
                //For Quote
                int? shippingId = 0;
                if (addressCart.Shipping.ShippingID == 0)
                {
                    shippingId = addressCart.Shipping.ShippingID != 0 ? addressCart.Shipping.ShippingID : shoppingCart.Shipping.ShippingID;
                }
                else
                {
                    shippingId = addressCart.Shipping.ShippingID != 0 ? addressCart.Shipping.ShippingID : this._ShippingID;
                }
                AddressModel address = userAccount?.ShippingAddress;
                address = IsNull(address) || Equals(address.AddressId, 0) ? shoppingCart.Payment?.ShippingAddress : address;
                addressCart.OrderShipmentID = CreateOrderShipment(address, shippingId, userAccount?.Email);

                SetOrderLineItems(order, addressCart);
            }
            return order;
        }

        public override void SetOrderShipmentDetails(ZnodeOrderFulfillment order)
        {
            foreach (ZnodeMultipleAddressCart addressCart in ShoppingCart?.AddressCarts)
            {
                //For Quote
                if (order.OrderLineItems[0].OmsOrderShipmentId > 0 && addressCart.OrderShipmentID == 0)
                {
                    addressCart.OrderShipmentID = order.OrderLineItems[0].OmsOrderShipmentId;
                }
                // Check to get the order shipment for the item  
                if (addressCart.OrderShipmentID > 0)
                {
                    foreach (OrderLineItemModel orderLineItemModel in order.OrderLineItems?.Where(w => w.OmsOrderShipmentId == addressCart.OrderShipmentID))
                    {
                        orderHelper.GetOrderShipmentAddress(addressCart.OrderShipmentID, orderLineItemModel);
                    }
                }
            }
        }

    }
}
