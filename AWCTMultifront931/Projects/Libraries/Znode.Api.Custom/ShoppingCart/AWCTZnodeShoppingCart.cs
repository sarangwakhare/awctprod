﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Znode.Api.Custom.Service.Service;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.ShoppingCart
{
    public class AWCTZnodeShoppingCart : ZnodeShoppingCart
    {
        private readonly IPublishProductHelper publishProductHelper = GetService<IPublishProductHelper>();
        // Gets total discount of applied to the items in the shopping cart.
        public override decimal Discount
        {
            get
            {
                decimal totalDiscount = OrderLevelDiscount + ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(x => x.ExtendedPriceDiscount + x.DiscountAmount);
                /*Original Code*/
                //totalDiscount = totalDiscount + TotalPartialRefundAmount;
                /*Nivi Code start for penny issue*/
                totalDiscount = GetPriceRoundOff(totalDiscount) + GetPriceRoundOff(TotalPartialRefundAmount);
                /*Nivi Code end for penny issue*/

                if (totalDiscount > SubTotal)
                {
                    return SubTotal;
                }

                return totalDiscount;
                // return Math.Round(totalDiscount, 2);
            }
        }
        public override decimal Total
        {
            get
            {
                /*Original Code*/
                //decimal total = (SubTotal - Discount) + ShippingCost + ShippingDifference + OrderLevelTaxes - GiftCardAmount - CSRDiscount + GetAdditionalPrice();
                /*Nivi Code start for penny issue*/
                decimal total = (GetPriceRoundOff(SubTotal) - GetPriceRoundOff(Discount)) + GetPriceRoundOff(ShippingCost) + GetPriceRoundOff(ShippingDifference) + GetPriceRoundOff(OrderLevelTaxes) - GetPriceRoundOff(GiftCardAmount) - GetPriceRoundOff(CSRDiscount) + GetPriceRoundOff(GetAdditionalPrice());
                /*Nivi Code end for penny issue*/

                return total;
                //return Math.Round(total, 2);
            }
        }
        protected override ZnodeProduct GetProductDetailsV2(PublishProductModel publishProduct, int portalId, int localeId, string countryCode = null, bool isGroupProduct = false, string parentSKU = "", int userId = 0, int omsOrderId = 0, string parentSKUProductName = null, int profileId = 0, List<TaxClassRuleModel> lstTaxClassSKUs = null, List<ConfigurableProductEntity> configEntities = null)
        {
            if ((IsNull(publishProduct)))
            {
                return null;
            }

            publishProduct.ParentSEOCode = parentSKU;
            //TODO
            if (IsNull(publishProduct.RetailPrice))
            {
                GetParentProductPriceDetails(publishProduct, portalId, localeId, parentSKU, userId, profileId);
            }
            string style = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "awctstyle")?.AttributeValues;
            publishProduct.Name = publishProduct.Name.Contains(style) ? publishProduct.Name : style + " " + publishProduct.Name;

            List<AttributesSelectValuesModel> inventorySetting = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.Vendor)?.SelectValues;
            string vendorCode = inventorySetting?.Count > 0 ? inventorySetting.FirstOrDefault().Code : string.Empty;

            ZnodeProduct product = new ZnodeProduct
            {
                ProductID = publishProduct.PublishProductId,
                SEOURL = publishProduct.SEOUrl,
                Name = isGroupProduct ? parentSKUProductName : publishProduct.Name,
                SKU = isGroupProduct ? parentSKU : publishProduct.SKU,
                SalePrice = publishProduct.SalesPrice,
                RetailPrice = publishProduct.RetailPrice.GetValueOrDefault(),
                QuantityOnHand = publishProduct.Quantity.GetValueOrDefault(),
                ZNodeTieredPriceCollection = GetZnodeProductTierPrice(publishProduct),
                TaxClassID = GetTaxClassBySKU(publishProduct.SKU, countryCode, lstTaxClassSKUs),
                AddOns = publishProduct.AddOns,
                IsPriceExist = isGroupProduct ? true : IsProductPriceExist(publishProduct.SalesPrice, publishProduct.RetailPrice),
                VendorCode = vendorCode,
                IsActive = publishProduct.IsActive,
                ProductCategoryIds = new int[] { publishProduct.ZnodeCategoryIds },
                AllowedTerritories = GetProductAttributeAllowedTerritoriesValue(publishProduct, ZnodeConstant.AllowedTerritories)
            };
            if (publishProduct.Attributes?.Count > 0)
            {
                product.AllowBackOrder = GetBooleanProductAttributeValue(publishProduct, ZnodeConstant.AllowBackOrdering);
                product.FreeShippingInd = GetBooleanProductAttributeValue(publishProduct, ZnodeConstant.FreeShipping);
                product.ShipSeparately = GetBooleanProductAttributeValue(publishProduct, ZnodeConstant.ShipSeparately);
                product.MinQty = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.MinimumQuantity);
                product.MaxQty = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.MaximumQuantity);
                product.InventoryTracking = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues?.FirstOrDefault()?.Code ?? string.Empty;
                product.ShippingRuleTypeCode = publishProduct.Attributes?.Where(x => x.AttributeCode == ZnodeConstant.ShippingCost)?.FirstOrDefault()?.SelectValues?.FirstOrDefault()?.Code;
                product.BrandCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.Brand)?.SelectValues?.FirstOrDefault()?.Code ?? string.Empty;
                product.Height = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Height);
                product.Width = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Width);
                product.Length = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Length);
                product.Weight = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Weight);
                product.UOM = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.UOM)?.SelectValues?.FirstOrDefault()?.Value;
                product.Container = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ShippingContainer)?.SelectValues[0]?.Value;
                product.Size = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ShippingSize)?.SelectValues[0]?.Code;
                product.PackagingType = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PackagingType)?.SelectValues[0]?.Value;
            }

            //to apply product promotional price
            product.ApplyPromotion();
            //set configurable product attributes
            if (publishProduct?.ParentPublishProductId > 0)
            {
                List<ConfigurableProductEntity> configEntiy = configEntities?.Where(x => x.ZnodeProductId == publishProduct.ParentPublishProductId).ToList();

                if (IsNotNull(configEntiy) && configEntiy?.Count > 0)
                {
                    string SeasonName = "Collection" + " - " + publishProduct?.Attributes?.FirstOrDefault(x => x.AttributeCode == "Season")?.SelectValues[0]?.Value;
                    ;
                    product.Description = string.IsNullOrEmpty(product.Description)
                                          ? (configEntiy?.Count > 0
                                          ? SeasonName
                                           + "<br>" + string.Join("<br>", publishProduct?.Attributes?.Where(x => x.IsConfigurable && (configEntiy?.FirstOrDefault()?.ConfigurableAttributeCodes?.Contains(x.AttributeCode)).GetValueOrDefault()).OrderBy(x => x.AttributeCode).Select(x => x.AttributeName + " - " + x.SelectValues?.FirstOrDefault()?.Value).Distinct())
                                          : string.Join("<br>", publishProduct?.Attributes?.Where(x => x.IsConfigurable)?.Select(x => x.AttributeName + " - " + x.SelectValues?.FirstOrDefault()?.Value)?.Distinct()))
                                          : product.Description;
                }
                else
                {
                    product.Description = string.IsNullOrEmpty(product.Description) ? string.Join("<br>", publishProduct?.Attributes?.Where(x => x.IsConfigurable)?.Select(x => x.AttributeName + " - " + x.SelectValues?.FirstOrDefault()?.Value)?.Distinct()) : product.Description; ;
                }

            }
            //to set product attributes 
            SetProductAttributes(product, publishProduct);
            return product;
        }

        private int GetTaxClassBySKU(string sku, string countryCode, List<TaxClassRuleModel> lstTaxClassSKUs)
        {
            int? taxClassId = lstTaxClassSKUs?.Where(x => x.SKU == sku && (x.DestinationCountryCode == countryCode || x.DestinationCountryCode == null))?.FirstOrDefault()?.TaxClassId;

            return taxClassId ?? 0;
        }
        protected override void BindCartProductDetails(ShoppingCartItemModel cartModel, int publishCatalogId, int localeId, string groupIdProductAttribute = "", GlobalSettingValues groupIdPersonalizeAttribute = null)
        {
            int catalogVersionId = GetCatalogVersionId(publishCatalogId);
            ProductEntity product = publishProductHelper.GetPublishProductBySKU(string.IsNullOrEmpty(cartModel.ConfigurableProductSKUs) ? cartModel.SKU : cartModel.ConfigurableProductSKUs, publishCatalogId, localeId, catalogVersionId);
            if (IsNotNull(product))
            {
                string StyleName = product.Attributes.FirstOrDefault(y => y.AttributeCode == "awctstyle")?.AttributeValues;
                cartModel.Description = GetShortDescription(product, cartModel);
                cartModel.ProductName = cartModel.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group) ? cartModel.ProductName : StyleName + " " + product.Name;
                cartModel.GroupId = GenerateGroupId(cartModel, product, groupIdProductAttribute, groupIdPersonalizeAttribute);
            }
        }
        /*Nivi Code start for penny issue*/
        public static decimal GetPriceRoundOff(decimal value)
        {
            int roundOff = Convert.ToInt32(DefaultGlobalConfigHelper.DefaultPriceRoundOff);
            return Math.Round((value), roundOff, MidpointRounding.AwayFromZero);
        }
        /*Nivi Code end for penny issue*/

    }
}
