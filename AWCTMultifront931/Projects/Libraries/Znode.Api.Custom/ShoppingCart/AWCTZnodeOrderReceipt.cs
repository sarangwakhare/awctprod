﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Api.Custom.ShoppingCart
{
    public class AWCTZnodeOrderReceipt : ZnodeOrderReceipt
    {
        private readonly IZnodeRepository<ZnodeOmsOrderDiscount> _orderDiscountRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _omsOrderLineItemRepository;
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodeAccount> _AccountRepository;
        private string _cultureCode = string.Empty;
        private string _currencyCode = string.Empty;
        public AWCTZnodeOrderReceipt(ZnodeOrderFulfillment order, ZnodeShoppingCart shoppingCart) : base(order, shoppingCart)
        {
            _orderDiscountRepository = new ZnodeRepository<ZnodeOmsOrderDiscount>();
            _orderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _AccountRepository = new ZnodeRepository<ZnodeAccount>();
            _omsOrderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
        }

        //public override DataTable SetOrderData()
        //{
        //    int AccountId = Convert.ToInt32(_userRepository.Table.Where(x => x.UserId == Order.UserID).FirstOrDefault().AccountId);
        //    int ExternalId = Convert.ToInt32(_AccountRepository.Table.Where(x => x.AccountId == AccountId).FirstOrDefault().ExternalId);
        //    ZnodeLogging.LogMessage("AccountId-" + Convert.ToString(AccountId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("ExternalId-" + Convert.ToString(ExternalId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

        //    ZnodeLogging.LogMessage("Order.CurrencyCode-" + Convert.ToString(Order.CurrencyCode), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("Order.CultureCode-" + Convert.ToString(Order.CultureCode), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    // Create new row
        //    DataTable orderTable = CreateOrderTable();
        //    DataRow orderRow = orderTable.NewRow();
        //    IZnodeOrderHelper helper = GetService<IZnodeOrderHelper>();
        //    PortalModel portal = helper.GetPortalDetailsByPortalId(Order.PortalId);
        //    _currencyCode = "USD";//Order.CurrencyCode;
        //    _cultureCode = "en-US";//Order.CultureCode;

        //    // Additional info
        //    orderRow["SiteName"] = portal?.StoreName ?? ZnodeConfigManager.SiteConfig.StoreName;
        //    orderRow["StoreLogo"] = helper.SetPortalLogo(Order.PortalId);
        //    orderRow["ReceiptText"] = string.Empty;
        //    orderRow["CustomerServiceEmail"] = Order.Email;//FormatStringComma(portal?.CustomerServiceEmail) ?? FormatStringComma(ZnodeConfigManager.SiteConfig.CustomerServiceEmail);
        //    orderRow["CustomerServicePhoneNumber"] = Convert.ToString(ExternalId); //portal?.CustomerServicePhoneNumber.Trim() ?? ZnodeConfigManager.SiteConfig.CustomerServicePhoneNumber.Trim();
        //    orderRow["FeedBack"] = FeedbackUrl;
        //    orderRow["ShippingName"] = Order?.ShippingName;
        //    //orderRow["ExpirationDate"] = (Order?.Custom1);

        //    //Payment info
        //    if (!String.IsNullOrEmpty(Order.PaymentTrancationToken))
        //    {
        //        orderRow["CardTransactionID"] = Order.PaymentTrancationToken;
        //        orderRow["CardTransactionLabel"] = Admin_Resources.LabelTransactionId;
        //    }

        //    orderRow["PaymentName"] = Order.PaymentDisplayName;

        //    if (!String.IsNullOrEmpty(Order.PurchaseOrderNumber))
        //    {
        //        orderRow["PONumber"] = Order.PurchaseOrderNumber;
        //        orderRow["PurchaseNumberLabel"] = Admin_Resources.LabelPurchaseOrderNumber;
        //    }

        //    //Customer info
        //    orderRow["OrderId"] = Order?.Order?.OrderNumber;
        //    orderRow["OrderDate"] = Order.OrderDateWithTime;

        //    orderRow["BillingAddress"] = GetOrderBillingAddress(Order.BillingAddress);
        //    orderRow["PromotionCode"] = Order.CouponCode;

        //    var addresses = ((ZnodePortalCart)ShoppingCart).AddressCarts;
        //    orderRow["ShippingAddress"] = addresses.Count > 1 ? Admin_Resources.MessageKeyShippingMultipleAddress : GetOrderShipmentAddress(Order.OrderLineItems.FirstOrDefault().ZnodeOmsOrderShipment);

        //    orderRow["TotalCost"] = GetFormatPriceWithCurrency(Order.Total);
        //    if (Order.AdditionalInstructions != null)
        //    {
        //        orderRow["AdditionalInstructions"] = Order.AdditionalInstructions;
        //        orderRow["AdditionalInstructLabel"] = Admin_Resources.LabelAdditionalNotes;
        //    }

        //    // Add rows to order table
        //    orderTable.Rows.Add(orderRow);
        //    return orderTable;
        //}

        public override DataTable CreateOrderTable()
        {
            DataTable orderTable = new DataTable();
            // Additional info
            orderTable.Columns.Add("SiteName");
            orderTable.Columns.Add("StoreLogo");
            orderTable.Columns.Add("ReceiptText");
            orderTable.Columns.Add("CustomerServiceEmail");
            orderTable.Columns.Add("CustomerServicePhoneNumber");
            orderTable.Columns.Add("FeedBack");
            orderTable.Columns.Add("AdditionalInstructions");
            orderTable.Columns.Add("AdditionalInstructLabel");

            // Payment info
            orderTable.Columns.Add("CardTransactionID");
            orderTable.Columns.Add("CardTransactionLabel");
            orderTable.Columns.Add("PaymentName");

            orderTable.Columns.Add("PONumber");
            orderTable.Columns.Add("PurchaseNumberLabel");

            // Customer info
            orderTable.Columns.Add("OrderId");
            orderTable.Columns.Add("OrderDate");
            orderTable.Columns.Add("UserId");
            orderTable.Columns.Add("BillingAddress");
            orderTable.Columns.Add("ShippingAddress");
            orderTable.Columns.Add("PromotionCode");
            orderTable.Columns.Add("TotalCost");
            // Returned total cost
            orderTable.Columns.Add("ReturnedTotalCost");
            orderTable.Columns.Add("StyleSheetPath");

            orderTable.Columns.Add("ShippingName");
            orderTable.Columns.Add("TrackingNumber");

            orderTable.Columns.Add("Custom1");
            orderTable.Columns.Add("Message");
            return orderTable;
        }
        public override string CreateOrderReceipt(string template)
        {
            if (string.IsNullOrEmpty(template))
            {
                return template;
            }

            //order to bind order details in data tabel
            System.Data.DataTable orderTable = SetOrderData();
            orderTable.Rows[0]["CustomerServiceEmail"] = Order.Email;
            int UserID = _orderDetailRepository.Table.FirstOrDefault(x => x.OmsOrderId == Order.OrderID).UserId;
            string AccountNo = _userRepository.Table.FirstOrDefault(x => x.UserId == Order.UserID).ExternalId;
            orderTable.Rows[0]["CustomerServicePhoneNumber"] = AccountNo;
            /*Original Code for dance*/
            //orderTable.Rows[0]["Custom1"] = Order.Custom2 == "S" ? "I want my items faster. Ship them as they become available (Additional shipping cost may apply)" : "Group my items into as few shipment as possible.";
            orderTable.Rows[0]["Custom1"] = Order.Custom2 == "S" ? "Ship them as they become available (Additional shipping cost may apply)" : "Group my items into as few shipments as possible.";
            orderTable.Rows[0]["Message"] = Order.Custom1;
            orderTable.Rows[0]["OrderDate"] = Order.OrderDateWithTime;
            //create order line Item
            DataTable orderlineItemTable = CreateOrderLineItemTable();

            //order to bind order amount details in data tabel
            DataTable orderAmountTable = SetOrderAmountData();

            DataTable DepositTable = SetDepositValue();

            //create multiple Address
            DataTable multipleAddressTable = CreateOrderAddressTable();

            //create multiple tax address
            DataTable multipleTaxAddressTable = CreateOrderTaxAddressTable();

            //bind line item data
            BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable);

            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(template);

            // Parse order table
            receiptHelper.Parse(orderTable.CreateDataReader());

            // Parse order line items table
            receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
            foreach (DataRow address in multipleAddressTable.Rows)
            {
                // Parse OrderLineItem
                DataView filterData = orderlineItemTable.DefaultView;

                List<DataTable> group = filterData.ToTable().AsEnumerable()
                .GroupBy(r => new { Col1 = r["GroupId"] })
                .Select(g => g.CopyToDataTable()).ToList();

                filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.ParseWithGroup("LineItems" + address["OmsOrderShipmentID"], group);

                //Parse Tax based on order shipment
                DataView amountFilterData = multipleTaxAddressTable.DefaultView;
                amountFilterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.Parse($"AmountLineItems{address["OmsOrderShipmentID"]}", amountFilterData.ToTable().CreateDataReader());
            }
            // Parse order amount table
            receiptHelper.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());
            receiptHelper.Parse("DepositAmountLineItems", DepositTable.CreateDataReader());
            //Replace the Email Template Keys, based on the passed email template parameters.

            // Return the HTML output
            return receiptHelper.Output;
        }
        //to create order amount table
        public virtual DataTable CreateDepositTable()
        {
            DataTable orderAmountTable = new DataTable();
            orderAmountTable.Columns.Add("DTitle");
            orderAmountTable.Columns.Add("DAmount");
            return orderAmountTable;
        }
        public void BuildDepositAmountTable(string title, decimal amount, DataTable orderAmountTable)
        {

            DataRow row = orderAmountTable.NewRow();
            row["DTitle"] = title;
            row["DAmount"] = GetFormatPriceWithCurrency(amount);

            orderAmountTable.Rows.Add(row);

        }
        /*Nivi Code start for Penny issue*/
        public void BuildDepositAmountTableCC(string title, decimal amount, DataTable orderAmountTable)
        {

            DataRow row = orderAmountTable.NewRow();
            row["DTitle"] = title;
            decimal price = Math.Truncate(amount * 100) / 100;
            row["DAmount"] = GetFormatPriceWithCurrency(price);

            orderAmountTable.Rows.Add(row);

        }
        /*Nivi Code end for Penny issue*/
        private DataTable SetDepositValue()
        {
            DataTable depositTable = CreateDepositTable();
            if (ShoppingCart.Payment.PaymentSettingId == 16)
            {
                BuildDepositAmountTable("Deposit Paid", Order.Total, depositTable);
                BuildDepositAmountTable("<Span style='color:Red'>* Balance must be paid before order ships. Balance Due</Span>", 0, depositTable);

            }
            else if (ShoppingCart.Payment.PaymentSettingId == 12)
            {
                BuildDepositAmountTable("Deposit Paid", Order.Total / 2, depositTable);
                /*Original Code*/
                //BuildDepositAmountTable("<Span style='color:Red'>* Balance must be paid before order ships. Balance Due</Span>", Order.Total / 2, depositTable);
                /*Nivi Code start for Penny issue*/
                decimal DepositPaid = Order.Total / 2;
                BuildDepositAmountTableCC("<Span style='color:Red'>* Balance must be paid before order ships. Balance Due</Span>", Order.Total - DepositPaid, depositTable);
                /*Nivi Code end for Penny issue*/
            }
            return depositTable;
        }

        //to get shipping address--OrderReceipt
        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "," + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}{"<br />"}" : Order.ShippingAddress.CompanyName;
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{orderShipment.ShipToCompanyName}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{", "}{orderShipment.ShipToStateCode}{", "}{orderShipment.ShipToCountry}{" "}{orderShipment.ShipToPostalCode}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}";
            }
            return string.Empty;
        }
        //to get Billing address--OrderReceipt
        public override string GetOrderBillingAddress(AddressModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street2 = string.IsNullOrEmpty(orderBilling.Address2) ? string.Empty : "," + orderBilling.Address2;
                orderBilling.CompanyName = IsNotNull(orderBilling?.CompanyName) ? $"{orderBilling?.CompanyName}{"<br />"}" : Order.BillingAddress.CompanyName;
                //return $"{orderBilling?.FirstName}{" "}{orderBilling?.LastName}{orderBilling?.CompanyName}{"<br />"}{orderBilling.Address1}{street2}{"<br />"}{ orderBilling.CityName}{"<br />"}{orderBilling.StateName}{"<br />"}{orderBilling.PostalCode}{"<br />"}{orderBilling.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.PhoneNumber}";
                return $"{orderBilling?.FirstName}{" "}{orderBilling?.LastName}{"<br />"}{orderBilling.Address1}{street2}{"<br />"}{ orderBilling.CityName}{", "}{orderBilling.StateName}{", "}{orderBilling.CountryName}{" "}{orderBilling.PostalCode}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.PhoneNumber}";
            }
            return string.Empty;
        }
        public override DataTable SetOrderAmountData()
        {

            string VolumeDiscount = ShoppingCart.Custom4;
            ZnodeLogging.LogMessage("VolumeDiscount Before-" + VolumeDiscount, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            if (VolumeDiscount == "-1")
            {
                VolumeDiscount = string.Empty;
            }
            ZnodeLogging.LogMessage("VolumeDiscount after-" + VolumeDiscount, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);

            int discount = 0;
            string volumeCode = "", CouponCode = "";
            decimal CouponDiscount = 0, VolumeDiscountAmt = 0;
            if (ShoppingCart.Custom5 == "Remove" || Convert.ToString(ShoppingCart.Custom1) == "Remove")
            {
                VolumeDiscount = string.Empty;
            }
            if (VolumeDiscount != string.Empty)
            {
                try
                {
                    discount = Convert.ToInt32(Convert.ToDecimal(VolumeDiscount) * 100);
                }
                catch (Exception)
                {
                    discount = 0;
                }
                IEnumerable<ZnodeShoppingCartItem> shoppingCartItems = ((ZnodePortalCart)ShoppingCart).AddressCarts.SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>());
                string ExclusionCategory = ConfigurationManager.AppSettings["ExclusionCategory"].ToString();// "Value,Clearance";
                decimal subtotal = Convert.ToDecimal(shoppingCartItems.Where(x => !ExclusionCategory.Split(',').Contains(x.Custom1)).Sum(x => x.Quantity * x.UnitPrice));
                VolumeDiscountAmt = Convert.ToDecimal((subtotal * discount) / 100);
            }

            // VolumeDiscountAmt = Math.Round(VolumeDiscountAmt, 2);  
            /*Original Code*/
            //decimal CouponDis = Convert.ToDecimal(Math.Round((ShoppingCart.Discount - VolumeDiscountAmt), 2));
            /*Nivi Code start for penny issue*/
            int roundOff = Convert.ToInt32(DefaultGlobalConfigHelper.DefaultPriceRoundOff);
            decimal CouponDis = Convert.ToDecimal(Math.Round((ShoppingCart.Discount - VolumeDiscountAmt), roundOff, MidpointRounding.AwayFromZero));
            /*Nivi Code start for penny issue*/
            if (discount > 0)
            {
                volumeCode = "(" + discount.ToString() + "%)";
            }
            if (CouponDis > 0)
            {
                CouponDiscount = CouponDis; //Order.Discount - //discountAmt;
                CouponCode = "(" + Order.CouponCode + ")";

            }
            // Create order amount table
            DataTable orderAmountTable = CreateOrderAmountTable();
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal")) ? Admin_Resources.LabelSubTotal : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal").ToString(), Order.SubTotal, orderAmountTable);

            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), Order.PST, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), Order.GST, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount")) ? "Volume Discount" + volumeCode : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount").ToString(), -VolumeDiscountAmt, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount")) ? "Coupon Discount" + CouponCode : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleCSRDiscountAmount").ToString(), -CouponDiscount, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax")) ? "18% Canadian Duty" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax").ToString(), Order.SalesTax, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost")) ? "Shipping" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost").ToString(), (Order.ShippingCost + Order.ShippingDifference), orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount")) ? Admin_Resources.LabelGiftCardAmount : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount").ToString(), -Order.GiftCardAmount, orderAmountTable);
            BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost")) ? "Tax" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost").ToString(), Order.TaxCost, orderAmountTable);
            //BuildOrderAmountTable(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax").ToString(), Order.SalesTax, orderAmountTable);


            return orderAmountTable;
        }
        public DataTable GetSizesDisplayOrder(string sizeliststr)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@SizeListString", sizeliststr, ParameterDirection.Input, SqlDbType.VarChar);
            DataSet data = objStoredProc.GetSPResultInDataSet("AWCT_GetSizeDisplayOrder");
            DataTable sizeDisplayOrderTable = data.Tables[0];
            return sizeDisplayOrderTable;
        }
        public override void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable)
        {
            List<OrderLineItemModel> OrderLineItemList = Order?.OrderLineItems.GroupBy(p => new { p.OmsOrderShipmentId }).Select(g => g.First()).ToList();
            IEnumerable<OrderShipmentModel> orderShipments = OrderLineItemList.Select(s => s.ZnodeOmsOrderShipment);

            int shipmentCounter = 1;

            foreach (OrderShipmentModel orderShipment in orderShipments)
            {
                DataRow addressRow = multipleAddressTable.NewRow();

                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = $"Shipment #{shipmentCounter++}{orderShipment.ShipName}";
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OmsOrderShipmentID"] = orderShipment.OmsOrderShipmentId;
                int counter = 0;
                /*Changes for Size Display Order*/
                List<string> sizelist = new List<string>();
                foreach (OrderLineItemModel lineitem in Order.OrderLineItems.Where(x => x.OmsOrderShipmentId == orderShipment.OmsOrderShipmentId).OrderBy(x => x.ProductName).ThenByDescending(s => s.OmsOrderLineItemsId))
                {
                    if (lineitem.OrderLineItemCollection.Count > 0)
                    {
                        OrderLineItemModel childLineItem = lineitem.OrderLineItemCollection[0];
                        childLineItem.Description.Replace("<br/>", "");
                        string[] spearator = { "<br>" };
                        string[] desc = childLineItem.Description.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                        sizelist.Add(desc[2].Replace("Awct_Size - ", "").Replace("<br/>", ""));
                    }
                }
                string sizeliststr = String.Join(",", sizelist);
                DataTable sizesWithDisplayOrder = GetSizesDisplayOrder(sizeliststr);
                /*Changes for Size Display Order*/
                foreach (OrderLineItemModel lineitem in Order.OrderLineItems.Where(x => x.OmsOrderShipmentId == orderShipment.OmsOrderShipmentId).OrderBy(x => x.ProductName).ThenByDescending(s => s.OmsOrderLineItemsId))
                {
                    IEnumerable<ZnodeShoppingCartItem> shoppingCartItems = ((ZnodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OmsOrderShipmentId).SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>());

                    lineitem.OrderLineItemCollection.RemoveAll(x => x.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns));
                    if (lineitem.OrderLineItemCollection?.Any(x => x.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)) ?? false)
                    {
                        foreach (OrderLineItemModel childLineItem in lineitem.OrderLineItemCollection)
                        {
                            ZnodeShoppingCartItem shoppingCartItem = null;
                            if (Order.Order.IsQuoteOrder)
                            {
                                shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && (!string.IsNullOrEmpty(s.SKU) ? childLineItem.Sku.Contains(s.SKU) : false) && s.OrderLineItemRelationshipTypeId.HasValue);
                                if (IsNull(shoppingCartItem))
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && childLineItem.Sku == s.Custom5 && s.OrderLineItemRelationshipTypeId.HasValue);
                                }
                            }
                            else
                            {
                                shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && s.SKU == childLineItem.Sku && s.OrderLineItemRelationshipTypeId.HasValue);
                            }

                            //Get ShoppingCartItem when GroupId is Null
                            //if (IsNull(shoppingCartItem))
                            //{
                            //    shoppingCartItem = shoppingCartItems.FirstOrDefault(m => m.Custom5.Equals(childLineItem.Sku));
                            //}
                            setGroupProductDetails(lineitem, childLineItem);

                            StringBuilder sb = new StringBuilder();
                            sb.Append(lineitem.ProductName);

                            if (!String.IsNullOrEmpty(shoppingCartItem?.Product?.DownloadLink?.Trim()))
                            {
                                sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                            }

                            if (orderLineItemTable != null)
                            {
                                string size = "", Color = "";
                                ZnodeLogging.LogMessage("Item Custom3-" + lineitem?.Custom3, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                                string[] DateColor = Convert.ToString(lineitem?.Custom3).Split('-');
                                string EstimateDate = "", ColorCode = "";
                                if (DateColor.Length > 0)
                                {
                                    EstimateDate = DateColor[0].ToString();
                                    ColorCode = DateColor[1].ToString();
                                }
                                StringBuilder PersonalizeDesc = new StringBuilder();
                                string personalizeDetails = Convert.ToString(lineitem.PersonaliseValuesDetail?.FirstOrDefault(x => x.PersonalizeCode == "OptionValue")?.PersonalizeValue);
                                if (!string.IsNullOrEmpty(personalizeDetails))
                                {
                                    if (personalizeDetails.ToString() != "PersonaliseCode[sku")
                                    {
                                        PersonalizeDesc.Append(personalizeDetails + "<br />");
                                        string fileName = Convert.ToString(lineitem.PersonaliseValuesDetail?.FirstOrDefault(x => x.PersonalizeCode == "Filename").PersonalizeValue);
                                        string SVGPath = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString() + "/" + ConfigurationManager.AppSettings["SVGPathDownload"].ToString();
                                        PersonalizeDesc.Append("<a href =\"" + SVGPath + fileName + "\" target =\"_blank\">Preview Design</a>");
                                    }


                                }
                                childLineItem.Description.Replace("<br/>", "");
                                string[] spearator = { "<br>" };
                                //Collection - Dance < br > Awct_Color - Fuchsia < br > Awct_Size - ISC < br />
                                string[] desc = childLineItem.Description.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                                Color = desc[1].Replace("Awct_Color - ", "");
                                size = desc[2].Replace("Awct_Size - ", "").Replace("<br/>", "");
                                /*Changes for Size Display Order*/
                                DataRow[] dr1 = sizesWithDisplayOrder.Select("Size='" + size + "'");
                                /*Changes for Size Display Order end*/
                                desc[0] = desc[0].Replace("Collection -", "");
                                StringBuilder productDesc = new StringBuilder();
                                if (Convert.ToString(childLineItem.Custom5) != string.Empty)
                                {
                                    //productDesc.Append(childLineItem.Description.Replace("Awct_Size", "Size").Replace("Awct_Color", "Color " + ColorCode) + "<br />");
                                    productDesc.Append(desc[0] + "<br />");
                                    string UploadFilePath = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString() + "/Data/Media/UploadDocument/" + childLineItem.Custom5;
                                    productDesc.Append("<a href =\"" + UploadFilePath + "\" target =\"_blank\">View File</a>");
                                }
                                else
                                {
                                    productDesc.Append(desc[0] + "<br />");
                                    // productDesc.Append(childLineItem.Description.Replace("Awct_Size", "Size").Replace("Awct_Color", "Color " + ColorCode));
                                }

                                if (childLineItem.Custom4 == "AddOn")
                                {

                                    string sku = Convert.ToString
                                        (_omsOrderLineItemRepository.Table.Where(x => x.OmsOrderLineItemsId == childLineItem.ParentOmsOrderLineItemsId)
                                                  .Select(x => x.Sku).FirstOrDefault().ToString());
                                    string Name = Convert.ToString
                                      (_omsOrderLineItemRepository.Table.Where(x => x.Sku == sku && x.Custom4 != "AddOn" && x.OmsOrderDetailsId == childLineItem.OmsOrderDetailsId)
                                                .Select(x => x.ProductName).FirstOrDefault());
                                    sku = sku.Split('_')[1];
                                    childLineItem.Description = sku + " " + Name;
                                }

                                DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                                orderlineItemDbRow["ProductImage"] = lineitem.ProductImagePath;
                                orderlineItemDbRow["Name"] = lineitem.ProductName;
                                orderlineItemDbRow["SKU"] = childLineItem.Sku;
                                orderlineItemDbRow["Description"] = productDesc;
                                orderlineItemDbRow["UOMDescription"] = PersonalizeDesc.ToString().Replace("\\n", "<br />");
                                orderlineItemDbRow["Quantity"] = childLineItem.Quantity;
                                orderlineItemDbRow["ShortDescription"] = EstimateDate;
                                orderlineItemDbRow["Custom2"] = size;
                                orderlineItemDbRow["Custom3"] = ColorCode + "-" + Color;
                                orderlineItemDbRow["Custom5"] = childLineItem.ProductName.ToUpper().Contains("SETUP") ? "Value" : productDesc.ToString();

                                /*Changes for Size Display Order*/
                                if (dr1.Length > 0)
                                {
                                    orderlineItemDbRow["Custom4"] = dr1[0]["DisplayOrder"];
                                }
                                /*Changes for Size Display Order end*/
                                if (shoppingCartItem != null)
                                {
                                    orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                                    orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                                    // orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                                }

                                orderlineItemDbRow["OmsOrderShipmentID"] = childLineItem.OmsOrderShipmentId;
                                orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;

                                orderLineItemTable.Rows.Add(orderlineItemDbRow);
                            }
                            counter++;
                        }
                    }
                    else
                    {
                        ZnodeShoppingCartItem shoppingCartItem = shoppingCartItems.ElementAt(counter++);

                        foreach (OrderLineItemModel orderLineItem in lineitem.OrderLineItemCollection)
                        {
                            setGroupProductDetails(lineitem, orderLineItem);
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append(lineitem.ProductName + "<br />");

                        if (!String.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
                        {
                            sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                        }

                        if (orderLineItemTable != null)
                        {
                            DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                            orderlineItemDbRow["ProductImage"] = lineitem.ProductImagePath;
                            orderlineItemDbRow["Name"] = sb.ToString();
                            orderlineItemDbRow["SKU"] = lineitem.Sku;
                            orderlineItemDbRow["Description"] = lineitem.Description;
                            orderlineItemDbRow["UOMDescription"] = string.Empty;
                            orderlineItemDbRow["Quantity"] = lineitem.OrderLineItemRelationshipTypeId.Equals((int)ZnodeCartItemRelationshipTypeEnum.Group) ? lineitem.GroupProductQuantity : Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
                            orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                            orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                            orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
                            orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                            orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;
                            orderLineItemTable.Rows.Add(orderlineItemDbRow);
                        }
                    }
                }
                //Code for SOrt by 
                DataView dv = orderLineItemTable.DefaultView;
                // dv.Sort = "Description,Name,Custom3,Custom2 ASC";
                dv.Sort = "Custom5,Name,Custom3,Custom4 ASC";
                orderLineItemTable = dv.ToTable();
                ZnodeMultipleAddressCart addressCart = ((ZnodePortalCart)ShoppingCart).AddressCarts.FirstOrDefault(y => y.OrderShipmentID == orderShipment.OmsOrderShipmentId);

                if (addressCart != null && orderShipments.Count() > 1)
                {
                    object globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
                    if (globalResourceObject != null)
                    {
                        BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), addressCart.SubTotal, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
                    }

                    BuildOrderShipmentTotalLineItem($"Shipping Cost({addressCart.Shipping.ShippingName})", addressCart.ShippingCost, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax").ToString(), addressCart.SalesTax, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), addressCart.VAT, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), addressCart.GST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), addressCart.HST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), addressCart.PST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
                }
                multipleAddressTable.Rows.Add(addressRow);
            }
        }


    }
}
