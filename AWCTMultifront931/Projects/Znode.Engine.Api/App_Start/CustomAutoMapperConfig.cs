﻿using AutoMapper;
using Znode.Custom.Data;
using Znode.Engine.Api.Models;
using Znode.Libraries.MongoDB.Data;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomProductModel;

namespace Znode.Engine.Api
{
    public static class CustomAutoMapperConfig
    {
        public static void Execute()
        {           
            Mapper.CreateMap<ZnodeCustomPortalDetail, CustomPortalDetailModel>().ReverseMap();
            Mapper.CreateMap<AWCTPublishProductModel, PublishProductModel>().ReverseMap();
            Mapper.CreateMap<PublishProductModel, AWCTPublishProductModel>();
            Mapper.CreateMap<ProductEntity, AWCTPublishProductModel>();
        }
    }
}