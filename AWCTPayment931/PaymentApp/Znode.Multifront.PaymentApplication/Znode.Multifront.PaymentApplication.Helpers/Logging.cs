﻿using System.Configuration;
using System.IO;
using System.Text;
using System.Web;

namespace Znode.Multifront.PaymentApplication.Helpers
{
    public class Logging
    {
        private static string _FilePath = "~/config/znodepaymentapilog.txt";
        public static void LogMessage(string message)
        {
            if (!Equals(ConfigurationManager.AppSettings["LogFilePath"], null))
            {
                _FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["LogFilePath"]);
                using (StreamWriter sw = (File.Exists(_FilePath)) ? File.AppendText(_FilePath) : File.CreateText(_FilePath))
                {
                    StringBuilder errMsg = new StringBuilder();
                    errMsg.AppendLine();
                    errMsg.AppendLine("*************************");
                    errMsg.AppendLine($"TimeStamp: {System.DateTime.Now.ToString()}");
                    errMsg.AppendLine(message);
                    errMsg.AppendLine("*************************");
                    sw.WriteLine(errMsg.ToString());
                }
            }
        }

    }
}
