﻿using System;
using System;
using System.Linq;
using Znode.Multifront.PaymentApplication.Data;
using Znode.Multifront.PaymentApplication.Helpers;
using Znode.Multifront.PaymentApplication.Models;
using Znode.Multifront.PaymentApplication.Providers.AuthorizeNetAPI;
using Znode.Multifront.PaymentFramework.Bussiness;

namespace Znode.Multifront.PaymentApplication.Providers
{
    /// <summary>
    /// This class will have all the methods of implementation of Authorize .Net payment provider 
    /// </summary>
    public class AuthorizeNetCustomerProvider : BaseProvider, IPaymentProviders
    {
        #region Public Methods
        /// <summary>
        /// Validate the credit card transaction number and returns status
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            GatewayResponseModel response = new GatewayResponseModel();
            try
            {
                //If Customer Profile is already created then call Create Transaction to complete the payment process.
                if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId) &&
                    !string.IsNullOrEmpty(paymentModel.CustomerPaymentProfileId))
                    return CreateTransaction(paymentModel);

                //To Create Customer Profile based on user input.
                response = CreateCustomer(paymentModel);
                return response;
            }
            catch (Exception ex)
            {
                Logging.LogMessage($"Exception Occurred :{ ex.Message},{ex.StackTrace.ToString()}");
                LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, ex.Message);

                response.IsSuccess = false;
                response.GatewayResponseData = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Create the customer profile
        /// </summary>
        /// <returns>The result of the operation</returns>
        private GatewayResponseModel CreateCustomerProfile(PaymentModel paymentModel)
        {
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            createCustomerProfileRequest request = new createCustomerProfileRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            customerProfileType customer = new customerProfileType();
            customer.email = paymentModel.BillingEmailId;
            customer.description = $"New customer {DateTime.Now.Ticks.ToString()}";

            if (!string.IsNullOrEmpty(paymentModel.ShippingFirstName))
            {
                customer.shipToList = new customerAddressType[1];
                customer.shipToList[0] = new customerAddressType
                {
                    firstName = paymentModel.ShippingFirstName,
                    lastName = paymentModel.ShippingLastName,
                    zip = paymentModel.ShippingPostalCode,
                    country = paymentModel.ShippingCountryCode,
                    address = paymentModel.ShippingStreetAddress1,
                    city = paymentModel.ShippingCity,
                    state = paymentModel.ShippingStateCode
                };
            }

            request.profile = customer;
            request.validationMode = paymentModel.GatewayTestMode ? validationModeEnum.testMode : validationModeEnum.liveMode;

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileResponse apiResponse = null;

            if (bResult)
                bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerProfileResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                gatewayResponse.IsSuccess = false;
                if (!Equals(errorResponse, null))
                {
                    gatewayResponse.GatewayResponseData = $"code: {errorResponse.messages.message[0].code}\n	  msg: {errorResponse.messages.message[0].text}";
                    gatewayResponse.ResponseText = errorResponse.messages.message[0].text;
                }
                return gatewayResponse;
            }

            if (bResult)
                apiResponse = (createCustomerProfileResponse)response;

            if (!Equals(apiResponse?.customerProfileId, null))
            {
                paymentModel.CustomerProfileId = apiResponse.customerProfileId;
                paymentModel.CustomerShippingAddressId = apiResponse.customerShippingAddressIdList.FirstOrDefault();
                gatewayResponse.CustomerShippingAddressId = apiResponse.customerShippingAddressIdList.FirstOrDefault();
                gatewayResponse.CustomerProfileId = apiResponse.customerProfileId;
                gatewayResponse.IsSuccess = true;
            }
            else
            {
                //gatewayResponse.ResponseText = apiResponse.messages.message[0].code + " " + apiResponse.messages.message[0].text;
                gatewayResponse.IsSuccess = false;
                gatewayResponse.ResponseText = "Unable to create customer profile.";
                ZnodeLogging.LogMessage("Payment Response"+ apiResponse.messages.message[0].code + " " + apiResponse.messages.message[0].text);
            }

            return gatewayResponse;
        }

        /// <summary>
        /// This method will create the customer payment profile
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns></returns>
        private GatewayResponseModel CreateCustomerPaymentProfile(PaymentModel paymentModel)
        {
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            customerPaymentProfileType newPaymentProfile = new customerPaymentProfileType();
            paymentType newPayment = new paymentType();

            creditCardType newCard = new creditCardType();
            newCard.cardNumber = paymentModel.CardNumber;
            newCard.expirationDate = paymentModel.CardExpiration;
            newPayment.Item = newCard;

            newPaymentProfile.payment = newPayment;
            //Save billing address for user.
            newPaymentProfile.billTo = new customerAddressType() { firstName = paymentModel.BillingFirstName, lastName = paymentModel.BillingLastName, address = $"{paymentModel.BillingStreetAddress1}{paymentModel.BillingStreetAddress2}", zip = paymentModel.BillingPostalCode, city = paymentModel.BillingCity, country = paymentModel.BillingCountryCode, phoneNumber = paymentModel.BillingPhoneNumber, state = paymentModel.BillingStateCode };
            createCustomerPaymentProfileRequest request = new createCustomerPaymentProfileRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            request.customerProfileId = paymentModel.CustomerProfileId;
            request.paymentProfile = newPaymentProfile;
            request.validationMode = paymentModel.GatewayTestMode ? validationModeEnum.testMode : validationModeEnum.liveMode;

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerPaymentProfileResponse apiResponse = null;

            if (bResult)
                bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerPaymentProfileResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                gatewayResponse.GatewayResponseData =
                    $"code: {errorResponse.messages.message[0].code}\n	  msg: {errorResponse.messages.message[0].text}";
                gatewayResponse.IsSuccess = false;
                gatewayResponse.ResponseText = errorResponse.messages.message[0].text;
                return gatewayResponse;
            }
            if (bResult)
                apiResponse = (createCustomerPaymentProfileResponse)response;
            if (!Equals(apiResponse, null))
            {
                gatewayResponse.IsSuccess = true;
                gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                gatewayResponse.CustomerShippingAddressId = paymentModel.CustomerShippingAddressId;
                gatewayResponse.CustomerPaymentProfileId = apiResponse.customerPaymentProfileId;
                gatewayResponse.ResponseText = apiResponse.messages.message.FirstOrDefault().text;
               // gatewayResponse.CardAuthCode = responseValues[4];
            }
            return gatewayResponse;
        }

        /// <summary>
        /// Create a transaciton(Authorise or capture transaction)
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the create transaction response.</returns>
        private GatewayResponseModel CreateTransaction(PaymentModel paymentModel)
        {
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            createCustomerProfileTransactionRequest request = new createCustomerProfileTransactionRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            profileTransactionType newTrans = new profileTransactionType();

            if (paymentModel.IsCapture)
            {
                profileTransPriorAuthCaptureType newItem = new profileTransPriorAuthCaptureType
                {
                    transId = paymentModel.TransactionId,
                    customerProfileId = paymentModel.CustomerProfileId,
                    customerPaymentProfileId = paymentModel.CustomerPaymentProfileId,
                    customerShippingAddressId = paymentModel.CustomerShippingAddressId,
                    amount = decimal.Parse(paymentModel.Total)
                };
                newTrans.Item = newItem;
            }
            else
            {
                profileTransAuthOnlyType newItem = new profileTransAuthOnlyType
                {
                    customerProfileId = paymentModel.CustomerProfileId,
                    customerPaymentProfileId = paymentModel.CustomerPaymentProfileId,
                    customerShippingAddressId = paymentModel.CustomerShippingAddressId,
                    amount = decimal.Parse(paymentModel.Total),
                    cardCode = paymentModel.CardSecurityCode,
                    order = new orderExType
                    {
                        purchaseOrderNumber = paymentModel.OrderId,
                        invoiceNumber = paymentModel.OrderId,
                    }
                };
                newTrans.Item = newItem;
            }

            request.transaction = newTrans;
          
            System.Xml.XmlDocument response_xml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out response_xml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileTransactionResponse api_response = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(response_xml, out response);
            if (!(response is createCustomerProfileTransactionResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                if (!Equals(errorResponse, null))
                    gatewayResponse.GatewayResponseData = $"code: { errorResponse.messages.message[0].code}, msg: {errorResponse.messages.message[0].text}";
                gatewayResponse.IsSuccess = false;
                return gatewayResponse;
            }
            if (bResult) api_response = (createCustomerProfileTransactionResponse)response;
            if (!Equals(api_response, null) && api_response.messages.resultCode.Equals(messageTypeEnum.Ok))
            {
                gatewayResponse.GatewayResponseData = $"code: {api_response.messages.message[0].code}, msg: {api_response.messages.message[0].text}";

                var responseValues = api_response.directResponse.Split(',');
                if (responseValues.Count() > 6 && !responseValues[6].Equals("0"))
                {
                    gatewayResponse.IsGatewayPreAuthorize = paymentModel.GatewayPreAuthorize;
                    gatewayResponse.TransactionId = responseValues[6];
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                    gatewayResponse.CustomerShippingAddressId = paymentModel.CustomerShippingAddressId;
                    gatewayResponse.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                    gatewayResponse.PaymentStatus = paymentModel.IsCapture ? ZnodePaymentStatus.CAPTURED : ZnodePaymentStatus.AUTHORIZED;
                    //Nivi code
                    gatewayResponse.CardAuthCode = responseValues[4];
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.GatewayResponseData = $"code: {api_response.messages.message[0].code}, msg: {api_response.messages.message[0].text}";
                }
            }
            else
            {
                gatewayResponse.IsSuccess = false;
                gatewayResponse.GatewayResponseData = $"code: {api_response.messages.message[0].code}, msg: {api_response.messages.message[0].text}";
            }

            return gatewayResponse;
        }

        /// <summary>
        /// This method will call the refund payment of Authorize .Net payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the refund response</returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            createCustomerProfileTransactionRequest req = new createCustomerProfileTransactionRequest();
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            profileTransRefundType trans = new profileTransRefundType();
            trans.amount = decimal.Parse(paymentModel.Total);
            trans.customerProfileId = paymentModel.CustomerProfileId;
            trans.customerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
            trans.transId = paymentModel.TransactionId;
            trans.order = new orderExType
            {
                purchaseOrderNumber = paymentModel.OrderId,
                invoiceNumber = paymentModel.OrderId,
            };
            req.transaction = new profileTransactionType();
            req.transaction.Item = trans;
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)req, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(req, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileTransactionResponse api_response = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerProfileTransactionResponse))
            {
                ANetApiResponse errorResponse = (ANetApiResponse)response;
                if (!Equals(errorResponse, null))
                    gatewayResponse.GatewayResponseData = $"code: {errorResponse.messages.message[0].code}, msg: {errorResponse.messages.message[0].text}";
                gatewayResponse.IsSuccess = false;
                return gatewayResponse;
            }
            if (bResult)
                api_response = (createCustomerProfileTransactionResponse)response;
            if (!Equals(api_response, null))
            {
                gatewayResponse.GatewayResponseData = $"code: {api_response.messages.message[0].code}, msg: {api_response.messages.message[0].text}";

                string[] responseValues = api_response.directResponse.Split(',');
                if (responseValues.Count() > 6 && api_response.messages.resultCode.Equals(messageTypeEnum.Ok))
                {
                    gatewayResponse.TransactionId = responseValues[6];
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.REFUNDED;
                }
            }
            return gatewayResponse;
        }

        /// <summary>
        /// This method will call the void payment of Authorize .Net payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the void response</returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            createCustomerProfileTransactionRequest request = new createCustomerProfileTransactionRequest();
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            profileTransVoidType trans = new profileTransVoidType();
            trans.customerProfileId = paymentModel.CustomerProfileId;
            trans.customerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
            trans.transId = paymentModel.TransactionId;
         
            request.transaction = new profileTransactionType();
            request.transaction.Item = trans;
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileTransactionResponse api_response = null;

            if (bResult)
                bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerProfileTransactionResponse))
            {
                ANetApiResponse errorResponse = (ANetApiResponse)response;
                if (!Equals(errorResponse, null))
                    gatewayResponse.GatewayResponseData = $"code: {errorResponse.messages.message[0].code}, msg: {errorResponse.messages.message[0].text}";
                gatewayResponse.IsSuccess = false;
                return gatewayResponse;
            }
            if (bResult)
                api_response = (createCustomerProfileTransactionResponse)response;
            if (!Equals(api_response, null))
            {
                gatewayResponse.GatewayResponseData = $"code: {api_response.messages.message[0].code}, msg: {api_response.messages.message[0].text}";
                string[] responseValues = api_response.directResponse.Split(',');
                if (responseValues.Count() > 6 && !responseValues[6].Equals("0"))
                {
                    gatewayResponse.TransactionId = responseValues[6];
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.VOIDED;
                }
            }
            return gatewayResponse;
        }

        /// <summary>
        /// This method will call the subscription
        /// </summary>
        /// <param name="paymentModel">PaymentModel model</param>
        /// <returns>Response from the payment provider</returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            GatewayResponseModel gatewayResponse = new GatewayResponseModel();
            ARBCreateSubscriptionRequest request = new ARBCreateSubscriptionRequest();

            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            ARBSubscriptionType sub = new ARBSubscriptionType();
            sub.name = paymentModel.Subscription.ProfileName;

            sub.payment = new paymentType();
            sub.payment.Item = new creditCardType { cardNumber = paymentModel.CardNumber, expirationDate = $"{paymentModel.CardExpirationYear}-{paymentModel.CardExpirationMonth}" };

            // subscription payment schedule
            sub.paymentSchedule = new paymentScheduleType();
            sub.paymentSchedule.startDate = DateTime.Today;
            sub.paymentSchedule.startDateSpecified = true;

            // Disable trial
            sub.paymentSchedule.trialOccurrences = 0;
            sub.paymentSchedule.trialOccurrencesSpecified = true;
            sub.trialAmount = 0.00M;
            sub.trialAmountSpecified = true;

            sub.billTo = new nameAndAddressType { firstName = paymentModel.BillingFirstName, lastName = paymentModel.BillingLastName };

            if (paymentModel.Subscription.TotalCycles.Equals(0))
                sub.paymentSchedule.totalOccurrences = 9999;
            else
                sub.paymentSchedule.totalOccurrences = short.Parse(paymentModel.Subscription.TotalCycles.ToString());

            sub.paymentSchedule.totalOccurrencesSpecified = true;

            sub.amount = paymentModel.Subscription.Amount;
            sub.amountSpecified = true;

            int frequencyLength = 0;
            sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
            sub.paymentSchedule.interval.unit = this.BillingPeriod(paymentModel.Subscription.Period, paymentModel.Subscription.Frequency, out frequencyLength);
            sub.paymentSchedule.interval.length = (short)frequencyLength;

            sub.order = new orderType();
            sub.order.invoiceNumber = paymentModel.Subscription.InvoiceNo;

            request.subscription = sub;
            request.refId = paymentModel.TransactionId;

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            ARBCreateSubscriptionResponse api_response = null;

            if (bResult)
                bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);

            if (bResult)
                api_response = (ARBCreateSubscriptionResponse)response;
            if (!Equals(api_response, null) && api_response.messages.resultCode.Equals(messageTypeEnum.Ok))
            {
                gatewayResponse.GatewayResponseData = $"code: {api_response.messages.message[0].code}, msg: {api_response.messages.message[0].text}";
                if (!string.IsNullOrEmpty(api_response.subscriptionId))
                {
                    gatewayResponse.TransactionId = api_response.subscriptionId;
                    gatewayResponse.IsSuccess = true;
                }
            }
            else
            {
                gatewayResponse.GatewayResponseData = $"code: {api_response.messages.message[0].code}, msg: {api_response.messages.message[0].text}";
                gatewayResponse.IsSuccess = false;
            }
            return gatewayResponse;

        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get the customer profile
        /// </summary>
        /// <param name="profile_id">The ID of the customer that we are getting the profile for</param>
        /// <returns>The profile that was returned</returns>
        private customerProfileMaskedType GetCustomerProfile(PaymentModel paymentModel)
        {
            customerProfileMaskedType out_profile = null;

            getCustomerProfileRequest request = new getCustomerProfileRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            request.customerProfileId = paymentModel.CustomerProfileId;

            System.Xml.XmlDocument response_xml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out response_xml, paymentModel.GatewayTestMode);
            object response = null;
            getCustomerProfileResponse api_response = null;

            if (bResult)
                bResult = XmlApiUtilities.ProcessXmlResponse(response_xml, out response);
            if (!(response is getCustomerProfileResponse))
            {
                ANetApiResponse ErrorResponse = (ANetApiResponse)response;
                return out_profile;
            }
            if (bResult)
                api_response = (getCustomerProfileResponse)response;
            if (!Equals(api_response, null))
                out_profile = api_response.profile;

            return out_profile;
        }

        /// <summary>
        /// This method will create customer profile 
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns customer details response</returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            GatewayResponseModel response = new GatewayResponseModel();
            GatewayConnector gatewayConnector = new GatewayConnector();
            bool isSuccess = false;
            //Customer Logged IN
            if (!paymentModel.IsAnonymousUser)
            {
                //First Time user CustomerGUID is null 
                if (string.IsNullOrEmpty(paymentModel.CustomerGUID))
                {
                    //Saved card for future use is True
                    if (paymentModel.IsSaveCreditCard)
                    {
                        //Create Customer and add it to vault.
                        response = CreatePaymentGatewayCustomer(paymentModel);
                        if (response.IsSuccess)
                            isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.PaymentToken;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.IsSuccess = isSuccess;
                        return response;
                    }
                    else //Saved card for future use is false
                        return CreatePaymentGatewayCustomer(paymentModel);//As it is - regular one. no need to save in database  
                }
                else //exisiting user CustomerGUID is present 
                {
                    if (paymentModel.IsSaveCreditCard)
                    {
                        response = CreatePaymentGatewayVault(paymentModel);
                        if (response.IsSuccess)
                            isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.PaymentToken;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.CustomerShippingAddressId = GetCustomerShippingAddressId(paymentModel);
                        response.IsSuccess = isSuccess;
                        return response;
                    }
                    else if (!string.IsNullOrEmpty(paymentModel.PaymentToken))
                    {
                        isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.PaymentToken;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.IsSuccess = isSuccess;
                        response.CustomerShippingAddressId = GetCustomerShippingAddressId(paymentModel);
                        return response;
                    }
                    else
                        return CreatePaymentGatewayCustomer(paymentModel);
                }
            }
            else
                return CreatePaymentGatewayCustomer(paymentModel);//As it is which is the normal one without vault.
        }

        /// <summary>
        /// This method will return shipping addressId by customer profileid
        /// </summary>
        /// <param name="paymentModel">payment Model</param>
        /// <returns>shipping Address Id</returns>
        private string GetCustomerShippingAddressId(PaymentModel paymentModel)
        {
            createCustomerShippingAddressRequest request = GetShippingRequest(paymentModel);
            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerShippingAddressResponse apiResponse = new createCustomerShippingAddressResponse();

            if (bResult && XmlApiUtilities.ProcessXmlResponse(responseXml, out response))
                apiResponse = (createCustomerShippingAddressResponse)response;

            return apiResponse.customerAddressId;
        }

        /// <summary>
        /// Get ShippingAddress API request Param
        /// </summary>
        /// <param name="paymentModel">payment Model</param>
        /// <returns>request param</returns>
        private createCustomerShippingAddressRequest GetShippingRequest(PaymentModel paymentModel)
        {
            createCustomerShippingAddressRequest request = new createCustomerShippingAddressRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            customerAddressType customer = new customerAddressType()
            {
                firstName = paymentModel.ShippingFirstName,
                lastName = paymentModel.ShippingLastName,
                zip = paymentModel.ShippingPostalCode,
                country = paymentModel.ShippingCountryCode,
                address = paymentModel.ShippingStreetAddress1,
                city = paymentModel.ShippingCity,
                state = paymentModel.ShippingStateCode
            };
            request.address = customer;
            request.customerProfileId = paymentModel.CustomerProfileId;
            return request;
        }

        /// <summary>
        /// To Customer new customer account and ADD voult
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns>returns gateway response</returns>
        private GatewayResponseModel CreatePaymentGatewayCustomer(PaymentModel paymentModel)
        {
            GatewayResponseModel response = CreateCustomerProfile(paymentModel);
            if (!string.IsNullOrEmpty(response.CustomerProfileId))
            {
                paymentModel.CustomerProfileId = response.CustomerProfileId;
                paymentModel.CustomerShippingAddressId = response.CustomerShippingAddressId;
                response = CreateCustomerPaymentProfile(paymentModel);
                paymentModel.CustomerPaymentProfileId = response.CustomerPaymentProfileId;
                if (response.IsSuccess && paymentModel.IsAnonymousUser)
                {
                    GatewayConnector gatewayConnector = new GatewayConnector();
                    response.IsSuccess = gatewayConnector.SavePaymentDetails(paymentModel);
                }
            }
            return response;
        }

        /// <summary>
        /// To vault using existing customer id
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns>returns gateway response</returns>
        private GatewayResponseModel CreatePaymentGatewayVault(PaymentModel paymentModel)
        {
            GatewayResponseModel response = new GatewayResponseModel();
            //get customer profile id from db using guid if not exit the create user
            PaymentMethodsService repository = new PaymentMethodsService();
            ZnodePaymentMethod payment = repository.GetPaymentMethod(paymentModel.PaymentApplicationSettingId, paymentModel.CustomerGUID);
            if (!Equals(payment, null) && !string.IsNullOrEmpty(payment.CustomerProfileId))
                paymentModel.CustomerProfileId = payment.CustomerProfileId;
            else
            {
                response = CreateCustomerProfile(paymentModel);
                paymentModel.CustomerProfileId = response.CustomerProfileId;
            }
            if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId))
            {
                response = CreateCustomerPaymentProfile(paymentModel);
                paymentModel.CustomerPaymentProfileId = response.CustomerPaymentProfileId;
                response.IsSuccess = !Equals(response.CustomerPaymentProfileId, null);
            }
            return Equals(response, null) ? new GatewayResponseModel() : response;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Calculate the billing period
        /// </summary>
        /// <param name="Unit">Unit for the billing</param>
        /// <param name="Frequency">Frequency for the billing</param>
        /// <param name="NewFrequency">New Frequency for the billing</param>
        /// <returns>Returns the ARB Subscription Unit</returns>
        protected ARBSubscriptionUnitEnum BillingPeriod(string Unit, string Frequency, out int NewFrequency)
        {
            int freqValue = int.Parse(Frequency);
            NewFrequency = freqValue;

            if (Unit.Equals("DAY", StringComparison.OrdinalIgnoreCase))
                return ARBSubscriptionUnitEnum.days;
            else if (Unit.Equals("WEEK", StringComparison.OrdinalIgnoreCase))
            {
                NewFrequency = freqValue * 7; // Change it to days.
                return ARBSubscriptionUnitEnum.days;
            }
            else if (Unit.Equals("MONTH", StringComparison.OrdinalIgnoreCase))
                return ARBSubscriptionUnitEnum.months;
            else if (Unit.Equals("YEAR", StringComparison.OrdinalIgnoreCase))
            {
                NewFrequency = freqValue * 12; // Change this value into months
                return ARBSubscriptionUnitEnum.months;
            }
            return ARBSubscriptionUnitEnum.days;
        }
        #endregion

        public TransactionDetailsModel GetTransactionDetails(PaymentModel paymentModel)
        {
            string transStatus = string.Empty;
            TransactionDetailsModel gatewayResponse = new TransactionDetailsModel();
            getTransactionDetailsRequest request = new getTransactionDetailsRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            request.transId = paymentModel.TransactionId;

            System.Xml.XmlDocument response_xml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out response_xml, paymentModel.GatewayTestMode);
            object response = null;
            getTransactionDetailsResponse api_response = null;

            if (bResult)
                bResult = XmlApiUtilities.ProcessXmlResponse(response_xml, out response);
            if (!(response is getTransactionDetailsResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                gatewayResponse.IsSuccess = false;
                if (!Equals(errorResponse, null))
                {
                    gatewayResponse.ResponseText = errorResponse.messages.message[0].text;
                }
                return gatewayResponse;
            }
            if (bResult)
                api_response = (getTransactionDetailsResponse)response;

            if (!Equals(api_response?.transaction?.transactionStatus, null))
            {
                gatewayResponse.TransactionStatus = api_response.transaction.transactionStatus;
                gatewayResponse.TransactionType = api_response.transaction.transactionType;
                gatewayResponse.TransactionId = api_response.transaction.transId;
                gatewayResponse.ResponseCode = api_response.transaction.responseReasonDescription;
                gatewayResponse.IsSuccess = true;
            }
            else
            {
                gatewayResponse.IsSuccess = false;
                gatewayResponse.ResponseText = "Unable to get transaction details.";
            }

            return gatewayResponse;
        }
    }
}
