﻿namespace SearchStringfromFiles
{
    partial class frmImageMigration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uxSearchString = new System.Windows.Forms.Button();
            this.uxCMDSelectTarget = new System.Windows.Forms.Button();
            this.uxCMDStartDownloading = new System.Windows.Forms.Button();
            this.uxCMDSelectSource = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.uxDisplyuxSourceDataBaseList = new System.Windows.Forms.Button();
            this.uxSourceDataBaseList = new System.Windows.Forms.ComboBox();
            this.uxSourceServerList = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTargetPassword = new System.Windows.Forms.TextBox();
            this.txtTargetUserName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uxExtention = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.uxMaxWidth = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.uxMaxHeight = new System.Windows.Forms.TextBox();
            this.uxCMDSaveSettings = new System.Windows.Forms.Button();
            this.uxRemoveDuplicate = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uxSearchString
            // 
            this.uxSearchString.Location = new System.Drawing.Point(11, 739);
            this.uxSearchString.Margin = new System.Windows.Forms.Padding(4);
            this.uxSearchString.Name = "uxSearchString";
            this.uxSearchString.Size = new System.Drawing.Size(100, 26);
            this.uxSearchString.TabIndex = 0;
            this.uxSearchString.Text = "Search";
            this.uxSearchString.UseVisualStyleBackColor = true;
            this.uxSearchString.Visible = false;
            // 
            // uxCMDSelectTarget
            // 
            this.uxCMDSelectTarget.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.uxCMDSelectTarget.Location = new System.Drawing.Point(17, 60);
            this.uxCMDSelectTarget.Margin = new System.Windows.Forms.Padding(4);
            this.uxCMDSelectTarget.Name = "uxCMDSelectTarget";
            this.uxCMDSelectTarget.Size = new System.Drawing.Size(190, 27);
            this.uxCMDSelectTarget.TabIndex = 1;
            this.uxCMDSelectTarget.Text = "Select Target";
            this.uxCMDSelectTarget.UseVisualStyleBackColor = true;
            this.uxCMDSelectTarget.Click += new System.EventHandler(this.uxCMDSelectTarget_Click);
            // 
            // uxCMDStartDownloading
            // 
            this.uxCMDStartDownloading.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.uxCMDStartDownloading.Location = new System.Drawing.Point(43, 263);
            this.uxCMDStartDownloading.Margin = new System.Windows.Forms.Padding(4);
            this.uxCMDStartDownloading.Name = "uxCMDStartDownloading";
            this.uxCMDStartDownloading.Size = new System.Drawing.Size(190, 34);
            this.uxCMDStartDownloading.TabIndex = 2;
            this.uxCMDStartDownloading.Text = "Start Importing";
            this.uxCMDStartDownloading.UseVisualStyleBackColor = true;
            this.uxCMDStartDownloading.Click += new System.EventHandler(this.uxCMDStartDownloading_Click);
            // 
            // uxCMDSelectSource
            // 
            this.uxCMDSelectSource.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.uxCMDSelectSource.Location = new System.Drawing.Point(17, 22);
            this.uxCMDSelectSource.Margin = new System.Windows.Forms.Padding(4);
            this.uxCMDSelectSource.Name = "uxCMDSelectSource";
            this.uxCMDSelectSource.Size = new System.Drawing.Size(190, 27);
            this.uxCMDSelectSource.TabIndex = 4;
            this.uxCMDSelectSource.Text = "Select Source ";
            this.uxCMDSelectSource.UseVisualStyleBackColor = true;
            this.uxCMDSelectSource.Click += new System.EventHandler(this.uxCMDSelectSource_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(25, 339);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(770, 10);
            this.progressBar1.TabIndex = 5;
            this.progressBar1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.OrangeRed;
            this.label1.Location = new System.Drawing.Point(224, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "*";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.uxDisplyuxSourceDataBaseList);
            this.groupBox2.Controls.Add(this.uxSourceDataBaseList);
            this.groupBox2.Controls.Add(this.uxSourceServerList);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtTargetPassword);
            this.groupBox2.Controls.Add(this.txtTargetUserName);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(798, -7);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1215, 96);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Source Database";
            this.groupBox2.Visible = false;
            // 
            // uxDisplyuxSourceDataBaseList
            // 
            this.uxDisplyuxSourceDataBaseList.BackColor = System.Drawing.Color.LightGray;
            this.uxDisplyuxSourceDataBaseList.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.uxDisplyuxSourceDataBaseList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uxDisplyuxSourceDataBaseList.Location = new System.Drawing.Point(1184, 48);
            this.uxDisplyuxSourceDataBaseList.Margin = new System.Windows.Forms.Padding(4);
            this.uxDisplyuxSourceDataBaseList.Name = "uxDisplyuxSourceDataBaseList";
            this.uxDisplyuxSourceDataBaseList.Size = new System.Drawing.Size(28, 26);
            this.uxDisplyuxSourceDataBaseList.TabIndex = 19;
            this.uxDisplyuxSourceDataBaseList.Text = "...";
            this.uxDisplyuxSourceDataBaseList.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.uxDisplyuxSourceDataBaseList.UseVisualStyleBackColor = false;
            this.uxDisplyuxSourceDataBaseList.Click += new System.EventHandler(this.uxDisplyuxSourceDataBaseList_Click);
            // 
            // uxSourceDataBaseList
            // 
            this.uxSourceDataBaseList.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.uxSourceDataBaseList.FormattingEnabled = true;
            this.uxSourceDataBaseList.Location = new System.Drawing.Point(651, 49);
            this.uxSourceDataBaseList.Margin = new System.Windows.Forms.Padding(4);
            this.uxSourceDataBaseList.Name = "uxSourceDataBaseList";
            this.uxSourceDataBaseList.Size = new System.Drawing.Size(531, 23);
            this.uxSourceDataBaseList.TabIndex = 18;
            this.uxSourceDataBaseList.SelectedIndexChanged += new System.EventHandler(this.uxSourceDataBaseList_SelectedIndexChanged);
            // 
            // uxSourceServerList
            // 
            this.uxSourceServerList.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.uxSourceServerList.FormattingEnabled = true;
            this.uxSourceServerList.Items.AddRange(new object[] {
            "MSSVR006"});
            this.uxSourceServerList.Location = new System.Drawing.Point(13, 49);
            this.uxSourceServerList.Margin = new System.Windows.Forms.Padding(4);
            this.uxSourceServerList.Name = "uxSourceServerList";
            this.uxSourceServerList.Size = new System.Drawing.Size(260, 23);
            this.uxSourceServerList.TabIndex = 16;
            this.uxSourceServerList.SelectedIndexChanged += new System.EventHandler(this.uxSourceServerList_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(469, 24);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 14);
            this.label6.TabIndex = 15;
            this.label6.Text = "Password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(287, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 14);
            this.label7.TabIndex = 14;
            this.label7.Text = "User Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(656, 24);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 13;
            this.label8.Text = "DataBase";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 24);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 14);
            this.label9.TabIndex = 12;
            this.label9.Text = "Server";
            // 
            // txtTargetPassword
            // 
            this.txtTargetPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTargetPassword.Location = new System.Drawing.Point(465, 50);
            this.txtTargetPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtTargetPassword.Name = "txtTargetPassword";
            this.txtTargetPassword.Size = new System.Drawing.Size(176, 21);
            this.txtTargetPassword.TabIndex = 11;
            this.txtTargetPassword.Text = "mrr@123";
            this.txtTargetPassword.UseSystemPasswordChar = true;
            // 
            // txtTargetUserName
            // 
            this.txtTargetUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTargetUserName.Location = new System.Drawing.Point(283, 50);
            this.txtTargetUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtTargetUserName.Name = "txtTargetUserName";
            this.txtTargetUserName.Size = new System.Drawing.Size(173, 21);
            this.txtTargetUserName.TabIndex = 10;
            this.txtTargetUserName.Text = "znode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.ForestGreen;
            this.label2.Location = new System.Drawing.Point(224, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.ForestGreen;
            this.label3.Location = new System.Drawing.Point(250, 272);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "*";
            // 
            // uxExtention
            // 
            this.uxExtention.Enabled = false;
            this.uxExtention.Location = new System.Drawing.Point(25, 311);
            this.uxExtention.Name = "uxExtention";
            this.uxExtention.Size = new System.Drawing.Size(770, 21);
            this.uxExtention.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.uxMaxWidth);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.uxMaxHeight);
            this.groupBox1.Controls.Add(this.uxCMDSelectTarget);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.uxCMDSelectSource);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(26, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(769, 231);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(196, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "Thumbnail Image Configuration";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(146, 172);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "max Width";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.ForestGreen;
            this.label10.Location = new System.Drawing.Point(224, 105);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "*";
            // 
            // uxMaxWidth
            // 
            this.uxMaxWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uxMaxWidth.Enabled = false;
            this.uxMaxWidth.Location = new System.Drawing.Point(149, 193);
            this.uxMaxWidth.Margin = new System.Windows.Forms.Padding(4);
            this.uxMaxWidth.Name = "uxMaxWidth";
            this.uxMaxWidth.Size = new System.Drawing.Size(109, 21);
            this.uxMaxWidth.TabIndex = 16;
            this.uxMaxWidth.Text = "150";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.button1.Location = new System.Drawing.Point(17, 98);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(190, 27);
            this.button1.TabIndex = 13;
            this.button1.Text = "Select Archive";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 172);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "max Height";
            // 
            // uxMaxHeight
            // 
            this.uxMaxHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uxMaxHeight.Enabled = false;
            this.uxMaxHeight.Location = new System.Drawing.Point(17, 193);
            this.uxMaxHeight.Margin = new System.Windows.Forms.Padding(4);
            this.uxMaxHeight.Name = "uxMaxHeight";
            this.uxMaxHeight.Size = new System.Drawing.Size(109, 21);
            this.uxMaxHeight.TabIndex = 11;
            this.uxMaxHeight.Text = "150";
            // 
            // uxCMDSaveSettings
            // 
            this.uxCMDSaveSettings.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.uxCMDSaveSettings.Location = new System.Drawing.Point(659, 225);
            this.uxCMDSaveSettings.Margin = new System.Windows.Forms.Padding(4);
            this.uxCMDSaveSettings.Name = "uxCMDSaveSettings";
            this.uxCMDSaveSettings.Size = new System.Drawing.Size(128, 24);
            this.uxCMDSaveSettings.TabIndex = 15;
            this.uxCMDSaveSettings.Text = "Save settings ";
            this.uxCMDSaveSettings.UseVisualStyleBackColor = true;
            this.uxCMDSaveSettings.Click += new System.EventHandler(this.uxCMDSaveSettings_Click);
            // 
            // uxRemoveDuplicate
            // 
            this.uxRemoveDuplicate.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.uxRemoveDuplicate.Location = new System.Drawing.Point(632, 273);
            this.uxRemoveDuplicate.Margin = new System.Windows.Forms.Padding(4);
            this.uxRemoveDuplicate.Name = "uxRemoveDuplicate";
            this.uxRemoveDuplicate.Size = new System.Drawing.Size(155, 24);
            this.uxRemoveDuplicate.TabIndex = 16;
            this.uxRemoveDuplicate.Text = "Remove duplicate files";
            this.uxRemoveDuplicate.UseVisualStyleBackColor = true;
            this.uxRemoveDuplicate.Visible = false;
            this.uxRemoveDuplicate.Click += new System.EventHandler(this.uxOpenAppFile_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // frmImageMigration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 354);
            this.Controls.Add(this.uxRemoveDuplicate);
            this.Controls.Add(this.uxCMDSaveSettings);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.uxExtention);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.uxCMDStartDownloading);
            this.Controls.Add(this.uxSearchString);
            this.Font = new System.Drawing.Font("Georgia", 8.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmImageMigration";
            this.Text = "Image Migration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmImageMigration_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button uxSearchString;
        private System.Windows.Forms.Button uxCMDSelectTarget;
        private System.Windows.Forms.Button uxCMDStartDownloading;
        private System.Windows.Forms.Button uxCMDSelectSource;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button uxDisplyuxSourceDataBaseList;
        private System.Windows.Forms.ComboBox uxSourceDataBaseList;
        private System.Windows.Forms.ComboBox uxSourceServerList;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTargetPassword;
        private System.Windows.Forms.TextBox txtTargetUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox uxExtention;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox uxMaxHeight;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox uxMaxWidth;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button uxCMDSaveSettings;
        private System.Windows.Forms.Button uxRemoveDuplicate;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

