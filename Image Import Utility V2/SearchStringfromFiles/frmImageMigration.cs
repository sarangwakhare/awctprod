﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using System.Configuration;
using System.Drawing.Imaging;
using System.Threading;

namespace SearchStringfromFiles
{
    public partial class frmImageMigration : Form
    {

        public string SourceFolderName = "";
        public string TargetFolderName = "";
        public string ArchiveFolderName = "";
        public string connString = "";
        public string fileName = "";
        public string ErrorLog = "";
        public string SearchFolder = "";
        public int maxWeidth;
        public int maxHeight;
        public DataTable dataTable = new DataTable();
        public ImageHelper imageHelper;
        public int successCount;
        public int failCount;
        StringBuilder progressLog = new StringBuilder();
        public frmImageMigration()
        {
            InitializeComponent();
            imageHelper = new ImageHelper();
        }


        private void uxCMDSelectTarget_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialogForTarget = new FolderBrowserDialog())
            {
                dialogForTarget.Description = "Select destination folder name";
                dialogForTarget.ShowNewFolderButton = false;
                dialogForTarget.RootFolder = Environment.SpecialFolder.MyComputer;
                if (dialogForTarget.ShowDialog() == DialogResult.OK)
                {
                    TargetFolderName = dialogForTarget.SelectedPath;
                    label2.Text = TargetFolderName.ToString();
                    uxCMDStartDownloading.Enabled = true;
                }

            }
        }
        private void ToCsV(DataGridView dGV, string filename)
        {
            string stOutput = "";
            // Export titles:
            string sHeaders = "";

            for (int j = 0; j < dGV.Columns.Count; j++)
                sHeaders = sHeaders.ToString() + Convert.ToString(dGV.Columns[j].HeaderText) + "\t";
            stOutput += sHeaders + "\r\n";
            // Export data.
            for (int i = 0; i < dGV.RowCount - 1; i++)
            {
                string stLine = "";
                for (int j = 0; j < dGV.Rows[i].Cells.Count; j++)
                    stLine = stLine.ToString() + Convert.ToString(dGV.Rows[i].Cells[j].Value) + "\t";
                stOutput += stLine + "\r\n";
            }
            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] output = utf16.GetBytes(stOutput);
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(output, 0, output.Length); //write the encoded file
            bw.Flush();
            bw.Close();
            fs.Close();
        }
        private void BuildConnectionString()
        {
            connString = System.Configuration.ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
        }

        private void uxCMDStartDownloading_Click(object sender, EventArgs e)
        {
            progressLog.AppendLine("Media import utility started at "+ DateTime.Now.ToString());
            BuildConnectionString();
            if (DAMTableBackup() == 0)
            {
                progressLog.AppendLine("Error while taking table backup.");
                LogHelper.ProgressLog(progressLog.ToString());
                return;
            }

            Displaynotify();
            if (@SourceFolderName.ToString() == "")
            {
                LogHelper.ProgressLog(progressLog.AppendLine("First select Folder name.").ToString());
                return;
            }
            else
            {
                if (label1.Text == "")
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("First select source folder name.").ToString());
                    return;
                }
                if (label2.Text == "")
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("First select target folder name.").ToString());
                    return;
                }
                if (label1.Text == label2.Text)
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("Source and target folder should be different.").ToString());
                    return;
                }
                if (label10.Text == "")
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("Archive folder should be configured.").ToString());
                    return;
                }
                if (label1.Text == label10.Text)
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("Source and archive folder folder should be different.").ToString());
                    return;
                }
                if (label2.Text == label10.Text)
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("Target and archive folder folder should be different.").ToString());
                    return;
                }

                if (Directory.Exists(label2.Text + "\\Thumbnail") == false)
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("Thumbnail folder should be required inside target folder.").ToString());
                    return;
                }

                if (int.TryParse(uxMaxHeight.Text.ToString(), out maxHeight))
                {
                    if (Convert.ToInt32(uxMaxHeight.Text) <= 0)
                    {
                        LogHelper.ProgressLog(progressLog.AppendLine("Maximum height should be greater than 0 : Recommended : 150.").ToString());
                        uxMaxHeight.Focus();
                        uxMaxHeight.Text = "150";
                        return;
                    }
                }
                else
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("Maximum height should be greater than 0 : Recommended : 150.").ToString());
                    uxMaxHeight.Focus();
                    uxMaxHeight.Text = "150";
                    return;
                }

                if (int.TryParse(uxMaxWidth.Text.ToString(), out maxWeidth))
                {
                    if (Convert.ToInt32(uxMaxWidth.Text) <= 0)
                    {
                        LogHelper.ProgressLog(progressLog.AppendLine("Maximum Width should be greater than 0 : Recommended : 150.").ToString());
                        uxMaxWidth.Focus();
                        uxMaxWidth.Text = "150";
                        return;
                    }
                }
                else
                {
                    LogHelper.ProgressLog(progressLog.AppendLine("Maximum Width should be greater than 0 : Recommended : 150.").ToString());
                    uxMaxWidth.Focus();
                    uxMaxWidth.Text = "150";
                    return;
                }


                SearchFolder = @SourceFolderName.ToString();
                
              

                label3.Text = "Importing ... " + @SourceFolderName.ToString();
                label3.Refresh();
                PullData(@SourceFolderName.ToString());
                label3.Text = "Completed Image migration";
                label3.Refresh();

                

                //Thread.Sleep(Timeout.Infinite);
                label3.Text = "Image archive is in process";
                label3.Refresh();
                
                MoveFolderPullData(@SourceFolderName.ToString());
                label3.Text = "Image archive is completed";
                label3.Refresh();

                this.Close();
            }
        }

      
        // your method to pull data from database to datatable   
        public void PullData(string sDir)
        {

            //Folder searching
            try
            {
                if (SearchFolder == sDir)
                    GetFilesInFolder(sDir);
                //progressBar1.Visible = true;

             
                foreach (string strFolderPath in Directory.GetDirectories(sDir))
                {
                 
                    GetFilesInFolder(strFolderPath);
                    PullData(strFolderPath);
                }
                LogHelper.ProgressLog(progressLog.ToString());
            }
            catch (System.Exception excpt)
            {
                LogHelper.ErrorLog(excpt);
                LogHelper.ProgressLog(progressLog.ToString());
            }
        }


        public void MoveFolderPullData(string sDir)
        {

            //Folder searching
            try
            {
              
                foreach (string strFolderPath in Directory.GetDirectories(sDir))
                {
                   MoveFolderPullData(strFolderPath);
                }

               // if (label1.Text != sDir)
                {
                    String directoryName = sDir;
                    DirectoryInfo dirInfo = new DirectoryInfo(label10.Text +"\\"+ sDir.Replace(sDir.Substring(0, 2), ""));
                    if (dirInfo.Exists == false)
                        Directory.CreateDirectory(label10.Text + "\\" + sDir.Replace(sDir.Substring(0, 2), ""));

                    List<String> MyMusicFiles = Directory
                                       .GetFiles(sDir, "*.*", SearchOption.AllDirectories).ToList();

                    foreach (string file in MyMusicFiles)
                    {
                        FileInfo mFile = new FileInfo(file);
                        // to remove name collisions
                        if (new FileInfo(dirInfo + "\\" + mFile.Name).Exists == false)
                        {
                            mFile.MoveTo(dirInfo + "\\" + mFile.Name);
                        }
                        else
                        {
                            new FileInfo(dirInfo + "\\" + mFile.Name).Delete();
                            mFile.MoveTo(dirInfo + "\\" + mFile.Name);
                        }
                    }

                  //  Directory.Delete(sDir);         
                }
          
            }
            catch (System.Exception excpt)
            {
                LogHelper.ErrorLog(excpt);
            }
        }

        private void GetFilesInFolder(string FolderPath)
        {
            int _fwidth = 0;
            int _fheight = 0;
            int _fsize = 0;
            long _flength = 0;
            string _sext = "";
            int _errorCode = 0;
            string[] extesnsions = new string[] { ".jpg", ".gif", ".png", ".jpeg", ".svg" };
            string destinationPath = $"{label2.Text}";

            foreach (string strfileName in Directory.GetFiles(FolderPath))
            {

                FileInfo finfo = new FileInfo(strfileName);
                if (extesnsions.Contains(finfo.Extension?.ToLower()))
                {
                    //Include Validation for with media type

                    _fsize = File.ReadAllBytes(strfileName).Length;
                    _flength = finfo.Length;
                    _sext = finfo.Extension;
                    string uniqueId = Guid.NewGuid().ToString();
                    _errorCode = submitMediaDetails(_fwidth, _fheight, _fsize, _flength, _sext, uniqueId, FolderPath, finfo.Name);
                    if (_errorCode == 1)
                    {
                        if (fileName == "")
                        {
                            File.Copy(FolderPath.ToString() + "\\" + finfo.Name.ToString(), label2.Text + "\\" + uniqueId + finfo.Name.ToString());
                            //create the different size images 
                            imageHelper.ResizeAndSaveImage(FolderPath.ToString() + "\\" + finfo.Name.ToString(), uniqueId + finfo.Name.ToString(), destinationPath);
                            //move image to thumbnail folder
                            if (finfo.Extension.ToString().Contains("svg"))
                                File.Copy(FolderPath.ToString() + "\\" + finfo.Name.ToString(), label2.Text + "\\Thumbnail\\" + uniqueId + finfo.Name.ToString());
                            else
                            {
                                EncoderDetails(_sext, out ImageCodecInfo jpgEncoder, out EncoderParameters myEncoderParameters);
                                System.Drawing.Image img = System.Drawing.Image.FromFile(strfileName);
                                _fwidth = img.Width;
                                _fheight = img.Height;
                                SetImageParameter(img, Convert.ToInt32(uxMaxHeight.Text), Convert.ToInt32(uxMaxWidth.Text)).Save(label2.Text + "\\Thumbnail\\" + uniqueId + finfo.Name.ToString(), jpgEncoder, myEncoderParameters);
                                img.Dispose();
                            }
                                           
                        }
                        else
                        {
                            File.Copy(strfileName.ToString(), label2.Text + "\\" + fileName.ToString(), true);
                            //replace the different size images 
                            imageHelper.ResizeAndSaveImage(FolderPath.ToString() + "\\" + finfo.Name.ToString(), fileName.ToString(), destinationPath);
                            if (finfo.Extension.ToString().Contains("svg"))
                                File.Copy(strfileName.ToString(), label2.Text + "\\Thumbnail\\" + fileName.ToString(), true);
                            else
                            {
                                System.Drawing.Image img1 = System.Drawing.Image.FromFile(strfileName);
                                _fwidth = img1.Width;
                                _fheight = img1.Height;
                                EncoderDetails(_sext, out ImageCodecInfo jpgEncoder, out EncoderParameters myEncoderParameters);
                                SetImageParameter(img1, Convert.ToInt32(uxMaxHeight.Text), Convert.ToInt32(uxMaxWidth.Text)).Save(label2.Text + "\\Thumbnail\\" + fileName.ToString(), jpgEncoder, myEncoderParameters);
                                img1.Dispose();
                            }

                        }
                    }
                    else { LogHelper.ErrorLog(new Exception("_errorCode returned from DB"), uniqueId + finfo.Name.ToString(), FolderPath.ToString()); }

                    //progressBar1.Minimum = 0;
                    uxExtention.Text = finfo.Extension.ToString();
                  
                    uxExtention.Refresh();
                    //progressBar1.Maximum = textLines.Count();
                    //copy on destination with inserting data in media tables with their respective folder name 
                    //by calling store procedure
                }
                else
                {
                    //Error log file were skipped while migating into database.
                    LogHelper.ErrorLog(new Exception("File skipped due to file extensin issue"), finfo.Name.ToString(), FolderPath.ToString());
                }
            }
        }

        private void EncoderDetails(string _sext, out ImageCodecInfo jpgEncoder, out EncoderParameters myEncoderParameters)
        {
            jpgEncoder = GetEncoder(GetImageFormat(_sext));
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 91L);
            myEncoderParameters.Param[0] = myEncoderParameter;
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        //get ImageFormat from string extentions
        private ImageFormat GetImageFormat(string extension)
        {
            switch (extension.ToLower())
            {
                case @".bmp":
                    return ImageFormat.Bmp;

                case @".gif":
                    return ImageFormat.Gif;

                case @".ico":
                    return ImageFormat.Icon;

                case @".jpg":
                case @".jpeg":
                    return ImageFormat.Jpeg;

                case @".png":
                    return ImageFormat.Png;

                case @".tif":
                case @".tiff":
                    return ImageFormat.Tiff;

                case @".wmf":
                    return ImageFormat.Wmf;

                default:
                    return ImageFormat.Png;
            }
        }

        private void uxCMDSelectSource_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.Description = "Select Folder Name of out project (better to select base folder of source code files)";
                dialog.ShowNewFolderButton = false;
                dialog.RootFolder = Environment.SpecialFolder.MyComputer;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    SourceFolderName = dialog.SelectedPath;
                    label1.Text = SourceFolderName.ToString();
                    uxCMDStartDownloading.Enabled = true;

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            progressBar1.Visible = false;
            //this.GetSQLInstances();
            Application.DoEvents();
            uxCMDSelectSource.Enabled = true;
            uxCMDSelectTarget.Enabled = true;
            label1.Text = "";
            label2.Text = "";
            uxCMDStartDownloading.Enabled = false;

            // this.uxSetPath_Click(0,e);
            //SourceFolderName = Properties.Settings.Default.SourcePath;
            //label1.Text = SourceFolderName.ToString();
            //TargetFolderName = Properties.Settings.Default.TargetPath;
            //label2.Text = TargetFolderName.ToString();
            //ArchiveFolderName = Properties.Settings.Default.ArchivePath;
            //label10.Text = ArchiveFolderName.ToString();

            SourceFolderName = $"{ConfigurationManager.AppSettings["SourcePath"]}";
            label1.Text = SourceFolderName.ToString();
            TargetFolderName = $"{ConfigurationManager.AppSettings["TargetPath"]}";
            label2.Text = TargetFolderName.ToString();
            ArchiveFolderName = $"{ConfigurationManager.AppSettings["ArchivePath"]}";
            label10.Text = ArchiveFolderName.ToString();

            if (label1.Text.Trim() != "" && label2.Text.Trim() != "" && label10.Text.Trim() != "")
            {
                uxCMDStartDownloading.Enabled = true;
                //uxCMDStartDownloading_Click(0, e);

            }

        }
        public void GetSQLInstances()
        {
            System.Data.Sql.SqlDataSourceEnumerator instance = System.Data.Sql.SqlDataSourceEnumerator.Instance;
            DataTable dataTable = instance.GetDataSources();
            if (dataTable.Rows.Count > 0)
            {
                foreach (System.Data.DataRow row in dataTable.Rows)
                {

                    if (row[1].ToString() != "")
                    {
                        uxSourceServerList.Items.Add(row[0].ToString() + '\\' + row[1].ToString());
                    }
                    else
                    {
                        uxSourceServerList.Items.Add(row[0].ToString());
                    }
                }
            }
        }
        protected void Displaynotify()
        {
            try
            {
               // notifyIcon1.Icon = new System.Drawing.Icon(Path.GetFullPath(@"image\graph.ico"));
                notifyIcon1.Text = "Imported images sucessfully";
                notifyIcon1.Visible = true;
                notifyIcon1.BalloonTipTitle = "Image Import Utlity";
                notifyIcon1.BalloonTipText = "Click Here to see details";
                notifyIcon1.ShowBalloonTip(100);
            }
            catch (Exception ex)
            {
                MessageBox.Show (ex.Message);
            }
        }
        private void GetSourceDataBaseList()
        {
            try
            {
                // Substitute your connection string below in conxString 
                if (uxSourceServerList.Text != "")
                {
                    using (SqlConnection sqlConx = new SqlConnection(connString))
                    {
                        sqlConx.Open();
                        DataTable tblDatabases = sqlConx.GetSchema("Databases");
                        sqlConx.Close();
                        uxSourceDataBaseList.Items.Clear();
                        foreach (DataRow row in tblDatabases.Rows)
                        {
                            uxSourceDataBaseList.Items.Add(row["database_name"]);
                        }
                        uxSourceDataBaseList.Sorted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void uxDisplyuxSourceDataBaseList_Click(object sender, EventArgs e)
        {
            BuildConnectionString();
            GetSourceDataBaseList();
        }

        private void uxSourceServerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            uxDisplyuxSourceDataBaseList_Click(0, e);
        }
        private int submitMediaDetails(int _fwidth,
            int _fheight,
            int _fsize,
            long _flength,
            string _sext,
            string _guid,
            string _folderName,
            string _fileName)
        {
            {
                _folderName = _folderName.Replace(ConfigurationManager.AppSettings["BaseFolderLocation"], "Root");
                int _errorReturn = 0;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Znode_ImportMediaFiles", con))
                    {
                        cmd.CommandTimeout = 500000000;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FileName", _fileName.ToString());
                        cmd.Parameters.AddWithValue("@Size", _fsize.ToString());
                        cmd.Parameters.AddWithValue("@Height", _fheight.ToString());
                        cmd.Parameters.AddWithValue("@Width", _fwidth.ToString());
                        cmd.Parameters.AddWithValue("@Length", _flength.ToString());
                        cmd.Parameters.AddWithValue("@GUID", _guid.ToString());
                        cmd.Parameters.AddWithValue("@Type", _sext.ToString());
                        cmd.Parameters.AddWithValue("@FolderPath", _folderName.ToString());
                        cmd.Parameters.Add("@Status", SqlDbType.Int);
                        cmd.Parameters.Add("@PresentMediaPath", SqlDbType.VarChar, 200);
                        cmd.Parameters.Add("@Error", SqlDbType.VarChar, 1000);
                        cmd.Parameters["@Status"].Direction = ParameterDirection.Output;
                        cmd.Parameters["@PresentMediaPath"].Direction = ParameterDirection.Output;
                        cmd.Parameters["@Error"].Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();
                        con.Close();
                        _errorReturn = Convert.ToInt32(cmd.Parameters["@Status"].Value.ToString());
                        fileName = cmd.Parameters["@PresentMediaPath"].Value.ToString();
                        ErrorLog = cmd.Parameters["@Error"].Value.ToString();
                    }
                }
                return _errorReturn;
            }
        }
        private int DAMTableBackup()
        {
            {
                int count = 0;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Znode_GetDAMTableBackUp", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        count=cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                return count;
            }
        }
        public void Create(string TName, string ConString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("CREATE TABLE [dbo].[" + TName + "]("
                                + "[SPName] varchar(400) ,"
                                + "[SourceOfFiles] varchar(1000) "
                                + ") ", new SqlConnection(ConString)))
                {
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }
            }
            catch (Exception)
            {

            }
        }

        private Image SetImageParameter(Image sourceImage, int maxHeight, int maxWidth)
        {
            //try
            //{
            int originalWidth = sourceImage.Width;
            int originalHeight = sourceImage.Height;

            float percentWidth = (float)maxWidth / originalWidth;
            float percentHeight = (float)maxHeight / originalHeight;

            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;

            int newWidth = (int)(originalWidth * percent);
            int newHeight = (int)(originalHeight * percent);

            Bitmap thumbnailBitmap = new Bitmap(newWidth, newHeight);

            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);

            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            thumbnailGraph.Clear(Color.White);

            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(sourceImage, imageRectangle);

            MemoryStream stream = new MemoryStream();
            Image imageToSave = thumbnailBitmap;

            return imageToSave;
            // }
            //catch { return 0; }
        }
        private void uxSourceDataBaseList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildConnectionString();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialogForTarget = new FolderBrowserDialog())
            {
                dialogForTarget.Description = "Select Archive Folder Name";
                dialogForTarget.ShowNewFolderButton = false;
                dialogForTarget.RootFolder = Environment.SpecialFolder.MyComputer;
                if (dialogForTarget.ShowDialog() == DialogResult.OK)
                {
                    ArchiveFolderName = dialogForTarget.SelectedPath;
                    label10.Text = ArchiveFolderName.ToString();
                    uxCMDStartDownloading.Enabled = true;
                }

            }
        }

        private void uxCMDSaveSettings_Click(object sender, EventArgs e)
        {
            //Set Automatic / Manual

            //Properties.Settings.Default.SourcePath = label1.Text;
            //Properties.Settings.Default.TargetPath= label2.Text;
            //Properties.Settings.Default.ArchivePath= label10.Text;
            
            //Properties.Settings.Default.Save();
            //Properties.Settings.Default.Reload();

            //save all the paths in app config file
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings["SourcePath"].Value = label1.Text;
            configuration.AppSettings.Settings["TargetPath"].Value = label2.Text;
            configuration.AppSettings.Settings["ArchivePath"].Value = label10.Text;
            configuration.Save(ConfigurationSaveMode.Full, true);
            ConfigurationManager.RefreshSection("appSettings");

        }

        private void uxOpenAppFile_Click(object sender, EventArgs e)
        {
            
                using (SqlConnection con = new SqlConnection(connString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT MediaPath FROM ZnodeDuplicateMediaPath", con))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {

                            string  _mediapath = (string)reader["MediaPath"];
                            if (File.Exists( label2.Text + "\\"+ _mediapath))
                            {
                                DoUpdate(_mediapath);
                            }
                        }
                    }
                }

       
        }

        private void DoUpdate(string MediaPath)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("UPDATE ZnodeDuplicateMediaPath SET IsExists  = 1 where MediaPath = '" + MediaPath + "'", con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void frmImageMigration_FormClosing(object sender, FormClosingEventArgs e)
        {
            LogHelper.ProgressLog(progressLog.ToString());
        }
    }
}
