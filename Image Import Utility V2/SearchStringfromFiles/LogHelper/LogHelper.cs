﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchStringfromFiles
{
    public static class LogHelper
    {
        public static void ErrorLog(Exception ex, string ImageName=null, string FolderPath = null)
        {
            string filePath = $"{ConfigurationManager.AppSettings["LogFilePath"]}";

            using (StreamWriter writer = new StreamWriter(string.Concat(filePath,"ImageErrorLog.txt"), true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine("Date : " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.WriteLine("Image Name : " + ImageName);
                writer.WriteLine("Folder path : " + FolderPath);
                while (ex != null)
                {
                    writer.WriteLine(ex.GetType().FullName);
                    writer.WriteLine("Message : " + ex.Message);
                    writer.WriteLine("StackTrace : " + ex.StackTrace);

                    ex = ex.InnerException;
                }
                writer.Close();
            }
        }
        public static void ProgressLog(string message)
        {
            string filePath = $"{ConfigurationManager.AppSettings["LogFilePath"]}";

            var fileName = "ImageProcessLog_" + $"{DateTime.Today:yyyy-MM-dd}.txt";

            using (StreamWriter writer = new StreamWriter(string.Concat(filePath,fileName), true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                //writer.WriteLine("Date : " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.WriteLine(message);
                writer.WriteLine("Media import utility stopping at : " + DateTime.Now.ToString());
                writer.Close();
            }
        }
    }
}
