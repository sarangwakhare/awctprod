
GO
/****** Object:  StoredProcedure [dbo].[Znode_ImportMediaFiles]    Script Date: 8/27/2019 11:23:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Znode_ImportMediaFiles] 
				 
(
	@FolderPath nvarchar(max),
	@FileName nvarchar(1000),
	@Size varchar(30),
	@Height char(10),
	@Width char(10),
	@Length char(10),
	@Type varchar(300),
	@GUID varchar(100),
	@Status int = 0 Output,
	@Error varchar(1000) = '' Output,
	@PresentMediaPath varchar(200) = null Output
)
AS 
/*
--@FolderPath = 'E:\MyDoc\MyAccount\PhotoGalary\Sanjay' ,
Declare @Status int, 
	@Error nvarchar(1000),
	@PresentMediaPath nvarchar(200) 
	exec [Znode_ImportMediaFiles] 
	@FolderPath = 'E:\Sanjay1\ImageMigration1\Media Import Testing1\items1\TP96416\imprint\JPG' ,
	@FileName = 'home-page-ad-1252.jpg' ,
	@Size = 7365579 ,
	@Height =234 ,
	@Width = 2342 ,
	@Length = 45 ,
	@Type = '.JPG' ,
	@GUID = '6a1fb3b1-a4c0-44dc-aeed-7380c527f0e438' ,
	@Status = @Status  Output,
	@Error = @Error Output,
	@PresentMediaPath = @PresentMediaPath Output
	select @Error
*/
Begin 
	SET NOCOUNT ON;
	DECLARE @UserId int = 2,
	        @Getdate datetime = getdate()			

	DECLARE @MaxMediaPathId INT 
	SET @MaxMediaPathId = (SELECT IDENT_CURRENT('ZnodeMediaPath') )

	declare @MaxMediaCategoryId int
	set @MaxMediaCategoryId = (select max(MediaCategoryId) from ZnodeMediaCategory )
	IF object_id('tempdb..#ParentFolderPath') IS NOT NULL
		DROP TABLE #ParentFolderPath

	IF object_id('tempdb..#FolderPath') IS NOT NULL
		DROP TABLE #FolderPath

	IF object_id('tempdb..#Temp') IS NOT NULL
		DROP TABLE #Temp

	IF object_id('tempdb..#InsertMediaPath') IS NOT NULL
		DROP TABLE #InsertMediaPath

	if object_id('tempdb..#ZnodeMediaPathLocale') is not null
			drop table #ZnodeMediaPathLocale

	SET @FolderPath = replace(@FolderPath,'\\','\')

	BEGIN TRAN MediaFiles
	BEGIN TRY
		
		INSERT INTO ZnodeMediaFilesLog(FolderPath,FileName,CreatedDate)
		SELECT @FolderPath, @FileName, GETDATE()
		--SET @FolderPath = substring(@FolderPath,1,CHARINDEX('\',@FolderPath,1))+'Root'+substring(@FolderPath,CHARINDEX('\',@FolderPath,1),len(@FolderPath))
		-------
		CREATE TABLE #ParentFolderPath ( Id Int, [MediaPath] Varchar(1000), ParentMediaPath Varchar(1000), MediaPathId int, ParentMediaPathId int, Id1 int identity)
		CREATE INDEX Ind_ParentFolderPath ON #ParentFolderPath([MediaPath]) 
		INSERT INTO #ParentFolderPath ( Id, [MediaPath])
		SELECT Id,replace(FP.Item,'\','') AS [MediaPath] 
		FROM dbo.split(@FolderPath,'\')FP
		WHERE FP.Item <> '' AND FP.Item NOT LIKE '%:%' AND FP.Item NOT LIKE '%FTP%' AND FP.Item NOT LIKE '%MRR Data%' ----FP.Item NOT LIKE '%.%' AND 
		ORDER BY Id

		----Update ParentPath 
		UPDATE PFP1 SET PFP1.ParentMediaPath = PFP.[MediaPath]
		FROM #ParentFolderPath PFP
		INNER JOIN #ParentFolderPath PFP1 ON PFP.Id = PFP1.Id-1

		--UPDATE ZMP set ZMP.ParentMediaPathId = null
		--FROM ZnodeMediaPath ZMP 
		--INNER JOIN ZnodeMediaPathLocale ZMPL ON ZMP.MediaPathId = ZMPL.MediaPathId
		--INNER JOIN #ParentFolderPath PFP ON ZMPL.[PathName] = PFP.[MediaPath]
		--where Id1 = 1


		CREATE TABLE #ZnodeMediaPathLocale (MediaPathId int, [PathName] nvarchar(600))
		DECLARE  @ZnodeMediaPathLocale TABLE(MediaPathId int, [PathName] nvarchar(600))


		CREATE INDEX #ZnodeMediaPathLocaleId on #ZnodeMediaPathLocale(MediaPathId)
		--CREATE INDEX #ZnodeMediaPathLocaleName on #ZnodeMediaPathLocale([PathName])

		INSERT INTO #ZnodeMediaPathLocale(MediaPathId,[PathName])
		SELECT MediaPathId,[PathName] FROM ZnodeMediaPathLocale
		WHERE [PathName] in (SELECT [MediaPath] FROM #ParentFolderPath) 
		
		;with cte as
		(
			select c.[PathName] as ParentPathName,
			b.[PathName],a.MediaPathId, a.ParentMediaPathId from ZnodeMediaPath a
			left join #ZnodeMediaPathLocale b on a.MediaPathId = b.MediaPathId 
			left join #ZnodeMediaPathLocale c on a.ParentMediaPathId = c.MediaPathId 
			where  Exists  (select TOP 1 1  from #ParentFolderPath PFP where ParentMediaPath is null and b.[PathName] = PFP.[MediaPath]) 
				  and a.ParentMediaPathId is null 
				  --or c.[PathName] in (select [MediaPath] from #ParentFolderPath where ParentMediaPath is null))
			union all
			select c1.[PathName] as ParentPathName,
			b1.[PathName],a1.MediaPathId, a1.ParentMediaPathId from ZnodeMediaPath a1
			inner join #ZnodeMediaPathLocale b1 on a1.MediaPathId = b1.MediaPathId 
			inner join #ZnodeMediaPathLocale c1 on a1.ParentMediaPathId = c1.MediaPathId 
			inner join cte cte1 on a1.ParentMediaPathId = cte1.MediaPathId
			
		)
		,cte2 as
		(
			--select distinct *  from cte where [PathName] in (select [MediaPath] from #ParentFolderPath)
			select distinct *  from cte where EXISTS (select * from #ParentFolderPath PFP WHERE ISNULL(cte.[PathName],'') = ISNULL(PFP.MediaPath,'') AND ISNULL(PFP.ParentMediaPath,'') = ISNULL(CTE.ParentPathName,''))

		) 
		,cte3 as
		(
			select distinct *  from cte2 
			where isnull(ParentMediaPathId,0) in (select isnull(MediaPathId,0) from cte2 )
			union
			select distinct *  from cte2 WHERE ( MediaPathId IS NULL or ParentMediaPathId IS NULL )
		)
		select ParentPathName, [PathName], MediaPathId,	ParentMediaPathId,Dense_rank() over(order by ParentMediaPathId) as Id 
		into #Temp1
		from cte3 
		
		delete from #Temp1 where ParentMediaPathId not in (select MediaPathId from #Temp1 where MediaPathId is not null) and ParentMediaPathId is not null

		select ParentPathName, [PathName], MediaPathId,	ParentMediaPathId,Dense_rank() over(order by ParentMediaPathId) as Id 
		into #Temp from #Temp1

		update a set a.MediaPathId = b.MediaPathId, a.ParentMediaPathId = b.ParentMediaPathId
		from #ParentFolderPath a
		inner join #Temp b on isnull(a.MediaPath,'') = isnull(b.PathName,'') and isnull(a.ParentMediaPath,'') = isnull(b.ParentPathName,'') and a.id1 = b.Id

		----Update ParentPath 
		UPDATE PFP1 SET PFP1.ParentMediaPathId = PFP.[MediaPathId]
		FROM #ParentFolderPath PFP
		INNER JOIN #ParentFolderPath PFP1 ON PFP.Id1 = PFP1.Id1-1
		WHERE PFP1.ParentMediaPathId IS NULL

		----Update MediaPath 
		UPDATE PFP SET PFP.[MediaPathId] = PFP1.[MediaPathId]
		FROM #ParentFolderPath PFP
		INNER JOIN #Temp PFP1 ON isnull(PFP.ParentMediaPathId,0) = isnull(PFP1.ParentMediaPathId,0) and PFP.ParentMediaPath = PFP1.ParentPathName and PFP.MediaPath = PFP1.[PathName]
		where PFP.MediaPathId is null

		;WITH CTE AS
		(
			SELECT min(Id) as Id FROM #ParentFolderPath WHERE [MediaPathId] is null
		)
		UPDATE #ParentFolderPath set [MediaPathId] = null WHERE  Id >= (select top 1 Id from CTE )
	
		SELECT MediaPath, ParentMediaPath, ROW_NUMBER() OVER (ORDER BY Id) Id1  INTO #FolderPath FROM #ParentFolderPath WHERE MediaPathId is null
		
		CREATE TABLE #InsertMediaPath ( MediaPathId INT)
		---- Insert into ZnodeMediaPath
		INSERT INTO ZnodeMediaPath(ParentMediaPathId,PathCode,IsActive,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		OUTPUT inserted.MediaPathId into #InsertMediaPath(MediaPathId)
		SELECT Null, 'EN', 1, @UserId, @Getdate, @UserId, @Getdate
		FROM #FolderPath FP

		---- Insert into ZnodeMediaPathLocale
		INSERT INTO ZnodeMediaPathLocale (LocaleId, MediaPathId, [PathName],CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT 1, IMP.MediaPathId, FP.MediaPath, @UserId, @Getdate, @UserId, @Getdate
		FROM #FolderPath FP
		INNER JOIN #InsertMediaPath IMP on @MaxMediaPathId+FP.Id1 = IMP.MediaPathId
	
		----Check Media present against given Path 
		IF EXISTS(SELECT * FROM ZnodeMediaCategory ZMC
					WHERE EXISTS( SELECT * FROM ZnodeMediaPathLocale ZMPL WHERE ZMPL.[PathName] = (SELECT Top 1 replace(Item,'\','') FROM dbo.split(@FolderPath,'\') WHERE Item <> '' AND Item NOT LIKE '%:%' ORDER BY Id DESC) AND ZMC.MediaPathId = ZMPL.MediaPathId )
					AND EXISTS( SELECT * FROM ZnodeMedia ZM WHERE ZMC.MediaId = ZM.MediaId and ZM.[FileName] = @FileName))
		BEGIN
			if exists(select * from #InsertMediaPath)
			begin
				SET @PresentMediaPath = ''
			end
			else
			begin
				--SET @PresentMediaPath =
				--(
				--	SELECT TOP 1 [Path] --Replace([Path],[FileName],'') 
				--	FROM ZnodeMedia ZM1 
				--	WHERE MediaId = (SELECT Top 1 MediaId FROM ZnodeMediaCategory ZMC
				--				WHERE EXISTS( SELECT * FROM ZnodeMediaPathLocale ZMPL WHERE ZMPL.[PathName] = (SELECT Top 1 replace(Item,'\','') FROM dbo.split(@FolderPath,'\') WHERE Item <> '' AND Item NOT LIKE '%:%' ORDER BY Id DESC) and ZMC.MediaPathId = ZMPL.MediaPathId )
				--				AND EXISTS( SELECT * FROM ZnodeMedia ZM WHERE ZMC.MediaId = ZM.MediaId and ZM.[FileName] = @FileName) )
				--)
				SET @PresentMediaPath =
				(
					SELECT TOP 1 [Path] --Replace([Path],[FileName],'') 
					FROM ZnodeMedia ZM1 
					WHERE MediaId = (SELECT Top 1 MediaId FROM ZnodeMediaCategory ZMC
								WHERE EXISTS( SELECT * FROM ZnodeMediaPathLocale ZMPL WHERE ZMPL.[PathName] = (SELECT Top 1 replace(Item,'\','') FROM dbo.split(@FolderPath,'\') WHERE Item <> '' AND Item NOT LIKE '%:%' ORDER BY Id DESC) 
								and ZMC.MediaPathId = (select top 1 MediaPathId from #ParentFolderPath order by id desc))
								AND EXISTS( SELECT * FROM ZnodeMedia ZM WHERE ZMC.MediaId = ZM.MediaId and ZM.[FileName] = @FileName) )
				)
			end
		END

		if isnull(@PresentMediaPath,'') <> ''
		begin
			------Update ZnodeMedia
			UPDATE ZnodeMedia 
			SET Size = @Size,
				Height = @Height,
				Width = @Width,
				[Length] = @Length,
				ModifiedDate = @Getdate
			WHERE [FileName] = @FileName And Path = @PresentMediaPath
		end
		else
		begin

			if not exists(select * from ZnodeMediaCategory a 
						inner join ZnodeMedia b on a.MediaId = b.MediaId  where MediaPathId = ( SELECT max(MediaPathId) FROM #InsertMediaPath )
						and b.FileName = @FileName and  ISNULL(@PresentMediaPath,'') = '')
			begin
				----Insert Into ZnodeMedia
				INSERT INTO ZnodeMedia(MediaConfigurationId,[Path],[FileName],Size,Height,Width,[Length],[Type],CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT 1, @GUID+@FileName ,  @FileName, @Size, @Height, @Width, @Length, @Type, @UserId, @Getdate, @UserId, @Getdate
				WHERE isnull(@FileName,'') <> ''
				--AND NOT EXISTS (SELECT * FROM ZnodeMedia WHERE [FileName] = isnull(@FileName,'') )
			end
		end

		----Update ParentMediaPath
		UPDATE ZMP SET ZMP.ParentMediaPathId = ZMPL.MediaPathId, ZMP.ModifiedDate = @Getdate
		FROM #FolderPath PFP
		INNER JOIN ZnodeMediaPathLocale ZMPL ON PFP.ParentMediaPath = ZMPL.[PathName]
		INNER JOIN ZnodeMediaPathLocale ZMPL1 ON PFP.MediaPath = ZMPL1.[PathName]
		INNER JOIN ZnodeMediaPath ZMP ON ZMPL1.MediaPathId = ZMP.MediaPathId
		WHERE exists(select * from #InsertMediaPath IMP where ZMPL1.MediaPathId = IMP.MediaPathId )
		and exists(select * from #InsertMediaPath IMP where ZMP.MediaPathId = IMP.MediaPathId )
		and exists(select * from #InsertMediaPath IMP where ZMPL.MediaPathId = IMP.MediaPathId )
		and ZMPL.MediaPathId is not null

		----Update ParentMediaPath
		UPDATE ZMP SET ZMP.ParentMediaPathId = PFP.ParentMediaPathId, ZMP.ModifiedDate = @Getdate
		FROM #ParentFolderPath PFP
		INNER JOIN ZnodeMediaPathLocale ZMPL ON PFP.ParentMediaPath = ZMPL.[PathName]
		INNER JOIN ZnodeMediaPathLocale ZMPL1 ON PFP.MediaPath = ZMPL1.[PathName]
		INNER JOIN ZnodeMediaPath ZMP ON ZMPL1.MediaPathId = ZMP.MediaPathId
		WHERE exists(select * from #InsertMediaPath IMP where ZMP.MediaPathId = IMP.MediaPathId )
		and PFP.ParentMediaPathId is not null

		if exists(select * from #InsertMediaPath)
		begin
			INSERT INTO ZnodeMediaCategory (MediaPathId,MediaId,MediaAttributeFamilyId,[Path],CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
			SELECT ( SELECT max(MediaPathId) FROM #InsertMediaPath ) AS MediaPathId,
				   ( SELECT top 1 MediaId FROM ZnodeMedia ZM WHERE ZM.[FileName] = @FileName ORDER BY 1 DESC) AS MediaId, 1 AS MediaAttributeFamilyId, null AS [Path], @UserId, @Getdate, @UserId, @Getdate
			WHERE ISNULL(@PresentMediaPath,'') = ''
			and not exists(select * from ZnodeMediaCategory ZMC 
						inner join ZnodeMedia ZM on ZMC.MediaId = ZM.MediaId  where MediaPathId = ( SELECT max(MediaPathId) FROM #InsertMediaPath )
						and ZM.FileName = @FileName)
			and ( SELECT top 1 MediaId FROM ZnodeMedia ZM WHERE ZM.[FileName] = @FileName ORDER BY 1 DESC) is not null
		end
		else
		begin
			INSERT INTO ZnodeMediaCategory (MediaPathId,MediaId,MediaAttributeFamilyId,[Path],CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
			SELECT ( SELECT max(MediaPathId) FROM #ParentFolderPath ZMPL WHERE ZMPL.MediaPath = (SELECT Top 1 replace(Item,'\','') FROM dbo.split(@FolderPath,'\') WHERE Item <> '' AND Item NOT LIKE '%:%' ORDER BY Id DESC) ) AS MediaPathId,
				   ( SELECT top 1 MediaId FROM ZnodeMedia ZM WHERE ZM.[FileName] = @FileName ORDER BY 1 DESC) AS MediaId, 1 AS MediaAttributeFamilyId, null AS [Path], @UserId, @Getdate, @UserId, @Getdate
			WHERE ISNULL(@PresentMediaPath,'') = ''
			and not exists(select * from ZnodeMediaCategory ZMC 
						inner join ZnodeMedia ZM on ZMC.MediaId = ZM.MediaId  where MediaPathId = ( SELECT max(MediaPathId) FROM #ParentFolderPath ZMPL WHERE ZMPL.MediaPath = (SELECT Top 1 replace(Item,'\','') FROM dbo.split(@FolderPath,'\') WHERE Item <> '' AND Item NOT LIKE '%:%' ORDER BY Id DESC) )
						and ZM.FileName = @FileName)
			and ( SELECT top 1 MediaId FROM ZnodeMedia ZM WHERE ZM.[FileName] = @FileName ORDER BY 1 DESC) is not null
		end

		--UPDATE ZMP set ZMP.ParentMediaPathId = ZMPL1.MediaPathId
		--FROM ZnodeMediaPath ZMP 
		--INNER JOIN ZnodeMediaPathLocale ZMPL ON ZMP.MediaPathId = ZMPL.MediaPathId
		--INNER JOIN #ParentFolderPath PFP ON ZMPL.[PathName] = PFP.MediaPath
		--CROSS APPLY ZnodeMediaPathLocale ZMPL1 
		--where Id1 = 1 and ZMPL1.[PathName] = 'Root'
		
		
			SET @Status = 1
		
		IF object_id('tempdb..#ParentFolderPath') IS NOT NULL
			DROP TABLE #ParentFolderPath

		IF object_id('tempdb..#FolderPath') IS NOT NULL
			DROP TABLE #FolderPath

		IF object_id('tempdb..#ZnodeMediaPathLocale') is not null
			DROP TABLE #ZnodeMediaPathLocale
	
	COMMIT TRAN MediaFiles

	END TRY 
	BEGIN CATCH 
		ROLLBACK TRAN MediaFiles
		SET @Status = 0
		DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), 
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ImportMediaFiles @FolderPath=' + CAST(@FolderPath AS VARCHAR(50)) + ',@FileName=' + CAST(@FileName AS VARCHAR(50)) + 
		',@Size=' + CAST(@Size AS VARCHAR(50)) + ',@Height=' + CAST(@Height AS VARCHAR(50)) + ',@Width=' + CAST(@Width AS VARCHAR(50))
		+ ',@Length=' + CAST(@Length AS VARCHAR(50))+ ',@Type=' + CAST(@Type AS VARCHAR(50))+ ',@GUID=' + CAST(@GUID AS VARCHAR(50))+ ',@Status=' + CAST(@Status AS VARCHAR(50));

		select ERROR_MESSAGE()
		SET @Error = ERROR_MESSAGE()
	END CATCH 

End
