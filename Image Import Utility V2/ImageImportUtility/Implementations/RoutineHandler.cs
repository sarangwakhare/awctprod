﻿using ImageImportUtility.Contracts;
using ImageImportUtility.Events;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace ImageImportUtility.Implementations
{
    internal class RoutineHandler : IRoutineHandler
    {
        public event EventHandler<ProcessFailureEventArgs> ValidationFailureEventHandler;
        public event EventHandler<ImageFailureEventArgs> DAMFailureEventHandler;
        public event EventHandler<ImageFailureEventArgs> ImageResizeFailureEventHandler;
        public event EventHandler<ImageFailureEventArgs> ImageArchiveFailureEventHandler;
        public event EventHandler<ImageSuccessEventArgs> ImageSuccessEventHandler;
        public event EventHandler<ProcessCompletionEventArgs> ProcessCompletionEventHandler;
        public event EventHandler<ProcessStartEventArgs> ProcessStartEventHandler;
        public event EventHandler<ImageFailureEventArgs> ImageNotFoundEventHandler;

        private long _ImageSuccessfullyProcessed = 0;
        private long _ImageProcessingFailed = 0;
        private long _TotalImageCount = 0;
        private decimal _TotalMediaSizeProcessed = 0;

        public void Start(string sourcePath, string destinationPath, string archivePath, int thumbnailImageHeight, int thumbnailImageWidth)
        {
            RaiseProcessStartEvent(sourcePath, destinationPath, archivePath, thumbnailImageHeight, thumbnailImageWidth);

            ResetCountFields();

            //Validation Checks.
            if (!IsValidDirectory(sourcePath))
            {
                RaiseValidationFailureEvent(sourcePath);
                return;
            }

            if (!IsValidDirectory(destinationPath, true))
            {
                RaiseValidationFailureEvent(destinationPath);
                return;
            }

            if (!IsValidDirectory(archivePath, true))
            {
                RaiseValidationFailureEvent(archivePath);
                return;
            }

            string datedArchiveSuccessFolder = $"{archivePath}\\{DateTime.Today.ToString("yyyy-MM-dd")}\\Success";
            string datedArchiveFailureFolder = $"{archivePath}\\{DateTime.Today.ToString("yyyy-MM-dd")}\\Failed";

            if (!IsValidDirectory(datedArchiveSuccessFolder, true))
            {
                RaiseValidationFailureEvent(datedArchiveSuccessFolder);
                return;
            }

            if (!IsValidDirectory(datedArchiveFailureFolder, true))
            {
                RaiseValidationFailureEvent(datedArchiveFailureFolder);
                return;
            }

            //Prepare thumbnail folder if does not already exist.
            if (!IsValidDirectory($"{destinationPath}\\Thumbnail", true))
            {
                RaiseValidationFailureEvent($"{destinationPath}\\Thumbnail");
                return;
            }

            /*
             * 1. Take the first folder from source. --- folderToOperate
             * 2. List out all the images found in that folder.
             * 3. Pass all images to Stored Procedure and get DAM name for each of the images.
             * 4. Copy each image to the destination path appropriately with different sizes.
             * 5. Archive each of the image to the archive directory maintaining the same folder structure. 
             * 6. Once all the images found in dictionary are copied, check if more images are found in the original folder.
             * 7. If found, repeat from Step 2.
            */

            //Step1 & 2.
            string folderToOperate;
            IEnumerable<string> files = GetFilesToOperate(sourcePath, out folderToOperate);

            while (files.Count() > 0)
            {
                foreach (string filePath in files)
                {
                    if (File.Exists(filePath))
                    {
                        //Increamenting Total Image Count.
                        this._TotalImageCount += 1;

                        FileInfo fileInfo = GetFileInfoForFilePath(filePath);

                        //Counting the file Size.
                        this._TotalMediaSizeProcessed += (decimal)fileInfo.Length / 1048576; //(1 MB = 1024x1024 bytes)

                        //Step3.
                        bool existsInMedia = false;
                        string damFileName = PerformDAMOperations(fileInfo, out existsInMedia);
                        bool isSuccessful = false;
                        if (string.IsNullOrEmpty(damFileName))
                        {
                            //Raise event to log skip incident.
                            RaiseDAMFailureEvent(filePath);

                            this._ImageProcessingFailed += 1;
                        }
                        else
                        {
                            //Step4.
                            bool isFileCopied = CopyImageToDestination(folderToOperate, fileInfo.Name, destinationPath, damFileName, existsInMedia, thumbnailImageHeight, thumbnailImageWidth);
                            if (!isFileCopied)
                            {
                                //Raise Event to log skip incident.
                                RaiseImageResizeFailureEvent(filePath);
                                this._ImageProcessingFailed += 1;
                            }
                            else
                            {
                                isSuccessful = true;
                            }
                        }

                        //Step5. Perform regardless of prior steps passing or failing.
                        string folderStructure = GetFolderStructurePath(folderToOperate, sourcePath);
                        string archiveSuccessFolderStructurePath = $"{datedArchiveSuccessFolder}{folderStructure}";
                        string archiveFailureFolderStructurePath = $"{datedArchiveFailureFolder}{folderStructure}";
                        
                        bool isFileArchived = false;
                        if (isSuccessful)
                        {
                            PrepareIntermediaryDirectories(archiveSuccessFolderStructurePath);
                            isFileArchived = MoveImageToDestination(fileInfo.FullName, $"{archiveSuccessFolderStructurePath}\\{fileInfo.Name}");
                        }
                        else
                        {
                            PrepareIntermediaryDirectories(archiveFailureFolderStructurePath);
                            isFileArchived = MoveImageToDestination(fileInfo.FullName, $"{archiveFailureFolderStructurePath}\\{fileInfo.Name}");
                        }

                        if (!isFileArchived)
                        {
                            RaiseImageArchiveFailureEvent(filePath);
                            if (isSuccessful) this._ImageProcessingFailed += 1;
                            isSuccessful = false;
                        }

                        if (isSuccessful == true)
                        {
                            RaiseImageSuccessEvent(filePath);
                            this._ImageSuccessfullyProcessed += 1;
                        }
                    }
                    else
                    {
                        RaiseImageNotFoundEvent(filePath);
                    }
                }
                //Step6 & 7.
                files = GetFilesToOperate(sourcePath, out folderToOperate);
            }

            RaiseProcessCompletionEvent();
        }

        private void ResetCountFields()
        {
            _ImageSuccessfullyProcessed = 0;
            _ImageProcessingFailed = 0;
            _TotalImageCount = 0;
            _TotalMediaSizeProcessed = 0;
        }

        private void PrepareIntermediaryDirectories(string archiveFolderStructurePath)
        {
            Directory.CreateDirectory(archiveFolderStructurePath);
        }

        private string GetFolderStructurePath(string folderPath, string sourcePath)
        {
            return folderPath.Replace(sourcePath, string.Empty);
        }

        private FileInfo GetFileInfoForFilePath(string filePath)
        {
            return new FileInfo(filePath);
        }

        private IEnumerable<string> GetFilesToOperate(string rootDirectory, out string selectedFolderPath)
        {
            IEnumerable<string> subDirectories = Directory.EnumerateDirectories(rootDirectory, "*", SearchOption.AllDirectories);

            IEnumerable<string> files = new List<string>();
            string selectedPath = null;
            foreach (string subDirectory in subDirectories)
            {
                files = Directory.EnumerateFiles($"{subDirectory}");
                if (files?.Count() > 0)
                {
                    selectedPath = subDirectory;
                    break;
                }
            }

            selectedFolderPath = selectedPath;

            return files;
        }

        private string PerformDAMOperations(FileInfo finfo, out bool existsInMedia)
        {
            try
            {
                existsInMedia = false;
                string[] extesnsions = new string[] { ".jpg", ".gif", ".png", ".jpeg", ".svg" };
                if (extesnsions.Contains(finfo.Extension?.ToLower()))
                {
                    int _fwidth = 0;
                    int _fheight = 0;
                    int _fsize = 0;
                    long _flength = 0;
                    string _sext = "";
                    int _errorCode = 0;

                    _fsize = File.ReadAllBytes(finfo.FullName).Length;
                    _flength = finfo.Length;
                    _sext = finfo.Extension;
                    string uniqueId = Guid.NewGuid().ToString();
                    string damFileName;
                    string errorLog;
                    _errorCode = submitMediaDetails(_fwidth, _fheight, _fsize, _flength, _sext, uniqueId, finfo.DirectoryName, finfo.Name, out damFileName, out errorLog);
                    if (_errorCode == 1)
                    {
                        //Raise Event for success.
                        if (string.IsNullOrEmpty(damFileName))
                        {
                            return uniqueId + finfo.Name.ToString();
                        }
                        else
                        {
                            existsInMedia = true;
                            return damFileName;
                        }
                    }
                    else
                    {
                        //Raise Event for failure.
                        return null;
                    }
                }
            }
            catch
            {
                existsInMedia = false;
            }

            return null;
        }

        private bool CopyImageToDestination(string copyFromPath, string sourceFileName, string copyToPath, string renameWithName, bool existsInMedia, int thumbnailImageHeight, int thumbnailImageWidth)
        {
            try
            {
                ImageHelper imageHelper = new ImageHelper();
                int _fwidth = 0;
                int _fheight = 0;
                string _sext = new FileInfo($"{copyFromPath}\\{sourceFileName}")?.Extension;
                if (!existsInMedia)
                {
                    File.Copy($"{copyFromPath}\\{sourceFileName}", $"{copyToPath}\\{renameWithName}");
                    //create the different size images 
                    imageHelper.ResizeAndSaveImage($"{copyFromPath}\\{sourceFileName}", renameWithName, copyToPath);
                    //move image to thumbnail folder
                    if (sourceFileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault().Contains("svg"))
                        File.Copy($"{copyFromPath}\\{sourceFileName}", $"{copyToPath}\\Thumbnail\\{renameWithName}");
                    else
                    {
                        EncoderDetails(_sext, out ImageCodecInfo jpgEncoder, out EncoderParameters myEncoderParameters);
                        Image img = Image.FromFile($"{copyFromPath}\\{sourceFileName}");
                        _fwidth = img.Width;
                        _fheight = img.Height;
                        SetImageParameter(img, thumbnailImageHeight, thumbnailImageWidth).Save($"{copyToPath}\\Thumbnail\\{renameWithName}", jpgEncoder, myEncoderParameters);
                        img.Dispose();
                    }
                }
                else
                {
                    File.Copy($"{copyFromPath}\\{sourceFileName}", $"{copyToPath}\\{renameWithName}", true);
                    //replace the different size images 
                    imageHelper.ResizeAndSaveImage($"{copyFromPath}\\{sourceFileName}", renameWithName, copyToPath);
                    if (sourceFileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault().Contains("svg"))
                        File.Copy($"{copyFromPath}\\{sourceFileName}", $"{copyToPath}\\Thumbnail\\{renameWithName}", true);
                    else
                    {
                        Image img1 = Image.FromFile($"{copyFromPath}\\{sourceFileName}");
                        _fwidth = img1.Width;
                        _fheight = img1.Height;
                        EncoderDetails(_sext, out ImageCodecInfo jpgEncoder, out EncoderParameters myEncoderParameters);
                        SetImageParameter(img1, thumbnailImageHeight, thumbnailImageWidth).Save($"{copyToPath}\\Thumbnail\\{renameWithName}", jpgEncoder, myEncoderParameters);
                        img1.Dispose();
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool MoveImageToDestination(string sourceFilePath, string targetFilePath)
        {
            try
            {
                if (File.Exists(targetFilePath))
                    File.Delete(targetFilePath);

                File.Move(sourceFilePath, targetFilePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool IsValidDirectory(string path, bool createIfNonExistent = false)
        {
            if (!Directory.Exists(path) && createIfNonExistent) 
                Directory.CreateDirectory(path);

            return Directory.Exists(path);
        }

        #region Events

        private void RaiseImageNotFoundEvent(string filePath)
        {
            ImageNotFoundEventHandler?.Invoke(this, new ImageFailureEventArgs()
            {
                Source = filePath,
                Error = ImageProcessError.ResizeFailed
            });
        }

        private void RaiseImageSuccessEvent(string filePath)
        {
            ImageSuccessEventHandler?.Invoke(this, new ImageSuccessEventArgs()
            {
                Source = filePath
            });
        }

        private void RaiseProcessCompletionEvent()
        {
            ProcessCompletionEventHandler?.Invoke(this, new ProcessCompletionEventArgs()
            {
                TotalImageCount = this._TotalImageCount,
                SuccessImageCount = this._ImageSuccessfullyProcessed,
                FailedImageCount = this._ImageProcessingFailed,
                TotalUploadSize = this._TotalMediaSizeProcessed
            });
        }

        private void RaiseProcessStartEvent(string sourcePath, string destinationPath, string archivePath, int thumbnailImageHeight, int thumbnailImageWidth)
        {
            ProcessStartEventHandler?.Invoke(this, new ProcessStartEventArgs()
            {
                FTPPath = sourcePath,
                DestinationPath = destinationPath,
                ArchivePath = archivePath,
                ThumnailImageHeight = thumbnailImageHeight,
                ThumnailImageWidth = thumbnailImageWidth
            });
        }

        private void RaiseValidationFailureEvent(string path)
        {
            ValidationFailureEventHandler?.Invoke(this, new ProcessFailureEventArgs()
            {
                Source = path,
                Error = ImageProcessError.DirectoryDoesNotExist
            });
        }

        private void RaiseImageArchiveFailureEvent(string filePath)
        {
            ImageArchiveFailureEventHandler?.Invoke(this, new ImageFailureEventArgs()
            {
                Source = filePath,
                Error = ImageProcessError.ArchiveFailed
            });
        }

        private void RaiseImageResizeFailureEvent(string filePath)
        {
            ImageResizeFailureEventHandler?.Invoke(this, new ImageFailureEventArgs()
            {
                Source = filePath,
                Error = ImageProcessError.ResizeFailed
            });
        }

        private void RaiseDAMFailureEvent(string filePath)
        {
            DAMFailureEventHandler?.Invoke(this, new ImageFailureEventArgs()
            {
                Source = filePath,
                Error = ImageProcessError.DAMProcessingFailed
            });
        }
        #endregion

        #region Copied Code

        private int submitMediaDetails(int _fwidth, int _fheight, int _fsize, long _flength, string _sext, string _guid, string _folderName, string _fileName, out string damFileName, out string errorLog)
        {
            string connString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            _folderName = _folderName.Replace(ConfigurationManager.AppSettings["BaseFolderLocation"], "Root");
            int _errorReturn = 0;
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("Znode_ImportMediaFiles", con))
                {
                    cmd.CommandTimeout = 500000000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FileName", _fileName.ToString());
                    cmd.Parameters.AddWithValue("@Size", _fsize.ToString());
                    cmd.Parameters.AddWithValue("@Height", _fheight.ToString());
                    cmd.Parameters.AddWithValue("@Width", _fwidth.ToString());
                    cmd.Parameters.AddWithValue("@Length", _flength.ToString());
                    cmd.Parameters.AddWithValue("@GUID", _guid.ToString());
                    cmd.Parameters.AddWithValue("@Type", _sext.ToString());
                    cmd.Parameters.AddWithValue("@FolderPath", _folderName.ToString());
                    cmd.Parameters.Add("@Status", SqlDbType.Int);
                    cmd.Parameters.Add("@PresentMediaPath", SqlDbType.VarChar, 200);
                    cmd.Parameters.Add("@Error", SqlDbType.VarChar, 1000);
                    cmd.Parameters["@Status"].Direction = ParameterDirection.Output;
                    cmd.Parameters["@PresentMediaPath"].Direction = ParameterDirection.Output;
                    cmd.Parameters["@Error"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();
                    con.Close();
                    _errorReturn = Convert.ToInt32(cmd.Parameters["@Status"].Value.ToString());
                    damFileName = cmd.Parameters["@PresentMediaPath"].Value.ToString();
                    errorLog = cmd.Parameters["@Error"].Value.ToString();
                }
            }
            return _errorReturn;
        }

        private void EncoderDetails(string _sext, out ImageCodecInfo jpgEncoder, out EncoderParameters myEncoderParameters)
        {
            jpgEncoder = GetEncoder(GetImageFormat(_sext));
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 91L);
            myEncoderParameters.Param[0] = myEncoderParameter;
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        //get ImageFormat from string extentions
        private ImageFormat GetImageFormat(string extension)
        {
            switch (extension.ToLower())
            {
                case @".bmp":
                    return ImageFormat.Bmp;

                case @".gif":
                    return ImageFormat.Gif;

                case @".ico":
                    return ImageFormat.Icon;

                case @".jpg":
                case @".jpeg":
                    return ImageFormat.Jpeg;

                case @".png":
                    return ImageFormat.Png;

                case @".tif":
                case @".tiff":
                    return ImageFormat.Tiff;

                case @".wmf":
                    return ImageFormat.Wmf;

                default:
                    return ImageFormat.Png;
            }
        }

        private Image SetImageParameter(Image sourceImage, int maxHeight, int maxWidth)
        {
            int originalWidth = sourceImage.Width;
            int originalHeight = sourceImage.Height;

            float percentWidth = (float)maxWidth / originalWidth;
            float percentHeight = (float)maxHeight / originalHeight;

            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;

            int newWidth = (int)(originalWidth * percent);
            int newHeight = (int)(originalHeight * percent);

            Bitmap thumbnailBitmap = new Bitmap(newWidth, newHeight);

            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);

            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            thumbnailGraph.Clear(Color.White);

            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(sourceImage, imageRectangle);

            MemoryStream stream = new MemoryStream();
            Image imageToSave = thumbnailBitmap;

            return imageToSave;
        }

        #endregion
    }
}
