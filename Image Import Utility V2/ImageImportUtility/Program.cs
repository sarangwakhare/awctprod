﻿using ImageImportUtility.Contracts;
using ImageImportUtility.Implementations;
using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;

namespace ImageImportUtility
{
    class Program
    {
        static void Main(string[] args)
        {
           // Console.WriteLine("Image Import Utility starting.");
            IRoutineHandler routineHandler = new RoutineHandler();
            RegisterEvents(routineHandler);
          //  Console.WriteLine("Events registered.");

            string sourcePath = ConfigurationManager.AppSettings["SourcePath"];
            string destinationPath = ConfigurationManager.AppSettings["DestinationPath"];
            string archivePath = ConfigurationManager.AppSettings["ArchivePath"];
            int thumbnailImageHeight = Convert.ToInt32(ConfigurationManager.AppSettings["ThumbnailImageHeight"]);
            int thumbnailImageWidth = Convert.ToInt32(ConfigurationManager.AppSettings["ThumbnailImageWidth"]);

            routineHandler.Start(sourcePath, destinationPath, archivePath, thumbnailImageHeight, thumbnailImageWidth);

            if (!args.Contains("--silent"))
            {
               Console.WriteLine("\n\nPress \"enter\" to exit.");
               Console.ReadLine();
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("\n\nExiting.");
                Thread.Sleep(5000);
            }
        }

        private static void RegisterEvents(IRoutineHandler routineHandler)
        {
            routineHandler.DAMFailureEventHandler += RoutineHandler_DAMFailureEventHandler;
            routineHandler.ImageArchiveFailureEventHandler += RoutineHandler_ImageArchiveFailureEventHandler;
            routineHandler.ImageResizeFailureEventHandler += RoutineHandler_ImageResizeFailureEventHandler;
            routineHandler.ImageSuccessEventHandler += RoutineHandler_ImageSuccessEventHandler;
            routineHandler.ProcessCompletionEventHandler += RoutineHandler_ProcessCompletionEventHandler;
            routineHandler.ProcessStartEventHandler += RoutineHandler_ProcessStartEventHandler;
            routineHandler.ValidationFailureEventHandler += RoutineHandler_ValidationFailureEventHandler;
            routineHandler.ImageNotFoundEventHandler += RoutineHandler_ImageNotFoundEventHandler;
        }

        private static void RoutineHandler_ImageNotFoundEventHandler(object sender, Events.ImageFailureEventArgs e)
        {
            //Console.WriteLine($"\nError: File \"{e.Source}\" was listed during the routine but cannot be found on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
            LogHelper.ExceptionLog($"\nError: File \"{e.Source}\" was listed during the routine but cannot be found on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
        }

        private static void RoutineHandler_ValidationFailureEventHandler(object sender, Events.ProcessFailureEventArgs e)
        {
            //Console.WriteLine($"\nError: Directory validation failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
            LogHelper.ExceptionLog($"\nError: Directory validation failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
        }

        private static void RoutineHandler_ProcessStartEventHandler(object sender, Events.ProcessStartEventArgs e)
        {
            //Console.WriteLine($"\nInfo: Image import process started on {e.EventLoggedOn} ({e.EventLogTimeZone}) with following parameters: ");
            //Console.WriteLine($"\t\t-- Source -> {e.FTPPath}");
            //Console.WriteLine($"\t\t-- Destination -> {e.DestinationPath}");
            //Console.WriteLine($"\t\t-- Archive -> {e.ArchivePath}");
            //Console.WriteLine($"\t\t-- Thumbnail Image Height -> {e.ThumnailImageHeight}");
            //Console.WriteLine($"\t\t-- Thumbnail Image Width -> {e.ThumnailImageWidth}");

            StringBuilder progressLog = new StringBuilder();
            progressLog.AppendLine($"\nInfo: Image import process started on {e.EventLoggedOn} ({e.EventLogTimeZone}) with following parameters: ");
            progressLog.AppendLine($"\t\t-- Source -> {e.FTPPath}");
            progressLog.AppendLine($"\t\t-- Destination -> {e.DestinationPath}");
            progressLog.AppendLine($"\t\t-- Archive -> {e.ArchivePath}");
            progressLog.AppendLine($"\t\t-- Thumbnail Image Height -> {e.ThumnailImageHeight}");
            progressLog.AppendLine($"\t\t-- Thumbnail Image Width -> {e.ThumnailImageWidth}");

            LogHelper.ProgressLog(progressLog.ToString());
        }

        private static void RoutineHandler_ProcessCompletionEventHandler(object sender, Events.ProcessCompletionEventArgs e)
        {
            //Console.WriteLine($"\nInfo: Image import process completed on {e.EventLoggedOn} ({e.EventLogTimeZone}) with following result: ");
            //Console.WriteLine($"\t\t-- Total Image Count -> {e.TotalImageCount}");
            //Console.WriteLine($"\t\t-- Total Upload Size -> {e.TotalUploadSize.ToString("0.#")} MB");
            //Console.WriteLine($"\t\t-- Success -> {e.SuccessImageCount}");
            //Console.WriteLine($"\t\t-- Failure -> {e.FailedImageCount}");
            //Console.WriteLine($"\t\t-- Success Percentage -> {(e.TotalImageCount > 0 ? e.SuccessPercent : 100)}");

            StringBuilder progressLog = new StringBuilder();
            progressLog.AppendLine($"\nInfo: Image import process completed on {e.EventLoggedOn} ({e.EventLogTimeZone}) with following result: ");
            progressLog.AppendLine($"\t\t-- Total Image Count -> {e.TotalImageCount}");
            progressLog.AppendLine($"\t\t-- Total Upload Size -> {e.TotalUploadSize.ToString("0.#")} MB");
            progressLog.AppendLine($"\t\t-- Success -> {e.SuccessImageCount}");
            progressLog.AppendLine($"\t\t-- Failure -> {e.FailedImageCount}");
            progressLog.AppendLine($"\t\t-- Success Percentage -> {(e.TotalImageCount > 0 ? e.SuccessPercent : 100)}");

            LogHelper.ProgressLog(progressLog.ToString());
        }

        private static void RoutineHandler_ImageSuccessEventHandler(object sender, Events.ImageSuccessEventArgs e)
        {
            //Do Nothing.
        }

        private static void RoutineHandler_ImageResizeFailureEventHandler(object sender, Events.ImageFailureEventArgs e)
        {
            //Console.WriteLine($"\nError: Image resize failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
            LogHelper.ExceptionLog($"\nError: Image resize failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
        }

        private static void RoutineHandler_ImageArchiveFailureEventHandler(object sender, Events.ImageFailureEventArgs e)
        {
            //Console.WriteLine($"\nError: Image archive failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
            LogHelper.ExceptionLog($"\nError: Image archive failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
        }

        private static void RoutineHandler_DAMFailureEventHandler(object sender, Events.ImageFailureEventArgs e)
        {
            //Console.WriteLine($"\nError: Image DAM processing failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
            LogHelper.ExceptionLog($"\nError: Image DAM processing failed for \"{e.Source}\" on {e.EventLoggedOn} ({e.EventLogTimeZone}).");
        }
    }
}
