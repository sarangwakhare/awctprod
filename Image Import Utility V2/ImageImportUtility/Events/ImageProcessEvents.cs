﻿using System;

namespace ImageImportUtility.Events
{
    public class BaseEventArgs : EventArgs
    {
        public string EventLoggedOn { get { return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); } }

        public string EventLogTimeZone { get { return TimeZone.CurrentTimeZone.StandardName; } }
    }

    public class ImageFailureEventArgs : BaseEventArgs
    {
        public string Source { get; set; }

        public ImageProcessError Error { get; set; }        
    }

    public class ProcessFailureEventArgs : BaseEventArgs
    {
        public string Source { get; set; }

        public ImageProcessError Error { get; set; }
    }

    public class ImageSuccessEventArgs : BaseEventArgs
    {
        public string Source { get; set; }
    }

    public class ProcessStartEventArgs : BaseEventArgs
    {
        public string FTPPath { get; set; }

        public string ArchivePath { get; set; }

        public string DestinationPath { get; set; }

        public int ThumnailImageHeight { get; set; }

        public int ThumnailImageWidth { get; set; }
    }

    public class ProcessCompletionEventArgs : BaseEventArgs
    {
        public long TotalImageCount { get; set; }

        public decimal TotalUploadSize { get; set; }

        public long SuccessImageCount { get; set; }

        public long FailedImageCount { get; set; }

        public float SuccessPercent { get { return (SuccessImageCount * 100) / TotalImageCount; } }        
    }

    public enum ImageProcessError
    {
        DirectoryDoesNotExist,
        ArchiveFailed,
        ResizeFailed,
        DAMProcessingFailed
    }
}
