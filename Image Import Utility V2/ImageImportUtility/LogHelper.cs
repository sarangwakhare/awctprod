﻿using System;
using System.Configuration;
using System.IO;

namespace ImageImportUtility
{
    public static class LogHelper
    {
        public static void ErrorLog(Exception ex, string ImageName=null, string FolderPath = null)
        {
            string errorLogPath = ConfigurationManager.AppSettings["LogFilePath"];

            IsValidDirectory(errorLogPath, true);

            string filePath = $"{errorLogPath}";

            using (StreamWriter writer = new StreamWriter(string.Concat(filePath,"ImageErrorLog.txt"), true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine("Date : " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.WriteLine("Image Name : " + ImageName);
                writer.WriteLine("Folder path : " + FolderPath);
                while (ex != null)
                {
                    writer.WriteLine(ex.GetType().FullName);
                    writer.WriteLine("Message : " + ex.Message);
                    writer.WriteLine("StackTrace : " + ex.StackTrace);

                    ex = ex.InnerException;
                }
                writer.Close();
            }
        }
        public static void ExceptionLog(string exceptionDetails = null)
        {
            string filePath = $"{ConfigurationManager.AppSettings["LogFilePath"]}";

            IsValidDirectory(filePath, true);

            using (StreamWriter writer = new StreamWriter(string.Concat(filePath, "ImageErrorLog.txt"), true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine(exceptionDetails);

                writer.Close();
            }
        }
        public static void ProgressLog(string message)
        {
            string filePath = $"{ConfigurationManager.AppSettings["LogFilePath"]}";

            IsValidDirectory(filePath, true);

            var fileName = "ImageProgressLog_" + $"{DateTime.Today:yyyy-MM-dd}.txt";

            using (StreamWriter writer = new StreamWriter(string.Concat(filePath,fileName), true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine(message);
                writer.Close();
            }
        }

        private static bool IsValidDirectory(string path, bool createIfNonExistent = false)
        {
            if (!Directory.Exists(path) && createIfNonExistent)
                Directory.CreateDirectory(path);

            return Directory.Exists(path);
        }
    }
}
