﻿using ImageImportUtilityNew.Events;

using System;

namespace ImageImportUtilityNew.Contracts
{
    internal interface IRoutineHandler
    {
        event EventHandler<ProcessFailureEventArgs> ValidationFailureEventHandler;
        event EventHandler<ImageFailureEventArgs> DAMFailureEventHandler;
        event EventHandler<ImageFailureEventArgs> ImageResizeFailureEventHandler;
        event EventHandler<ImageFailureEventArgs> ImageArchiveFailureEventHandler;
        event EventHandler<ImageSuccessEventArgs> ImageSuccessEventHandler;
        event EventHandler<ProcessCompletionEventArgs> ProcessCompletionEventHandler;
        event EventHandler<ProcessStartEventArgs> ProcessStartEventHandler;
        event EventHandler<ImageFailureEventArgs> ImageNotFoundEventHandler;

        void Start(string sourcePath, string destinationPath, string archivePath, int thumbnailImageHeight, int thumbnailImageWidth);
    }
}
