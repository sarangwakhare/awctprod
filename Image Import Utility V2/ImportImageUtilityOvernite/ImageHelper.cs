﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace ImageImportUtilityNew
{
    public class ImageHelper
    {
        //fields for different size image generation
      
        public string DestinationPath = ConfigurationManager.AppSettings["DestinationPath"];
        public int SmallThumbImgWidth = Convert.ToInt16(ConfigurationManager.AppSettings["SmallThumbImgWidth"]);
        public int ThumbImgWidth = Convert.ToInt16(ConfigurationManager.AppSettings["ThumbImgWidth"]);
        public string BucketName = ConfigurationManager.AppSettings["BucketName"];
        private long QualityFactor = 90;

        //Resize the image and save it for all Sizes
        public void ResizeAndSaveImage(string imageNamewithPath, string imageName, string destinationPath,int PortalId)
        {
            SaveThumbnailImage(imageNamewithPath, imageName, destinationPath, ThumbImgWidth, BucketName, PortalId);
         // SaveSmallThumbanailImage(imageNamewithPath, imageName, destinationPath, SmallThumbImgWidth, BucketName, PortalId);

        }
        //Save the thumbnail images
        private void SaveThumbnailImage(string imageNamewithPath, string imageName, string destinationPath, int thumbImgWidth, string bucketName, int portalId)
        {
            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, thumbImgWidth, thumbImgWidth);
            SaveResizedImage(imageToSave, thumbImgWidth, destinationPath, imageName, bucketName, portalId);
        }

        //Save the small thumbnail images
        private void SaveSmallThumbanailImage(string imageNamewithPath, string imageName, string destinationPath, int smallThumbImgWidth, string bucketName, int portalId)
        {

            Image img = GetImageToUse(imageNamewithPath);
            Image imageToSave = ResizeImage(img, smallThumbImgWidth, smallThumbImgWidth);
            SaveResizedImage(imageToSave, smallThumbImgWidth, destinationPath, imageName, bucketName, portalId);
        }
        //Get the image to use
        private Image GetImageToUse(string imageNamewithPath)
        {
            Image img = null;
            byte[] stream = GetImageStream(imageNamewithPath);
            using (MemoryStream mStream = new MemoryStream(stream))
            {
                img = Image.FromStream(mStream);
            }

            return img;
        }

        //Get image in stream
        private byte[] GetImageStream(string url)
        {
            byte[] imageData = null;

            using (System.Net.WebClient wc = new System.Net.WebClient())
            {
                imageData = wc.DownloadData(url);
            }

            return imageData;
        }
        //Get image format
        private ImageFormat GetImageFormat(string extension)
        {
            switch (extension.ToLower())
            {
                case @".bmp":
                    return ImageFormat.Bmp;
                case @".gif":
                    return ImageFormat.Gif;
                case @".ico":
                    return ImageFormat.Icon;
                case @".jpg":
                case @".jpeg":
                    return ImageFormat.Jpeg;
                case @".png":
                    return ImageFormat.Png;
                case @".tif":
                case @".tiff":
                    return ImageFormat.Tiff;
                case @".wmf":
                    return ImageFormat.Wmf;
                default:
                    return ImageFormat.Png;
            }
        }
        public ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
        //Save the resized image
        private void SaveResizedImage(Image imageToSave, int folderName, string destinationPath, string imageName, string bucketName, int portalId)
        {
            try
            {
                if (!Equals(imageToSave, null))
                {
                    string localHostPath = $"~/{bucketName}/Catalog/{portalId}";
                    string destPath = $"{destinationPath}/{folderName}/{imageName}";

                    ImageFormat imageFormat = GetImageFormat(Path.GetExtension(imageName));

                    ImageCodecInfo jpgEncoder = GetEncoder(imageFormat);
                    Encoder myEncoder = Encoder.Quality;
                    EncoderParameters myEncoderParameters = new EncoderParameters(1);
                    EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, QualityFactor);
                    myEncoderParameters.Param[0] = myEncoderParameter;

                    using (MemoryStream stream = new MemoryStream())
                    {

                        imageToSave.Save(stream, jpgEncoder, myEncoderParameters);
                        SaveLocalImage(portalId.ToString(), folderName.ToString(), imageName, stream, destinationPath);
                        imageToSave = null;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        //Save image for local server
        private void SaveLocalImage(string portalId, string folderName, string imageName, MemoryStream stream, string destinationPath)
        {
            string path = destinationPath;
            path += "\\Catalog";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path += "\\" + portalId.ToString();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path += "\\" + folderName.ToString();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path += "\\" + imageName;
            if (File.Exists(path))//if file already exists then only replace the file
            {
                //write to file
                using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    stream.WriteTo(file);
                }
            }
            else
            {
                //write to file
                using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    stream.WriteTo(file);
                }
            }
        }

        //Create the rezised image
        private Image ResizeImage(Image sourceImage, int maxHeight, int maxWidth)
        {
            try
            {
                int originalWidth = sourceImage.Width;
                int originalHeight = sourceImage.Height;

                float percentWidth = (float)maxWidth / originalWidth;
                float percentHeight = (float)maxHeight / originalHeight;

                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;

                int newWidth = (int)(originalWidth * percent);
                int newHeight = (int)(originalHeight * percent);

                Bitmap thumbnailBitmap = new Bitmap(sourceImage, newWidth, newHeight);

                Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);

                thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbnailGraph.SmoothingMode = SmoothingMode.AntiAlias;
                thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                thumbnailGraph.Clear(Color.White);

                Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbnailGraph.DrawImage(sourceImage, imageRectangle);

                Image imageToSave = thumbnailBitmap;

                return imageToSave;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
